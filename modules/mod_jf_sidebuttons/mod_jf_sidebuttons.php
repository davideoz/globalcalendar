<?php
/**
 * @package 	JF Side Buttons
 * @author		JoomForest.com
 * @email		support@joomforest.com
 * @website		http://www.joomforest.com
 * @copyright	Copyright (C) 2011-2016 JoomForest.com, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

// ini_set('display_errors', 'On');
// error_reporting(E_ALL | E_STRICT);

// no direct access
defined('_JEXEC') or die('Restricted access');

// Main Variables
$base = JURI::base();
$assets_path = $base.'modules/mod_jf_sidebuttons/assets/';
$jf_doc = JFactory::getDocument();

/* START - FUNCTIONS ==================================================================================================== */
	// Params
		$jf_sb_fa									= $params->get('jf_sb_fa','');
		$jf_sb_fa_stylesheet						= $params->get('jf_sb_fa_stylesheet','');
		$jf_sb_direction							= $params->get('jf_sb_direction','');
		$jf_sb_styles								= $params->get('jf_sb_styles','');
		
		$jf_list_count	= 20;
		for ($loop = 1; $loop <= $jf_list_count; $loop += 1) {	$Item[$loop] 			= $params->get('jf_sb_'.$loop.'','');			}
		for ($loop = 1; $loop <= $jf_list_count; $loop += 1) {	$Item_exterL[$loop] 	= $params->get('jf_sb_'.$loop.'_exterL','');	}
		for ($loop = 1; $loop <= $jf_list_count; $loop += 1) {	$Item_menuItem[$loop] 	= $params->get('jf_sb_'.$loop.'_menuItem','');	}
		for ($loop = 1; $loop <= $jf_list_count; $loop += 1) {	$Item_target[$loop] 	= $params->get('jf_sb_'.$loop.'_target','');	}
		for ($loop = 1; $loop <= $jf_list_count; $loop += 1) {	$Item_faIcon[$loop] 	= $params->get('jf_sb_'.$loop.'_faIcon','');	}
		for ($loop = 1; $loop <= $jf_list_count; $loop += 1) {	$Item_Img[$loop] 		= $params->get('jf_sb_'.$loop.'_Img','');		}
		for ($loop = 1; $loop <= $jf_list_count; $loop += 1) {	$Item_Style[$loop] 		= $params->get('jf_sb_'.$loop.'_Style','');		}
		for ($loop = 1; $loop <= $jf_list_count; $loop += 1) {	$Item_txt[$loop] 		= $params->get('jf_sb_'.$loop.'_txt','');		}

	// CALL
		$jf_doc->addStyleSheet($assets_path.'jf_sb.min.css');
		// FONT-AWESOME
		if ($jf_sb_fa) {
			$jf_doc->addStyleSheet(''.$jf_sb_fa_stylesheet.'');
			// $jf_doc->addScriptDeclaration('alert("enabled fontawesome");');
		}
		// GOOGLE FONTS
			$jf_gfont			= $params->get('jf_gfont','');
			$jf_gfont_Sheet		= $params->get('jf_gfont_Sheet','');
			$jf_gfont_Name		= $params->get('jf_gfont_Name','');
			if ($jf_gfont) {
				if($jf_gfont_Sheet != '') {$jf_doc->addStyleSheet($jf_gfont_Sheet);}
				$jf_doc->addStyleDeclaration('.jf_sidebuttons{font-family:"'.$jf_gfont_Name.'",sans-serif}');
			}
		$jf_doc->addStyleDeclaration(''.$jf_sb_styles.'');
/*   END - FUNCTIONS ==================================================================================================== */


$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
require JModuleHelper::getLayoutPath('mod_jf_sidebuttons', $params->get('layout', 'default'));