<?php
/**
 * @package 	JF Side Buttons
 * @author		JoomForest.com
 * @email		support@joomforest.com
 * @website		http://www.joomforest.com
 * @copyright	Copyright (C) 2011-2016 JoomForest.com, All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<ul class="jf_sidebuttons <?php echo $jf_sb_direction; ?>">
	<?php for ($jf_list = 1; $jf_list <= $jf_list_count; $jf_list += 1) { ?>
		<?php if ($Item[$jf_list]) { ?>
			<li>
				<a href="<?php if($Item_exterL[$jf_list] != '') { ?><?php echo $Item_exterL[$jf_list]; ?><?php } else { ?><?php echo JRoute::_('index.php?Itemid='.$Item_menuItem[$jf_list]); ?><?php } ?>" target="_<?php echo $Item_target[$jf_list]; ?>">
					<?php if($Item_faIcon[$jf_list] != '') { ?>
						<i class="fa <?php echo $Item_faIcon[$jf_list]; ?>" style="<?php echo $Item_Style[$jf_list]; ?>"></i>
						<?php echo $Item_txt[$jf_list]; ?>
					<?php } else { ?>
						<img src="<?php echo $Item_Img[$jf_list]; ?>" style="<?php echo $Item_Style[$jf_list]; ?>">
						<?php echo $Item_txt[$jf_list]; ?>
					<?php } ?>
				</a>
			</li>
		<?php } ?>
	<?php } ?>
</ul>