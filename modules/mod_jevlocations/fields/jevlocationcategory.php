<?php

/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id: jevcategorynew.php 2983 2011-11-10 14:02:23Z geraintedwards $
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die();

include_once(JPATH_LIBRARIES.'/joomla/form/fields/list.php');

class JFormFieldJevlocationcategory extends JFormFieldList
{

	protected $type = 'Jevlocationcategory';

	/**
	 * Method to get the field options.
	 *
	 * @return	array	The field option objects.
	 * @since	1.6
	 */
	/*protected function getOptions()
	{
		// Initialize variables.
		$session = JFactory::getSession();
		$options = array();

		// Initialize some field attributes.
		$extension = $this->element['extension'] ? (string) $this->element['extension'] : (string) $this->element['scope'];

		$db = JFactory::getDbo();
		$db->setQuery("Select id as value, title as text from #__jevlocation_categories where section=".$db->quote($extension));
		$options = $db->loadObjectList();

		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;

	}*/
	public function getOptions()
	{
		return "";
	}

	public function getInput()
	{
		$this->value = "";
		return "";
	}
	public function getLabel()
	{
		return "";
	}

}

