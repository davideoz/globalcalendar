<?php
/**
 * Life Carousel module main file
 *
 * @package 	Life Carousel
 * @copyright 	Copyright (C) 2018 Davide Casiraghi (http://www.movementmeetslife.com). All rights reserved.
 * @license 	GNU General Public License version 2 or later; http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
	defined( '_JEXEC' ) or die ;

// define directory separator
	if(!defined('DS')){
		define('DS',DIRECTORY_SEPARATOR);
	}

// Include the syndicate functions only once
	require_once dirname( __FILE__ ) . DS . 'helper.php';

// Instantiate class flexColumn
	$megaFooter = new megaFooter; 

// Get joomla module class
	$moduleclass_sfx = htmlspecialchars( $params->get( 'moduleclass_sfx' ) );

// GET module parameters
	$parameters = $megaFooter->getModuleParameters($params);
		

	//dump($parameters['kind_of_footer'],"KIND");

	switch ($parameters['kind_of_footer']) {
		case 'sticky_footer':
			$megaFooter->loadCssandJs($parameters['kind_of_footer']);
		
			$codeToRender = $megaFooter->prepareStickyfooterCodeToRender($params);
		break;

		case 'mega_footer':
			$megaFooter->loadCssandJs($parameters['kind_of_footer']);

			$codeToRender = $megaFooter->prepareMegafooterCodeToRender($parameters);
		break;
	}


require JModuleHelper::getLayoutPath( 'mod_megafooter', $params->get( 'layout', 'default' ) );

