<?php

// This is the main workhorse model that handles the business logic

// No direct access
	defined( '_JEXEC' ) or die;


class megaFooter{

    /**
     *  LOAD module CSS and JS
     *  @param none
     *  @return none
    **/

	function loadCssandJs($kindOfFooter){

		$document = &JFactory::getDocument();

		switch ($kindOfFooter) {
			case 'sticky_footer':
				//$document->addScript(JURI::root(true) . '/media/mod_megafooter/js/mod_stickyfooter.js');
				//$document->addStyleSheet(JURI::root(true) . '/media/mod_megafooter/css/mod_stickyfooter.min.css');
			break;
			case 'mega_footer':
				//$document->addScript(JURI::root(true) . '/media/mod_megafooter/js/mod_megafooter.js');
				//$document->addStyleSheet(JURI::root(true) . '/media/mod_megafooter/css/mod_megafooter.min.css');
			break;
		}
	}


	// **********************************************************************

    /**
     *  Check if we are in HP
     *  @param none 		 
     *  @return boolean $ret    1 if we are in HP
    **/

	function checkIfHP(){
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$ret = ($menu->getActive() == $menu->getDefault()) ? '1' : '0';

		return $ret;
	}

	// **********************************************************************

	/**
     *  GET a certain amount of articles from a category
     *  @param integer $catID  	ID of the category
     *  @param integer $num 	number of articles to retrieve (LIMIT)
     *  @return array $ret 		the array containing the articles
    **/

	function getCategoryArticles($catID,$num){

		// Connect to DB
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

		// QUERY to get articles from the category
			$query = "
				SELECT 
				i.id AS id,
				i.title AS title, 
				i.introtext AS introtext,
				i.fulltext AS full_text,
				i.ordering AS ordering 

				FROM #__content AS i
				WHERE catid = $catID 
				ORDER BY ordering DESC
				LIMIT $num";


			// removed this direct JSON parameters Because availables from MYSQL server version: 5.7 (on Aruba is 5.6, check in phpmyadmin)
				/*
				images->>'$.image_intro' AS image_intro,
				images->>'$.image_fulltext' AS image_fulltext,
				images->>'$.image_fulltext' AS image_fulltext,
				urls->>'$.urla' AS url_a,
				urls->>'$.urlatext' AS url_a_text,
                urls->>'$.targeta' AS url_a_target,
                urls->>'$.urla' AS url_b,
                urls->>'$.urlatext' AS url_b_text,
                urls->>'$.targeta' AS url_b_target
                */

		// Retrieve data
			$db->setQuery($query);
			$ret = $db->loadObjectList();

			return $ret;
	}

	// **********************************************************************

	/**
     *  GET specific articles trough the specifies 
     *  @param integer $INstring	String for the query that specify IDs of the articles to extract eg. IN (1,2,3)
     *  @param integer $num 		number of articles to retrieve 
     *  @return array $ret 			the array containing the articles
    **/

	function getSpecificArticles($INstring){

    	// Connect to DB
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

		// QUERY to get articles from the category
			$query = "
				SELECT 
				i.id AS id,
				i.title AS title, 
				i.introtext AS introtext,
				i.fulltext AS full_text,
				i.ordering AS ordering 
				
				FROM #__content AS i
				WHERE id IN $INstring 
				ORDER BY ordering DESC";
				

				// removed this direct JSON parameters Because availables from MYSQL server version: 5.7 (on Aruba is 5.6, check in phpmyadmin)
				/*
				images->>'$.image_intro' AS image_intro,
				images->>'$.image_fulltext' AS image_fulltext,
				images->>'$.image_fulltext' AS image_fulltext,
				urls->>'$.urla' AS url_a,
				urls->>'$.urlatext' AS url_a_text,
                urls->>'$.targeta' AS url_a_target,
                urls->>'$.urla' AS url_b,
                urls->>'$.urlatext' AS url_b_text,
                urls->>'$.targeta' AS url_b_target

				*/

		// Retrieve data
			$db->setQuery($query);
			$ret = $db->loadObjectList();
			

			//dump($ret,"SPECIFIC ARTICLES");

		return $ret;
	}

 // **********************************************************************

    /**
     *  GET module parameters from joomla backend
     *  @param $params 		  array of the defauld joomla variable containing the module parameters 
     *  @return array $ret    the multidimentional array containing the parameters
    **/

    function getModuleParameters($params){
    	$ret = array();
    	$i = 0;

    	$ret['kind_of_footer'] = $params->get('kind_of_footer');
    	
	   	$ret['column_number'] = $params->get('column_number');
	   	$ret['column_flex'][1] = $params->get('column_1_flex');
	   	$ret['column_flex'][2] = $params->get('column_2_flex');
	   	$ret['column_flex'][3] = $params->get('column_3_flex');
	   	$ret['column_flex'][4] = $params->get('column_4_flex');
	   	
	   	// Wrapper 
			$ret['wrapper']['max_width'] = $params->get('wrapper_max_width');
			$ret['wrapper']['margin'] = $params->get('wrapper_margin');
			$ret['wrapper']['justify_content'] = $params->get('justify_content');
			$ret['wrapper']['flex_wrap'] = $params->get('flex_wrap');
			$ret['wrapper']['flex_flow'] = $params->get('flex_flow');

	   	// Wrapper Background
		   	$ret['background']['type'] = $params->get('background_type');
		   	$ret['background']['color'] = $params->get('background_color');
		   	$ret['background']['gradient'] = $params->get('background_gradient');
		   	$ret['background']['opacity'] = $params->get('opacity');
		   	$ret['background']['image'] = "url(images/structure/footer/background/".$params->get('background_image').")";
		   	$ret['background']['image_position'] = $params->get('background_image_position');
	   	
		// Menus general settings
		   	$ret['menu']['voices_font_size'] = $params->get('menu_voices_font_size');
		   	$ret['menu']['voices_font_family'] = $params->get('menu_voices_font_family');
		   	$ret['menu']['voices_color'] = $params->get('menu_voices_color');

		   	$ret['menu']['title_font_size'] = $params->get('menu_title_font_size');
		   	$ret['menu']['title_font_family'] = $params->get('menu_title_font_family');
		   	$ret['menu']['title_color'] = $params->get('menu_title_color');

		   	$ret['menu']['transparent_desktop_hp'] = $params->get('menu_transparent_desktop_hp');

	   	// Items to show in columns
		   	if ($params->get('menu_1_show') == 1){

		   		$ret['items'][$i]['item'] = "menu_1";
		   		$ret['items'][$i]['column'] = $params->get('menu_1_assign_to_column');
		   		$ret['items'][$i]['order'] = $params->get('menu_1_order');
		   		$ret['items'][$i]['menu'] = $params->get('menu_1');
		   		$ret['items'][$i]['menu_title'] = $params->get('menu_1_title');
		   		$i++;
		   	}

		   	if ($params->get('menu_2_show') == 1){
		   		$ret['items'][$i]['item'] = "menu_2";
		   		$ret['items'][$i]['column'] = $params->get('menu_2_assign_to_column');
		   		$ret['items'][$i]['order'] = $params->get('menu_2_order');
		   		$ret['items'][$i]['menu'] = $params->get('menu_2');
		   		$ret['items'][$i]['menu_title'] = $params->get('menu_2_title');
		   		$i++;
		   	}
	   		
	   		if ($params->get('brand_show') == 1){
	   			$ret['items'][$i]['item'] = "brand";
		   		$ret['items'][$i]['column'] = $params->get('brand_assign_to_column');
		   		$ret['items'][$i]['order'] = $params->get('brand_order');
		   		$ret['items'][$i]['brand_image'] = $params->get('brand_image');
		   		$ret['items'][$i]['brand_width'] = $params->get('brand_width');
		   		$i++;
	   		}

	   		if ($params->get('newsletter_show') == 1){
	   			$ret['items'][$i]['item'] = "newsletter";
		   		$ret['items'][$i]['column'] = $params->get('newsletter_assign_to_column');
		   		$ret['items'][$i]['order'] = $params->get('newsletter_order');
		   		$ret['items'][$i]['invitation_message'] = $params->get('newsletter_invitation_message');
		   		$ret['items'][$i]['mailchimp_code'] = $params->get('newsletter_mailchimp_code');
		   		$i++;
	   		}
		   	 
	   		if ($params->get('article_id_1') != null){
	   			$ret['items'][$i]['item'] = "article_1";
		   		$ret['items'][$i]['column'] = $params->get('article_assign_to_column_1');
		   		$ret['items'][$i]['order'] = $params->get('article_order_1');
		   		$ret['items'][$i]['article_id'] = $params->get('article_id_1');
		   		$ret['items'][$i]['article_show_title'] = $params->get('article_show_title_1');
		   		$ret['items'][$i]['align'] = $params->get('article_text_align_1');
		   		$i++;
	   		}

	   		if ($params->get('article_id_2') != null){
	   			$ret['items'][$i]['item'] = "article_1";
		   		$ret['items'][$i]['column'] = $params->get('article_assign_to_column_2');
		   		$ret['items'][$i]['order'] = $params->get('article_order_2');
		   		$ret['items'][$i]['article_id'] = $params->get('article_id_2');
		   		$ret['items'][$i]['article_show_title'] = $params->get('article_show_title_2');
		   		$ret['items'][$i]['align'] = $params->get('article_text_align_2');
		   		$i++;
	   		}

	   		if ($params->get('social_show') == 1){
	   			$ret['items'][$i]['item'] = "social";
		   		$ret['items'][$i]['column'] = $params->get('social_assign_to_column');
		   		$ret['items'][$i]['order'] = $params->get('social_order');
		   		$ret['items'][$i]['align'] = $params->get('social_text_align');
		   		$ret['items'][$i]['links']['facebook'] = $params->get('social_facebook');
				$ret['items'][$i]['links']['youtube'] = $params->get('social_youtube');
				$ret['items'][$i]['links']['instagram'] = $params->get('social_instagram');
				$ret['items'][$i]['links']['twitter'] = $params->get('social_twitter');
				$ret['items'][$i]['links']['linkedin'] = $params->get('social_linkedin');
		   		$ret['items'][$i]['icons']['size'] = $params->get('social_icon_size');
		   		$ret['items'][$i]['icons']['color'] = $params->get('social_icon_color');
		   		$i++;
	   		}
    	
    	// Order items
	    	usort($ret['items'], function($a, $b) {
			    return $a['order'] <=> $b['order'];
			});

    	// Get Articles datas
	    	$article_id_1 = $params->get('article_id_1');
	    	$article_id_2 = $params->get('article_id_2');

    		$INstring = "(";
    			if ($article_id_1!=null)
    				$INstring .= $article_id_1;
    			if ($article_id_2!=null){
    				if ($article_id_1!=null)
    					$INstring .= ",";
    				$INstring .= $article_id_2;
    			}
   			$INstring .= ")";

			if (($article_id_1!=null)||($article_id_2!=null))
				$ret['articles'] = $this->getSpecificArticles($INstring);
	    	

			//dump($ret,"ret module_");
		return $ret;
	}


	// **********************************************************************

    /**
     *  Get menu items
     *  @param $menuType 		  the menu to render
     *  @return array $ret    the array with the menu items
    **/

    function getMenuItems($menuType){

		// Get selected menu
			//$menuType = $value['menu'];

		// Connect to DB
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

		// QUERY to get menu items
			$query = "
				SELECT 
				m.id AS id,
				m.title AS title,
				m.link AS link, 
				m.published AS published,
				m.level AS level,
				m.language AS language,
				m.lft AS ordering,
				m.menutype AS menutype 
				FROM #__menu AS m 
				WHERE menutype = '$menuType'
					AND level = 1 
					AND published = 1
				ORDER BY ordering ASC";

			//removed-> params->>'$.page_title' AS page_title,
				// Because available from MYSQL server version: 5.7 (on Aruba is 5.6, check in phpmyadmin)

		// Retrieve data
			$db->setQuery($query);
			$ret = $db->loadObjectList();

			return $ret;
    }


	// **********************************************************************

    /**
     *  Prepare megafooter code to render
     *  @param $params 		  array with collected parameters
     *  @return string $ret    code to render
    **/

    function prepareMegafooterCodeToRender($parameters){
    	//dump($parameters,"parameters");
    	$ret = "";
		
		// Voices and title style
			$menu_voices_style = "color:".$parameters['menu']['voices_color']."; ";
			$menu_voices_style .= "font-size:".$parameters['menu']['voices_font_size']."; ";
			$menu_voices_style .= "font-family:".$parameters['menu']['voices_font_family']."; ";
			$menu_title_style = "color:".$parameters['menu']['title_color']."; ";
			$menu_title_style .= "font-size:".$parameters['menu']['title_font_size']."; ";
			$menu_title_style .= "font-family:".$parameters['menu']['title_font_family']."; ";

			$menu_voices_color = "color:".$parameters['menu']['voices_color']."; ";

    	// Columns code
    		$column_1  = "<aside style='flex: ".$parameters['column_flex'][1].";'>"; 
    		$column_2  = "<aside style='flex: ".$parameters['column_flex'][2].";'>"; 
    		$column_3  = "<aside style='flex: ".$parameters['column_flex'][3].";'>"; 
    		$column_4  = "<aside style='flex: ".$parameters['column_flex'][4].";'>"; 
	    	
	    	foreach ($parameters['items'] as $key => $value) {

	    		$column_variable_name = "column_" . $value['column'];
	    		
	 			switch ($value['item']) {
	 				case 'brand':
	 					
	 					$image_style = "";
	 					if ($value['brand_width']!= null)
		 						$image_style .= "width:".$value['brand_width']."; ";

	 					// Code to render
	 						${$column_variable_name} .= "<div class='image'><img style='".$image_style."' src='".JURI::base()."images/brand/".$value['brand_image']."'/></div>";
	 					break;

	 				case 'newsletter':
	 					
						// MailChimp Signup Form - Embedded forms (Naked) - Show only requested fields - Change " with ' or maybe not
	  					
	  					// Code to render
	  						$newsletterCode = "<div class='newsletter'>";
								$newsletterCode .= "<b style='".$menu_title_style."'>".$value['invitation_message']."</b>";
								$newsletterCode .= $value['mailchimp_code'];
							$newsletterCode .="</div>";

	  						${$column_variable_name} .= $newsletterCode;

	 					break;
	 				
	 				case 'menu_1':
	 				case 'menu_2':
	 					
	 					// Get menu items
	 						$menuItems = $this->getMenuItems($value['menu']);

						// Code to render
							${$column_variable_name} .= "<div class='menu'>";
								// Menu title
									if ($value['menu_title'] != null)
										${$column_variable_name} .= "<h4 style='".$menu_title_style."'>".$value['menu_title']."</h4>";

								// Menu voices 
									${$column_variable_name} .= "<ul class='nav menu'>";
										foreach ($menuItems as $key => $menuItem) {

											// GET Item title from element name or Page Display -> Browser Page Title
												//$itemTitle = ($menuItem->page_title != "") ? $itemTitle = $menuItem->page_title : $itemTitle = $menuItem->title;
											// Item
												${$column_variable_name} .= "<li><a href='".$menuItem->link."&Itemid=".$menuItem->id."' style='".$menu_voices_style."'>".$menuItem->title."</a></li>";
										}
									${$column_variable_name} .= "</ul>";
							${$column_variable_name} .= "</div>";
	 					break;

	 				case 'article_1':
	 				case 'article_2':
	 										
	 					foreach ($parameters['articles'] as $key => $article) {
	 						// check if in the articles loaded from DB there is one that has the same id of this item
	 						if ($article->id = $value['article_id']){
	 							
	 							if ($value['align']!= null)
		 							$article_style = "text-align:".$value['align']."; ";
	 							
		 						// Code to render
		 							${$column_variable_name} .= "<div class='article' style='".$menu_voices_color.$article_style."'>";
		 								${$column_variable_name} .= $article->introtext.$article->full_text;
		 							${$column_variable_name} .= "</div>";
	 						}
	 					}
	 					
	 					break;

	 				case 'social': 
	 						if ($value['align']!= null)
	 							$social_style = "text-align:".$value['align']."; ";
	 						
	 						$icons_style  = "color:".$value['icons']['color']."; ";
	 						$icons_style  .= "font-size:".$value['icons']['size']."; ";

	 						// Code to render
		 						${$column_variable_name} .= "<div class='social_media' style='".$social_style."'>";

			 						foreach ($value['links'] as $key => $social_link) {
			 							
			 							// chose the proper icon
				 							switch ($key) {
				 								case 'facebook':
				 									$icon = "<i class='fab fa-facebook-square' aria-hidden='true' style='".$icons_style."'></i>";
				 									break;
				 								case 'youtube':
				 									$icon = "<i class='fab fa-youtube-square' aria-hidden='true' style='".$icons_style."'></i>";
				 									break;
				 								case 'instagram':
				 									$icon = "<i class='fab fa-instagram' aria-hidden='true' style='".$icons_style."'></i>";
				 									break;
				 								case 'twitter':
				 									$icon = "<i class='fab fa-twitter-square' aria-hidden='true' style='".$icons_style."'></i>";
				 									break;
				 								case 'linkedin':
				 									$icon = "<i class='fab fa-linkedin' aria-hidden='true' style='".$icons_style."'></i>";
				 									break;

				 									
				 							}

			 							if ($social_link != null)
				 							${$column_variable_name} .= "<a target='_blank' href='".$social_link."'>".$icon."</a>";
			 						}
			 					${$column_variable_name} .= "</div>";
	 						
	 					break;
	 			}
	    	}

			
		// Wrapper container styles
    		$wrapper_style = "";
    	
    		if ($parameters['wrapper']['max_width'] != "")
    			$wrapper_style .= "max-width:".$parameters['wrapper']['max_width']."; ";

    		if ($parameters['wrapper']['margin'] != "")
    			$wrapper_style .= "margin:".$parameters['wrapper']['margin']."; ";
  			
    		$wrapper_style  .= "justify-content:".$parameters['wrapper']['justify_content']."; ";
    		$wrapper_style  .= "flex-flow:".$parameters['wrapper']['flex_flow']."; ";
    		$wrapper_style  .= "flex-wrap:".$parameters['wrapper']['flex_wrap']."; ";


			switch ($parameters['background']['type']) {
    			// Colored
	    			case 1:
	    				$wrapper_style  .= "background-color:".$parameters['background']['color']."; ";
	    				break;
    			// Gradient
	    			case 2:
	    				$wrapper_style  .= "background:".$parameters['background']['gradient']."; ";
	    				break;
    			// Image
	    			case 3:
	    				$wrapper_style  .= "background-image:".$parameters['background']['image']."; ";
    					$wrapper_style  .= "background-position:".$parameters['background']['image_position']."; ";
	    				break;
    		}

	    // Prepare code to render
	    	$column_1 .= "</aside>";
	    	$column_2 .= "</aside>";
	    	$column_3 .= "</aside>";
	    	$column_4 .= "</aside>";


	    	$ret .= "<footer class='megaFooter wrapper' style='".$wrapper_style."'>";

	    		// Add to the code to render the code of all the active columns 
		    		for ($i=1; $i < $parameters['column_number']+1; $i++) { 
		    			$ret .= ${"column_".$i};   //eg. $ret .= $column_1;
		    		}
	    		
		    $ret .= "</footer>";
    
    	return $ret;
    }

	
	// **********************************************************************

    /**
     *  Prepare the menu code for the sticky footer
     *  @param $params 		  array with collected parameters
     *  @param $menuItems 	  the items of the menu
     *  @return string $ret   code to render
    **/
	
	function printStickyMenu($parameters, $menuItems, $menuAlignment){

		// Sef language tag
    		$lang = JFactory::getLanguage();
			$sef_lang = array_shift(explode('-', $lang->getTag()));

		// Get Active Item ID
			$app = JFactory::getApplication();
			$active_menu_item_id = $app->getMenu()->getActive()->id;

		// Styles and classes
			$menu_class = $menuAlignment;
			$menu_voices_style = "";

		// Prepare code to render
			$ret .= "<ul class='nav navbar-nav ".$menu_class."'>";

				foreach ($menuItems as $key => $menuItem) {

					$itemIsActiveClass = ($menuItem->id == $active_menu_item_id) ? " active" : "";

					// GET Item title from element name or Page Display -> Browser Page Title
						$itemTitle = ($menuItem->page_title != "") ? $itemTitle = $menuItem->page_title : $itemTitle = $menuItem->title;
					// Item
						$link = JRoute::_($menuItem->link."&Itemid=".$menuItem->id."&lang=".$sef_lang);
						$ret .= "<li class='".$itemIsActiveClass."'><a href='".$link."' style='".$menu_voices_style."'>".$itemTitle."</a></li>";
				}
			$ret .= "</ul>";

		return $ret;
	}




	// **********************************************************************

    /**
     *  Prepare sticky footer code to render
     *  @param $params 		  array with collected parameters
     *  @return string $ret    code to render
    **/

    function prepareStickyfooterCodeToRender($params){

    	// Get if we are in HP
			$weAreInHomepage = $this->checkIfHP();

    	// Styles and classes
	    	$stickyFooter_style = "";
	    		$stickyFooter_style .= "background-color:".$params->get('background_color')."; ";
    		
	    	$stickyFooter_class .= "stickyFooter ";
	    	if (($params->get('footer_transparent_desktop_hp'))&&($weAreInHomepage))
				$stickyFooter_class .= "transparent-sticky-footer-hp ";



    	$ret .= "<div class='paddingBalanceForStickyFooter'></div>";
    	$ret .= "<footer class='".$stickyFooter_class."' style='".$stickyFooter_style."'>";
			
    		// Language flag module (Falang)
    			if($params->get('language_show') == 1){
					$document = JFactory::getDocument();
				 	$renderer = $document->loadRenderer('module');
				 	$isModuleEnabled = JModuleHelper::isEnabled('mod_falang');

				 	if ($isModuleEnabled){
					 	$Module = JModuleHelper::getModule('mod_falang');
					 	$ret .= $renderer->render($Module);
				 	}
			 	}

			// Menu 1
    			if ($params['menu_1_show'] == 1){
	 				$menuItems = $this->getMenuItems($params->get('menu_1'));
	 				$menuAlignment = $params->get('sticky_footer_menu_1_align');
	 				$ret .= $this->printStickyMenu($params, $menuItems, $menuAlignment);
				}
			// Menu 2
				if ($params['menu_2_show'] == 1){
					$menuItems = $this->getMenuItems($params->get('menu_2'));
					$menuAlignment = $params->get('sticky_footer_menu_2_align');
					$ret .= $this->printStickyMenu($params, $menuItems, $menuAlignment);
				}
			// Article
				if ($params['article_id_1'] != null){
					$INstring = "(".$params['article_id_1'].")";
					$articles = $this->getSpecificArticles($INstring);
					$ret .= "<div class='text'>".$articles[0]->introtext."</div>";
				}

	    $ret .= "</footer>";

    	return $ret;
    }

	// **********************************************************************

}

?>