<?php

// This is the main workhorse model that handles the business logic

// No direct access
	defined( '_JEXEC' ) or die;

class reportAbuse{

	/**
     *  LOAD module CSS and JS
     *  @param none
     *  @return none
    **/

	function loadCssandJs(){
		$document = &JFactory::getDocument();
		$document->addScript(JURI::root(true) . '/media/mod_reportabuse/js/mod_reportabuse.js');
		$document->addStyleSheet(JURI::root(true) . '/media/mod_reportabuse/css/mod_reportabuse.min.css');
	}

	// **********************************************************************

    /**
     *  Prepare carousel code to render
     *  @param $articleID 		  The id of the article to show above the carousel
     *  @param $imagesFolder	The path to the images folder
     *  @return string $ret    code to render
    **/

    function prepareCodeToRender(){

 		$ret = "<div class='reportAbuse'>";
 			$ret .= "<button type='button' class='press press-orange press-ghost press-round '>Report misuse</button>";
 		$ret .= "</div>";

		
    	return $ret;
    }

// **********************************************************************


    function sendMail(){
        $jinput = JFactory::getApplication()->input;
        $reason = $jinput->get('reason', null, null);
        $message = $jinput->get('message', null, null);

        if ($reason!=null){


            //if (file_exists('swiftmailer/lib/swift_required.php')) {
                //include 'swiftmailer/lib/swift_required.php';
                //echo "File exist and loaded";
            //}
            //else {
            //    echo "File not found";
            //}

            require_once dirname( __FILE__ ) . DS . 'swiftmailer' . DS . 'lib' . DS . 'swift_required.php';


        // GET parameters
            //$link = JRequest::getVar('link');
            //$abuserMail = JRequest::getVar('creator_mail');

            $eventlink = isset($_POST['eventlink']) ? $_POST['eventlink'] : ''; 
            $reasonOfAbuseCode = isset($_POST['reason']) ? $_POST['reason'] : '';
            $reasonMessage = isset($_POST['message']) ? $_POST['message'] : '';
            $abuserMail = isset($_POST['creator_mail']) ? $_POST['creator_mail'] : ''; 
            $adminMain = 'davide.casiraghi@gmail.com';


        // Assign a text to reason of abuse
            switch ($reasonOfAbuseCode) {
                case 1:
                    $reasonOfAbuse = "Not about Contact Improvisation";
                break;
                case 2:
                    $reasonOfAbuse = "It contains wrong informations";
                break;
                case 3:
                    $reasonOfAbuse = "It is not translated in english";
                break;
                case 4:
                    $reasonOfAbuse = "Other reasons";
                break;
                default:
                    $reasonOfAbuse = "Nothing specified";
                break;
            }


        // Create the Transport
            //$transport = Swift_SmtpTransport::newInstance('localhost', 25);
            $transport = Swift_SmtpTransport::newInstance('smtp.googlemail.com', 465, 'ssl')
              ->setUsername('davide.casiraghi@gmail.com')
              ->setPassword('tnyrbixxgkquzlqv');  //Pass generated here - https://stackoverflow.com/questions/42558903/expected-response-code-250-but-got-code-535-with-message-535-5-7-8-username

        // Create the Mailer using your created Transport
            $mailer = Swift_Mailer::newInstance($transport);

        // Create the message
            $message = Swift_Message::newInstance()

              // Give the message a subject
              ->setSubject('Report from CI Global Calendar')

              // Set the From address with an associative array
              ->setFrom(array('davide.casiraghi@gmail.com' => 'Davide Casiraghi'))

              // Set the To addresses with an associative array
              ->setTo(array('davide.casiraghi@gmail.com', 'davide.casiraghi@email.it'));

              // Give it a body
              $message->setBody(
                "A user report an abuse in the following event:<br>".
                "<a href=".$eventlink.">".$eventlink."</a><br><br>".
                "Why: ".$reasonOfAbuse."<br><br>".
                "Message: ".$reasonMessage
                , 'text/html');

              // And optionally an alternative body
              //$message->addPart('<q>Why: '.$reasonOfAbuse.'</q>', 'text/html')
              //$message->addPart('<q>Message: '.$reasonMessage.'</q>', 'text/html');

          // Optionally add any attachments
          //->attach(Swift_Attachment::fromPath('my-document.pdf'))
          //;

              
        // Send the message
            try {
                
                // Send
                    $mailer->send($message);
                
                // Send confirmation
                    echo "<div style='padding:20px 20px 40px 20px;'>";
                        echo "<h2 style='color: #f08e0d;'>Thank you for your report</h2>\n";
                        echo "The administrators will check soon the event you have specified\n";
                    echo "</div>";
            }
            catch (\Swift_TransportException $e) {
                // Write error details in the popup
                    echo $e->getMessage();
            }



        }


    }
    

}

?>

