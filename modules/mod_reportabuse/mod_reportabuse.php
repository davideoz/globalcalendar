<?php
/**
 * Report abuse module main file
 *
 * @package 	Life Carousel
 * @copyright 	Copyright (C) 2018 Davide Casiraghi (http://www.movementmeetslife.com). All rights reserved.
 * @license 	GNU General Public License version 2 or later; http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
	defined( '_JEXEC' ) or die ;

// define directory separator
	if(!defined('DS')){
		define('DS',DIRECTORY_SEPARATOR);
	}

// Include the syndicate functions only once
	require_once dirname( __FILE__ ) . DS . 'helper.php';

// Get Template path
	//$app = JFactory::getApplication();
	//$templatePath = JURI::base(true).'/templates/'.$app->getTemplate();

// Instantiate class lifeVideo
	$reportAbuse = new reportAbuse; 

// Load CSS and JS
	$reportAbuse->loadCssandJs();

/*
// GET module parameters
	$moduleclass_sfx = htmlspecialchars( $params->get( 'moduleclass_sfx' ) );
	$catId = $params->get('category_id');
	$text_background_color = $params->get('text_background_color');

	$parameters['cat_id'] = $params->get('category_id');
	$parameters['menu_item_id'] = $params->get('menu_item_id');
	$parameters['text']['color'] = $params->get('text_color');
	$parameters['text']['background_color'] = $params->get('text_background_color');

// Get articles
	$articles = $lifeCarouselArticles->getArticles('category', $parameters['cat_id'], 100);

// Get Category name
	$categoryName = $lifeCarouselArticles->getCategoryName($parameters['cat_id']);
	*/

// Get code to render
	$codeToRender = $reportAbuse->prepareCodeToRender();


require JModuleHelper::getLayoutPath( 'mod_reportabuse', $params->get( 'layout', 'default' ) );

