<!--

The default.php file is the modules "default view". 
It is in charge of displaying the module to the user. 
It is essentially html with php includes and loops. 
This file performs a foreach loop to diaplay the $rows array which is the array of users.

-->

<?php


    require_once JPATH_ROOT . '/components/mod_upcomingevents/helpers/route.php';


// Connect to DB
    $db = JFactory::getDBO();
    $query = $db->getQuery(true);

// Query to select single item
    $query = "
        SELECT i.id, i.title AS title, CONCAT(i.introtext, i.fulltext) AS text, alias, created as date, u.name AS username
        FROM #__k2_items AS i
        LEFT JOIN #__users AS u ON i.created_by = u.id
        WHERE i.catid = $categoryID
        AND i.id != $introitemID
        AND i.trash = 0
        AND i.published = 1
        ORDER BY date DESC
        ";


// Retrieve data
    $db->setQuery($query);
    $result = $db->loadObjectList();


// Print intro item
    //$itemSlug = $result[0]->id.':'.$result[0]->alias;
    $itemLink = JRoute::_(GeneralinewsHelperRoute::getGeneralinewsRoute($result[0]->id, $menuVoiceID));
    $pageLink = JRoute::_(GeneralinewsHelperRoute::getGeneralinewsPageRoute($menuVoiceID));

      
    echo "<div class='moduleNews expoNews'>";

        echo "<table>";
            echo "<tr>";
                // Image
                    echo "<td>";
                        echo "<a class='imagelink' href='".$itemLink."'>";
                            echo "<img src='/media/k2/items/cache/".md5("Image".$result[0]->id)."_M.jpg'/>";
                        echo "</a>";
                    echo "</td>";
                // Text
                    echo "<td>";
                        echo "<div class='textContainer'>";
                            echo "<b><a class='title' href='".$itemLink."'>".$result[0]->title."</a></b>";
                            echo "<div class='introtext'>";
                                
                                echo "<span class='itemDateCreated'>" . JHTML::_('date', $result[0]->date, "d/m/Y") . "</span>";
                                echo " | by ".$result[0]->username;

                            echo "</div>";

                        echo "</div>";

                        echo "<a class='readMore' href='".$itemLink."'>Read more</a>";



                    echo "</td>";

            echo "</tr>";
        echo "</table>";

        echo "<div class='readAllNews'>";
            echo "<a href='".$pageLink."'>Read all the articles</a>";           
        echo "</div>";

          
 echo "</div>";




?>





