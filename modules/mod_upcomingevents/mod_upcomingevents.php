<?php

//This file acts as a controller directing the functions and files

//don't allow other scripts to grab and execute our file
defined('_JEXEC') or die('Direct Access to this location is not allowed.');


// Include the syndicate functions only once
	//require_once dirname(__FILE__).'/helper.php';
	//require_once JPATH_ROOT . '/components/mod_upcomingevents/helper.php';

	
	//equire_once JPATH_ROOT . '/components/com_contactevents/assets/cookies.php';
	JLoader::register('ModUpcomingeventsHelper', __DIR__ . '/helper.php');

// Load CSS and JS
	$document = &JFactory::getDocument();
	$document->addScript(JURI::root(true) . '/modules/mod_upcomingevents/tmpl/js/upcomingevents.js');
	$document->addStyleSheet(JURI::root(true) . '/modules/mod_upcomingevents/tmpl/css/upcomingevents.css');

// Get Parameters
	$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
	$layout_type = $params->get('layout_type');
	/*$categoryID = $params->get('category');
	$quantity = $params->get('quantity');
	$introitemID = $params->get('intro_item_id');
	$menuVoiceID = $params->get('menu_voice_id');*/

//Returns the path of the layout file
	if ($layout_type == 'layoutCommunication') require JModuleHelper::getLayoutPath('mod_upcomingevents', $params->get('layout', 'communication'));
	else if ($layout_type == 'layoutExpo') require JModuleHelper::getLayoutPath('mod_upcomingevents', $params->get('layout', 'expo'));


// Get events
	//$list = modUpcomingeventsHelper::getList($params);
	//echo $list;

//if (!count($list)) {
//	return;
//}





?>