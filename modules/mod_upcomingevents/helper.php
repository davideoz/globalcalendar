<?php

// This is the main workhorse model that handles the business logic

//No access
defined( '_JEXEC' ) or die;

/**
 * Helper for mod_upcomingevents
 *
 * @package     Joomla.Site
 * @subpackage  mod_upcomingevents
 * @since       1.5
 */
class ModUpcomingeventsHelper{



	/**
     * GET list of upcoming events of specified person or venue
     *
     * @access     public
     * @param      string 		  "teacher" or "organizer" or "venue"
     * @param      number 		  if of the person or venue
     * @return     array          list of evets related of the specified resource
     */

	static function getList($kind, $name){

		// Get a db connection  and create new query object.
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

		// Compose query
			$query = "
				SELECT *
				FROM #__distilled_homepage_events AS a
				JOIN 
				( SELECT ev_id, MIN(repetition_id) as rp_id
				  FROM #__distilled_homepage_events GROUP BY ev_id
				) as b
				ON a.repetition_id = b.rp_id AND a.ev_id = b.ev_id
				";

			switch ($kind) {
				case 'teacher':
					$query .= "AND teachers LIKE '%".$name."%' ";
				break;
				case 'organizer':
					$query .= "AND organizers LIKE '%".$name."%' ";
				break;
				case 'venue':
					//$query = "SELECT count(*) as counter ";
				break;
			}

			$query .= " ORDER BY date_start";

		// Execute query
			$db->setQuery($query);
			$results = $db->loadObjectList();

			//dump($kind,"kind");
			//dump($query,"query");
			//dump($results,"results");
		return $results; 

	}




	/**
     * Print the list of events
     *
     * @access     public
     * @param      string 		  "teacher" or "organizer" or "venue"
     * @param      number 		  if of the person or venue
     * @return     array          list of evets related of the specified resource
     */

	static function printList($events){

		echo "<h3 class='title'>Upcoming events</h3>";
		echo "<div class='list'>";

			//dump($events,"events");

			foreach ($events as $key => $event) {
				
				// To give rows different colors (white/gray)
	            	$oddEven = ($key % 2 == 0) ? 'odd' : 'even';
		            
	            // Check if the event last just one day 
        			$start_end_sameday = strcmp(date('d - m', $event->date_start),date('d - m', $event->date_end));
	
    			// This is for the square color of the date (year0, year1, year2)
    				$yearFromNow = date('Y', $event->date_start) - date("Y");

    				$dateStart = date('d/m/Y', $event->date_start);
    				$dateStart_day = date('d', $event->date_start);
    				$dateStart_month = date('M', $event->date_start);

    				$dateEnd = date('d/m/Y', $event->date_end);
    				$dateEnd_day = date('d', $event->date_end);
    				$dateEnd_month = date('M', $event->date_end);

	            // Create LINK
	            	$alias = str_replace(' ', '-', strtolower($event->summary));
					$link = JURI::root(true) ."/index.php/events/eventdetail/".$event->rp_id."/-/".$alias;


				// Print ROW
	            	echo "<div class='row ".$oddEven."'>";

			            // Date
				            echo "<div class='col-md-1 date'>";
				            	
				            	if ($start_end_sameday){
				            		echo "<div class='row' style='margin:0;'>";

						            	echo "<div class='col-xs-6'>";
						            		
						            		// Mobile
						            			echo "<div class='date_start dateMobile year".$yearFromNow."'>";
								            		echo $dateStart;
							            		echo "</div>";
						            		// Desktop
							            		echo "<div class='date_start dateDesktop year".$yearFromNow."' tooltip='".$dateStart."' style='display:none;'>";
								            		echo $dateStart_day;
								            		echo "<br>";
								            		echo $dateStart_month;
						            			echo "</div>";
					            			
					            		echo "</div>";

						            	echo "<div class='col-xs-6'>";
						            		
							            		// Mobile
						            				echo "<div class='date_end dateMobile year".$yearFromNow."'>";
									            		echo $dateEnd;
								            		echo "</div>";
						            			// Desktop
								            		echo "<div class='date_end dateDesktop year".$yearFromNow."' tooltip='".$dateEnd."' style='display:none;'>";
									            		echo $dateEnd_day;
									            		echo "<br>";
									            		echo $dateEnd_month;
								            		echo "</div>";
						            		
					            		echo "</div>";
				            		echo "</div>";
			            		}
			            		else{
			            			echo "<div class='row' style='margin:0; padding-right:3px;'>";
				            			
						            		// Mobile
			            						echo "<div class='date_start dateMobile year".$yearFromNow."' tooltip='".$dateStart."' style='width:100%;'>";
												
								            		echo $dateStart;
							            		echo "</div>";
				            				// Desktop
					            				echo "<div class='date_start dateDesktop year".$yearFromNow."' tooltip='".$dateStart."' style='width:100%; display:none;'>";
								            		echo $dateStart_day;
								            		echo "<br>";
								            		echo $dateStart_month;
							            		echo "</div>";
					            		
				            		echo "</div>";
			            		}
			            		
				            echo "</div>";
				            echo "<div class='col-md-3 vcenter link'><a href='".$link."'>".$event->summary."</a></div>";
				            echo "<div class='col-md-2 vcenter teachers'>";
				            	echo "<i class='fa fa-users' style='margin-right:10px;'></i>";
				            	echo $event->teachers;
							echo "</div>";	
				            echo "<div class='col-md-2 vcenter category'>";
				            	echo "<i class='fa fa-tag' style='margin-right:10px;'></i>";
				            	echo $event->cat_name;
				            echo "</div>";


				            echo "<div class='col-md-3 vcenter location'>";
				            	echo "<i class='fa fa-map-marker' style='margin-right:10px;'></i>";
				            	echo $event->venue." <br> ".$event->city.", ".$event->country;
							echo "</div>";				            

							if (strlen($event->facebook_link) != 0){
								echo "<div class='col-md-1 vcenter facebook'><a href='".$event->facebook_link."' target='_blank'><i class='fa fa-facebook-square'></i></a></div>";			
							}
				            
	        		echo "</div>";


	        		
	        		 
			} /* end foreach */

		echo "</div>";

	}

}

?>