<?php
/**
 * Life Newsletter module Default layout
 *
 * @package 	Life Newsletter
 * @copyright 	Copyright (C) 2018 Davide Casiraghi (http://www.movementmeetslife.com). All rights reserved.
 * @license 	GNU General Public License version 2 or later; http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined( '_JEXEC' ) or die ;
?>


<!-- Render the code -->  
<?php echo $codeToRender?>


