<?php

// This is the main workhorse model that handles the business logic

// No direct access
	defined( '_JEXEC' ) or die;


class lifeHeaderMenu{

    /**
     *  LOAD module CSS and JS for Menu BAR
     *  @param none
     *  @return none
    **/

	function loadCssandJsBar(){
		//$document = &JFactory::getDocument();
		//$document->addScript(JURI::root(true) . '/media/mod_lifeheadermenu/js/mod_lifeheadermenu-bar.js');
		//$document->addStyleSheet(JURI::root(true) . '/media/mod_lifeheadermenu/css/mod_lifeheadermenu-bar.min.css');
	}

	// **********************************************************************

   /**
     *  LOAD module CSS and JS for Menu BAR
     *  @param none
     *  @return none
    **/

	function loadCssandJsHamburger(){
		//$document = &JFactory::getDocument();
		//$document->addScript(JURI::root(true) . '/media/mod_lifeheadermenu/js/mod_lifeheadermenu-hamburger.js');
		//$document->addStyleSheet(JURI::root(true) . '/media/mod_lifeheadermenu/css/mod_lifeheadermenu-hamburger.min.css');
	}

	// **********************************************************************

    /**
     *  GET module parameters from joomla backend
     *  @param $params 		  array of the defauld joomla variable containing the module parameters 
     *  @return array $ret    the multidimentional array containing the parameters
    **/

    function getModuleParameters($params){
    	$ret = array();

    	// Kind of menu
    		$ret['menu_kind'] = $params->get('menu_kind');

    	// Menu Bar 
    		$ret['bar']['height'] = $params->get('menu_bar_height'); 
    		$ret['bar']['scalable'] = $params->get('menu_bar_scalable');	
    		$ret['bar']['starting_height'] = $params->get('menu_bar_starting_height');
    		$ret['bar']['menu_sticky'] = $params->get('menu_sticky');

    		$ret['bar']['bottom_border_height'] = $params->get('menu_bar_bottom_border_height');
    		$ret['bar']['bottom_border_color'] = $params->get('menu_bar_bottom_border_color');

    		$ret['items']['font_size'] = $params->get('menu_items_font_size');  //aaaaa
    		$ret['items']['font_family'] = $params->get('menu_items_font_family');
    		$ret['items']['line_height'] = $params->get('menu_items_line_height');

			
    	// Colors
    		$ret['colors']['background'] = $params->get('menu_bg_color');
    		$ret['colors']['background_hover'] = $params->get('menu_bg_color_hover');
    		$ret['colors']['opacity'] = $params->get('menu_bg_opacity');
    		$ret['colors']['font'] = $params->get('menu_font_color');
    		$ret['colors']['font_hover'] = $params->get('menu_font_hover_color');
    		$ret['colors']['transparent_desktop_hp'] = $params->get('menu_transparent_desktop_hp');
    		
	   	// Menu 1
	    	if ($params->get('menu_1_show') == 1){
	    		$ret['menu'][1]['items'] = $this->getMenuItems($params->get('menu_1'));
	    		$ret['menu'][1]['alignment'] = $params->get('menu_1_alignment');
	    		$ret['menu'][1]['access'] = $params->get('menu_1_access');
	   		}
	   	// Menu 2
	   		if ($params->get('menu_2_show') == 1){
		   		$ret['menu'][2]['items'] = $this->getMenuItems($params->get('menu_2'));
	    		$ret['menu'][2]['alignment'] = $params->get('menu_2_alignment');
	    		$ret['menu'][2]['access'] = $params->get('menu_2_access');
	   		}
	   	// Menu 3
	   		if ($params->get('menu_3_show') == 1){
		   		$ret['menu'][3]['items'] = $this->getMenuItems($params->get('menu_3'));
	    		$ret['menu'][3]['alignment'] = $params->get('menu_3_alignment');
	    		$ret['menu'][3]['access'] = $params->get('menu_3_access');
	   		}

	   	// Brand
	   		if ($params->get('brand_show') == 1){
		   		$ret['brand']['image'] = JURI::base()."images/brand/".$params->get('brand_image');
		   		$ret['brand']['width'] = $params->get('brand_width');
	   		}
	   	// Language
	   		$ret['language_show'] = $params->get('language_show'); 

		
			//dump($ret,"ret module_");
		return $ret;
	}

	// **********************************************************************

    /**
     *  GET menu items
     *  @param $menuType  	  the menu type selected in the module parameters that correspond to the menu
     *  @return array $ret    the array containing the menu items
    **/

	function getMenuItems($menuType){						

		// Connect to DB
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

		// QUERY to get menu items
			$query = "
				SELECT 
				m.id AS id,
				m.title AS title,
				m.link AS link, 
				m.published AS published,
				m.level AS level,
				m.parent_id AS parent_id,
				m.language AS language,
				m.lft AS ordering,
				m.menutype AS menutype, 
				m.access AS accesslevel 
				FROM #__menu AS m 
				WHERE menutype = '$menuType'
					AND published = 1
				ORDER BY ordering ASC";

		//removed-> params->>'$.page_title' AS page_title,
			// Because available from MYSQL server version: 5.7 (on Aruba is 5.6, check in phpmyadmin)

		// Retrieve data
			$db->setQuery($query);
			$menuItems = $db->loadObjectList();

		// Create the items tree
			$itemsTree = array(); 
			foreach ($menuItems as $key => $item) {
				if ($item->id == 1)
					$itemsTree[$item->id] = $item;
				else
					$itemsTree[$item->parent_id][$item->id] = $item;
			}
			
			$ret = $itemsTree;

		return $ret;
							
}

	// **********************************************************************

    /**
     *  Get MAIN MENU and BRAND Style and Classes
     *  @param $id 		  Id of the parent menu item (1 print the level 1 elements)
     *  @return string $ret    code to render
    **/

	function getMainMenuStyleClasses($parameters, $weAreInHomepage){
		$headerMenu_class = "";

		// Menu Class
	        if (($parameters['colors']['transparent_desktop_hp'])&&($weAreInHomepage))
	            $headerMenu_class .= "transparent-header-menu-hp ";
	        if ($parameters['bar']['menu_sticky'])
	        	$headerMenu_class .= "navbar-fixed-top ";
        
        // Menu Style 
	        $headerMenu_style = "";
	        $headerMenu_style .= "background-color: ".$parameters['colors']['background']."; ";
	        $headerMenu_style .= "opacity: ".$parameters['colors']['opacity']."; ";
	        //$headerMenu_style .= "color: ".$parameters['colors']['font']."; ";
	        $headerMenu_style .= "height: ".$parameters['bar']['height']."px; "; 

	        $headerMenu_style .= "border-bottom: ".$parameters['bar']['bottom_border_height']." solid;";
    		
	        if ($parameters['bar']['bottom_border_color'] != NULL)
	    		$headerMenu_style .= "border-color: ".$parameters['bar']['bottom_border_color']."; ";
	    	else
	    		$headerMenu_style .= "border-color: transparent; ";


    		$headerMenu_style .= "font-size:".$parameters['items']['font_size']."; ";
	    	$headerMenu_style .= "font-family:".$parameters['items']['font_family']."; ";




	    // Brand

	        //dump($parameters['bar']['bottom_border_height'], "border height"); // aaaaaaa
	        $brand_style = "width: ".$parameters['brand']['width'];

	    // Menu items
	        $menuItemLink_style = "line-height: ".$parameters['items']['line_height']."; ";
	        $menuItemLink_style .= "background-color: ".$parameters['colors']['background']."; ";
	        $menuItemLink_style .= "color: ".$parameters['colors']['font']."; ";

	        if ($parameters['bar']['scalable']){
	        	$menuItemLink_style .= "line-height: ".$parameters['bar']['starting_height']."px; ";
				/*$brand_style  .= "height:".($parameters['bar']['starting_height']+30)."px; ";*/
			}
			else
				/*$brand_style  .=  "height: ".$parameters['bar']['height']."px; ";*/

		// Menu items hover
			$menuItemLinkHover_style = "background-color: ".$parameters['colors']['background_hover']."; ";
			$menuItemLinkHover_style .= "color: ".$parameters['colors']['font_hover']."; ";

		return array('class' => $headerMenu_class, 'style' => $headerMenu_style, 'item_link_style' => $menuItemLink_style, 'item_link_hover_style' => $menuItemLinkHover_style,'brand_style' => $brand_style);
	}

	// **********************************************************************

    /**
     *  Check if we are in HP
     *  @param none 		 
     *  @return boolean $ret    1 if we are in HP
    **/

	function checkIfHP(){
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$ret = ($menu->getActive() == $menu->getDefault()) ? '1' : '0';

		return $ret;
	}

	// **********************************************************************

    /**
     *  Prepare code to render for Menu HAMBURGER + OVERLAY
     *  @param $params 		  array with collected parameters
     *  @return string $ret    code to render
    **/

	function prepareCodeToRenderHamburger($parameters){
		
		// Load CSS and JS
			$this->loadCssandJsHamburger();

		// Sef language tag
    		$lang = JFactory::getLanguage();
			$sef_lang = array_shift(explode('-', $lang->getTag()));

		// Get Active Item ID
			$app = JFactory::getApplication();
			$active_menu_item_id = $app->getMenu()->getActive()->id;

			$linkClass = $parameters['colors']['font_hover'];
			$toggleButtonClass = $parameters['colors']['font_hover'];

		// Get the access levels the user is authorized to view - Every user (1- Pubic), Unlogged User (5 - Guest), Logged user (2 - Registered)
			$user = JFactory::getUser();
			$authorisedViewLevels = $user->getAuthorisedViewLevels();

		// Prepare code to render
				$ret .= "<header class='lifeHeaderMenu'>";			
					$ret .="<div class='navbar-header'>";
						$ret .="<div class='burgher'>";
							$ret .="<div class='burgher-background'></div>";
							$ret .="<div class='toggle-button ".$toggleButtonClass."' >";
								$ret .="<span class='bar top'></span>";
								$ret .="<span class='bar middle'></span>";
								$ret .="<span class='bar bottom'></span>";
							$ret .="</div>";
						$ret .="</div>";
					$ret .="</div>";

			        $ret .="<nav class='overlay'>";

			        // For each menu
                    	foreach ($parameters['menu'] as $key => $menu) {

                    		// Check if menu aligned right
	                    		$menuAlignedRightClass = ($menu['alignment'] == "right") ? "navbar-right" : "";

	                    	// Menu code
					            $ret .="<ul class='menu'>";
						            foreach ($menu['items'][1] as $key => $item) {
						            	$hasSubMenu = (array_key_exists($key,$menu['items'])) ? 1 : 0;		
										$itemIsActiveClass = ($item->id == $active_menu_item_id) ? " active" : "";
										$link = JRoute::_($item->link."&Itemid=".$item->id."&lang=".$sef_lang);

										if (in_array($item->accesslevel, $authorisedViewLevels)){
							            	$ret .= "<li class='item-".$item->id.$itemIsActiveClass."'>";
			                    				$ret .= "<a href='".$link."' style='".$menuStyle['item_link_style']."' class='".$linkClass."'>";
				                    				$ret .= $item->title;
				                    				// Add arrow down to items that have submenus
					                    				if ($hasSubMenu)
					                    					$ret .= "<span class='caret'></span>";
			                    				$ret .= "</a>";
			                				$ret .= "</li>";
		                				}

						            }
					            $ret .="</ul>";
					    }

					    // Language flag module (Falang)
						    $ret .= "<div class= 'languageFlag'>";
						    	$ret .= "Change language";

					    			if($parameters['language_show'] == 1){
										$document = JFactory::getDocument();
									 	$renderer = $document->loadRenderer('module');
									 	$isModuleEnabled = JModuleHelper::isEnabled('mod_falang');

									 	if ($isModuleEnabled){
										 	$Module = JModuleHelper::getModule('mod_falang');
										 	$ret .= $renderer->render($Module);
									 	}
								 	}
						 	$ret .= "</div>";


			       $ret .=" </nav>";

	    		$ret .="</header>";

    		return $ret;
	}

	// **********************************************************************

    /**
     *  Prepare code to render for Menu BAR
     *  @param $params 		  array with collected parameters
     *  @return string $ret    code to render
    **/

    function prepareCodeToRenderBar($parameters){

    	// Load CSS and JS
			$this->loadCssandJsBar();

    	// Get if we are in HP
			$weAreInHomepage = $this->checkIfHP();

    	// Menu & brand class and style
			$menuStyle = $this->getMainMenuStyleClasses($parameters, $weAreInHomepage);

		// Sef language tag
    		$lang = JFactory::getLanguage();
			$sef_lang = array_shift(explode('-', $lang->getTag()));

		// Get Active Item ID
			$app = JFactory::getApplication();
			$active_menu_item_id = $app->getMenu()->getActive()->id;

		// Get the access levels the user is authorized to view - Every user (1- Pubic), Unlogged User (5 - Guest), Logged user (2 - Registered)
			$user = JFactory::getUser();
			$authorisedViewLevels = $user->getAuthorisedViewLevels();

    	// Prepare code to render
	    	$ret = "<div class='lifeHeaderMenu'>";
	    		$ret .= "<div class='marginForFixedMenu'></div>";

		    	$ret .= "<div style='".$menuStyle['style'].";' class='navbar navbar-default ".$menuStyle['class']."' role='navigation'>";
			    	
		    		// Language flag module (Falang)
		    			if($parameters['language_show'] == 1){
							$document = JFactory::getDocument();
						 	$renderer = $document->loadRenderer('module');
						 	$isModuleEnabled = JModuleHelper::isEnabled('mod_falang');

						 	if ($isModuleEnabled){
							 	$Module = JModuleHelper::getModule('mod_falang');
							 	$ret .= $renderer->render($Module);
						 	}
					 	}

		    		// Hamburger menu
		    			$ret .= "<div class='navbar-header'>";
	                        $ret .= "<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>";
	                          $ret .= "<span class='sr-only'>Toggle navigation</span>";
	                          $ret .= "<span class='icon-bar'></span>";
	                          $ret .= "<span class='icon-bar'></span>";
	                          $ret .= "<span class='icon-bar'></span>";
	                        $ret .= "</button>";

	                        // Brand
		                        if ($parameters['brand']['image']!= null){
		                        	$homepageLink = JURI::base();
		                        	$ret .="<a class='navbar-brand' href='".$homepageLink."'><img style='".$menuStyle['brand_style']."' src='".$parameters['brand']['image']."'/></a>";
		                        }

	                      $ret .= "</div>";

	                // Desktop menu
	                    $ret .= "<div class='navbar-collapse collapse'>";
	                    	
	                    	// For each menu
		                    	foreach ($parameters['menu'] as $key => $menu) {
		                    		
		                    		// Check if menu aligned right
			                    		$menuAlignedRightClass = ($menu['alignment'] == "right") ? "navbar-right" : "";

			                    	// Menu code
			                    		$ret .= "<ul class='nav navbar-nav ".$menuAlignedRightClass."'>";
				                    		foreach ($menu['items'][1] as $key => $item) {
				                    			$hasSubMenu = (array_key_exists($key,$menu['items'])) ? 1 : 0;		
												$itemIsActiveClass = ($item->id == $active_menu_item_id) ? " active" : "";
												$link = JRoute::_($item->link."&Itemid=".$item->id."&lang=".$sef_lang);
			                    				
			                    				if (in_array($item->accesslevel, $authorisedViewLevels)){
					                    			$ret .= "<li class='item-".$item->id.$itemIsActiveClass."'>";
					                    				
					                    				$ret .= "<a href='".$link."' style='".$menuStyle['item_link_style']."' class='".$linkClass."'>";
						                    				$ret .= $item->title;
						                    				// Add arrow down to items that have submenus
							                    				if ($hasSubMenu)
							                    					$ret .= "<span class='caret'></span>";
					                    				$ret .= "</a>";

				                    				// Sub menu
					                    				if ($hasSubMenu){	
					                    					$ret .= "<ul class='dropdown-menu'>";

					                    						foreach ($menu['items'][$key] as $sub_key => $sub_item) {
					                    							
				                    								$hasSubMenu = (array_key_exists($sub_key,$menu['items'])) ? 1 : 0;
							                    					$itemIsActiveClass = ($sub_item->id == $active_menu_item_id) ? " active" : "";
							                    					$link = JRoute::_($sub_item->link."&Itemid=".$sub_item->id."&lang=".$sef_lang);

							                    					if (in_array($sub_item->accesslevel, $authorisedViewLevels)){
						                    							$ret .= "<li class='item-".$sub_item->id.$itemIsActiveClass."'>";
										                    				$ret .= "<a href='".$link."' style='".$menuItemLink_style."' class='".$linkClass."'>";
										                    				$ret .= $sub_item->title;
										                    					// Add arrow down to items that have submenus
												                    				if ($hasSubMenu)
							                    										$ret .= "<span class='caret'></span>";
										                    				$ret .= "</a>";

										                    				// Sub sub menu
											                    				if ($hasSubMenu){	
						                    										$ret .= "<ul class='dropdown-menu'>";
						                    											foreach ($menu['items'][$sub_key] as $sub_sub_key => $sub_sub_item) {
						                    												$link = JRoute::_($sub_sub_item->link."&Itemid=".$sub_sub_item->id."&lang=".$sef_lang);

						                    												if (in_array($sub_sub_item->accesslevel, $authorisedViewLevels)){
							                    												$ret .= "<li class='item-".$sub_sub_item->id.$subItemClass."'>";
							                    													$ret .= "<a href='".$link."' style='".$menuItemLink_style."' class='".$linkClass."'>";
																                    					$ret .= $sub_sub_item->title;
																                    				$ret .= "</a>";
							                    												$ret .= "</li>";
						                    												}
						                    											}
						                    										$ret .= "</ul>"; /* END sub-sub-menu */
											                    				}
										                    			$ret .= "</li>";
					                    							}
					                    						}
					                    					$ret .= "</ul>"; /* END sub-menu */
					                    				}
					                    			$ret .= "</li>";
					                    		}
				                    		}
			                    		$ret .= "</ul>"; /* END menu */
		                    	}
	                    $ret .= "</div>"; /*end Desktop menu (navbar-collapse)*/
		    	$ret .= "</div>";


		    	// Hover menu colors //ccccccc
			    	//$ret .= "<div class='hoverActiveColors' style='color:green; background-color:yellow;'></div>";
		    		$ret .= "<div class='hoverActiveColors' style='".$menuStyle['item_link_hover_style']."'></div>";


		    $ret .= "</div>";
		    
    	return $ret;
    }

    
}

?>