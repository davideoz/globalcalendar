/**
 * @package Extly.Library
 * @subpackage lib_extly - Extly Framework
 *
 * @author Prieco S.A. <support@extly.com>
 * @copyright Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link http://www.extly.com http://support.extly.com
 */

/*jslint plusplus: true, browser: true, sloppy: true */
/*global jQuery, Request, Joomla, alert, Backbone */

define('xtcronjob-expression-field', [], function( ) {

	var ExpressionView = Backbone.View.extend({
	    events: {
	        'change .minute-part': 'onChangeMhdmd',
	        'change .hour-part': 'onChangeMhdmd',
	        'change .day-part': 'onChangeMhdmd',
	        'change .month-part': 'onChangeMhdmd',
	        'change .weekday-part': 'onChangeMhdmd',
	    },

	    onChangeMhdmd: function() {
	        var minute2 = this.$('.minute-part').val();
	        var hour2 = this.$('.hour-part').val();
	        var day2 = this.$('.day-part').val();
	        var month2 = this.$('.month-part').val();
	        var weekday2 = this.$('.weekday-part').val();

	        var mhdmd = minute2 + " " + hour2 + " " + day2 + " " + month2 + " " + weekday2;
	        this.$('.unix_mhdmd-part').val(mhdmd);
	    }
	});

	// Initialize the application view
	var forms = jQuery('.cronjob-expression-form');
	_.each(forms, function(form)
		{
			var expressionView = new ExpressionView({el : form});
			window.expressionView = expressionView;
		});

});
