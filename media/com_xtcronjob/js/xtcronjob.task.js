/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 *
 * @author      Prieco S.A.
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */

/*jslint plusplus: true, browser: true, sloppy: true */
/*global jQuery, Request, Joomla, alert, Backbone */

define('xtcronjob.task', [], function( ) {

	var TaskView = Backbone.View.extend({
	    events: {
	        'change #type': 'onChangeType'
	    },

	    onChangeType : function onChangeType() {
			var typeId = this.$('#type').val();

			// TASKS_TYPE_CURL_AKEEBA
			if (typeId === '8') {
				this.$('#file').val('index.php?option=com_akeeba&view=backup&key=SECRETKEY&profile=PROFILE');
			}
		}

	});

	// Initialize the application view
	var taskView = new TaskView({ el:jQuery("#adminForm") });

});