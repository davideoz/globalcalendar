

window.onload = function() {
	if( jQuery("footer.megaFooter").length ){

		// Fix the problem of the mega footer detached from the bottom of the screen when the page contents are too few 
			var footerDistanceFromTop = jQuery("footer.megaFooter").offset().top;
			var footerHeight = jQuery("footer.megaFooter").height();
			var documentHeight = jQuery(document).height();
			gapFromBottom = documentHeight - footerDistanceFromTop - footerHeight;
			
			console.log("documentHeight: " + documentHeight);
			console.log("footerDistanceFromTop: " + footerDistanceFromTop);
			console.log("footerHeight: " + footerHeight);
			console.log("Gap from bottom: " + gapFromBottom);

			if ((gapFromBottom) > 0){
				jQuery("footer.megaFooter").css({"margin-top": gapFromBottom});
			}
	}
};

jQuery(document).ready(function () {

  	if( jQuery("footer.megaFooter").length ){

	    // Assign inheritaded color to article <strong> and <a>
		    if (jQuery(".menu h4").length) {
		        var titleColor = jQuery(".menu h4").css('color');
		        jQuery(".article strong").css("color",titleColor);
		        jQuery(".article a").css("color",titleColor);
		    }
		    if (jQuery(".newsletter").length) {
		        var titleColor = jQuery(".newsletter b").css('color');
		        jQuery(".article strong").css("color",titleColor);
		        jQuery(".article a").css("color",titleColor);
		    }
	}

});


