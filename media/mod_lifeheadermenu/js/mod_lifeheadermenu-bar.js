
// When scroll down add class sticky-header to the menu top bar
	jQuery(window).scroll(function(event){            
	    fromTop = (jQuery(window).scrollTop() );
	    //console.log(fromTop);

	    headerOffset = 0;

	    if (fromTop > headerOffset) {
	    	jQuery('.navbar-default').addClass('transparent-bar');
	    }
	    else{
	    	jQuery('.navbar-default').removeClass('transparent-bar');
	    }

	});

// Add responsive device class to BODY
	function deviceClass(){

		var browserWidth = jQuery(window).width();
		//console.log(browserWidth);
		switch (true) {
		    case (browserWidth < 767):
		        jQuery('body').removeClass();
		        jQuery('body').addClass('device-xs');
		        break;
		    case (browserWidth < 991):
		        jQuery('body').removeClass();
		        jQuery('body').addClass('device-sm');
		        break;
		    case (browserWidth < 1199):
		        jQuery('body').removeClass();
		        jQuery('body').addClass('device-md');
		        break;
		    case (browserWidth > 1200):
		        jQuery('body').removeClass();
		        jQuery('body').addClass('device-lg');
		        break;
		    default:
		        //alert("none");
		        break;
		}
	}

	// Call the function every window resize 
		jQuery(window).resize(function() {
			deviceClass();
		});




jQuery(document).ready(function () {
	
	// Add device class to body
		deviceClass();
		
	  	
});

