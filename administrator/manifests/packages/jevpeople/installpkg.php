<?php

/**
 * JEvents Component for Joomla 2.5.x
 *
 * @version     $Id: scriptfile.php 1430 2010-11-11 14:15:05Z royceharding $
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2012 GWE Systems Ltd, 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

class pkg_jevpeopleInstallerScript
{
	public function preflight($type, $parent) {
		// Joomla! broke the update call, so we have to create a workaround check.
		$db = JFactory::getDbo ();
		$db->setQuery ( "SELECT enabled FROM #__extensions WHERE element = 'com_jevents' and type='component'" );
		$is_enabled = $db->loadResult ();
	
		if ($is_enabled == 1){
			$manifest  =  JPATH_SITE . "/administrator/components/com_jevents/manifest.xml";
			if (! $manifestdata = $this->getValidManifestFile ( $manifest )) {
				Jerror::raiseWarning ( null, 'JEvents Must be installed first to use this addon.');
				return false;
			}

			$app = new stdClass ();
			$app->name = $manifestdata ["name"];
			$app->version = $manifestdata ["version"];

			if (version_compare( $app->version , '3.1.14', "lt")) {
				Jerror::raiseWarning ( null, 'A minimum of JEvents V3.1.14 is required for this addon. <br/>Please update JEvents first.' . $rel );
				return false;
			} else {
				$this->hasJEventsInst = 1;
				return;
			}
		} else {
                        $this->hasJEventsInst = 0;
                        if ($is_enabled == 0) {
                            Jerror::raiseWarning ( null, 'JEvents has been disabled, please enable it first.' . $rel );
                            return false;
                        } elseif(!$is_enabled) {
                            Jerror::raiseWarning ( null, 'This Addon Requires JEvents core to be installed.<br/>Please first install JEvents' . $rel );
                            return false;                        
                        }
		}
	}
 

	// TODO enable plugins
	public function update()
	{

		return true;
	}

	public function install($adapter)
	{
		return true;
	}


	public function uninstall($adapter)
	{
		
	}
	
	/*
	 * enable the plugins
	 */
	function postflight($type, $parent) 
	{
		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)

		if ($type == 'install') {	
			// enable plugin
			$db = JFactory::getDbo();
			$query = "UPDATE #__extensions SET enabled=1 WHERE folder='jevents' and type='plugin' and element='jevpeople'";
			$db->setQuery($query);
			$db->query();
		}
	}	
	// Manifest validation
	function getValidManifestFile($manifest)
	{
		$filecontent = JFile::read($manifest);
		if (stripos($filecontent, "jevents.net") === false && stripos($filecontent, "gwesystems.com") === false && stripos($filecontent, "joomlacontenteditor") === false && stripos($filecontent, "virtuemart") === false && stripos($filecontent, "sh404sef") === false)
		{
			return false;
		}
		// for JCE and Virtuemart only check component version number
		if (stripos($filecontent, "joomlacontenteditor") !== false || stripos($filecontent, "virtuemart") !== false || stripos($filecontent, "sh404sef") !== false || strpos($filecontent, "JCE") !== false)
		{
			if (strpos($filecontent, "type='component'") === false && strpos($filecontent, 'type="component"') === false)
			{
				return false;
			}
		}
	
		$manifestdata = JApplicationHelper::parseXMLInstallFile($manifest);
		if (!$manifestdata)
			return false;
		if (strpos($manifestdata["authorUrl"], "jevents") === false && strpos($manifestdata["authorUrl"], "gwesystems") === false && strpos($manifestdata["authorUrl"], "joomlacontenteditor") === false && strpos($manifestdata["authorUrl"], "virtuemart") === false && strpos($manifestdata['name'], "sh404SEF") === false)
		{
			return false;
		}
		return $manifestdata;
	
	}

}
