<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

defined('_JEXEC') or die;

class FAQBookProViewDashboard extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	public function display($tpl = null)
	{
		$utilities = new FAQBookProHelperUtilities();
		
		$this->navbar = $utilities->getNavbarHTML();
		$this->sidebar = $utilities->getSideMenuHTML();
				
		$this->addTitle();
						
		parent::display($tpl);
	}
	
	protected function addTitle()
	{
		JToolbarHelper::title(JText::_('COM_FAQBOOKPRO_HOME'), '');
	}
}
