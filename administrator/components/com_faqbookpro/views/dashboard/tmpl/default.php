<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

defined('_JEXEC') or die;

$token = JSession::getFormToken();
?>

<div id="mn-cpanel"><!-- Main Container -->
	
	<?php echo $this->navbar; ?>	
	
	<div id="mn-main-container" class="main-container container-fluid">
	
		<a id="menu-toggler" class="menu-toggler" href="#">
			<span class="menu-text"></span>
		</a>

		<div id="mn-sidebar" class="sidebar">
		
			<?php echo $this->sidebar; ?>
		
		</div>
		
		<div class="main-content">
			
			<div class="page-header clearfix"> </div>
			
			<div class="page-content mn-dashboard">
    	
				<div class="row-fluid">
					
					<div class="thumbnail">
						<a href="<?php echo JRoute::_('index.php?option=com_faqbookpro&view=sections'); ?>">
							<i class="fa fa-list-ul"></i>
							<span class="thumbnail-title">
								<?php echo JText::_('COM_FAQBOOKPRO_SECTIONS'); ?>
							</span>
						</a>
					</div>
					
					<div class="thumbnail">
						<a href="<?php echo JRoute::_('index.php?option=com_faqbookpro&view=topics'); ?>">
							<i class="fa fa-folder-open"></i>
							<span class="thumbnail-title">
								<?php echo JText::_('COM_FAQBOOKPRO_TOPICS'); ?>
							</span>
						</a>
					</div>
										
					<div class="thumbnail">
						<a href="<?php echo JRoute::_('index.php?option=com_faqbookpro&view=questions'); ?>">
							<i class="fa fa-question-circle"></i>
							<span class="thumbnail-title">
								<?php echo JText::_('COM_FAQBOOKPRO_QUESTIONS'); ?>
							</span>
						</a>
					</div>					
					
					<div class="thumbnail">
						<a href="http://www.minitek.gr/support/documentation/joomla-extensions/components/minitek-faq-book" target="_blank">
							<i class="fa fa-book"></i>
							<span class="thumbnail-title">
								<?php echo JText::_('COM_FAQBOOKPRO_DOCUMENTATION'); ?>
							</span>
						</a>
					</div>
					
					<div class="thumbnail">
						<a href="<?php echo JRoute::_('index.php?option=com_faqbookpro&view=about'); ?>">
							<i class="fa fa-info-circle"></i>
							<span class="thumbnail-title">
								<?php echo JText::_('COM_FAQBOOKPRO_ABOUT'); ?>
							</span>
						</a>
					</div>
					
					<div class="thumbnail">
						<a href="<?php echo JRoute::_('index.php?option=com_config&view=component&component=com_faqbookpro&path=&return='.base64_encode(JURI::getInstance()->toString())); ?>">
							<i class="fa fa-gear"></i>
							<span class="thumbnail-title">
								<?php echo JText::_('COM_FAQBOOKPRO_CONFIGURATION'); ?>
							</span>
						</a>
					</div>
					
					<div class="thumbnail">
						<a href="https://extensions.joomla.org/extension/minitek-faq-book/" target="_blank" style="position: relative;">
							<i class="fa fa-star" style="color: #ffcb52;"></i>
							<span class="thumbnail-title">
								<?php echo JText::_('COM_FAQBOOKPRO_DASHBOARD_LIKE_THIS_EXTENSION'); ?>
							</span>
                          	<span class="small" style="position: absolute;bottom: 5px;width: 100%;left: 0;opacity: 0.9;">
								<?php echo JText::_('COM_FAQBOOKPRO_DASHBOARD_LEAVE_A_REVIEW_ON_JED'); ?>
							</span>
						</a>
					</div>	
					
				</div>
								
			</div>
			
		</div>
		
	</div>
	
</div>