<?php
/**
 * @version    CVS: 1.0.7
 * @package    Com_Countries
 * @author     Davide Casiraghi <davide.casiraghi@gmail.com>
 * @copyright  2017 Davide Casiraghi
 * @license    GNU General Public License versione 2 o successiva; vedi LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Country controller class.
 *
 * @since  1.6
 */
class CountriesControllerCountry extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'countries';
		parent::__construct();
	}
}
