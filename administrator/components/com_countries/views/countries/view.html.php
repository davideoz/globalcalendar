<?php

/**
 * @version    CVS: 1.0.7
 * @package    Com_Countries
 * @author     Davide Casiraghi <davide.casiraghi@gmail.com>
 * @copyright  2017 Davide Casiraghi
 * @license    GNU General Public License versione 2 o successiva; vedi LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Countries.
 *
 * @since  1.6
 */
class CountriesViewCountries extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		CountriesHelpersCountries::addSubmenu('countries');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = CountriesHelpersCountries::getActions();

		JToolBarHelper::title(JText::_('COM_COUNTRIES_TITLE_COUNTRIES'), 'countries.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/country';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('country.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('countries.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('country.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('countries.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('countries.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'countries.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('countries.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('countries.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'countries.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('countries.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_countries');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_countries&view=countries');

		$this->extra_sidebar = '';
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);
		//Filter for the field continent
		$select_label = JText::sprintf('COM_COUNTRIES_FILTER_SELECT_LABEL', 'Continent');
		$options = array();
		$options[0] = new stdClass();
		$options[0]->value = "Europe";
		$options[0]->text = "Europe";
		$options[1] = new stdClass();
		$options[1]->value = "North America";
		$options[1]->text = "North America";
		$options[2] = new stdClass();
		$options[2]->value = "South America";
		$options[2]->text = "South America";
		$options[3] = new stdClass();
		$options[3]->value = "Asia";
		$options[3]->text = "Asia";
		$options[4] = new stdClass();
		$options[4]->value = "Africa";
		$options[4]->text = "Africa";
		$options[5] = new stdClass();
		$options[5]->value = "Oceania";
		$options[5]->text = "Oceania";
		JHtmlSidebar::addFilter(
			$select_label,
			'filter_continent',
			JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.continent'), true)
		);

	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
			'a.`country`' => JText::_('COM_COUNTRIES_COUNTRIES_COUNTRY'),
			'a.`countrycode`' => JText::_('COM_COUNTRIES_COUNTRIES_COUNTRYCODE'),
			'a.`continent`' => JText::_('COM_COUNTRIES_COUNTRIES_CONTINENT'),
		);
	}
}
