<?php

defined('_JEXEC') or die('Restricted access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Component
 * @author              Steven Palmer
 * @author url          https://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /assets/en-GB.license.txt
 * @copyright           Copyright (c) 2017 Steven Palmer All rights reserved.
 *
 *
 * CoalaWeb Members is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

JLoader::import('helpers.coalawebtraffic', JPATH_COMPONENT_ADMINISTRATOR);

JFormHelper::loadFieldClass('list');

class JFormFieldCountries extends JFormFieldList
{
	public $type = 'Countries';

	protected function getOptions()
	{
        $json = file_get_contents(JPATH_COMPONENT . '/assets/countries/country-code.json');

        //Decode it reading to used
        $obj = json_decode($json, true);
		
		$options = array();


        foreach ($obj  as $key => $value) {
            $option = new stdClass();
            $option->value = strtolower($value['Code']);

            $option->text = $value['Name'];
            $options[] = $option;
        }
		
		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);


		return $options;
	}

}
