<?php

/**
 * @package             Joomla
 * @subpackage          com_coalawebtraffic
 * @author              Steven Palmer
 * @author url          https://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /assets/en-GB.license.txt
 * @copyright           Copyright (c) 2017 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/gpl.html>.
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('joomla.installer.helper');

/**
 * Methods supporting a control panel
 *
 * @package    Joomla.Administrator
 * @subpackage com_coalawebtraffic
 */
class CoalawebtrafficModelGeoupload extends JModelLegacy {

    /**
     * Refresh GEO data
     *
     * @return boolean
     *
     * @since 1.1.0
     */
    public function geoRefresh()
    {
        $result = true;

        //Start our database queries
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        // Fields set to NULL
        $fields = array(
            $db->qn('country_code') . ' = NULL',
            $db->qn('country_name') . ' = NULL',
            $db->qn('city') . ' = NULL',
            $db->qn('location_latitude') . ' = NULL',
            $db->qn('location_longitude') . ' = NULL',
            $db->qn('location_time_zone') . ' = NULL',
            $db->qn('continent_code') . ' = NULL'
        );

        $query->update($db->qn('#__cwtraffic'))->set($fields);

        $db->setQuery($query);

        try {
            $db->execute();
        } catch (Exception $e) {
            $result = false;
            return $result;
        }

        return $result;
    }

}