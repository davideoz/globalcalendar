<?php

/**
 * @package             Joomla
 * @subpackage          com_coalawebtraffic
 * @author              Steven Palmer
 * @author url          https://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /assets/en-GB.license.txt
 * @copyright           Copyright (c) 2017 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/gpl.html>.
 */
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

JTable::addIncludePath(JPATH_COMPONENT . '/tables');

jimport('joomla.installer.helper');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

/**
 * Methods supporting a control panel
 *
 * @package    Joomla.Administrator
 * @subpackage com_coalawebtraffic
 */
class CoalawebtrafficModelManage extends JModelLegacy {
    
    /**
     * Delete (Purge) all the Traffic data from its associated tables and reset index
     * 
     * @return boolean
     */
    public function purge() {
        $deleted = 0;
        $result = true;

        $tables = array(
            '#__cwtraffic',
            '#__cwtraffic_total',
            '#__cwtraffic_whoisonline',
            '#__cwtraffic_locations'
        );

        $db = $this->getDBO();
        
        while (count($tables)) {
            $table = array_shift($tables);

                // The table needs repair
                $db->setQuery('TRUNCATE TABLE ' . $db->qn($table));
                try {
                    $db->execute();
                    $deleted++;
                } catch (Exception $exc) {
                    $result = false;
                }
        }

        return $result;
    }

    /**
     * Delete (Purge) all the CoalaWeb Traffic log files
     *
     * @return boolean
     */
    public function purgeLogs()
    {
        $deleted = 0;
        $test = true;

        $logNames = array(
            'com_coalawebtraffic_sql.log.php',
            'com_coalawebtraffic_debug.log.php',
            'mod_cwtrafficstats.log.php',
            'plg_cwtraffic_count.log.php'
        );

        $path = JFactory::getConfig()->get('log_path');

        if (JFolder::exists($path)) {
            $archiveFiles = JFolder::files($path);

            foreach ($archiveFiles as $archive) {
                if (in_array($archive, $logNames)) {
                    try {
                        JFile::delete($path . '/' . $archive);
                        $deleted++;
                    } catch (Exception $exc) {
                        $test = false;
                    }
                }
            }
        } else {
            $test = false;
        }

        $affected = [
            'test' => $test,
            'deleted' => $deleted
        ];

        return $affected;
    }
}