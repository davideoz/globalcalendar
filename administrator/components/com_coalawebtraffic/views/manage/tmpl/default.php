<?php
defined('_JEXEC') or die('Restricted Access');
/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic Component
 * @author              Steven Palmer
 * @author url          https://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /assets/en-GB.license.txt
 * @copyright           Copyright (c) 2017 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/gpl.html>.
 */
JHtml::_('jquery.framework');
JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

$doc= JFactory::getDocument();
$doc->addScript(JURI::root(true) . '/media/coalawebtraffic/components/traffic/js/sweetalert.min.js');
$doc->addStyleSheet(JURI::root(true) . '/media/coalawebtraffic/components/traffic/css/sweetalert.css')
        
?>

<?php if (!empty($this->sidebar)) : ?>
    <!-- sidebar -->
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <!-- end sidebar -->
    <div id="j-main-container" class="span10">
<?php else : ?>
    <div id="j-main-container">
<?php endif; ?>

<div id="cpanel-v2" class="span8 well">
    <div class="row-fluid">
    <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'tools')); ?>
        
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'tools', JText::_('COM_CWTRAFFIC_TITLE_TOOLS', true)); ?>
        
            <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
                <div class="icon">
                    <a class="red-dark purge-traffic" href="<?php echo JRoute::_('index.php?option=com_coalawebtraffic&task=manage.purge&'. JSession::getFormToken() .'=1' ); ?>">
                        <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_PURGE'); ?>" src="<?php echo JURI::root() ?>/media/coalaweb/components/generic/images/icons/icon-48-cw-trash-v2.png" />
                        <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_VISITORS'); ?></span>
                    </a>
                </div>
            </div>

        <?php if ($this->isPro): ?>
            <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
                <div class="icon">
                    <a class="red-dark purge-locations" href="<?php echo JRoute::_('index.php?option=com_coalawebtraffic&task=manage.purgeLocations&'. JSession::getFormToken() .'=1' ); ?>">
                        <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_PURGE_LOCATIONS'); ?>" src="<?php echo JURI::root() ?>/media/coalaweb/components/generic/images/icons/icon-48-cw-trash-v2.png" />
                        <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_LOCATIONS'); ?></span>
                    </a>
                </div>
            </div>
        <?php endif; ?>

        <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
            <div class="icon">
                <a class="red-dark purge-logs" href="<?php echo JRoute::_('index.php?option=com_coalawebtraffic&task=manage.purgeLogs&'. JSession::getFormToken() .'=1' ); ?>">
                    <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_PURGE_LOGS'); ?>" src="<?php echo JURI::root() ?>/media/coalaweb/components/generic/images/icons/icon-48-cw-trash-v2.png" />
                    <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_LOGS'); ?></span>
                </a>
            </div>
        </div>

        <?php if ($this->isPro && !$this->initial): ?>
            <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
                <div class="icon">
                    <a class="green-dark sync-locations" href="<?php echo JRoute::_('index.php?option=com_coalawebtraffic&task=manage.syncLocations&'. JSession::getFormToken() .'=1' ); ?>">
                        <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_SYNC_LOCATIONS'); ?>" src="<?php echo JURI::root() ?>/media/coalaweb/components/generic/images/icons/icon-48-cw-geo-v2.png" />
                        <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_SYNC_LOCATIONS'); ?></span>
                    </a>
                </div>
            </div>
        <?php endif; ?>

           <?php if ($this->isPro): ?>
                <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
                    <div class="icon">
                        <a class="orange-dark optimize" href="<?php echo JRoute::_('index.php?option=com_coalawebtraffic&task=manage.optimize&'. JSession::getFormToken() .'=1' ); ?>">
                            <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_OPTIMIZE'); ?>" src="<?php echo JURI::root() ?>/media/coalaweb/components/generic/images/icons/icon-48-cw-speed-v2.png" />
                            <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_OPTIMIZE'); ?></span>
                        </a>
                    </div>
                </div>
            <?php endif; ?>

           <?php if ($this->isPro): ?>
                <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
                    <div class="icon">
                        <a class="aqua-dark addbots" href="<?php echo JRoute::_('index.php?option=com_coalawebtraffic&task=manage.addRobots&'. JSession::getFormToken() .'=1' ); ?>">
                            <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_BOTS'); ?>" src="<?php echo JURI::root() ?>/media/coalaweb/components/generic/images/icons/icon-48-cw-upload-v2.png" />
                            <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_BOTS'); ?></span>
                        </a>
                    </div>
                </div>
            <?php endif; ?>

        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'reports', JText::_('COM_CWTRAFFIC_TITLE_REPORTS', true)); ?>
        
            <div style="float:<?php echo ($lang->isRTL()) ? 'right' : 'left'; ?>;">
                <div class="icon">
                    <a class="aqua-light" href="<?php echo JRoute::_('index.php?option=com_coalawebtraffic&task=visitors.csvreportall&'. JSession::getFormToken() .'=1' ); ?>">
                        <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_REPORT'); ?>" src="<?php echo JURI::root() ?>/media/coalaweb/components/generic/images/icons/icon-48-cw-download-v2.png" />
                        <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_REPORT'); ?></span>
                    </a>
                </div>
            </div>

        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        <?php if ($this->isPro): ?>
            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'backup', JText::_('COM_CWTRAFFIC_TITLE_BACKUP', true)); ?>
                <fieldset >
                    <legend ><?php echo JText::_('COM_CWTRAFFIC_TITLE_DB') ?></legend>
                    <form name="upload" method="post" enctype="multipart/form-data">
                        <div class="alert alert-info">
                            <span class="icon-info-circle"></span>
                            <?php echo JText::_('COM_CWTRAFFIC_DB_BACKUP_MESSAGE'); ?>
                        </div>

                        <button type="submit" class="btn btn-info">
                            <span class="icon icon-download"></span>
                            <?php echo JText::_('COM_CWTRAFFIC_BACKUP_DB_BTN') ?>
                        </button>
                        <input type="hidden" name="option" value="com_coalawebtraffic" />
                        <input type="hidden" name="view" value="manage" />
                        <input type="hidden" name="task" value="manage.doBackup" />
                        <?php echo JHtml::_( 'form.token' ); ?>

                    </form>
                </fieldset>
            <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php endif; ?>
       
        <?php if ($this->isPro): ?>
            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'restore', JText::_('COM_CWTRAFFIC_TITLE_RESTORE', true)); ?>
                <fieldset >
                    <legend ><?php echo JText::_('COM_CWTRAFFIC_TITLE_DB') ?></legend>
                    <form id="upload" name="upload" method="post" enctype="multipart/form-data">

                        <div class="alert alert-info">
                            <span class="icon-info-circle"></span>
                            <?php echo JText::_('COM_CWTRAFFIC_DB_RESTORE_MESSAGE'); ?>
                        </div>

                        <div class="alert alert-danger">
                            <span class="icon-notification-circle"></span>
                            <?php echo JText::_('COM_CWTRAFFIC_DB_RESTORE_WARNING'); ?>
                        </div>

                        <input style="height:30px" type="file" id="backup-file" name="file_upload" />
                        <button type="submit" class="btn btn-info upload">
                            <span class="icon icon-upload"></span>
                            <?php echo JText::_('COM_CWTRAFFIC_RESTORE_DB_BTN') ?>
                        </button>
                        <input type="hidden" name="option" value="com_coalawebtraffic" />
                        <input type="hidden" name="view" value="manage" />
                        <input type="hidden" name="task" value="manage.restoreBackup" />
                        <?php echo JHtml::_( 'form.token' ); ?>

                    </form>
                </fieldset>
            <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php endif; ?>
        
    <?php echo JHtml::_('bootstrap.endTabSet'); ?>
    </div>
</div>
<div id="tabs" class="span4">
    <div class="row-fluid">

    <?php
    $options = array(
        'onActive' => 'function(title, description){
        description.setStyle("display", "block");
        title.addClass("open").removeClass("closed");
    }',
        'onBackground' => 'function(title, description){
        description.setStyle("display", "none");
        title.addClass("closed").removeClass("open");
    }',
        'startOffset' => 0, // 0 starts on the first tab, 1 starts the second, etc...
        'useCookie' => true, // this must not be a string. Don't use quotes.
        'startTransition' => 1,
    );
    ?>

    <?php echo JHtml::_('sliders.start', 'slider_group_id', $options); ?>

    <?php echo JHtml::_('sliders.panel', JText::_('COM_CWTRAFFIC_SLIDER_TITLE_SUPPORT'), 'slider_1_id'); ?>
    <div class="well well-large">
        <?php echo JText::_('COM_CWTRAFFIC_SUPPORT_DESCRIPTION'); ?>
    </div>
        
    <?php if (!$this->isPro): ?>
        <?php echo JHtml::_('sliders.panel', JText::_('COM_CWTRAFFIC_SLIDER_TITLE_UPGRADE'), 'slider_2_id'); ?>
         <div class="well well-large">
             <div class="alert alert-danger">
                 <span class="icon-power-cord"></span> <?php echo JText::_('COM_CWTRAFFIC_MSG_UPGRADE'); ?>
             </div>
        </div>
    <?php endif; ?>
        
    <?php echo JHtml::_('sliders.end'); ?>
</div>
</div>
</div>
<script>
  jQuery.noConflict();
  
  jQuery('a.purge-traffic').on('click',function(e){
    e.preventDefault(); // Prevent the href from redirecting directly
    var linkURL = jQuery(this).attr("href");
    warnBeforePurge(linkURL);
  });

  jQuery('a.purge-locations').on('click',function(e){
      e.preventDefault(); // Prevent the href from redirecting directly
      var linkURL = jQuery(this).attr("href");
      warnBeforePurgeLocations(linkURL);
  });

  jQuery('a.purge-logs').on('click',function(e){
      e.preventDefault(); // Prevent the href from redirecting directly
      var linkURL = jQuery(this).attr("href");
      warnBeforePurgeLogs(linkURL);
  });
  
  jQuery('a.optimize').on('click',function(e){
    e.preventDefault(); // Prevent the href from redirecting directly
    var linkURL = jQuery(this).attr("href");
    warnBeforeOptimize(linkURL);
  });

  jQuery('a.sync-locations').on('click',function(e){
      e.preventDefault(); // Prevent the href from redirecting directly
      var linkURL = jQuery(this).attr("href");
      warnBeforeSyncLocations(linkURL);
  });
  
  jQuery('a.addbots').on('click',function(e){
    e.preventDefault(); // Prevent the href from redirecting directly
    var linkURL = jQuery(this).attr("href");
    warnBeforeAddbots(linkURL);
  });
  
  jQuery('.upload').on('click',function(e){
    e.preventDefault(); // Prevent form from being submitted
    var form = jQuery(this).parents('form');
    if (jQuery('#backup-file').val() == '') {
        warnEmptyRestore();
    } else {
        warnBeforeRestore(form);
    }
    
  });


    function warnEmptyRestore() {
        swal({
            title: "<?php echo JText::_('COM_CWTRAFFIC_FILE_MISSING_POPUP_TITLE'); ?>",
            text: "<?php echo JText::_('COM_CWTRAFFIC_FILE_MISSING_POPUP_MSG'); ?>",
            type: "warning",
        });
    }

    function warnBeforeRestore(form) {
        swal({
            title: "<?php echo JText::_('COM_CWTRAFFIC_RESTORE_POPUP_TITLE'); ?>",
            text: "<?php echo JText::_('COM_CWTRAFFIC_RESTORE_POPUP_MSG'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "<?php echo JText::_('COM_CWTRAFFIC_RESTORE_POPUP_BUTTON'); ?>",
            closeOnConfirm: false
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    }

    function warnBeforePurge(linkURL) {
        swal({
          title: "<?php echo JText::_('COM_CWTRAFFIC_PURGE_POPUP_TITLE'); ?>",
          text: "<?php echo JText::_('COM_CWTRAFFIC_PURGE_POPUP_MSG'); ?>",
          type: "warning",
          html: true,
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
        }, function() {
          // Redirect the user
          window.location.href = linkURL;
        });
    }

    function warnBeforePurgeLocations(linkURL) {
      swal({
          title: "<?php echo JText::_('COM_CWTRAFFIC_PURGE_LOCATIONS_POPUP_TITLE'); ?>",
          text: "<?php echo JText::_('COM_CWTRAFFIC_PURGE_LOCATIONS_POPUP_MSG'); ?>",
          type: "warning",
          html: true,
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
      }, function() {
          // Redirect the user
          window.location.href = linkURL;
      });
    }

  function warnBeforePurgeLogs(linkURL) {
      swal({
          title: "<?php echo JText::_('COM_CWTRAFFIC_PURGE_LOGS_POPUP_TITLE'); ?>",
          text: "<?php echo JText::_('COM_CWTRAFFIC_PURGE_LOGS_POPUP_MSG'); ?>",
          type: "warning",
          html: true,
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
      }, function() {
          // Redirect the user
          window.location.href = linkURL;
      });
  }

    function warnBeforeOptimize(linkURL) {
    swal({
      title: "<?php echo JText::_('COM_CWTRAFFIC_OPTIMIZE_POPUP_TITLE'); ?>",
      text: "<?php echo JText::_('COM_CWTRAFFIC_OPTIMIZE_POPUP_MSG'); ?>",
      type: "warning",
      html: true,
      showCancelButton: true
    }, function() {
      // Redirect the user
      window.location.href = linkURL;
    });
    }

  function warnBeforeSyncLocations(linkURL) {
      swal({
          title: "<?php echo JText::_('COM_CWTRAFFIC_SYNC_LOCATIONS_POPUP_TITLE'); ?>",
          text: "<?php echo JText::_('COM_CWTRAFFIC_SYNC_LOCATIONS_POPUP_MSG'); ?>",
          type: "warning",
          html: true,
          showCancelButton: true
      }, function() {
          // Redirect the user
          window.location.href = linkURL;
      });
  }

    function warnBeforeAddbots(linkURL) {
    swal({
      title: "<?php echo JText::_('COM_CWTRAFFIC_ADDBOTS_POPUP_TITLE'); ?>",
      text: "<?php echo JText::_('COM_CWTRAFFIC_ADDBOTS_POPUP_MSG'); ?>",
      type: "warning",
      html: true,
      showCancelButton: true
    }, function() {
      // Redirect the user
      window.location.href = linkURL;
    });
    }

  </script>

