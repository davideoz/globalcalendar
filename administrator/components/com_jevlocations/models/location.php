<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');

/**
 * Locations Component Location Model
 *
 */
class LocationsModelLocation extends JModelLegacy
{
	/**
	 * Location id
	 *
	 * @var int
	 */
	var $_loc_id = null;

	/**
	 * Location data
	 *
	 * @var array
	 */
	var $_data = null;

	public $lastrow = null;

	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		parent::__construct();

		$array = JRequest::getVar( 'cid', JRequest::getVar( 'loc_id', array(0), 'default', 'array' ), 'default', 'array' );
		$edit	= JRequest::getVar('edit',true);
		if($edit)
		{
			$this->setId((int) $array[0]);
		}
	}

	/**
	 * Method to set the location identifier
	 *
	 * @access	public
	 * @param	int Location identifier
	 */
	function setId($id)
	{
		// Set location id and wipe data
		$this->_loc_id		= $id;
		$this->_data	= null;
	}

	/**
	 * Method to get a location
	 *
	 * @since 1.5
	 */
	function &getData()
	{
		// Load the location data
		if ($this->_loadData())
		{
			// Initialize some variables
			$user = JFactory::getUser();

			// Check to see if the category is published
			$compparams = JComponentHelper::getParams("com_jevlocations");
			$usecats = $compparams->get("usecats",0);
			if ($usecats){
				if (is_null($this->_data->catid) || $this->_data->catid==0){
					JError::raiseWarning(100, JText::_("City_Not_Found"));
				}
				else if (isset($this->_data->cat_access)) {

					// Check whether category access level allows access
					$aids =  JEVHelper::getAid($user, 'array');
					if (!(in_array($this->_data->cat_access[0], $aids))) {
						JError::raiseError( 403, JText::_( 'ALERTNOTAUTH' ) );
						return;
					}

				}
			}
		}
		else  $this->_initData();

		return $this->_data;
	}

	/**
	 * Tests if location is checked out
	 *
	 * @access	public
	 * @param	int	A user id
	 * @return	boolean	True if checked out
	 * @since	1.5
	 */
	function isCheckedOut( $uid=0 )
	{
		if ($this->_loadData())
		{
			if ($uid) {
				return ($this->_data->checked_out && $this->_data->checked_out != $uid);
			} else {
				return $this->_data->checked_out;
			}
		}
	}

	/**
	 * Method to checkin/unlock the location
	 *
	 * @access	public
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function checkin()
	{
		if ($this->_loc_id)
		{
			$location =  $this->getTable();
			if(! $location->checkin($this->_loc_id)) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}
		return false;
	}

	/**
	 * Method to checkout/lock the location
	 *
	 * @access	public
	 * @param	int	$uid	User ID of the user checking the article out
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function checkout($uid = null)
	{
		if ($this->_loc_id)
		{
			// Make sure we have a user id to checkout the article with
			if (is_null($uid)) {
				$user	= JFactory::getUser();
				$uid	= $user->get('id');
			}
			// Lets get to it and checkout the thing...
			$location = $this->getTable();
			if(!$location->checkout($uid, $this->_loc_id)) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}

			return true;
		}
		return false;
	}

	/**
	 * Method to store the location
	 *
	 * @access	public
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function store($data)
	{
		$dispatcher = JEventDispatcher::getInstance();
		JPluginHelper::importPlugin("jevents");
		$res = $dispatcher->trigger('onBeforeSaveLocation', array($data));

		JTable::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jevlocations/tables');

		$row = $this->getTable();
		$catTable = $this->getTable('locationscatmap');

		if (isset($data["loc_id"]) && $data["loc_id"] > 0 ){
			$row->load((int) $data["loc_id"]);
		}

		// Bind the form fields to the location table
		if (!$row->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Create the timestamp for the date

		//if new item, order last
		if (!$row->loc_id) {
			$row->ordering = $row->getNextOrder( );
		}

		// Make sure the location table is valid
		if (!$row->check()) {
			$this->setError($row->getError());
			return false;
		}

		// Store the location table to the database
		if (!$row->store()) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		$this->lastrow = $row;

        // Store last saved location id in registry
        $reg = JRegistry::getInstance("com_jevlocations");
        $reg->set("last_loc_id", $this->lastrow->loc_id);

		// Now assign categories
		// We get old Categories
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('locid,catid');
		$query->from('#__jevlocations_catmap');
		$query->where('locid = '.$this->lastrow->loc_id);
		$db->setQuery($query);
		$catList = $db->loadObjectList('catid');

		//We remove all categories assignation for that location
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->delete();
		$query->from('#__jevlocations_catmap');
		$query->where('locid = '.$this->lastrow->loc_id);
		$db->setQuery($query);
		try{
			$db->execute();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
		}

		$catMapData['locid'] = $this->lastrow->loc_id;

		if (!isset($data["loccat"])){
			$data["loccat"] = array();
		}
		foreach($data["loccat"] as $catId)
		{
			$catMapData['catid'] = $catId;
			try
			{
				$catTable->bind($catMapData);
			}
			catch(InvalidArgumentException $e)
			{
				JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
			}
			try
			{
				$catTable->store();
			} catch (Exception $e) {
				JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
			}
		}

		// Now do any custom fields
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$template = $compparams->get("fieldtemplate","");
		$customfields = array();
		if ($template!=""){
			$xmlfile = JPATH_SITE."/plugins/jevents/jevcustomfields/customfields/templates/".$template;
			if (file_exists($xmlfile)){
				$db->setQuery("SELECT * FROM #__jev_customfields3 WHERE target_id=".intval($row->loc_id). " AND targettype='com_jevlocations'");

				$cfdata  = $db->loadObjectList('name');
				$customdata = array();
				foreach ($cfdata as $dataelem)
				{
					if (strpos($dataelem->name, ".") !== false)
					{
						$dataelem->name = str_replace(".", "_", $dataelem->name);
					}
					$customdata[$dataelem->name] = $dataelem->value;
				}

				$params = JevCfForm::getInstance("com_jevlocations.customfields", $xmlfile, array('control' => 'jform', 'load_data' => true), true, "/form");
				$params->bind($customdata);

				// clean out the defunct data!!
				$sql = "DELETE FROM #__jev_customfields3 WHERE target_id=".intval($row->loc_id). " AND targettype='com_jevlocations'";
				$db->setQuery($sql);
				$success =  $db->query();

				$newparams = array();
				$groups = $params->getFieldsets();
				foreach ($groups as $group => $element)
				{
					if ($params->getFieldCountByFieldSet($group))
					{
						$newparams = array_merge($newparams, $params->renderToBasicArray('params', $group));
					}
				}
				$params = $newparams;

				foreach ($params as $param) {
					if (!array_key_exists($param["name"],$data["jform"])) continue;

					if (!is_array($data["jform"][$param["name"]])){
						if ($param["allowraw"])
						{
							$customfield = $data["jform"][$param["name"]];
						}
						else if ($param["allowhtml"]){
							static $safeHtmlFilter;
							if (!isset($safeHtmlFilter)){
								$safeHtmlFilter =  JFilterInput::getInstance(null, null, 1, 1);
							}
							$customfield  = $safeHtmlFilter->clean($data["jform"][$param["name"]]);
						}
						else{
							$customfield = JFilterInput::getInstance()->clean($data["jform"][$param["name"]]);
						}
					}
					else {
						$customfield = implode(",",$data["jform"][$param["name"]]);
					}

					$sql = "INSERT INTO  #__jev_customfields3 (value, target_id, targettype, name ) VALUES(".$db->Quote($customfield).", ".intval($row->loc_id).",'com_jevlocations', ".$db->Quote($param["name"]).")";
					$db->setQuery($sql);
					$success =  $db->query();
				}

			}
		}

		return true;
	}

	/**
	 * Method to remove a location
	 *
	 * @access	public
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function delete($cid = array())
	{
		$result = false;

		if (count( $cid ))
		{
			JArrayHelper::toInteger($cid);
			$cids = implode( ',', $cid );
			$query = $this->_db->getQuery(true);
			$query->delete();
			$query->from('#__jev_locations');
			$query->where('loc_id IN ('.$cids.')');
			$this->_db->setQuery($query);
			try
			{
				$this->_db->execute();
			}
			catch (RuntimeException $e)
			{
				JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
				return false;
			}
			$query = $this->_db->getQuery(true);
			$query->delete();
			$query->from('#__jevlocations_catmap');
			$query->where('locid IN ('.$cids.')');
			$this->_db->setQuery( $query );
			try
			{
				$this->_db->execute();
			}
			catch (RuntimeException $e)
			{
				JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
				return false;
			}
		}

		// Now ensure no jevents location orphans
		$query = 'UPDATE #__jevents_vevdetail SET location="" WHERE location IN ( '.$cids.' )';
		$this->_db->setQuery( $query );
		if(!$this->_db->query()) {
			$this->setError($this->_db->getErrorMsg());
		}


		return true;
	}

	/**
	 * Method to (un)publish a location
	 *
	 * @access	public
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function publish($cid = array(), $publish = 1)
	{
		$user 	= JFactory::getUser();

		if (count( $cid ))
		{
			JArrayHelper::toInteger($cid);
			$cids = implode( ',', $cid );

			$query = 'UPDATE #__jev_locations'
			. ' SET published = '.(int) $publish
			. ' WHERE loc_id IN ( '.$cids.' )'
			. ' AND ( checked_out = 0 OR ( checked_out = '.(int) $user->get('id').' ) )'
			;
			$this->_db->setQuery( $query );
			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}

		return true;
	}

	/**
	 * Method to (un)globalise a location
	 *
	 * @access	public
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function globalise($cid = array(), $global = 1)
	{

		if (count( $cid ))
		{
			JArrayHelper::toInteger($cid);
			$cids = implode( ',', $cid );

			$query = 'UPDATE #__jev_locations'
			. ' SET global = '.(int) $global
			. ' WHERE loc_id IN ( '.$cids.' )'
			;
			$this->_db->setQuery( $query );
			if (!$this->_db->query()) {
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}

		return true;
	}


	/**
	 * Method to move a location
	 *
	 * @access	public
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function move($direction)
	{
		$row = $this->getTable();
		if (!$row->load($this->_loc_id)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		if (!$row->move( $direction, ' catid = '.(int) $row->catid.' and loccat = '.(int) $row->loccat.' AND published >= 0 ' )) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		return true;
	}

	/**
	 * Method to move a location
	 *
	 * @access	public
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function saveorder($cid = array(), $order)
	{
		$row = $this->getTable();
		$groupings = array();

		// update ordering values
		for( $i=0; $i < count($cid); $i++ )
		{
			$row->load( (int) $cid[$i] );
			// track categories
			$groupings[] = $row->loccat;

			if ($row->ordering != $order[$i])
			{
				$row->ordering = $order[$i];
				if (!$row->store()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
		}

		// execute updateOrder for each parent group
		/*$groupings = array_unique( $groupings );
		foreach ($groupings as $group){
			$row->reorder('catid = '.(int) $group);
		}*/

		return true;
	}

	/**
	 * Method to load content location data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _loadData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$query = $this->_db->getQuery(true);
			$query->select('w.*, GROUP_CONCAT(cc.title SEPARATOR ",") AS category_list,GROUP_CONCAT(map.catid SEPARATOR ",") AS catid_list,cc1.title AS c1title, cc2.title AS c2title, cc3.title AS c3title, cc.published AS cat_pub, cc.access AS cat_access');
			$query->from('#__jev_locations AS w');
			$query->leftjoin('#__jevlocations_catmap AS map ON map.locid = w.loc_id');
			$query->leftjoin('#__categories AS cc ON cc.id = map.catid');
			$query->leftjoin('#__jevlocation_categories AS cc1 ON cc1.id = w.catid');
			$query->leftjoin('#__jevlocation_categories AS cc2 ON cc1.parent_id = cc2.id');
			$query->leftjoin('#__jevlocation_categories AS cc3 ON cc2.parent_id = cc3.id');
		         $query->where('w.loc_id = '.(int) $this->_loc_id);
			$query->group("w.loc_id");
			$this->_db->setQuery($query);
			$this->_data = $this->_db->loadObject();

                        // to allow Falang to translate - strip out the group_contat
                        if ((boolean) $this->_data) {
                            $query = $this->_db->getQuery(true);
                            $query->select('cc.title , cc.id');
                            $query->from('#__categories AS cc ');
                            $query->leftjoin('#__jevlocations_catmap AS map ON cc.id = map.catid');
                            $query->where('map.locid = '.(int) $this->_loc_id);
                            $this->_db->setQuery($query);
                            $category_list = $this->_db->loadColumn(0);
                            if ($category_list && count ($category_list)){
                                $this->_data->category_list = implode(", ", $category_list);
                            }
                        }

			return (boolean) $this->_data;
		}
		return true;
	}

	/**
	 * Method to initialise the location data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _initData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$this->_data = $this->getTable();
			return (boolean) $this->_data;
		}
		return true;
	}
}
