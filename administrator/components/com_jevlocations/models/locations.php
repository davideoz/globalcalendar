<?php

/**
 * JEvents List Locations Module
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2006-2015 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die('Restricted access');

jimport('joomla.application.component.model');

/**
 * Locations Component Locations Model
 *
 */
class LocationsModelLocations extends JModelLegacy
{

	/**
	 * Category ata array
	 *
	 * @var array
	 */
	var $_data = null;
	/**
	 * Category total
	 *
	 * @var integer
	 */
	var $_total = null;
	/**
	 * Pagination object
	 *
	 * @var object
	 */
	var $_pagination = null;

	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		parent::__construct();

		$option = JEVEX_COM_COMPONENT;
		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible

		// Get the pagination request variables
		if ($mainframe->isAdmin()){
			$limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
			$limitstart = $mainframe->getUserStateFromRequest($option . 'limitstart', 'limitstart', 0, 'int');
		}
		else
		{
			$itemid = JFactory::getApplication()->input->get("Itemid",0,"int");
			$limit = $mainframe->getUserStateFromRequest('global.list.limit'.$itemid, 'locations_limit', $mainframe->getCfg('list_limit'), 'int');
			$limitstart = $mainframe->getUserStateFromRequest($option . 'limitstart'.$itemid, 'locations_limitstart', 0, 'int');
		}

		// In case limit has been changed, adjust limitstart accordingly
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);

	}

	function checkin($ids = array())
	{
		$checkin = false;
		if (count($ids) > 0)
		{
			foreach ($ids as $id) {
				$item = &$this->getTable('location');
				$checkin = $item->checkIn($id);

				if (!$checkin)
				{
					$this->setError($this->_db->getErrorMsg());
				}
			}
		}
		return $checkin;
	}

	/*
	 * Method to batch process locations
	 */
	function batch($commands, $cid)
	{
		if (!empty($commands['categories']))
		{

		}
		if (!empty($commands['city']))
		{

		}
	}

	/**
	 * Method to get location item data
	 *
	 * @access public
	 * @return array
	 */
	function getData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$limitstart = $this->getState('limitstart');
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
			$db = JFactory::getDBO();
			//echo $db->getQuery();
			echo $db->getErrorMsg();
		}

		return $this->_data;

	}

	/**
	 * Get list of items for public list in frontend
	 *
	 * @return unknown
	 */
	function getPublicData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_publicdata))
		{
			$query = $this->_buildPublicQuery();
			$this->_publicdata = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));
			JevlocationsHelper::getLocationsCustomFields($this->_publicdata);
			$db = JFactory::getDBO();
			echo $db->getErrorMsg();
		}


		return $this->_publicdata;

	}

	/**
	 * Method to get the total number of location items
	 *
	 * @access public
	 * @return integer
	 */
	function getTotal()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_total))
		{
			$query = $this->_buildQuery();
			$this->_total = $this->_getListCount($query);
		}

		return $this->_total;

	}

	/**
	 * Method to get the total number of location items
	 *
	 * @access public
	 * @return integer
	 */
	function getPublicTotal()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_total))
		{
			$query = $this->_buildPublicQuery();
			$this->_total = $this->_getListCount($query);
		}
		// should we reset the list to the start?
		if ($this->getState("limitstart")>0  && $this->_total  < $this->getState("limitstart")){
			 $this->setState("limitstart",0);
			JRequest::setVar("limitstart",0);
		}
		return $this->_total;

	}

	/**
	 * Method to get a pagination object for the locations
	 *
	 * @access public
	 * @return integer
	 */
	function getPagination()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_pagination))
		{
			$mainframe = JFactory::getApplication(); // RSH 10/12/10 Make compatible with J!1.5 and J!1.6
			if ($mainframe->isAdmin()){
				jimport('joomla.html.pagination');
				$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'));
			}
			else
			{
				jimport('joomla.html.pagination');
				$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit'), "locations_");
			}
		}

		return $this->_pagination;

	}

	/**
	 * Method to get a pagination object for the locations
	 *
	 * @access public
	 * @return integer
	 */
	function getPublicPagination()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_pagination))
		{
			$mainframe = JFactory::getApplication(); // RSH 10/12/10 Make compatible with J!1.5 and J!1.6
			if ($mainframe->isAdmin()){
				jimport('joomla.html.pagination');
				$this->_pagination = new JPagination($this->getPublicTotal(), $this->getState('limitstart'), $this->getState('limit'));
			}
			else
			{
				$this->_pagination = new JPagination($this->getPublicTotal(), $this->getState('limitstart'), $this->getState('limit'),"locations_");
			}
		}

		return $this->_pagination;

	}

	function _buildQuery()
	{
		// Get the WHERE and ORDER BY clauses for the query
		$where = $this->_buildContentWhere();
		$orderby = $this->_buildContentOrderBy();

		$query = ' SELECT loc.*, GROUP_CONCAT(DISTINCT map.catid SEPARATOR ","), cc1.title AS c1title, cc2.title AS c2title, cc3.title AS c3title, cat.title as category, GROUP_CONCAT(cat.title SEPARATOR ",") as categories_list,us.username, u.name AS editor  '
				. ' FROM #__jev_locations AS loc '
				. ' LEFT JOIN #__jevlocations_catmap AS map ON map.locid = loc.loc_id'
				. ' LEFT JOIN #__categories AS cat ON cat.id = map.catid '
				. ' LEFT JOIN #__jevlocation_categories AS cc1 ON cc1.id = loc.catid '
				. ' LEFT JOIN #__jevlocation_categories AS cc2 ON cc1.parent_id = cc2.id '
				. ' LEFT JOIN #__jevlocation_categories AS cc3 ON cc2.parent_id = cc3.id '
				. ' LEFT JOIN #__users AS u ON u.id = loc.checked_out'
				. ' LEFT JOIN #__users AS us ON us.id = loc.created_by'
				. $where
				. ' GROUP BY loc.loc_id'
				. $orderby
		;
		return $query;

	}

	function _buildPublicQuery()
	{
		// Get the WHERE and ORDER BY clauses for the query
		$where = $this->_buildPublicContentWhere();
		$orderby = $this->_buildContentOrderBy();

		$query = ' SELECT loc.*, cc1.title AS c1title, cc2.title AS c2title, cc3.title AS c3title, cat.title as category, GROUP_CONCAT(DISTINCT cat.title SEPARATOR ",") as categories_list, GROUP_CONCAT(map.catid SEPARATOR ",") AS catid_list, u.name AS editor  ';

// for joomfish
		$query .= ' , cat.id as catid1, cc1.id as cc1id, cc2.id as cc2id, cc3.id as cc3id ';

		// If we have geocoding of visitors' location then incorporate distance too
		if (JRequest::getFloat("needdistance", 0))
		{
			$lat = JRequest::getFloat("lat", 999);
			$lon = JRequest::getFloat("lon", 999);
			$km = JRequest::getInt("km", 0) ? 1.609344 : 1;

			//	$query .= ",(((acos(sin(($lat*pi()/180)) * sin((loc.geolat*pi()/180))+cos(($lat*pi()/180)) * cos((loc.geolat*pi()/180)) * cos((($lon- loc.geolon)*pi()/180))))*180/pi())*60*1.1515*$km) as distance";
			$query .= ",(((acos(sin(RADIANS($lat)) * sin(RADIANS(loc.geolat))+cos(RADIANS($lat)) * cos(RADIANS(loc.geolat)) * cos((RADIANS($lon- loc.geolon)))))*180/pi())*60*1.1515*$km) as distance";
		}

		$compparams = JComponentHelper::getParams("com_jevlocations");

		$jevparams = JComponentHelper::getParams("com_jevents");

		$groupby = " GROUP BY loc.loc_id";
		if ($compparams->get("onlywithevents", 0))
		{
			if ($jevparams->get("multicategory", 0))
			{
				$join = "\n LEFT JOIN #__jevents_catmap as catmap ON catmap.evid = rpt.eventid";
			}
			else {
				$join = "";
			}

			if (!isset($this->datamodel))
			{
				include_once(JPATH_SITE . "/components/" . JEV_COM_COMPONENT . "/jevents.defines.php");
				$this->datamodel = new JEventsDataModel();

				JPluginHelper::importPlugin('jevents');
			}
			// process the new plugins
			// get extra data and conditionality from plugins
			$extrawhere = array();
			$extrajoin = array();
			$extrafields = "";  // must have comma prefix
			$extratables = "";  // must have comma prefix
			$needsgroup = false;

			$filters = jevFilterProcessing::getInstance(array("published", "justmine", "category", "search"));
			$filters->setWhereJoin($extrawhere, $extrajoin);
			$needsgroup = $filters->needsGroupBy();

			$dispatcher = JEventDispatcher::getInstance();
			$dispatcher->trigger('onListIcalEvents', array(& $extrafields, & $extratables, & $extrawhere, & $extrajoin, & $needsgroup));

			$extrajoin = ( count($extrajoin) ? " \n LEFT JOIN " . implode(" \n LEFT JOIN ", $extrajoin) : '' );
			$extrawhere = ( count($extrawhere) ? ' AND ' . implode(' AND ', $extrawhere) : '' );

			$where .= $extrawhere;
			$join = $join . $extrajoin;

			if (strpos($join, "#__jev_locations AS loc") === false)
			{
				//$join .= "\n LEFT JOIN #__jev_locations AS loc on loc.loc_id= det.location";
			}
			$query .= "\n FROM #__jevents_repetition as rpt"
					. "\n LEFT JOIN #__jevents_vevent as ev ON rpt.eventid = ev.ev_id"
					. "\n LEFT JOIN #__jevents_icsfile as icsf ON icsf.ics_id=ev.icsid "
					. "\n LEFT JOIN #__jevents_vevdetail as det ON det.evdet_id = rpt.eventdetail_id"
					. "\n LEFT JOIN #__jevents_rrule as rr ON rr.eventid = rpt.eventid";
			$query .= $join;
			if (strpos(strtolower($join), "#__jevlocations_catmap as map") === false)
			{
					$query .= ' LEFT JOIN #__jevlocations_catmap AS map ON map.locid = loc.loc_id';
			}
			if (strpos(strtolower($join), "#__categories as cat") === false)
			{
					$query .= ' LEFT JOIN #__categories AS cat ON cat.id = map.catid ';
			}
					$query .= ' LEFT JOIN #__jevlocation_categories AS cc1 ON cc1.id = loc.catid '
					. ' LEFT JOIN #__jevlocation_categories AS cc2 ON cc1.parent_id = cc2.id '
					. ' LEFT JOIN #__jevlocation_categories AS cc3 ON cc2.parent_id = cc3.id '
					. ' LEFT JOIN #__users AS u ON u.id = loc.checked_out'
					. $where
					. $groupby
					. $orderby
			;
		}
		else
		{
			$query .= ' FROM #__jev_locations AS loc ';
			$query .= ' LEFT JOIN #__jevlocations_catmap AS map ON map.locid = loc.loc_id'
					. ' LEFT JOIN #__categories AS cat ON cat.id = map.catid '
					. ' LEFT JOIN #__jevlocation_categories AS cc1 ON cc1.id = loc.catid '
					. ' LEFT JOIN #__jevlocation_categories AS cc2 ON cc1.parent_id = cc2.id '
					. ' LEFT JOIN #__jevlocation_categories AS cc3 ON cc2.parent_id = cc3.id '
					. ' LEFT JOIN #__users AS u ON u.id = loc.checked_out'
					. $where
					. $groupby
					. $orderby
			;
		}
		return $query;

	}

	function _buildContentOrderBy()
	{
		$option = JEVEX_COM_COMPONENT;
		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible

		$itemid = JFactory::getApplication()->input->get("Itemid",0,"int");

		if($this->state->task!="locations_blog")
		{
			$filter_order = $mainframe->getUserStateFromRequest($option . 'loc_filter_order'.$itemid, 'filter_order', '', 'cmd');
			$filter_order_Dir = $mainframe->getUserStateFromRequest($option . 'loc_filter_order_Dir'.$itemid, 'filter_order_Dir', '', 'word');
		}
		else
		{
			$filter_order ="";
			$filter_order_Dir = "";
		}
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats", 0);

		// If we have geocoding of visitors' location then use this as the sort algorithm
		if (JRequest::getFloat("sortdistance", 0))
		{
			$orderby = ' ORDER BY distance ASC, ';
		}
		else
		{
			$orderby = ' ORDER BY ';
		}

		if (!$usecats)
		{
			if ($filter_order == '')
			{
				if($compparams->get("showpriority",0) == 1)
				{
					$orderby .= ' loc.priority DESC, ' ;
				}
				if ($compparams->get("deforder", 0) == 0)
				{
					$orderby .= 'loc.country, loc.state, loc.city, loc.title ' ;
				}
				else if ($compparams->get("deforder", 0) == 1)
				{
					$orderby .= 'loc.title ' ;
				}
				else
				{
					$orderby .= 'loc.ordering ' ;
				}
			}
			else
			{
				if ($compparams->get("deforder", 0) == 0)
				{
					$orderby .= $filter_order . ' ' . $filter_order_Dir . ' ,  loc.country, loc.state, loc.city,  loc.title ASC';
				}
				else if ($compparams->get("deforder", 0) == 1)
				{
					$orderby .= $filter_order . ' ' . $filter_order_Dir . ' , loc.title ASC';
				}
				else
				{
					$orderby .= $filter_order . ' ' . $filter_order_Dir . ' , loc.ordering ' ;
				}
			}
		}
		else
		{
			if ($filter_order == '')
			{
				if($compparams->get("showpriority",0) == 1)
				{
					$orderby .= 'loc.priority DESC, ' ;
				}

				if ($compparams->get("deforder", 0) == 0)
				{
					$orderby .= 'c3title,  c2title,  c1title, loc.title ' ;
				}
				else if ($compparams->get("deforder", 0) == 1)
				{
					$orderby .= 'loc.title ' ;
				}
				else
				{
					$orderby .= 'loc.ordering ' ;
				}
			}
			else
			{
				if ($compparams->get("deforder", 0) == 0)
				{
					$orderby .= $filter_order . ' ' . $filter_order_Dir . ' ,  c3title,  c2title,  c1title,  loc.title ASC';
				}
				else if ($compparams->get("deforder", 0) == 1)
				{
					$orderby .= $filter_order . ' ' . $filter_order_Dir . ' , loc.title ASC';
				}
				else
				{
					$orderby .= $filter_order . ' ' . $filter_order_Dir . ' , loc.ordering ASC';
				}
			}
		}

		return $orderby;

	}

	function _buildContentWhere()
	{
		$option = JEVEX_COM_COMPONENT;
		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible

		$db = JFactory::getDBO();
		$itemid = JFactory::getApplication()->input->get("Itemid",0,"int");
		$filter_state = $mainframe->getUserStateFromRequest($option . 'loc_filter_state'.$itemid, 'filter_state', '', 'word');
		$filter_creator = $mainframe->getUserStateFromRequest($option . 'loc_filter_created_by'.$itemid, 'filter_created_by', 0, 'email');

		$filter_catid = $mainframe->getUserStateFromRequest($option . 'loc_filter_catid'.$itemid, 'filter_catid', 0, 'int');
		$filter_loccat = $mainframe->getUserStateFromRequest($option . 'loc_filter_loccat'.$itemid, 'filter_loccat', 0, 'int');
		$search = $mainframe->getUserStateFromRequest($option . 'loc_search', 'search'.$itemid, '', 'string');
		$search = JString::strtolower($search);

		$where = array();

		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats", 0);
		if ($usecats)
		{
			if ($filter_catid > 0)
			{
				$where[] = ' cc1.id = ' . (int) $filter_catid . ' or cc2.id = ' . (int) $filter_catid . ' or cc3.id = ' . (int) $filter_catid;
			}
			if (trim($search) != "")
			{
				$where[] = 'LOWER(loc.title) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false);
			}
		}
		else if (trim($search) != "")
		{
			$where[] = ' (LOWER(loc.title) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false)
					. ' OR LOWER(loc.city) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false)
					. ' OR LOWER(loc.state) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false)
					. ' OR LOWER(loc.country) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false)
					. ')';
		}

		if ($filter_loccat > 0)
		{
			$where[] = ' ( cat.id = ' . (int) $filter_loccat. ')';
		}

		if ($filter_state)
		{
			if ($filter_state == 'P')
			{
				$where[] = 'loc.published = 1';
			}
			else if ($filter_state == 'U')
			{
				$where[] = 'loc.published = 0';
			}
		}

		if($filter_creator)
		{

			if(is_numeric($filter_creator))
			{
				$where[] = 'loc.created_by = '. (int) $filter_creator;
			}
			else
			{

				$where[] = 'loc.anonemail = '. $db->Quote($filter_creator);
			}
		}

		$canShowGlobal = JRequest::getVar("showglobal", true);
		$canShowAll = JRequest::getVar("showall", false);
		$user =  JFactory::getUser();
		if (!$canShowAll || !JEVHelper::isAdminUser($user))
		{
			$doneit = true;
			if ($compparams->get("groupusage", 0)){
				$groups = $user->getAuthorisedGroups();
				$where[] = ' (loc.global = 1 OR loc.created_by=' . $user->id . ' OR loc.groupusage IN ('.implode(",",$groups) .' ) )';
			}
			else {
				$where[] = ' (loc.global = 1 OR loc.created_by=' . $user->id . '  )';
			}
		}
		if (!$canShowGlobal)
		{
			$where[] = ' loc.created_by=' . $user->id;
		}
		else if ($this->getState("select"))
		{
			$loctype = $this->getState("loctype");
			switch ($loctype) {
				case 0:
					if (!intval($compparams->get("selectfromall", 0)) && !isset($doneit)) {
						if ($compparams->get("groupusage", 0)){
							$groups = $user->getAuthorisedGroups();
							$where[] = ' (loc.global = 1 OR loc.created_by=' . $user->id . ' OR loc.groupusage IN ('.implode(",",$groups) .' ) )';
						}
						else {
							$where[] = ' (loc.global = 1 OR loc.created_by=' . $user->id . '  )';
						}
					}
					break;
				case 1;
					$where[] = ' loc.created_by=' . $user->id;
					break;
				case 2;
					$where[] = ' loc.global = 1';
					break;
			}
		}


		$cityfilter = $compparams->get("cityfilter", array());
                if (is_string($cityfilter)){
                    $cityfilter = str_replace(", ",",",$cityfilter);
                    $cityfilter = str_replace(" , ",",",$cityfilter);
                    if (trim($cityfilter)==""){
                        $cityfilter = array();
                    }
                    else {
                        $cityfilter = explode(",", $cityfilter);
                    }
                }
                if (count($cityfilter)) {
                    foreach ($cityfilter as &$cfilter)
                    {
                            if (trim($cfilter)!="") {
                                $cfilter = $db->Quote($db->escape(trim($cfilter), true), false);
                            }
                            unset($cfilter);
                    }
                    $cityfilter = implode(",", $cityfilter);
                    if ($cityfilter!=""){
                        $where[] = "CONCAT_WS(' -> ',loc.country,loc.state,loc.city) IN (" . $cityfilter . ")";
                    }
                }

		$statefilter = $compparams->get("statefilter", "");
                if (is_string($statefilter)){
                    $statefilter = str_replace(", ",",",$statefilter);
                    $statefilter = str_replace(" , ",",",$statefilter);

                    if (trim($statefilter)==""){
                        $statefilter = array();
                    }
                    else {
                        $statefilter = explode(",", $statefilter);
                    }
                }

		if (count($statefilter))
		{
			foreach ($statefilter as &$sfilter)
			{
                            if (trim($sfilter)!="") {
				$sfilter = $db->Quote($db->escape(trim($sfilter), true), false);
                            }
                            unset($sfilter);
			}
			$statefilter = implode(",", $statefilter);
                        if ($statefilter!=""){
                            $where[] = "CONCAT_WS(' -> ',loc.country,loc.state) IN (" . $statefilter . ")";
                        }
		}

		$countryfilter = $compparams->get("countryfilter", array());
                if (is_string($countryfilter)){
                    $countryfilter = str_replace(", ",",",$countryfilter);
                    $countryfilter = str_replace(" , ",",",$countryfilter);

                    if (trim($countryfilter)==""){
                        $countryfilter = array();
                    }
                    else {
                        $countryfilter = explode(",", $countryfilter);
                    }
                }
                if (count($countryfilter)) {
                    foreach ($countryfilter as &$cfilter)
                    {
                        if (trim($cfilter)!="") {
                            $cfilter = $db->Quote($db->escape(trim($cfilter), true), false);
                        }
                        unset($cfilter);
                    }
                    $countryfilter = implode(",", $countryfilter);
                    if ($countryfilter!=""){
                        $where[] = "loc.country IN (" . $countryfilter . ")";
                    }
                }

		// filtering on priority/featured level
		if (JRequest::getInt("jlpriority_fv", 0) > 0)
		{
			$where[] = "loc.priority>=" . JRequest::getInt("jlpriority_fv", 0);
		}


		$where = ( count($where) ? ' WHERE ' . implode(' AND ', $where) : '' );

		return $where;

	}

	function _buildPublicContentWhere()
	{
		$option = JEVEX_COM_COMPONENT;
		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible
		JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");

		$db = JFactory::getDBO();
		$itemid = JFactory::getApplication()->input->get("Itemid",0,"int");
		$filter_state = $mainframe->getUserStateFromRequest($option . 'loc_filter_state'.$itemid, 'filter_state', '', 'word');
		$filter_catid = $mainframe->getUserStateFromRequest($option . 'loc_filter_catid'.$itemid, 'filter_catid', 0, 'int');
		$filter_loccat = $mainframe->getUserStateFromRequest($option . 'loc_filter_loccat'.$itemid, 'filter_loccat', 0, 'int');
		$loccity_fv = JFactory::getApplication()->getUserStateFromRequest($option . 'loc_city_fv'.$itemid, 'loccity_fv', "", 'string');

		$search = $mainframe->getUserStateFromRequest($option . 'loc_search'.$itemid, 'search', '', 'string');
		$search = JString::strtolower($search);

		$where = array();

		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats", 0);
		if ($usecats)
		{
			if ($filter_catid > 0)
			{
				$where[] = ' cc1.id = ' . (int) $filter_catid . ' or cc2.id = ' . (int) $filter_catid . ' or cc3.id = ' . (int) $filter_catid;
			}
			if (trim($search) != "")
			{
				//$where[] = '(LOWER(loc.title) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false) . ' OR LOWER(loc.description) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false).')';
				$where[] = '(LOWER(loc.title) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false).')';
			}
		}
		else if (trim($search) != "")
		{
			$where[] = ' (LOWER(loc.title) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false)
					. ' OR LOWER(loc.city) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false)
					. ' OR LOWER(loc.state) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false)
					. ' OR LOWER(loc.country) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false)
					//. ' OR LOWER(loc.description) LIKE ' . $db->Quote('%' . $db->escape($search, true) . '%', false)
					. ')';
		}

		if ($filter_loccat > 0)
		{
			$where[] = ' ( cat.id = ' . (int) $filter_loccat . ')';
		}

		$where[] = 'loc.published = 1';
		$user =  JFactory::getUser();
		if ($compparams->get("onlyglobal", 1))
			$where[] = ' (loc.global = 1 OR loc.created_by=' . $user->id . ')';

		$cityfilter = $compparams->get("cityfilter", array());
                if (is_string($cityfilter)){
                    $cityfilter = str_replace(", ",",",$cityfilter);
                    $cityfilter = str_replace(" , ",",",$cityfilter);
                    if (trim($cityfilter)==""){
                        $cityfilter = array();
                    }
                    else {
                        $cityfilter = explode(",", $cityfilter);
                    }
                }
                if (count($cityfilter)) {
                    foreach ($cityfilter as &$cfilter)
                    {
                            if (trim($cfilter)!="") {
                                $cfilter = $db->Quote($db->escape(trim($cfilter), true), false);
                            }
                            unset($cfilter);
                    }
                    $cityfilter = implode(",", $cityfilter);
                    if ($cityfilter!=""){
                        $where[] = "CONCAT_WS(' -> ',loc.country,loc.state,loc.city) IN (" . $cityfilter . ")";
                    }
                }

		$statefilter = $compparams->get("statefilter", "");
                if (is_string($statefilter)){
                    $statefilter = str_replace(", ",",",$statefilter);
                    $statefilter = str_replace(" , ",",",$statefilter);

                    if (trim($statefilter)==""){
                        $statefilter = array();
                    }
                    else {
                        $statefilter = explode(",", $statefilter);
                    }
                }

		if (count($statefilter))
		{
			foreach ($statefilter as &$sfilter)
			{
                            if (trim($sfilter)!="") {
				$sfilter = $db->Quote($db->escape(trim($sfilter), true), false);
                            }
                            unset($sfilter);
			}
			$statefilter = implode(",", $statefilter);
                        if ($statefilter!=""){
                            $where[] = "CONCAT_WS(' -> ',loc.country,loc.state) IN (" . $statefilter . ")";
                        }
		}

		$countryfilter = $compparams->get("countryfilter", array());
                if (is_string($countryfilter)){
                    $countryfilter = str_replace(", ",",",$countryfilter);
                    $countryfilter = str_replace(" , ",",",$countryfilter);

                    if (trim($countryfilter)==""){
                        $countryfilter = array();
                    }
                    else {
                        $countryfilter = explode(",", $countryfilter);
                    }
                }
                if (count($countryfilter)) {
                    foreach ($countryfilter as &$cfilter)
                    {
                        if (trim($cfilter)!="") {
                            $cfilter = $db->Quote($db->escape(trim($cfilter), true), false);
                        }
                        unset($cfilter);
                    }
                    $countryfilter = implode(",", $countryfilter);
                    if ($countryfilter!=""){
                        $where[] = "loc.country IN (" . $countryfilter . ")";
                    }
                }

		//We detect old category ids and translate them to new multicategory system
		$catfilters = $compparams->get("catfilter", "");
		if (is_array($catfilters))
		{
			JArrayHelper::toInteger($catfilters);
			foreach($catfilters as $index => $catfilter)
			{
				$catfilters[$index] = JevLocationsHelper::getNewCategory($catfilter);
			}
                        if (count($catfilters)>0){
                            $catfilters = implode(",", $catfilters);
			  if ($catfilters != ""){
				$where[] = ' ( cat.id  IN (' . $catfilters  . ') )';
			  }
                        }
		}
		else if ($catfilters != "")
		{
			$catfilters = JevLocationsHelper::getNewCategory(intval($catfilters));
			if ($catfilters != ""){
				$where[] = ' ( cat.id  IN (' . $catfilters  . '))';
			}
		}

		$multicatfilters = $compparams->get("multicatfilter", "");
		if (is_array($multicatfilters))
		{
			JArrayHelper::toInteger($multicatfilters);
			if (count ($multicatfilters)>0){
				$multicatfilters = implode(",", $multicatfilters);
				if ($multicatfilters != ""){
					$where[] = ' ( cat.id  IN (' . $multicatfilters  . ') )';
				}
                        }
		}
		else if ($multicatfilters != "0" && $multicatfilters != "")
		{
			$multicatfilters = intval($multicatfilters);
			if ($multicatfilters > 0 ){
				$where[] = ' ( cat.id  IN (' . $multicatfilters  . ') )';
			}
		}


		// filtering on priority/featured level
		if (JRequest::getInt("jlpriority_fv", 0) > 0)
		{
			$where[] = "loc.priority>=" . JRequest::getInt("jlpriority_fv", 0);
		}

		if ($compparams->get("onlywithevents", 0))
		{

			jimport("joomla.utilities.date");
			$startdate = new JDate("-" . $compparams->get("checkeventbefore", 30) . " days");
			$enddate = new JDate("+" . $compparams->get("checkeventafter", 30) . " days");

			$startdate = $startdate->toSql();
			$enddate = $enddate->toSql();

			$where[] = "ev.state=1";
			$where[] = " rpt.endrepeat >= '" . $startdate . "' AND rpt.startrepeat <= '" . $enddate . "'";
		}

		/*
		  $withevents = $compparams->get("withevents",0);
		  if ($withevents){
		  $where[] = 1;
		  }
		 */
		$where = ( count($where) ? ' WHERE ' . implode(' AND ', $where) : '' );

		return $where;

	}

	// VERY CRUDE TEST
	function hasEvents($loc_id, $startdate, $enddate)
	{
		$db = JFactory::getDBO();
		$option = JRequest::getCmd("option");
		$params = JComponentHelper::getParams($option);

		$user = JFactory::getUser();
		$aid	= JEVHelper::getAid($user, 'string');

		if ($params->get("ignorefiltermodule", 1))
		{
			$query = "SELECT count(ev.ev_id) "
					. "\n FROM #__jevents_repetition as rpt"
					. "\n LEFT JOIN #__jevents_vevent as ev ON rpt.eventid = ev.ev_id"
					. "\n LEFT JOIN #__jevents_vevdetail as det ON det.evdet_id = rpt.eventdetail_id"
					. "\n WHERE ev.state=1"
					. "\n AND rpt.endrepeat >= '" . $startdate . "' AND rpt.startrepeat <= '" . $enddate . "'"
					. "\n AND det.location=$loc_id"
					. "\n AND ev.access  " . (version_compare(JVERSION, '1.6.0', '>=') ? ' IN (' . $aid . ')' : ' <=  ' . $aid)
					. " LIMIT 1";

			$db->setQuery($query);
			return $db->loadResult();
		}
		else
		{
			if (!isset($this->datamodel))
			{
				include_once(JPATH_SITE . "/components/" . JEV_COM_COMPONENT . "/jevents.defines.php");
				$this->datamodel = new JEventsDataModel();

				JPluginHelper::importPlugin('jevents');
			}

			$db = JFactory::getDBO();

			// process the new plugins
			// get extra data and conditionality from plugins
			$extrawhere = array();
			$extrajoin = array();
			$extrafields = "";  // must have comma prefix
			$extratables = "";  // must have comma prefix
			$needsgroup = false;

			$filters = jevFilterProcessing::getInstance(array("published", "justmine", "category", "search"));
			$filters->setWhereJoin($extrawhere, $extrajoin);
			$needsgroup = $filters->needsGroupBy();

			$dispatcher = JEventDispatcher::getInstance();
			$dispatcher->trigger('onListIcalEvents', array(& $extrafields, & $extratables, & $extrawhere, & $extrajoin, & $needsgroup));

			$jevparams = JComponentHelper::getParams("com_jevents");

			if ($jevparams->get("multicategory", 0))
			{
				$extrajoin[] = "\n #__jevents_catmap as catmap ON catmap.evid = rpt.eventid";
				$extrajoin[] = "\n #__categories AS catmapcat ON catmap.catid = catmapcat.id";
				$extrawhere[] = " catmapcat.access IN (" . JEVHelper::getAid($user) . ")";
				$extrawhere[] = " catmap.catid IN(" . $this->datamodel->accessibleCategoryList() . ")";
				$needsgroup = true;
				$catwhere = "\n WHERE 1 ";
			}
			else {
				$catwhere = "\n WHERE ev.catid IN(" . $this->datamodel->accessibleCategoryList() . ")";
			}

			$extrajoin = ( count($extrajoin) ? " \n LEFT JOIN " . implode(" \n LEFT JOIN ", $extrajoin) : '' );
			$extrawhere = ( count($extrawhere) ? ' AND ' . implode(' AND ', $extrawhere) : '' );

			$query = "SELECT count(ev.ev_id)";
			$query .= "\n FROM #__jevents_repetition as rpt"
					. "\n LEFT JOIN #__jevents_vevent as ev ON rpt.eventid = ev.ev_id"
					. "\n LEFT JOIN #__jevents_icsfile as icsf ON icsf.ics_id=ev.icsid "
					. "\n LEFT JOIN #__jevents_vevdetail as det ON det.evdet_id = rpt.eventdetail_id"
					. "\n LEFT JOIN #__jevents_rrule as rr ON rr.eventid = rpt.eventid"
					. $extrajoin
					. $catwhere
					. "\n AND rpt.endrepeat >= '$startdate' AND rpt.startrepeat <= '$enddate'"

					// Must suppress multiday events that have already started
					. "\n AND NOT (rpt.startrepeat < '$startdate' AND det.multiday=0) "
					. $extrawhere
					. "\n AND ev.access  " . (version_compare(JVERSION, '1.6.0', '>=') ? ' IN (' . $aid . ')' : ' <=  ' . $aid)
					. "  AND icsf.state=1 "
					. "\n AND icsf.access  " . (version_compare(JVERSION, '1.6.0', '>=') ? ' IN (' . $aid . ')' : ' <=  ' . $aid)
					. "\n AND det.location=$loc_id LIMIT 1"
			;
			$db->setQuery($query);
			return $db->loadResult();
		}

	}

}
