<?php
/**
 * copyright (C) 2008 GWE Systems Ltd - All rights reserved
 */

defined( 'JPATH_BASE' ) or die( 'Direct Access to this location is not allowed.' );

jimport('joomla.application.component.controller');
include_once(JPATH_ADMINISTRATOR."/components/com_jevents/jevents.defines.php");

class AdminLocationsController extends JControllerForm {

	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask( 'list',  'overview' );
		$this->registerTask( 'apply',  'apply' );
		$this->registerDefaultTask("overview");
		JPluginHelper::importPlugin('jevents');
	}

	function overview( )
	{
		$this->_authoriseAccess();

		// get the view
		$viewName = "locations";
		$this->view = $this->getView($viewName,"html");

		// Set the layout
		$this->view->setLayout('list');
		$this->view->assign('title'   , JText::_("COM_JEVLOCATIONS_LOCATIONS_LIST"));
		$jevuser = JEVHelper::getAuthorisedUser();
		$this->view->assign('jevuser',$jevuser);

		// Get/Create the model
		if ($model =  $this->getModel($viewName, "LocationsModel")) {
			// Push the model into the view (as default)
			$this->view->setModel($model, true);
		}

		$this->view->overview();
	}

	/**
	 * Check in of one or more records.
	 *
	 * @return  boolean  True on success
	 *
	 * @since   1.6
	 */
	public function checkin()
	{

	    		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$ids = $this->input->post->get('loc_id', array(), 'array');
		$model = $this->getModel('locations', "LocationsModel");
		//var_dump($ids);die;
		$return = $model->checkin($ids);

		if ($return === false)
		{
			// Checkin failed.
			$message = JText::sprintf('JLIB_APPLICATION_ERROR_CHECKIN_FAILED', $model->getError());
			$this->setRedirect(JRoute::_('index.php?option=com_jevlocations&task=locations.overview', false), $message, 'error');

			return false;
		}
		else
		{
			// Checkin succeeded.
			$message = JText::plural('COM_JEVLOCATIONS_N_ITEMS_CHECKED_IN', count($ids));
			$this->setRedirect(JRoute::_('index.php?option=com_jevlocations&task=locations.overview', false), $message);

			return true;
		}
	}

	function select( )
	{
		//$this->_authoriseAccess();

		// get the view
		$viewName = "locations";
		$this->view = $this->getView($viewName,"html");

		// Set the layout
		$this->view->setLayout('select');
		$this->view->assign('title'   , JText::_("COM_JEVLOCATIONS_LOCATIONS_LIST"));

		$this->fixCreationPermissions();
		$jevuser = JEVHelper::getAuthorisedUser();
		$this->view->assign('jevuser',$jevuser);

		JRequest::setVar('filter_state','P','post');
		// Get/Create the model
		if ($model =  $this->getModel($viewName, "LocationsModel")) {
			// Push the model into the view (as default)
			$this->view->setModel($model, true);
		}

		$this->view->select();
	}

	function edit($key = null, $urlVar = null)
	{
	    $jinput = JFactory::getApplication()->input;

       $cid = JRequest::getVar( 'cid', JRequest::getVar( 'loc_id', array(0), 'post', 'array' ), 'post', 'array' );
       if (is_array($cid) && isset($cid[0])){
           $cid = (int) $cid[0];
       }

		$this->_authoriseAccess($cid);

		// get the view
		$viewName = "locations";
		$this->view = $this->getView($viewName,"html");

		$jinput->set('hidemainmenu', 1 );

		$returntask	= $jinput->get( 'returntask', "locations.overview");

		if ($returntask!="locations.list" && $returntask!="locations.overview" && $returntask!="locations.select"){
			$returntask="locations.overview";
		}
		$this->view->assign('returntask'   , $returntask);

		// Set the layout
		$this->view->setLayout('edit');
		$this->view->assign('title'   , JText::_("COM_JEVLOCATIONS_LOCATION_EDIT"));
		$jevuser = JEVHelper::getAuthorisedUser();
		$this->view->assign('jevuser',$jevuser);

		// Get/Create the model
		if ($model =  $this->getModel("location", "LocationsModel")) {
			// Push the model into the view (as default)
			$this->view->setModel($model, true);
		}

		// Get the media component configuration settings
		$params = JComponentHelper::getParams('com_media');
		// Set the path definitions
		define('JEVP_MEDIA_BASE',    JPATH_ROOT.'/'.$params->get('image_path', 'images/stories'));
		define('JEVP_MEDIA_BASEURL', JURI::root(true).'/'.$params->get('image_path', 'images/stories'));

		$this->view->edit();
	}

	function apply()
	{
        $jinput = JFactory::getApplication()->input;
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );

        $cid = $jinput->post->get('cid', array());

        if(is_array($cid) && $cid[0] === '') {
            $cid[0] = 0;
        }

		if($this->dosave($cid))
		{
			$user = JFactory::getUser();

			$returntask = "locations.edit";

			if ($user->id !=0  && method_exists($this,str_replace("locations.","",$returntask))){
				$returntask = str_replace("locations.","",$returntask);
				//doSave function handles $cid within it's self, so we need to handle it here for new locations on save too.
				if(is_array($cid)) {
				    $cid = (int) $cid[0];
				}
                if ($cid === 0){
                    // get last saved location id from registry
                    $reg = JRegistry::getInstance("com_jevlocations");
                    $cid = $reg->get("last_loc_id", 0);
                    JRequest::setVar( 'cid', $cid);
                }

				return $this->$returntask();
			}

			$tmpl = "";
			if ($returntask == "locations.select" || JRequest::getString("tmpl","") == "component"){
				$tmpl = "&tmpl=component";
			}

			// anon users only
			$Itemid = JFactory::getApplication()->getMenu()->getActive()->id; // RSH 10/11/10 Make 1.5/1.6 compatible
			if ($user->id ==0) {
				die('2');
				$link = JRoute::_('index.php?option=com_jevlocations&task='.$returntask. $tmpl."&Itemid=".$Itemid	);
			}
			else {
				die('3');
				$link = JRoute::_('index.php?option=com_jevlocations&task='.$returntask. $tmpl."&Itemid=".$Itemid	);
			}
			$this->setRedirect($link, $msg); //$msg not set...
		}
	}

	function batch($model = null){
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('locations', '', array());

		// Preset the redirect
		$this->setRedirect(JRoute::_('index.php?option=com_jevlocations&task=locations.overview' . $this->getRedirectToListAppend(), false));

		$vars = $this->input->post->get('batch', array(), 'array');
		$cid  = $this->input->post->get('loc_id', array(), 'array');

		$model->batch($vars, $cid);

		return parent::batch($model);
	}

	function cancel($key = null){
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );

		$cid = JRequest::getVar( 'cid', JRequest::getVar( 'loc_id', array(0), 'post', 'array' ), 'post', 'array' );
		$this->_authoriseAccess((int) $cid[0]);

		$model =  $this->getModel("location", "LocationsModel");

		$model->getData();

		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();

		if (JRequest::getString("tmpl","")=="component" && JRequest::getString('pop',0)==1) {
			ob_end_clean();
				?>
				<script type="text/javascript">
					window.parent.jQuery('#jevLocationModal', window.parent.document).modal('hide');
				</script>
				<?php
				exit();
		}

		$user = JFactory::getUser();
		if ($user->id ==0) {
			$returntask	= JRequest::getVar( 'returntask', "locations.locations");
			if ($returntask!="locations.select"){
				JRequest::setVar("returntask","locations.locations");
			}

		}

		$returntask	= JRequest::getVar( 'returntask', "locations.overview");
		if ($returntask!="locations.list" && $returntask!="locations.overview" && $returntask!="locations.select"  && $returntask!="locations.locations") {
			$returntask="locations.overview";
		}
		$tmpl = "";

		if ($user->id !=0  && method_exists($this,str_replace("locations.","",$returntask))){
			$returntask = str_replace("locations.","",$returntask);
			return $this->$returntask();
		}
		if ($returntask=="locations.select" || JRequest::getString("tmpl","")=="component"){
			$tmpl ="&tmpl=component";
		}
		$link = JRoute::_('index.php?option=com_jevlocations&task='.$returntask . $tmpl);
		$this->setRedirect($link);
	}

	function dosave($cid = array(0))
	{
	    $jinput = JFactory::getApplication()->input;
		$params = JComponentHelper::getParams("com_jevlocations");
		$post	= $jinput->post->getArray(array(), null, 'RAW');

		if(!$params->get("allowraw", 0))
		{
			if (version_compare(JVERSION, '3.7.1', '>='))
			{
				$filter = JFilterInput::getInstance(null, null, 1, 1);

				//Joomla! no longer provides HTML allowed in JInput so we need to fetch raw
				//Then filter on through with JFilterInput to HTML

				foreach ($post as $key => $row)
				{
					//Single row check
					if (!is_array($row))
					{
						$post[$key] = $filter->clean($row, 'HTML');
					}
					else
					{
						//1 Deep row check
						foreach ($post[$key] as $key1 => $sub_row)
						{
							//2 Deep row check
							if (!is_array($sub_row))
							{
								$post[$key][$key1] = $filter->clean($sub_row, 'HTML');
							}
							else
							{
								foreach ($sub_row as $key2 => $sub_sub_row)
								{
									$post[$key][$key1][$key2] = $filter->clean($sub_sub_row, 'HTML');
								}
							}
						}
					}
				}
			}
		}

		$post['loc_id'] = (int) $cid[0];
		$this->_authoriseAccess($post['loc_id']);

		// ensure no dodgy setting of global values !
		$jevuser = JEVHelper::getAuthorisedUser(); // Not being used...


		$user = JFactory::getUser();
		if (!JevLocationsHelper::canCreateGlobal())
		{
			$post["global"]=0;
		}
		else if (!isset($post["global"]))
		{
			// anon users only - set global true and published false
			if ($user->id == 0)
			{
				$post["global"]=1;
				$post["published"]=$params->get("anonpublished",0);
				$returntask	= $jinput->get( 'returntask', "locations.locations");
				if ($returntask!="locations.select")
				{
				    $jinput->set("returntask","locations.locations");
				}
			}
		}
		else
		{
			if ($user->id == 0)
			{
				$post["published"]=$params->get("anonpublished",0);
			}
		}

		//We check now max number of own locations
		if( ($post['loc_id'] == 0) && !$post["global"] && !JevLocationsHelper::canCreateOwn())
		{			
			return false;
		}

		$model = $this->getModel("location", "LocationsModel");

		if ($model->store($post))
		{
            $msg = JText::_( 'COM_JEVLOCATIONS_LOCATION_SAVED' , true);
			$params = JComponentHelper::getParams("com_jevlocations");
			if ($user->id ==0 && !$params->get("anonpublished",0)) {
				$msg = JText::_( 'COM_JEVLOCATIONS_LOCSAVED_REVIEWED' );
			}
			if ($user->id == 0) {
				$location = $model->lastrow;
				$this->notifyAdmin($location);
			}

			JFactory::getApplication()->enqueueMessage($msg, 'message');

			if ($this->task === 'savecopy') {
			    //Set the new loc id for correct redirection.
			    $jinput->set('loc_id', $model->lastrow->loc_id);
			    $jinput->set('cid', array(0 => $model->lastrow->loc_id));
            }

		}
		else
		{
			$msg = JText::_( 'COM_JEVLOCATIONS_ERROR_SAVING_LOCATION', true)." - ". $model->getError() ;
			JFactory::getApplication()->enqueueMessage($msg, 'error');
			return false;
		}

		// Check the table in so it can be edited.... we are done with it anyway
		$model->checkin();

		if (JRequest::getString("tmpl","") == "component" )
		{
			ob_end_clean();
				?>
				<?php

				if (isset($model->lastrow->loc_id) && $model->lastrow->loc_id>0) {

				?>
				<div id="location"><?php echo $model->lastrow->title;?></div>
				 <?php } ?>
				<script type="text/javascript">
					function selectThisLocation(locid, elem){
						var title = elem.innerHTML;
						var locn = window.parent.document.getElementById('locn');
						if (locn){
							locn.value  = locid;
						}
						var evlocation = window.parent.document.getElementById('evlocation');
						if (evlocation){
							evlocation.value = title;
							// If actually selecting a location for a menu item we do something different:
							var menu_location = window.parent.document.getElementById('menu_location');
							if (menu_location){
								menu_location.value = "jevl:"+locid;
							}
						}
						// else this is a menu so do something different
						else {
							return window.parent.sortableLocations.selectThisLocation(locid, elem);
						}

						window.parent.jQuery('#jevLocationModal', window.parent.document).modal('hide');
						return false;
					}

					<?php if (isset($model->lastrow->loc_id) && $model->lastrow->loc_id>0) { ?>
						selectThisLocation(<?php echo $model->lastrow->loc_id;?>, document.getElementById('location'));
					 <?php } ?>

					window.parent.alert("<?php echo $msg;?>");
				</script>
				<?php
				exit();
		}
		return true;
	}

	function save($key = null, $urlVar = null)
	{
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );
		$cid = JRequest::getVar( 'cid', JRequest::getVar( 'loc_id', array(0), 'post', 'array' ), 'post', 'array' );

		if($this->dosave($cid))
		{
			$user = JFactory::getUser();

			$returntask	= JRequest::getVar( 'returntask', "locations.overview");
			if ($returntask!="locations.list" && $returntask!="locations.locations" && $returntask!="locations.overview" && $returntask!="locations.select"){
				$returntask="locations.overview";
			}
			if ($user->id !=0  && method_exists($this,str_replace("locations.","",$returntask))){
				$returntask = str_replace("locations.","",$returntask);
				return $this->$returntask();
			}

			$tmpl = "";
			if ($returntask=="locations.select" || JRequest::getString("tmpl","")=="component"){
				$tmpl ="&tmpl=component";
			}

			// anon users only
			$Itemid = JFactory::getApplication()->getMenu()->getActive()->id; // RSH 10/11/10 Make 1.5/1.6 compatible
			if ($user->id ==0) {
				$link = JRoute::_('index.php?option=com_jevlocations&task=' . $returntask . $tmpl . "&Itemid=" . $Itemid	);
			}
			else {
				$link = JRoute::_('index.php?option=com_jevlocations&task=' . $returntask . $tmpl . "&Itemid=" . $Itemid	);
			}
			$this->setRedirect($link, $msg); // $msg not set.
		}
	}

	function savecopy()
	{
		// Check for request forgeries
        $jinput = JFactory::getApplication()->input;

		JSession::checkToken() or jexit( 'Invalid Token' );

		$save = $this->dosave();

		$user = JFactory::getUser();

		$returntask	= "locations.edit";
		$Itemid = JFactory::getApplication()->getMenu()->getActive()->id; // RSH 10/11/10 Make 1.5/1.6 compatible
        $itemid_string = isset($Itemid) && $Itemid !== '' ? '&Itemid=' . $Itemid : '';
		$tmpl = "";
		if ($returntask=="locations.select" || JRequest::getString("tmpl","")=="component"){
			$tmpl ="&tmpl=component";
		}

		if($jinput->get('task', '') === 'locations.savecopy') {
			$cid = $jinput->get('loc_id');
			$link = 'index.php?option=com_jevlocations&task='.$returntask. '&cid[]=' . $cid . $tmpl. $itemid_string;
			$this->setRedirect($link, $msg); // &msg not defined.
            return true;
		}

		if ($user->id != 0  && method_exists($this,str_replace("locations.","",$returntask))){
			$returntask = str_replace("locations.","",$returntask);
			return $this->$returntask();
		}

		// anon users only
		if ($user->id ==0) {
			$link = JRoute::_('index.php?option=com_jevlocations&task='.$returntask. $tmpl."&Itemid=".$Itemid	);
		}
		else {
			$link = JRoute::_('index.php?option=com_jevlocations&task='.$returntask. $tmpl."&Itemid=".$Itemid	);
		}
		$this->setRedirect($link, $msg); // &msg not defined.
	}


	function delete()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );

		$cid = JRequest::getVar( 'cid', JRequest::getVar( 'loc_id', array(), 'post', 'array' ), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$this->_authoriseAccess($cid);

		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'COM_JEVLOCATIONS_SELECT_ITEM_TO_DELETE' ) );
		}

		$model =  $this->getModel("location", "LocationsModel");
		$model->delete($cid);

		$returntask	= JRequest::getVar( 'returntask', "locations.overview");
		if ($returntask!="locations.list" && $returntask!="locations.overview" && $returntask!="locations.select"){
			$returntask="locations.overview";
		}
		if (method_exists($this,str_replace("locations.","",$returntask))){
			$returntask = str_replace("locations.","",$returntask);
			return $this->$returntask();
		}

		$tmpl = "";
		if (JRequest::getString("tmpl","")=="component"){
			$tmpl ="&tmpl=component";
		}

		$this->setRedirect( JRoute::_('index.php?option=com_jevlocations&task=locations.list' . $tmpl));
	}


	function publish()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );

		$cid = JRequest::getVar( 'cid', JRequest::getVar( 'loc_id', array(), 'post', 'array' ), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$this->_authoriseAccess($cid);

		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'COM_JEVLOCATIONS_SELECT_ITEM_TO_PUBLISH' ) );
		}

		$model =  $this->getModel("location", "LocationsModel");
		if(!$model->publish($cid, 1)) {
			echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
		}

		$returntask	= JRequest::getVar( 'returntask', "locations.overview");
		if ($returntask!="locations.list" && $returntask!="locations.overview" && $returntask!="locations.select"){
			$returntask="locations.overview";
		}
		if (method_exists($this,str_replace("locations.","",$returntask))){
			$returntask = str_replace("locations.","",$returntask);
			return $this->$returntask();
		}

		$tmpl = "";
		if ( JRequest::getString("tmpl","")=="component"){
			$tmpl ="&tmpl=component";
		}

		$this->setRedirect(JRoute::_( 'index.php?option=com_jevlocations&task=locations.list' . $tmpl));
	}


	function unpublish()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );

		$cid = JRequest::getVar( 'cid', JRequest::getVar( 'loc_id', array(), 'post', 'array' ), 'post', 'array' );
		JArrayHelper::toInteger($cid);

		$this->_authoriseAccess($cid);

		if (count( $cid ) < 1) {
			JError::raiseError(500, JText::_( 'COM_JEVLOCATIONS_SELECT_ITEM_TO_UNPUBLISH' ) );
		}

		$model =  $this->getModel("location", "LocationsModel");
		if(!$model->publish($cid, 0)) {
			echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
		}

		$returntask	= JRequest::getVar( 'returntask', "locations.overview");
		if ($returntask!="locations.list" && $returntask!="locations.overview" && $returntask!="locations.select"){
			$returntask="locations.overview";
		}
		if (method_exists($this,str_replace("locations.","",$returntask))){
			$returntask = str_replace("locations.","",$returntask);
			return $this->$returntask();
		}

		$tmpl = "";
		if (JRequest::getString("tmpl","")=="component"){
			$tmpl ="&tmpl=component";
		}

		$this->list();
		//$this->setRedirect( JRoute::_('index.php?option=com_jevlocations&task=locations.list'.$tmpl) );
	}


	function globalise()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );
		if (JevLocationsHelper::canCreateGlobal()){

		$cid = JRequest::getVar( 'cid', JRequest::getVar( 'loc_id', array(), 'post', 'array' ), 'post', 'array' );
			JArrayHelper::toInteger($cid);

			$this->_authoriseAccess($cid);

			if (count( $cid ) < 1) {
				JError::raiseError(500, JText::_( 'COM_JEVLOCATIONS_SELECT_ITEM_TO_PUBLISH' ) );
			}

			$model =  $this->getModel("location", "LocationsModel");
			if(!$model->globalise($cid, 1)) {
				echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
			}

			$returntask	= JRequest::getVar( 'returntask', "locations.overview");
			if ($returntask!="locations.list" && $returntask!="locations.overview" && $returntask!="locations.select"){
				$returntask="locations.overview";
			}
			if (method_exists($this,str_replace("locations.","",$returntask))){
				$returntask = str_replace("locations.","",$returntask);
				return $this->$returntask();
			}

			$tmpl = "";
			if ( JRequest::getString("tmpl","")=="component"){
				$tmpl ="&tmpl=component";
			}
		}
		$this->setRedirect(JRoute::_( 'index.php?option=com_jevlocations&task=locations.list' . $tmpl));
	}


	function privatise()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );

		if (JevLocationsHelper::canCreateGlobal()){

		$cid = JRequest::getVar( 'cid', JRequest::getVar( 'loc_id', array(), 'post', 'array' ), 'post', 'array' );
			JArrayHelper::toInteger($cid);

			$this->_authoriseAccess($cid);

			if (count( $cid ) < 1) {
				JError::raiseError(500, JText::_( 'COM_JEVLOCATIONS_SELECT_ITEM_TO_UNPUBLISH' ) );
			}

			$model =  $this->getModel("location", "LocationsModel");
			if(!$model->globalise($cid, 0)) {
				echo "<script> alert('".$model->getError(true)."'); window.history.go(-1); </script>\n";
			}

			$returntask	= JRequest::getVar( 'returntask', "locations.overview");
			if ($returntask!="locations.list" && $returntask!="locations.overview" && $returntask!="locations.select"){
				$returntask="locations.overview";
			}
			if (method_exists($this,str_replace("locations.","",$returntask))){
				$returntask = str_replace("locations.","",$returntask);
				return $this->$returntask();
			}

			$tmpl = "";
			if (JRequest::getString("tmpl","")=="component"){
				$tmpl ="&tmpl=component";
			}

		}
		//$this->list();
		$this->setRedirect( JRoute::_('index.php?option=com_jevlocations&task=locations.list'.$tmpl) );
	}

	function upload(){

		// Check for request forgeries
		JRequest::checkToken( 'request' ) or jexit( 'Invalid Token' );

		$this->view = $this->getView("locations","html");

		if (!JevLocationsHelper::canUploadImages()){
				$this->view->setLayout('noauth');
				$this->view->assign('msg'   , JText::_("COM_JEVLOCATIONS_NOT_AUTHORISED"));
				$this->view->display();
				return;
		}

		$this->view->setLayout('upload');

		$folder		= JRequest::getVar( 'folder', '', '', 'path' );
		$field		= JRequest::getVar( 'field', '', '' );

		// Get the media component configuration settings
		$params = JComponentHelper::getParams('com_media');
		// Set the path definitions
		define('JEVP_MEDIA_BASE',    JPATH_ROOT.'/'.$params->get('image_path', 'images/stories'));
		define('JEVP_MEDIA_BASEURL', JURI::root(true).'/'.$params->get('image_path', 'images/stories'));

		$jevLocationsHelper = new JevLocationsHelper();
		foreach ($_FILES as $fname=>$file) {
			if ($fname != $field."_file") continue;
			if (strpos($fname,"image")===0){
				$filename = $jevLocationsHelper->processImageUpload($fname);
				$this->view->assign("filetype","image");
				$oname = $_FILES[$fname]['name'];
				$this->view->assign("oname",$oname);
			}
		}
		$this->view->assign("fname",$field."_file");
		$this->view->assign("filename",$filename);
		$this->view->display();

	}


	function orderup()
	{
		$this->_authoriseAccess();

		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );

		$model =  $this->getModel("locations", "LocationsModel");
		$model->move(-1);

		$tmpl = "";
		if (JRequest::getString("tmpl","")=="component"){
			$tmpl ="&tmpl=component";
		}

		$this->setRedirect( JRoute::_('index.php?option=com_jevlocations&task=locations.list' .$tmpl));
	}

	function orderdown()
	{
		$this->_authoriseAccess();

		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );

		$model =  $this->getModel("locations", "LocationsModel");
		$model->move(1);
		$tmpl = "";
		if (JRequest::getString("tmpl","")=="component"){
			$tmpl ="&tmpl=component";
		}

		$this->setRedirect( JRoute::_('index.php?option=com_jevlocations&task=locations.list'.$tmpl) );
	}

	function saveorder()
	{
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );

		$cid = JRequest::getVar( 'cid', JRequest::getVar( 'loc_id', array(), 'post', 'array' ), 'post', 'array' );
		$order 	= JRequest::getVar( 'order', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);
		JArrayHelper::toInteger($order);

		$this->_authoriseAccess($cid);

		$model =  $this->getModel("location", "LocationsModel");
		$model->saveorder($cid, $order);

		$tmpl = "";
		if (JRequest::getString("tmpl","")=="component"){
			$tmpl ="&tmpl=component";
		}

		$msg = 'New ordering saved';
		$this->setRedirect( JRoute::_('index.php?option=com_jevlocations&task=locations.list' .  $tmpl, false),$msg);
	}


	/**
	 * This mechanism currently only checks to see if user is authorised to do anything to locations
	 *
	 * @param unknown_type $locid
	 */
	function _authoriseAccess($locid=0)
	{
		$jeventsParams = JComponentHelper::getParams('com_jevents');
		$params = JComponentHelper::getParams("com_jevlocations");

		$authorisedOnly = $jeventsParams->get("authorisedonly",0);

		$jevuser = JEVHelper::getAuthorisedUser();

		if (is_null($jevuser))
		{
			if (!$authorisedOnly)
			{
				$juser = JFactory::getUser();

				if( $juser->authorise('core.create', 'com_jevlocations'))
				{
					return true;
				}
			}

			// check if anonymous users can add or save locations
			if (!isset($this->_task))
			{
				$this->_task = $this->task;
			}

			if (($locid==0 && $this->_task=='save') || (is_array($locid) && isset($locid[0]) && $locid[0]==0 && $this->_task=='edit' ) || $this->_task=='cancel')
			{
				$anoncreate = $params->get("anoncreate",25);
				$user = JFactory::getUser();
				if ($anoncreate && $user->id==0)
				{
					// Now make sure captcha is valid
					if ($this->_task=='save')
					{
						$plugin = JPluginHelper::getPlugin('jevents', 'jevanonuser' );
						$pluginparams = new JRegistry($plugin->params);

						if ($pluginparams->get("recaptchapublic",false))
						{
							$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible
							$jevrecaptcha = $mainframe->getUserState("jevrecaptcha");

							if ($jevrecaptcha == "ok")
							{
								$mainframe->setUserState("jevrecaptcha","error");
								return true;
							}
							// Belt and braces
							if (version_compare(JVERSION, "1.6.0", 'ge'))
							{
								require_once(JPATH_SITE.'/plugins/jevents/jevanonuser/anonuserlib/recaptcha.php');
							}
							else
							{
								require_once(JPATH_SITE.'/plugins/jevents/anonuserlib/recaptcha.php');
							}

							$response = recaptcha_check_answer($pluginparams->get("recaptchaprivate",false),JRequest::getString("REMOTE_ADDR","","server"), JRequest::getString("recaptcha_challenge_field"),JRequest::getString("recaptcha_response_field"));

							if ($response->is_valid)
							{
								// check for valid email and name ?
								$mainframe->setUserState("jevrecaptcha","ok");
								return true;
							}
							else
							{
								JFactory::getLanguage()->load( 'plg_jevents_jevanonuser',JPATH_ADMINISTRATOR );
								echo "<script> alert('".JText::_("COM_JEVLOCATIONS_RECAPTCHA_ERROR",true)."'); window.history.go(-1); </script>\n";
								exit();
							}
						}
					}
					else
					{
						return true;
					}
				}
				else if ($jevuser->cancreateown || $jevuser->cancreateglobal)
				{
					// if jevents is not in authorised only mode then switch off this user's permissions
					if (!$authorisedOnly)
					{
						$maxOwnLocations = $params->get("max_art",5);
						$juser = JFactory::getUser();

						if( !$juser->authorise('core.create', 'com_jevlocations'))
						{
								$jevuser->cancreateown=false;
								$jevuser->cancreateglobal=false;
						}

					}

					return true;
				}

		$isLocationCreator = false;
		$dispatcher = JEventDispatcher::getInstance();
		$dispatcher->trigger('isLocationCreator', array(& $isLocationCreator));
		if ($isLocationCreator)
		{
			return $isLocationCreator;
		}

		$this->setRedirect( 'index.php', JText::_("COM_JEVLOCATIONS_NOT_AUTHORISED") );
		$this->redirect();
			}
		}
	}

	function fixCreationPermissions()
	{
		$jevuser = JEVHelper::getAuthorisedUser();
		
		if 	($jevuser && ($jevuser->cancreateown || $jevuser->cancreateglobal))
		{
			// if jevents is not in authorised only mode then switch off this user's permissions
			$params = JComponentHelper::getParams('com_jevents');
			$authorisedonly = $params->get("authorisedonly",0);

			if (!$authorisedonly)
			{
				$params = JComponentHelper::getParams("com_jevlocations");
				$loc_own = $params->get("loc_own",25);
				$juser = JFactory::getUser();

				if (!$juser->authorise('core.create', 'com_jevlocations'))
				{
					$jevuser->cancreateown=false;
					$jevuser->cancreateglobal=false;
				}

				$loc_global = $params->get("loc_global",24);

				if (version_compare(JVERSION, "1.6.0", 'ge'))
				{
					if( !$juser->authorise('core.createglobal', 'com_jevlocations'))
					{
						$jevuser->cancreateown=false;
						$jevuser->cancreateglobal=false;
					}
				}
				else if ($juser->gid < intval($loc_global))
				{
					$jevuser->cancreateown=false;
					$jevuser->cancreateglobal=false;
				}
			}

			return true;
		}
	}


	private function notifyAdmin($location){
		$params = JComponentHelper::getParams("com_jevlocations");
		$anoncreate = $params->get("anoncreate",25);
		$user = JFactory::getUser();
		if ($anoncreate && $user->id==0){
			$locadmin = $params->get("locadmin",62);

			$subject = $params->get("notifysubject","");
			$message = $params->get("notifymessage","");
			if ($subject =="" || $message =="") return;
			$message = str_replace("{TITLE}",$location->title,$message);
			$message = str_replace("{NAME}",$location->anonname,$message);
			$message = str_replace("{EMAIL}",$location->anonemail,$message);
			$adminuser = JEVHelper::getUser($locadmin);
			JFactory::getMailer()->sendMail($adminuser->email,$adminuser->name,$adminuser->email,$subject,$message);
		}
	}
}
