<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd, 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

defined( '_JEXEC'  ) or die( 'Restricted access' );

// TODO replace with JevDate

class JevLocationsHTML{


	/**
	 * Build HTML selection list of categories
	 *
	 * @param int $catid				Selected catid
	 * @param string $args				Additional HTML attributes for the <select> tag
	 * @param string $catidList			Restriction list of categories
	 * @param boolean $with_unpublished	Set true to build list with unpublished categories
	 * @param boolean $require_sel		First entry: true = Choose one category, false = All categories
	 * @param int $catidtop				Top level category ancestor
	 */
	public static function buildCategorySelect( $catid, $args, $catidList=null, $with_unpublished=false, $require_sel=false, $catidtop=0, $fieldname="catid", $sectionname=JEV_COM_COMPONENT, $excludeid=false, $order="ordering")
	{

		$user = JFactory::getUser();
		$db	= JFactory::getDBO();

		$catsql = 'SELECT c.id, c.published, c.title as ctitle,p.title as ptitle, gp.title as gptitle, ggp.title as ggptitle ' .
		", c.ordering as ordering".
		// for Joomfish onlu
		' , p.id as pid, gp.id as gpid, ggp.id as ggpid '.
		' FROM #__jevlocation_categories AS c' .
		' LEFT JOIN #__jevlocation_categories AS p ON p.id=c.parent_id' .
		' LEFT JOIN #__jevlocation_categories AS gp ON gp.id=p.parent_id ' .
		' LEFT JOIN #__jevlocation_categories AS ggp ON ggp.id=gp.parent_id ' .
		//' LEFT JOIN #__jevlocation_categories AS gggp ON gggp.id=ggp.parent_id ' .
		 "WHERE c.access  " . (version_compare(JVERSION, '1.6.0', '>=') ?  ' IN (' . JEVHelper::getAid($user) . ')'  :  ' <=  ' . JEVHelper::getAid($user)) .
		 ' AND c.section = '. $db->Quote($sectionname);

		if ($with_unpublished) {
			$catsql .= ' AND c.published >= 0';
		} else {
			$catsql .= ' AND c.published = 1';
		}
		if ($excludeid) $catsql .= ' AND c.id NOT IN ('.$excludeid.')';
		if (is_string($catidList) && strlen(trim($catidList)) ) {
			$catsql .=' AND c.id IN (' . trim($catidList) . ')';
		}
		$catsql .=" ORDER BY c.ordering";

		$db->setQuery($catsql);
		//echo $db->_sql;
		$rows = $db->loadObjectList('id');

		$dispatcher	= JEventDispatcher::getInstance();
		$dispatcher->trigger('onGetCategoryData', array (& $rows));

		foreach ($rows as $key=>$option) {
			$title = $option->ctitle;
			if (!is_null($option->ptitle)){
				// this doesn't; work in Joomfish
				//$title = $option->ptitle."=>".$title;
				if (array_key_exists($option->pid,$rows)){
					$title = $rows[$option->pid]->ctitle."=>".$title;
				}
				else {
					$title = $option->ptitle."=>".$title;
				}
			}
			if (!is_null($option->gptitle)){
				// this doesn't; work in Joomfish
				//$title = $option->gptitle."=>".$title;
				if (array_key_exists($option->gpid,$rows)){
					$title = $rows[$option->gpid]->ctitle."=>".$title;
				}
				else {
					$title = $option->gptitle."=>".$title;
				}
			}
			if (!is_null($option->ggptitle)){
				// this doesn't; work in Joomfish
				//$title = $option->ggptitle."=>".$title;
				if (array_key_exists($option->ggpid,$rows)){
					$title = $rows[$option->ggpid]->ctitle."=>".$title;
				}
				else {
					$title = $option->ggptitle."=>".$title;
				}
			}
			/*
			if (!is_null($option->gggptitle)){
			$title = $option->gggptitle."=>".$title;
			}
			*/
			$rows[$key]->name = $title;
		}
		JArrayHelper::sortObjects($rows, $order);

		$t_first_entry = ($require_sel) ? JText::_('COM_JEVLOCATIONS_EVENT_CHOOSE_CATEGORY') : JText::_('COM_JEVLOCATIONS_ALL_CATEGORIES');
		//$categories[] = JHTML::_('select.option', '0', JText::_('JEV_EVENT_CHOOSE_CATEG'), 'id', 'name' );
		$categories[] = JHTML::_('select.option', '0', $t_first_entry, 'id', 'name' );


		if ($with_unpublished) {
			for ($i=0;$i<count($rows);$i++) {
				if ($rows[$i]->published == 0) $rows[$i]->name = $rows[$i]->name . '('. JText::_('JEV_NOT_PUBLISHED') . ')';
			}
		}

		$categories = array_merge( $categories, $rows );
		$clist = JHTML::_('select.genericlist', $categories, $fieldname, $args, 'id', 'name', $catid );

		return $clist;
	}

	public static function buildCategorySelect2( $catid, $args, $catidList=null, $with_unpublished=false, $require_sel=false, $catidtop=0, $fieldname="catid", $sectionname=JEV_COM_COMPONENT, $excludeid=false, $order="ordering", $multiple=false)
	{
		$user = JFactory::getUser();
		$db	= JFactory::getDBO();

		$options = JHtml::_('category.options', "com_jevlocations", array('filter.published' => array(1)));

		$clist = "";
		if($options)
		{
			if (is_string($catidList) && strlen(trim($catidList)) ) {
				$allowedCatids = explode(",",$catidList);
				JArrayHelper::toInteger($allowedCatids);
				foreach ($options as $index => $option) {
					if (!in_array($option->value, $allowedCatids)){
						unset($options[$index]);
					}
				}
			}

			$t_first_entry = ($require_sel) ? JText::_('COM_JEVLOCATIONS_EVENT_CHOOSE_CATEGORY') : JText::_('COM_JEVLOCATIONS_ALL_CATEGORIES');
			//$categories[] = JHTML::_('select.option', '0', JText::_('JEV_EVENT_CHOOSE_CATEG'), 'id', 'name' );
			$categories[] = JHTML::_('select.option', '0', $t_first_entry, 'value', 'text' );


			if ($with_unpublished) {
				for ($i=0;$i<count($options);$i++) {
					if ($options[$i]->disable == 1) $options[$i]->disable = $options[$i]->disable . '('. JText::_('JEV_NOT_PUBLISHED') . ')';
				}
			}

			$categories = array_merge( $categories, $options );
			if($multiple)
			{
				$clist ='<select name="'.$fieldname.'" '.$args.' multiple="multiple" size="4" style="width:300px;">';
				$clist .= JHtml::_('select.options',$options,'value','text',explode(',',$catid));
				$clist .='</select>';
			}
			else
			{
				$clist = JHTML::_('select.genericlist', $categories, $fieldname, $args,'value','text',$catid);
			}
		}

		return $clist;
	}

	public static function buildCreatorSelect($selected)
	{
		$db	= JFactory::getDBO();

		$query = $db->getQuery(true);
		$query->select('CASE a.created_by
								WHEN "0" THEN a.anonemail
								ELSE a.created_by END	AS id,
							CASE a.created_by
								WHEN "0" THEN CONCAT(a.anonname,"
								(' . JText::_('COM_JEVLOCATIONS_OVERVIEW_FILTER_BY_ANONYMOUS') .')")
								ELSE b.username END	AS name')
		->from('#__jev_locations AS a')
		->join('LEFT OUTER', '#__users AS b ON b.id = a.created_by')
		->group('a.created_by');

		$db->setQuery($query);

		$creators = $db->loadAssocList();

		$filter = array_unshift($creators, array('id'=>'','name'=>JText::_('COM_JEVLOCATIONS_OVERVIEW_FILTER_BY_CREATED_BY')) );
		$filter = JHTML::_('select.genericlist', $creators, 'filter_created_by', 'onchange="Joomla.submitform();"', 'id', 'name',$selected);

		return $filter;
	}
	public static function buildMultiCategorySelect($fieldname="jevloccats")
	{
		$loccats = JHtml::_('category.options', "com_jevlocations");

		$input = "<div style='float: left;text-align:left;'>";
		$input .= "<select multiple='multiple' id='jevloccats' name='".$fieldname."'  size='5' onchange='updateJevLocCats();'>";
		$invalue = array();
		$selected = (count($invalue) == 0) ? "selected='selected'" : "";
		//$input .= "<option value='' $selected>---</option>";
		foreach ($loccats as $loccat)
		{
			$title = $loccat->text;
			$catid = $loccat->value;
			$selected = in_array($loccat->value, $invalue) ? "selected='selected'" : "";
			$input .= "<option value='$catid' $selected>$title</option>";
		}
		$input .= "</select>";
		//$input .= '<input type="hidden"  name="' . $name . '"   id="jevloccat" value="' . $value . '" />';
		$input .= "</div>";

		return $input;
	}

	static public function getURLElement($url)
	{
		if(!empty($url))
		{

			if (strpos($url, "http://") === false && strpos($url, "https://")  === false)
			{
				$url = "http://" . trim($url);
			}

			$searches       = array('http:///', 'https:///');
			$replacements   = array('http://', 'https://');

			$url = str_replace($searches, $replacements, $url);
			$url = '<a href="' . $url . '"  target="_blank" rel="noopener noreferrer">' . $url . '</a>';
		}
		return $url;
	}
}
