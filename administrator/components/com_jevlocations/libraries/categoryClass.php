<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */

defined( '_JEXEC'  ) or die( 'Restricted access' );

class JTableOldCategory extends JTable
{
	/** @var int Primary key */
	var $id					= null;
	/** @var int */
	var $parent_id			= null;
	/** @var string The menu title for the category (a short name)*/
	var $title				= null;
	/** @var string The full name for the category*/
	var $name				= null;
	/** @var string The the alias for the category*/
	var $alias				= null;
	/** @var string */
	var $image				= null;
	/** @var string */
	var $section				= null;
	/** @var int */
	var $image_position		= null;
	/** @var string */
	var $description			= null;
	/** @var boolean */
	var $published			= 1;
	/** @var boolean */
	var $checked_out			= 0;
	/** @var time */
	var $checked_out_time		= 0;
	/** @var int */
	var $ordering			= null;
	/** @var int */
	var $access				= null;
	/** @var string */
	var $params				= null;

	/**
	* @param database A database connector object
	*/
	function __construct( &$db )
	{
		parent::__construct( '#__jevlocation_categories', 'id', $db );
	}

	/**
	 * Overloaded check function
	 *
	 * @access public
	 * @return boolean
	 * @see JTable::check
	 * @since 1.5
	 */
	function check()
	{
		// check for valid name
		if (trim( $this->title ) == '') {
			$this->setError(JText::_( 'JEV_ERR_CAT_MUST_HAVE_NAME') );
			return false;
		}

		// check for existing name
		/*$query = 'SELECT id'
		. ' FROM #__jevlocation_categories '
		. ' WHERE title = '.$this->_db->Quote($this->title)
		. ' AND section = '.$this->_db->Quote($this->section)
		;
		$this->_db->setQuery( $query );

		$xid = intval( $this->_db->loadResult() );
		if ($xid && $xid != intval( $this->id )) {
			$this->_error = JText::sprintf( 'WARNNAMETRYAGAIN', JText::_( 'CATEGORY' ) );
			return false;
		}*/

		if(empty($this->alias)) {
			$this->alias = $this->title;
		}
		$this->alias = JApplication::stringURLSafe($this->alias);
		if(trim(str_replace('-','',$this->alias)) == '') {
			$datenow = JFactory::getDate();
			$this->alias = $datenow->format("%Y-%m-%d-%H-%M-%S");
		}

		return true;
	}
}


class JevLocationsCategory extends JTableOldCategory {

	// catid is a temporary field to ensure no duplicate mappings are created.
	// this can be removed from database and application after full migration
	var $catid 			= null;

	// security check
	function bind( $array , $section="com_jevlocations") {
		$array['id'] = isset($array['id']) ? intval($array['id']) : 0;
		
		parent::bind($array);
		// Fill in the gaps
		// RSH 10/18/10 - Make J!1.6 Compatable
		if (version_compare(JVERSION, "1.6.0", 'ge')) {
			//$this->extension=$section;
			// TODO - What happened to image_position ??
			$this->name=$this->title;
			$this->section=$section;
			$this->image_position="left";
		} else {
			$this->name=$this->title;
			$this->section=$section;
			$this->image_position="left";
		}	
		return true;		
	}
	
}

