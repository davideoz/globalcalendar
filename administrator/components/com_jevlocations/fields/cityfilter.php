<?php

/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

class JFormFieldCityfilter extends JFormFieldList {

    protected $type = 'Cityfilter';

    public function getOptions() {
        $locparams = JComponentHelper::getParams("com_jevlocations");
        $db = JFactory::getDBO();
        $usecats = $locparams->get("usecats", 0);
        if ($usecats) {
            $rows = array();            
            return $rows;
        } else {

            // Make sure there aren't too many
            $sql = 'SELECT count(distinct(CONCAT(loc.country,loc.state,loc.city))) FROM #__jev_locations as loc where loc.published=1 AND loc.global=1  AND  (loc.country !="" OR loc.state !="" OR loc.city !="")';
            $db->setQuery($sql);
            $count = $db->loadResult();
            $rows = array();
            if (intval($count) < 500)
            {
            
                $sql = 'SELECT distinct(CONCAT_WS(" -> ",loc.country,loc.state,loc.city)) as text, CONCAT_WS(" -> ",loc.country,loc.state,loc.city) as value  FROM #__jev_locations as loc ' .
                        ' WHERE loc.published = 1 AND loc.global=1' .
                        ' AND  loc.state !=""  ' .
                        ' ORDER BY loc.country ASC, loc.state ASC, loc.city ASC';
                $db->setQuery($sql);
                $rows = @$db->loadObjectList();
            }
            return $rows;
        }
    }
    
    function getInput() {
        if (is_string($this->value)){
            $this->value = str_replace(", ",",",$this->value);
            $this->value = str_replace(" , ",",",$this->value);
            $this->value = explode(",", $this->value);
        }
        return parent::getInput();
    }
    

}
