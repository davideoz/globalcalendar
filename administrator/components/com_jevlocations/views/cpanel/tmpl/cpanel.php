<?php defined('_JEXEC') or die('Restricted access'); ?>
<div id="jevents" class="span12">
	<form action="index.php" method="post" id="adminForm" name="adminForm" >
		<?php if (!empty($this->sidebar))
		{
			?>
			<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
			</div>
		<?php		
		}
		?>
		<div id="j-main-container" class="span10">
			<div id="cpanel" class="well well-small clearfix ">
					<?php
			// Must load admin language files
			$lang = JFactory::getLanguage();
			$lang->load("com_jevents", JPATH_ADMINISTRATOR);
			
			$link = "index.php?option=" . JEVEX_COM_COMPONENT . "&task=locations.overview";
			$this->_quickiconButtonWHover($link, "LocationsCool.png", "LocationsHot.png", JText::_('COM_JEVLOCATIONS'), "/administrator/components/com_jevlocations/assets/images/");

			$compparams = JComponentHelper::getParams("com_jevlocations");
			$usecats = $compparams->get("usecats", 0);
			if ($usecats)
			{
				$link = "index.php?option=" . JEVEX_COM_COMPONENT . "&task=categories.list";
				$this->_quickiconButtonWHover($link, "CountryStateCityCool.png", "CountryStateCityHot.png", JText::_('COM_JEVLOCATIONS_COUNTRY_STATE_CITY'), "/administrator/components/com_jevlocations/assets/images/");
			}

			$link = "index.php?option=com_categories&extension=".JEVEX_COM_COMPONENT;
			$this->_quickiconButtonWHover($link, "LocationCategoryCool.png", "LocationCategoryHot.png", JText::_('COM_JEVLOCATIONS_LOCATION_CATEGORIES'), "/administrator/components/com_jevlocations/assets/images/");

			if (JEVHelper::isAdminUser())
			{
				$link = "index.php?option=" . JEV_COM_COMPONENT . "&task=defaults.list&filter_layout_type=jevlocations";
				$this->_quickiconButtonWHover($link, "cpanel/LayoutsCool.png", "cpanel/LayoutsHot.png", JText::_('JEV_LAYOUT_DEFAULTS'), "/administrator/components/" . JEV_COM_COMPONENT . "/assets/images/");
			}

			$link = "index.php?option=" . JEV_COM_COMPONENT . "&task=cpanel.cpanel";
			$this->_quickiconButtonWHover($link, "cpanel/EventsCool.png", "cpanel/EventsHot.png", JText::_('COM_JEVLOCATIONS_RETURN_TO_JEVENTS'), "/administrator/components/" . JEV_COM_COMPONENT . "/assets/images/");
			
			?>
			</div>
		</div>
		<input type="hidden" name="task" value="cpanel" />
		<input type="hidden" name="act" value="" />
		<input type="hidden" name="option" value="<?php echo JEVEX_COM_COMPONENT; ?>" />
	</form>
</div>
