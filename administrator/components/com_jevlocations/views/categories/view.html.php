<?php
/**
 * JEvents Locations Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C)  2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die();

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the component
 *
 * @static
 */
class AdminCategoriesViewCategories extends JViewLegacy
{
	function overview($tpl = null)
	{

		if (version_compare(JVERSION, '3.0', ">=")){
			JHtml::stylesheet('com_jevlocations/jevlocations3x.css',array(),true);
		} else {
			JHTML::stylesheet( 'administrator/components/com_jevlocations/assets/css/jevlocations.css');
		}
		JHTML::stylesheet('administrator/components/com_jevlocations/assets/css/eventsadmin.css');
		// Lets check if we have editted before! if not... rename the custom file.
		if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
		{
			// It is definitely now created, lets load it!
			JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
		}

		$document = JFactory::getDocument();
		$document->setTitle(JText::_('JEV_COUNTRY_STATE_CITY'));

		// Set toolbar items for the page
		$this->section  = JRequest::getString("section","com_jevlocations");
		JToolBarHelper::title( JText::_(  'JEV_COUNTRY_STATE_CITY' ), 'jevlocations' );

		JToolBarHelper::publishList('categories.publish');
		JToolBarHelper::unpublishList('categories.unpublish');
		JToolBarHelper::addNew('categories.edit');
		JToolBarHelper::editList('categories.edit');
		JToolBarHelper::deleteList('','categories.delete');
		JToolBarHelper::spacer();
		JToolBarHelper::custom( 'cpanel.cpanel', 'default.png', 'default.png', JText::_('COM_JEVLOCATIONS_ADMIN_CPANEL'), false );
		//JToolBarHelper::help( 'screen.categories', true);

		JSubMenuHelper::addEntry(JText::_('COM_JEVLOCATIONS_ADMIN_CPANEL'), 'index.php?option='.JEVEX_COM_COMPONENT, true);

		$params = JComponentHelper::getParams(JEVEX_COM_COMPONENT);
		//$section = $params->getValue("section",0);

		$option = JEVEX_COM_COMPONENT;
		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible

		$search	= $mainframe->getUserStateFromRequest( $option.'loccats_search', 'search', '', 'string' );
		$search	= JString::strtolower($search);
		// search filter
		$lists['search']= $search;
		$this->assignRef('lists', $lists);

		// Include jQuery framework
		JHtml::_('jquery.framework');
		JHtml::_('jquery.ui', array('core'));
		JHtml::_('bootstrap.framework');
	}

	function edit($tpl = null)
	{
		JRequest::setVar( 'hidemainmenu', 1 );

		if (version_compare(JVERSION, '3.0', ">=")){
			JHtml::stylesheet('com_jevlocations/jevlocations3x.css',array(),true);
		} else {
			JHTML::stylesheet( 'administrator/components/com_jevlocations/assets/css/jevlocations.css');
		}
		JHTML::stylesheet('administrator/components/com_jevlocations/assets/css/eventsadmin.css');
		// Lets check if we have editted before! if not... rename the custom file.
		if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
		{
			// It is definitely now created, lets load it!
			JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
		}

		$document = JFactory::getDocument();
		$document->setTitle(JText::_('JEV_COUNTRY_STATE_CITY'));

		// Set toolbar items for the page
		$this->section  = JRequest::getString("section","com_jevlocations");
		JToolBarHelper::title( JText::_( 'JEV_COUNTRY_STATE_CITY' ), 'jevlocations' );

		JToolBarHelper::save('categories.save');
		JToolBarHelper::cancel('categories.list');
		//JToolBarHelper::help( 'screen.categories.edit', true);

		JSubMenuHelper::addEntry(JText::_('COM_JEVLOCATIONS_ADMIN_CPANEL'), 'index.php?option='.JEVEX_COM_COMPONENT, true);

		$params = JComponentHelper::getParams(JEVEX_COM_COMPONENT);
		//$section = $params->getValue("section",0);

		// Include jQuery framework
		JHtml::_('jquery.framework');
		JHtml::_('jquery.ui', array('core'));
		JHtml::_('bootstrap.framework');
	}

	/**
	 * Control Panel display function
	 *
	 * @param template $tpl
	 */
	function display($tpl = null)
	{
		$layout = $this->getLayout();
		if (method_exists($this,$layout)){
			$this->$layout($tpl);
		}

		parent::display($tpl);
	}

	function displaytemplate($tpl = null)
	{
		return parent::display($tpl);
	}

	function catFilter(){
		// get list of top level categories for dropdown filter

		// RSH 10/18/10 J!.16 compatable
		if (version_compare(JVERSION, "1.6.0", 'ge')) {
			//$column = 'extension';
			//$parent_id = 1;
			$column = 'section';
			$parent_id = 0;
		} else {
			$column = 'section';
			$parent_id = 0;
		}

		$query = 'SELECT cc.id AS value, cc.title AS text FROM #__jevlocation_categories AS cc WHERE ' . $column . ' = "com_jevlocations" AND parent_id = ' . $parent_id . ' ORDER BY title';
		$db = JFactory::getDBO();
		$db->setQuery($query);
		$options = $db->loadObjectList();

		$any = new stdClass();
		$any->text=JText::_( 'COM_JEVLOCATIONS_ANY' );
		$any->value=-1;
		$options = (count($options) > 0) ? $options : array();
		array_unshift($options,$any);

		return JText::_( 'COM_JEVLOCATIONS_COUNTRY' ) . " : ". JHTML::_('select.genericlist', $options, 'catid', 'class="inputbox" size="1" onchange="form.submit();"', 'value', 'text', $this->catfilter);
	}

	function catFilter2(){
		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible
		// RSH 10/18/10 J!.16 compatable
		if (version_compare(JVERSION, "1.6.0", 'ge')) {
			$column = 'section';
			$parent_id = 1;
		} else {
			$column = 'section';
			$parent_id = 0;
		}
		$catfilter	= intval( $mainframe->getUserStateFromRequest( "cat_catid", 'catid', 0 ));
		if ($catfilter>0) $catfilter = " AND pc.id=$catfilter";
		else $catfilter="";

		// get list of top level categories for dropdown filter
		$query = 'SELECT cc.id AS value, cc.title AS text FROM #__jevlocation_categories AS cc LEFT JOIN #__jevlocation_categories AS pc on cc.parent_id = pc.id WHERE cc.' . $column . ' = "com_jevlocations" ' .$catfilter . ' AND pc.parent_id = ' . $parent_id . ' ORDER BY cc.title';
		$db = JFactory::getDBO();
		$db->setQuery($query);
		$options = $db->loadObjectList();

		$any = new stdClass();
		$any->text=JText::_( 'COM_JEVLOCATIONS_ANY' );
		$any->value=-1;
		$options = (count($options) > 0) ? $options : array();
		array_unshift($options,$any);

		return JText::_( 'COM_JEVLOCATIONS_STATE' ) . " : ". JHTML::_('select.genericlist', $options, 'catid2', 'class="inputbox" size="1" onchange="form.submit();"', 'value', 'text', $this->catfilter2);
	}

}
