<?php
/**
 * copyright (C) 2008 GWE Systems Ltd - All rights reserved
 * @version     $Id$
 * @package     JEvents Locations
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die();

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the component
 *
 * @static
 */
class AdminCatsViewCats extends JViewLegacy  
{
	function overview($tpl = null)
	{
		if (version_compare(JVERSION, '3.0', ">=")){
			JHtml::stylesheet('com_jevlocations/jevlocations3x.css',array(),true);
		} else {
			JHTML::stylesheet( 'administrator/components/com_jevlocations/assets/css/jevlocations.css');  	
		}
		JHTML::stylesheet('administrator/components/com_jevlocations/assets/css/eventsadmin.css');
		// Lets check if we have editted before! if not... rename the custom file.
		if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
		{
			// It is definitely now created, lets load it!
			JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
		}

		$document = JFactory::getDocument();
		$document->setTitle(JText::_( 'CATS' ));
		
		// Set toolbar items for the page
		$this->section  = JRequest::getString("section","com_jevlocations2");
		JToolBarHelper::title( JText::_(  'CATS_LOCATIONS' ), 'jevlocations' );
	
		JToolBarHelper::publishList('cats.publish');
		JToolBarHelper::unpublishList('cats.unpublish');
		JToolBarHelper::addNew('cats.edit');
		JToolBarHelper::editList('cats.edit');
		JToolBarHelper::deleteList('','cats.delete');
		JToolBarHelper::spacer();
		JToolBarHelper::custom( 'cpanel.cpanel', 'default.png', 'default.png', "CONTROL PANEL", false );
		//JToolBarHelper::help( 'screen.cats', true);

		JSubMenuHelper::addEntry(JText::_('COM_JEVLOCATIONS_ADMIN_CPANEL'), 'index.php?option='.JEVEX_COM_COMPONENT, true);
		
		$params = JComponentHelper::getParams(JEVEX_COM_COMPONENT);
		//$section = $params->getValue("section",0);
		
		$option = JEVEX_COM_COMPONENT;
		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible
		
		$search				= $mainframe->getUserStateFromRequest( $option.'loccat_search',			'search',			'',				'string' );
		$search				= JString::strtolower( $search );
		// search filter
		$lists['search']= $search;
		$this->assignRef('lists',		$lists);
		
		JHTML::_('behavior.tooltip');
	}	


	function edit($tpl = null)
	{
		JRequest::setVar( 'hidemainmenu', 1 );
		
		if (version_compare(JVERSION, '3.0', ">=")){
			JHtml::stylesheet('com_jevlocations/jevlocations3x.css',array(),true);
		} else {
			JHTML::stylesheet( 'administrator/components/com_jevlocations/assets/css/jevlocations.css');  	
		}
		JHTML::stylesheet('administrator/components/com_jevlocations/assets/css/eventsadmin.css');
		// Lets check if we have editted before! if not... rename the custom file.
		if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
		{
			// It is definitely now created, lets load it!
			JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
		}

		$document = JFactory::getDocument();
		$document->setTitle(JText::_( 'CATS' ));
		
		// Set toolbar items for the page
		$this->section  = JRequest::getString("section","com_jevlocations2");
		JToolBarHelper::title( JText::_( 'CATS_LOCATIONS' ), 'jevlocations' );
	
		JToolBarHelper::save('cats.save');
		JToolBarHelper::cancel('cats.list');
		//JToolBarHelper::help( 'screen.cats.edit', true);

		JSubMenuHelper::addEntry(JText::_('COM_JEVLOCATIONS_ADMIN_CPANEL'), 'index.php?option='.JEVEX_COM_COMPONENT, true);
		
		$params = JComponentHelper::getParams(JEVEX_COM_COMPONENT);
		//$section = $params->getValue("section",0);
				
		JHTML::_('behavior.tooltip');
	}	

	/**
	 * Control Panel display function
	 *
	 * @param template $tpl
	 */
	function display($tpl = null)
	{
		$layout = $this->getLayout();
		if (method_exists($this,$layout)){
			$this->$layout($tpl);
		} 			

		parent::display($tpl);
	}
	
	function displaytemplate($tpl = null)
	{
		return parent::display($tpl);
	}
	
	function catFilter(){
		// get list of top level cats for dropdown filter
		if (version_compare(JVERSION, "1.6.0", 'ge')) {
			//$column = 'extension';
			//$parent_id = 1;
			$column = 'section';
			$parent_id = 0;
		} else {
			$column = 'section';
			$parent_id = 0;
		}

		$query = 'SELECT cc.id AS value, cc.title AS text FROM #__jevlocation_categories AS cc WHERE ' . $column . ' = "com_jevlocations2" AND parent_id='.$parent_id.' order by title';
		$db = JFactory::getDBO();
		$db->setQuery($query);
		$options = $db->loadObjectList();

		$any = new stdClass();
		$any->text=JText::_( 'COM_JEVLOCATIONS_ANY' );
		$any->value=-1;
		$options = (count($options) > 0) ? $options : array();
		array_unshift($options, $any);  // RSH 10/18/10 Added check for empty array

		return JText::_("JEV_PARENT_CATEGORY") . " : ". JHTML::_('select.genericlist', $options, 'catid', 'class="inputbox" size="1" onchange="form.submit();"', 'value', 'text', $this->catfilter);
	}
}