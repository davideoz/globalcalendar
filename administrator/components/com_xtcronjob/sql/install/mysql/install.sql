DROP TABLE IF EXISTS `#__xtcronjob_tasks`;
CREATE TABLE IF NOT EXISTS `#__xtcronjob_tasks` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `catid` int(11) NOT NULL DEFAULT '0',

    `type` tinyint(1) NOT NULL DEFAULT '0',
    `file` varchar(255) NOT NULL DEFAULT '',

    `ran_at` datetime DEFAULT NULL,
    `enabledlogs` tinyint(4) NOT NULL DEFAULT '0',
    `log_text` longtext NOT NULL,
    `unix_mhdmd` varchar(255) NOT NULL DEFAULT '',
    `last_run` int(1) NOT NULL DEFAULT '1',

    `created` datetime NOT NULL,
    `created_by` int(11) NOT NULL DEFAULT '0',
    `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    `modified_by` int(11) NOT NULL DEFAULT '0',
    `checked_out` int(11) NOT NULL DEFAULT '0',
    `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',

    `ordering` int(11) NOT NULL DEFAULT '0',
    `published` int(11) NOT NULL DEFAULT '1',

    `params` longtext NOT NULL DEFAULT '',

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8;

