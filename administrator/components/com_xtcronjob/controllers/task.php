<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

include_once 'default.php';

/**
 * XTCronjob
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
*/
class XtcronjobControllerTask extends XtcronjobControllerDefault
{
	/**
	 * copy.
	 *
	 * @return	void
	 *
	 * @since	1.5
	 */
	public function copy()
	{
		$user = JFactory::getUser();

		if (!$user->authorise('core.create', 'com_xtcronjob'))
		{
			return JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
		}

		$model = $this->getThisModel();
		$model->setIDsFromRequest();
		$id = $model->getId();

		$item = $model->getItem();
		$key = $item->getKeyName();

		if ($item->$key == $id)
		{
			$item->id = 0;
			$item->title = 'Copy of ' . $item->title;
			$item->ordering = 0;
			$item->created = '0000-00-00 00:00:00';
			$item->created_by = 0;
			$item->modified = '0000-00-00 00:00:00';
			$item->modified_by = 0;
			$item->checked_out_time = '0000-00-00 00:00:00';
			$item->checked_out = 0;
			$item->published = 0;
		}

		$status = $model->save($item);

		// Redirect
		$option = FOFInput::getCmd('option', 'com_xtcronjob', $this->input);
		$view = FOFInput::getCmd('view', 'category', $this->input);
		$url = 'index.php?option=' . $option . '&view=' . $view;

		if (!$status)
		{
			$this->setRedirect($url, $model->getError(), 'error');
		}
		else
		{
			$this->setRedirect($url);
		}

		$this->redirect();
	}
}
