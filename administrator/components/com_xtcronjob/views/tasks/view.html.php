<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * XtcronjobViewTasks
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
 */
class XtcronjobViewTasks extends FOFViewHtml
{
	/**
	 * onDisplay.
	 *
	 * @param   string  $tpl  Param
	 *
	 * @return	void
	 *
	 * @since	1.5
	 */
	protected function onDisplay($tpl = null)
	{
		Extly::initApp();
		Extly::loadAwesome();

		JLoader::import('extly.html.egrid');

		return parent::onDisplay($tpl);
	}
}
