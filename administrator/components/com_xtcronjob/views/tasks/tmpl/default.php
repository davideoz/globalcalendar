<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

$base_folder = rtrim(JUri::base(), '/');

if (substr($base_folder, -13) == 'administrator')
{
	$base_folder = rtrim(substr($base_folder, 0, -13), '/');
}

$this->loadHelper('select');

$hasAjaxOrderingSupport = $this->hasAjaxOrderingSupport();

?>
<div class="extly">
	<div class="row-fluid">
		<div class="span12">

			<form name="adminForm" id="<?php echo Extly::getFormId(); ?>" action="index.php" method="post">
				<input type="hidden" name="option" id="option" value="com_xtcronjob" /> <input type="hidden" name="view" id="view" value="tasks" /> <input type="hidden" name="task" id="task" value="browse" /> <input type="hidden" name="boxchecked" id="boxchecked"
					value="0" /> <input type="hidden" name="hidemainmenu" id="hidemainmenu" value="0" /> <input type="hidden" name="filter_order" id="filter_order" value="<?php echo $this->lists->order ?>" /> <input type="hidden" name="filter_order_Dir"
					id="filter_order_Dir" value="<?php echo $this->lists->order_Dir ?>" /> <input type="hidden" name="<?php echo JFactory::getSession()->getFormToken();?>" value="1" />
				<table class="adminlist table table-striped" id="itemsList" >
					<thead>
						<tr>
							<?php
							if ($hasAjaxOrderingSupport !== false)
							{
								?>
							<th width="20px"><?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'ordering', $this->lists->order_Dir, $this->lists->order, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
							</th>
							<?php
							}
							?>
							<th width="20"><input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
							</th>
							<th><?php echo JHTML::_('grid.sort', 'COM_XTCRONJOB_TASKS_FIELD_TITLE', 'title', $this->lists->order_Dir, $this->lists->order, 'browse'); ?>
							</th>
							<th width="160"><?php echo JHTML::_('grid.sort', 'LBL_TASKS_CATEGORY', 'category_id', $this->lists->order_Dir, $this->lists->order, 'browse'); ?>
							</th>
							<th width="160"><?php echo JHTML::_('grid.sort', 'COM_XTCRONJOB_TASKS_FIELD_TYPE', 'type', $this->lists->order_Dir, $this->lists->order, 'browse'); ?>
							</th>
							<?php
							if ($hasAjaxOrderingSupport === false)
							{
								?>
							<th class="order"><?php echo JHTML::_('grid.sort', 'JFIELD_ORDERING_LABEL', 'ordering', $this->lists->order_Dir, $this->lists->order, 'browse'); ?> <?php
							echo JHTML::_('grid.order', $this->items);
							?>
							</th>
							<?php
							}
							?>
							<th></th>
							<th width="80"><?php echo JHTML::_('grid.sort', 'JPUBLISHED', 'published', $this->lists->order_Dir, $this->lists->order, 'browse'); ?>
							</th>
						</tr>
						<tr >
							<?php
							if ($hasAjaxOrderingSupport !== false)
							{
								?>
							<td></td>
							<?php
							}
							?>
							<td></td>
							<td class="form-inline"><input type="text" name="title" id="title" value="<?php echo $this->escape($this->getModel()->getState('title'));?>" class="input-medium" onchange="document.adminForm.submit();"
								placeholder="<?php echo JText::_('COM_XTCRONJOB_TASKS_FIELD_TITLE') ?>" /> <nobr>
									<button class="btn btn-mini" onclick="this.form.submit();">
										<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>
									</button>
									<a class="xtd-btn-reset"><small><?php echo JText::_('JSEARCH_RESET'); ?></small></a>
								</nobr>
							</td>
							<td><?php echo XTCronjobHelperSelect::categories($this->getModel()->getState('category'), 'category', array('onchange' => 'this.form.submit();', 'class' => 'input-medium')); ?>
							</td>
							<td><?php echo XTCronjobHelperSelect::tasktypes($this->getModel()->getState('type'), 'type', array('onchange' => 'this.form.submit();', 'class' => 'input-medium')) ?>
							</td>
							<?php
							if ($hasAjaxOrderingSupport === false)
							{
								?>
							<td></td>
							<?php
							}
							?>
							<td></td>
							<td><?php echo XTCronjobHelperSelect::published($this->getModel()->getState('published'), 'published', array('onchange' => 'this.form.submit();', 'class' => 'input-small')) ?>
							</td>
						</tr>
					</thead>
					<tfoot>
						<tr >
							<td colspan="20"><?php
							EHtml::renderPagination($this);
							?>
							</td>
						</tr>
					</tfoot>
					<tbody>
						<?php
						if ($count = count($this->items))
						{
							?>
						<?php
						$i = 0;
						$m = 1;

						foreach ($this->items as $item)
						{
							$m = 1 - $m;

							$checkedout = ($item->checked_out != 0);
							$ordering = $this->lists->order == 'ordering';
							?>
						<tr class="row<?php echo $m?>" >
							<?php
							if ($hasAjaxOrderingSupport !== false)
							{
								?>
							<td class="order nowrap center hidden-phone"><?php
							if ($this->perms->editstate)
							{
								$disableClassName = '';
								$disabledLabel	  = '';

								if (!$hasAjaxOrderingSupport['saveOrder'])
								{
									$disabledLabel    = JText::_('JORDERINGDISABLED');
									$disableClassName = 'inactive tip-top';
								}
								?> <span class="sortable-handler <?php echo $disableClassName?>" title="<?php echo $disabledLabel?>" rel="tooltip"> <i class="icon-menu"></i>
							</span> <input type="text" style="display: none" name="order[]" size="5" value="<?php echo $item->ordering;?>" class="input-mini text-area-order " /> <?php
							}
							else
							{
								?> <span class="sortable-handler inactive"> <i class="icon-menu"></i>
							</span> <?php
							}
							?>
							</td>
							<?php
							}
							?>
							<td>
							<?php echo JHTML::_('grid.id', $i, $item->id, $checkedout); ?>
							</td>
							<td><a href="index.php?option=com_xtcronjob&view=tasks&task=edit&id=<?php echo (int) $item->id ?>"> <?php
							echo htmlentities($item->title, ENT_COMPAT, 'UTF-8');

							echo ' (' . $item->unix_mhdmd . ')';

							?>
							</a>
							</td>
							<td><?php echo $item->catid ? XTCronjobHelperSelect::getCategoryName($item->catid) : '&mdash;'; ?>
							</td>
							<td><span class="category-type-<?php echo $item->type ?>"> <?php echo JText::_('COM_XTCRONJOB_TASKS_TYPE_' . strtoupper($item->type)); ?>
							</span>
							</td>
							<?php
							if ($hasAjaxOrderingSupport === false)
							{
								?>
							<td class="order">
							<span class="order-arrow"><?php
							echo $this->pagination->orderUpIcon($i, true, 'orderup', 'Move Up', $ordering);
							?>
							</span>
							<span class="order-arrow"><?php echo $this->pagination->orderDownIcon($i, $count, true, 'orderdown', 'Move Down', $ordering); ?>
							</span>
							<?php
							$disabled = $ordering ?  '' : 'disabled="disabled"';
							?> <input type="text" name="order[]" size="5" value="<?php echo $item->ordering;?>" <?php echo $disabled ?> class="input-ordering" style="text-align: center" />
							</td>
							<?php
							}
							?>
							<td>
							<?php

							echo EHtmlGrid::lockedWithIcons($checkedout);

							?>
							</td>
							<td><?php echo EHtmlGrid::published($item, $i); ?>
							</td>
						</tr>
						<?php
						$i++;
						}

						?>
						<?php
						}
						else
						{
							?>
						<tr>
							<td colspan="10" align="center"><?php echo JText::_('COM_XTCRONJOB_COMMON_NOITEMS_LABEL') ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>

			</form>

		</div>
	</div>
</div>
