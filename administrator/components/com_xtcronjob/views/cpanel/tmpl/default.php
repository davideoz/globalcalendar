<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

?>
<div class="extly">
	<div class="row-fluid">
		<div class="span12">
			<h1>XTCronjob</h1>
			<p>
				<?php echo JText::_('COM_XTCRONJOB_DESCRIPTION'); ?>
			</p>
		</div>
	</div>
</div>
