<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * XtcronjobViewTask
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
 */
class XtcronjobViewTask extends FOFViewHtml
{
	/**
	 * display.
	 *
	 * @param   string  $tpl  Param
	 *
	 * @return	void
	 *
	 * @since	1.5
	 */
	public function display($tpl = null)
	{
		$file = EHtml::getRelativeFile('js', 'com_xtcronjob/xtcronjob.task.min.js');

		if ($file)
		{
			$this->assign('jsApp', $file);
			Extly::initApp(CXTCRONJOB_VERSION, $file);
		}

		return parent::display($tpl);
	}
}
