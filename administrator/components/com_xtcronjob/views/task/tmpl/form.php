<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

$editor = JFactory::getEditor();

$this->loadHelper('select');
$this->loadHelper('filtering');

?>

<div class="extly">

	<form name="adminForm" id="adminForm" action="index.php" method="post" class="form form-horizontal cronjob-expression-form">
		<input type="hidden" name="option" value="com_xtcronjob" /> <input type="hidden" name="view" value="tasks" /> <input type="hidden" name="task" value="" /> <input type="hidden" name="id" value="<?php echo $this->item->id ?>" /> <input type="hidden"
			name="<?php echo JFactory::getSession()->getFormToken();?>" value="1" />

		<div class="row-fluid">

			<div class="span6">

				<div class="control-group">
					<label for="title" class="control-label"><?php echo JText::_('COM_XTCRONJOB_TASKS_FIELD_TITLE'); ?> </label>
					<div class="controls">
						<input type="text" name="title" id="title" value="<?php echo $this->item->title ?>">
					</div>
				</div>

				<div class="control-group">
					<label for="catid" class="control-label"><?php echo JText::_('LBL_TASKS_CATEGORY'); ?> </label>
					<div class="controls">
						<?php echo XTCronjobHelperSelect::categories($this->item->catid, 'catid') ?>
					</div>
				</div>

				<div class="control-group">
					<label for="type" class="control-label"><?php echo JText::_('COM_XTCRONJOB_TASKS_FIELD_TYPE'); ?> </label>
					<div class="controls">
						<?php echo XTCronjobHelperSelect::tasktypes($this->item->type, 'type') ?>
					</div>
				</div>

				<div class="control-group">
					<label for="file" class="control-label"><?php echo JText::_('COM_XTCRONJOB_TASKS_FIELD_FILE'); ?> </label>
					<div class="controls">
						<input type="text" name="file" id="file" value="<?php echo $this->item->file ?>">
					</div>
				</div>

<?php

				echo EHtml::cronjobExpressionControl(
					$this->item->unix_mhdmd,
					'unix_mhdmd',
					'COM_XTCRONJOB_TASKS_FIELD_UNIX_MHDMD',
					'COM_XTCRONJOB_TASKS_FIELD_UNIX_MHDMD_DESC',
					'unix_mhdmd',
					null,
					$this->get('jsApp')
				);

?>

				<div class="control-group">
					<label for="published" class="control-label"><?php echo JText::_('JPUBLISHED'); ?> </label>
					<div class="controls">
						<?php echo EHtmlSelect::booleanlist($this->item->published, 'published'); ?>
					</div>
				</div>

				<div class="control-group">
					<label for="enabledlogs" class="control-label"><?php echo JText::_('COM_XTCRONJOB_TASKS_FIELD_ENABLEDLOGS'); ?> </label>
					<div class="controls">
						<?php echo EHtmlSelect::booleanlist($this->item->enabledlogs, 'enabledlogs'); ?>
					</div>
				</div>

				<div class="control-group">
					<label for="now" class="control-label"><?php
					echo JText::_('JDATE'); ?> </label>
					<div class="controls">
						<input type="text" id="now" value="<?php

						$now = new JDate;
						echo $now;

						?>" class="uneditable-input" readonly="readonly">
					</div>
				</div>

			</div>

			<div class="span6">

				<div class="control-group">
					<label for="title" class="control-label"><?php echo JText::_('COM_XTCRONJOB_TASK_FIELD_LAST_RUN_LABEL'); ?> </label>
					<div class="controls">
						<input type="text" name="disabled_last_run" id="disabled_last_run" class="disabled" value="<?php echo ($this->item->last_run ? 'OK!' : 'Error'); ?>" disabled>
					</div>
				</div>

				<div class="control-group">
					<label for="title" class="control-label"><?php echo JText::_('COM_XTCRONJOB_XTCRONJOB_HEADING_RAN_AT'); ?> </label>
					<div class="controls">
						<input type="text" name="disabled_ran_at" id="disabled_ran_at" class="disabled" value="<?php echo $this->item->ran_at; ?>" disabled>
					</div>
				</div>

				<div class="control-group">
					<label for="catid" class="control-label"><?php echo JText::_('COM_XTCRONJOB_TASK_FIELD_LAST_LOG_LABEL'); ?> </label>
					<div class="controls">
						<textarea name="disabled_log_text" id="disabled_log_text" class="disabled" rows="10" disabled><?php echo $this->item->log_text; ?></textarea>
					</div>
				</div>

			</div>

		</div>
	</form>
</div>
