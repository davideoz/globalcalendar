<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * XtcronjobModelCategories
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
 */
class XtcronjobModelCategories extends FOFModel
{
	/**
	 * buildQuery
	 *
	 * @param   bool  $overrideLimits  Param
	 *
	 * @return	void
	 */
	public function buildQuery($overrideLimits = false)
	{
		$db = $this->getDbo();

		$query = FOFQueryAbstract::getNew($db)
			->select('*')
			->from($db->qn('#__categories'));

		$query->where($db->qn('extension') . ' = ' . $db->q('com_xtcronjob'));

		$fltTitle	= $this->getState('title', null, 'string');

		if ($fltTitle)
		{
			$query->where($db->qn('title') . ' LIKE ' . $db->q("%$fltTitle%"));
		}

		$fltPublished	= $this->getState('published', null, 'cmd');

		if ($fltPublished != '')
		{
			$query->where($db->qn('published') . ' = ' . $db->q($fltPublished));
		}

		$fltFrontend	= $this->getState('frontend', 0, 'int');

		if ($fltFrontend != 0)
		{
			$order = 'ordering';
			$dir = 'ASC';
		}
		else
		{
			$order = $this->getState('filter_order', 'id', 'cmd');

			if (!in_array($order, array_keys($this->getTable()->getData())))
			{
				$order = 'id';
			}

			$dir = $this->getState('filter_order_Dir', 'DESC', 'cmd');
		}

		$query->order($order . ' ' . $dir);

		return $query;
	}
}
