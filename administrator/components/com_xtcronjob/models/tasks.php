<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * XTCronjobModelTasks
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
 */
class XtcronjobModelTasks extends FOFModel
{
	/**
	 * buildQuery
	 *
	 * @param   bool  $overrideLimits  Param
	 *
	 * @return	void
	 */
	public function buildQuery($overrideLimits = false)
	{
		$db = $this->getDbo();

		$query = FOFQueryAbstract::getNew($db)
			->select('*')
			->from($db->quoteName('#__xtcronjob_tasks'));

		$fltTitle		= $this->getState('title', null, 'string');

		if ($fltTitle)
		{
			$fltTitle = "%$fltTitle%";
			$query->where($db->qn('title') . ' LIKE ' . $db->q($fltTitle));
		}

		$fltCategory = $this->getState('category', null, 'int');

		if ($fltCategory)
		{
			$query->where($db->qn('catid') . ' = ' . $db->q($fltCategory));
		}

		$fltType = $this->getState('type', null, 'cmd');

		if ($fltType)
		{
			$query->where($db->qn('type') . ' = ' . $db->q($fltType));
		}

		$fltPublished	= $this->getState('published', null, 'cmd');

		if ($fltPublished != '')
		{
			$query->where($db->qn('published') . ' = ' . $db->q($fltPublished));
		}

		$search = $this->getState('search', null);

		if ($search)
		{
			$search = '%' . $search . '%';
			$query->where($db->qn('title') . ' LIKE ' . $db->quote($search));
		}

		$order = $this->getState('filter_order', 'ordering', 'cmd');

		if (!in_array($order, array_keys($this->getTable()->getData())))
		{
			$order = 'id';
		}

		$dir = $this->getState('filter_order_Dir', 'ASC', 'cmd');
		$query->order($order . ' ' . $dir);

		return $query;
	}
}
