<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * XTCronjobToolbar
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
*/
class XtcronjobToolbar extends FOFToolbar
{
	/**
	 * getMyViews.
	 *
	 * @return	array
	 *
	 * @since	1.5
	 */
	protected function getMyViews()
	{
		$views = array(
				'tasks'
		);

		return $views;
	}

	/**
	 * renderSubmenu.
	 *
	 * @return	void
	 *
	 * @since	1.5
	 */
	public function renderSubmenu()
	{
		parent::renderSubmenu();
		$this->appendLink(JText::_('COM_XTCRONJOB_TITLE_CATEGORIES'), 'index.php?option=com_categories&view=categories&extension=com_xtcronjob');
	}

	/**
	 * onTasksBrowse.
	 *
	 * @return	void
	 *
	 * @since	1.5
	 */
	public function onTasksBrowse()
	{
		$this->_onBrowseWithCopy();
	}

	/**
	 * _onBrowseWithCopy.
	 *
	 * @return	void
	 *
	 * @since	1.5
	 */
	public function _onBrowseWithCopy()
	{
		// Set toolbar title
		$subtitle_key = FOFInput::getCmd('option', 'com_foobar', $this->input) . '_TITLE_' . strtoupper(FOFInput::getCmd('view', 'cpanel', $this->input));
		JToolBarHelper::title(
			JText::_(FOFInput::getCmd('option', 'com_foobar', $this->input)) . ' &ndash; <small>' . JText::_($subtitle_key) . '</small>',
			str_replace('com_', '', FOFInput::getCmd('option', 'com_foobar', $this->input))
		);

		// Add toolbar buttons
		if ($this->perms->create)
		{
			JToolBarHelper::addNew();
		}

		if ($this->perms->edit)
		{
			JToolBarHelper::editList();
		}

		if ($this->perms->create || $this->perms->edit)
		{
			JToolBarHelper::divider();
		}

		if ($this->perms->editstate)
		{
			JToolBarHelper::publishList();
			JToolBarHelper::unpublishList();
			JToolBarHelper::divider();
		}

		if ($this->perms->create)
		{
			JToolBarHelper::custom('copy', 'copy.png', 'copy_f2.png', 'COM_XTCRONJOB_COMMON_COPY_LABEL', false);
			JToolBarHelper::divider();
		}

		if ($this->perms->delete)
		{
			$msg = JText::_(FOFInput::getCmd('option', 'com_foobar', $this->input) . '_CONFIRM_DELETE');
			JToolBarHelper::deleteList($msg);
		}

		$this->renderSubmenu();
	}
}
