<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * XtcronjobHelperSelect
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
*/
class XtcronjobHelperSelect
{
	/**
	 * tasks.
	 *
	 * @param   string  $selected  The key that is selected
	 * @param   string  $id        The id for the field
	 * @param   array   $attribs   Additional HTML attributes for the <select> tag*
	 *
	 * @return  string  HTML
	 *
	 * @since   3.0
	 */
	public static function tasks($selected = null, $id = 'category', $attribs = array())
	{
		$items = FOFModel::getTmpInstance('Tasks', 'XTCronjobModel')
		->nobeunpub(1)
		->getItemList(true);

		$options = array();
		$options[] = JHTML::_('select.option', 0, '- ' . JText::_('COM_XTCRONJOB_COMMON_TASK_SELECT_LABEL') . ' -');

		if (count($items))
		{
			foreach ($items as $item)
			{
				$options[] = JHTML::_('select.option', $item->id, $item->title);
			}
		}

		return self::genericlist($options, $id, $attribs, $selected, $id);
	}

	/**
	 * categories.
	 *
	 * @param   string  $selected  The key that is selected
	 * @param   string  $id        The id for the field
	 * @param   array   $attribs   Additional HTML attributes for the <select> tag*
	 *
	 * @return  string  HTML
	 *
	 * @since   3.0
	 */
	public static function categories($selected = null, $id = 'category', $attribs = array())
	{
		$items = FOFModel::getTmpInstance('Categories', 'XtcronjobModel')
		->getItemList(true);

		$options = array();
		$options[] = JHTML::_('select.option', 0, '- ' . JText::_('LBL_CATEGORY_SELECT') . ' -');

		if (count($items))
		{
			foreach ($items as $item)
			{
				$options[] = JHTML::_('select.option', $item->id, $item->title);
			}
		}

		return self::genericlist($options, $id, $attribs, $selected, $id);
	}

	/**
	 * getCategoryName.
	 *
	 * @param   string  $category_id  Param
	 *
	 * @return  string
	 *
	 * @since   3.0
	 */
	public static function getCategoryName($category_id)
	{
		$categories = null;

		if (is_null($categories))
		{
			$items = FOFModel::getTmpInstance('Categories', 'XtcronjobModel')
			->published(1)
			->getItemList(true);

			if (count($items))
			{
				foreach ($items as $item)
				{
					$categories[$item->id] = $item->title;
				}
			}
		}

		if (array_key_exists($category_id, $categories))
		{
			return $categories[$category_id];
		}
		else
		{
			return '';
		}
	}

	/**
	 * genericlist.
	 *
	 * @param   array   $list      The key that is selected
	 * @param   string  $name      The name for the field
	 * @param   array   $attribs   Additional HTML attributes for the <select> tag*
	 * @param   string  $selected  The key that is selected
	 * @param   string  $idTag     The id for the field
	 *
	 * @return  string  HTML
	 *
	 * @since   3.0
	 */
	protected static function genericlist($list, $name, $attribs, $selected, $idTag)
	{
		if (empty($attribs))
		{
			$attribs = null;
		}
		else
		{
			$temp = '';

			foreach ($attribs as $key => $value)
			{
				$temp .= $key . ' = "' . $value . '"';
			}

			$attribs = $temp;
		}

		return JHTML::_('select.genericlist', $list, $name, $attribs, 'value', 'text', $selected, $idTag);
	}

	/**
	 * tasktypes.
	 *
	 * @param   string  $selected  The key that is selected
	 * @param   string  $id        The id for the field
	 * @param   array   $attribs   Additional HTML attributes for the <select> tag*
	 *
	 * @return  string  HTML
	 *
	 * @since   3.0
	 */
	public static function tasktypes($selected = null, $id = 'type', $attribs = array() )
	{
		$options = array();
		$options[] = JHTML::_('select.option', '', '- ' . JText::_('COM_XTCRONJOB_TASKS_TYPE_SELECT') . ' -');
		$options[] = JHTML::_('select.option', '4', JText::_('COM_XTCRONJOB_TASKS_TYPE_PLUGIN_FOLDER_EVENT'));
		$options[] = JHTML::_('select.option', '5', JText::_('COM_XTCRONJOB_TASKS_TYPE_SSH_COMMAND'));
		$options[] = JHTML::_('select.option', '6', JText::_('COM_XTCRONJOB_TASKS_TYPE_WEB_URL_FOPEN'));
		$options[] = JHTML::_('select.option', '7', JText::_('COM_XTCRONJOB_TASKS_TYPE_WEB_URL_FSOCKOPEN'));
		$options[] = JHTML::_('select.option', '8', JText::_('COM_XTCRONJOB_TASKS_TYPE_CURL_AKEEBA'));

		return self::genericlist($options, $id, $attribs, $selected, $id);
	}

	/**
	 * published.
	 *
	 * @param   string  $selected  The key that is selected
	 * @param   string  $id        The id for the field
	 * @param   array   $attribs   Additional HTML attributes for the <select> tag*
	 *
	 * @return  string  HTML
	 *
	 * @since   3.0
	 */
	public static function published($selected = null, $id = 'enabled', $attribs = array())
	{
		$options = array();
		$options[] = JHTML::_('select.option', '', '- ' . JText::_('COM_XTCRONJOB_COMMON_STATE_SELECT_LABEL') . ' -');
		$options[] = JHTML::_('select.option', 0, JText::_('JUNPUBLISHED'));
		$options[] = JHTML::_('select.option', 1, JText::_('JPUBLISHED'));

		return self::genericlist($options, $id, $attribs, $selected, $id);
	}
}
