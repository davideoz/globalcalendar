<?php

/**
 * @package     Extly.Library
 * @subpackage  lib_extly - Extly Framework
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * TaskManagerHelper class
 *
 * @package     Extly.Library
 * @subpackage  lib_extly
 * @since       3.0
*/
abstract class TaskManagerHelper
{
	const TASKS_TYPE_PLUGIN_NATIVE_EVENT = 1;
	const TASKS_TYPE_PLUGIN_EXTENDED_EVENT = 2;
	const TASKS_TYPE_PLUGIN_RULE_EVENT = 3;
	const TASKS_TYPE_PLUGIN_FOLDER_EVENT = 4;
	const TASKS_TYPE_SSH_COMMAND = 5;
	const TASKS_TYPE_WEB_URL_FOPEN = 6;
	const TASKS_TYPE_WEB_URL_FSOCKOPEN = 7;
	const TASKS_TYPE_CURL_AKEEBA = 8;

	/**
	 * isConfigurablePlugin.
	 *
	 * @param   int  $type  Param
	 *
	 * @return  bool
	 *
	 * @since   3.0
	 */
	public static function isConfigurablePlugin($type)
	{
		return (($type == self::TASKS_TYPE_PLUGIN_NATIVE_EVENT) ||
				($type == self::TASKS_TYPE_PLUGIN_EXTENDED_EVENT) ||
				($type == self::TASKS_TYPE_PLUGIN_RULE_EVENT));
	}

	/**
	 * getConfigurationUrl.
	 *
	 * @param   int     $type    Param
	 * @param   string  $file    Param
	 * @param   string  $option  Param
	 * @param   int     $ruleid  Param
	 *
	 * @return  string
	 *
	 * @since   3.0
	 */
	public static function getConfigurationUrl($type, $file, $option, $ruleid)
	{
		$plugin = self::loadPlugin($file);

		switch ($type)
		{
			case self::TASKS_TYPE_PLUGIN_NATIVE_EVENT:
				return 'index.php?option=com_plugins&task=plugin.edit&extension_id=' . $plugin->id;
				break;
			case self::TASKS_TYPE_PLUGIN_EXTENDED_EVENT:
				return 'index.php?option=' . $option . '&task=xlugin.edit&extension_id=' . $plugin->id . '&rule_id=' . $ruleid;
				break;
			case self::TASKS_TYPE_PLUGIN_RULE_EVENT:
				return 'index.php?option=' . $option . '&task=xlugin.edit&extension_id=' . $plugin->id . '&rule_id=' . $ruleid;
				break;
		}

		return null;
	}

	/**
	 * loadPlugin.
	 *
	 * @param   string  $file  Param
	 *
	 * @return  object
	 *
	 * @since   3.0
	 */
	public static function loadPlugin($file)
	{
		$parts = explode('.', $file);

		if (count($parts) != 3)
		{
			return null;
		}

		$pluginFolder = $parts[0];
		$pluginName = $parts[1];
		$pluginEvent = $parts[2];

		$id = self::_getPluginId($pluginFolder, $pluginName);

		if (empty($id))
		{
			return null;
		}

		$plugin = JPluginHelper::getPlugin($pluginFolder, $pluginName);

		if (empty($plugin))
		{
			return null;
		}

		$plugin->id = $id;

		return $plugin;
	}

	/**
	 * _getPluginId.
	 *
	 * @param   string  $pluginFolder  Param
	 * @param   string  $pluginName    Param
	 *
	 * @return  string
	 *
	 * @since   3.0
	 */
	private static function _getPluginId($pluginFolder, $pluginName)
	{
		$user = JFactory::getUser();
		$levels = implode(',', $user->getAuthorisedViewLevels());

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('extension_id as id')
		->from('#__extensions')
		->where('enabled >= 1')
		->where('type =' . $db->Quote('plugin'))
		->where('state >= 0')
		->where('access IN (' . $levels . ')')
		->where('folder = ' . $db->q($pluginFolder))
		->where('element = ' . $db->q($pluginName))
		->order('ordering');

		return $db->setQuery($query)->loadResult();
	}

	/**
	 * loadPluginFolder.
	 *
	 * @param   string  $folder  Param
	 *
	 * @return  bool
	 *
	 * @since   3.0
	 */
	public static function loadPluginFolder($folder)
	{
		$parts = explode('.', $folder);

		if (count($parts) == 2)
		{
			// The elements are the plugin group and event names respectively:
			$pluginGroup = $parts[0];
			$pluginEvent = $parts[1];
			$c = JPluginHelper::importPlugin($pluginGroup);

			if (count($c) > 0)
			{
				return true;
			}
		}

		return false;
	}
}
