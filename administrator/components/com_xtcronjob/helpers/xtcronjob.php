<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * xtcronjob component helper.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_content
 * @since       1.6
 */
class XtcronjobHelper
{
	public static $extension = 'com_xtcronjob';

	/**
	 * Configure the Linkbar.
	 *
	 * @param   string  $vName  The name of the active view.
	 *
	 * @return  void
	 */
	public static function addSubmenu($vName)
	{
		JLoader::import('extly.extlyframework');

		if (EXTLY_J3)
		{
			JHtmlSidebar::addEntry(
				JText::_('COM_XTCRONJOB_TITLE_TASKS'),
				'index.php?option=com_xtcronjob&view=tasks',
				$vName == 'tasks'
			);
			JHtmlSidebar::addEntry(
				JText::_('COM_XTCRONJOB_TITLE_CATEGORIES'),
				'index.php?option=com_categories&view=categories&extension=com_xtcronjob',
				$vName == 'categories'
			);
		}
		else
		{
			JSubMenuHelper::addEntry(
				JText::_('COM_XTCRONJOB_TITLE_TASKS'),
				'index.php?option=com_xtcronjob&view=tasks',
				$vName == 'tasks'
			);

			JSubMenuHelper::addEntry(
				JText::_('COM_XTCRONJOB_TITLE_CATEGORIES'),
				'index.php?option=com_categories&view=categories&extension=com_xtcronjob',
				$vName == 'categories'
			);
		}
	}
}
