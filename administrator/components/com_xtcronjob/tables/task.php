<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * XTCronjobTableTask
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
 */
class XtcronjobTableTask extends FOFTable
{
	/**
	 * Instantiate the table object
	 *
	 * @param   string     $table  Param
	 * @param   string     $key    Param
	 * @param   JDatabase  &$db    The Joomla! database object	 *
	 */
	public function __construct($table, $key, &$db)
	{
		parent::__construct('#__xtcronjob_tasks', 'id', $db);

		$this->_columnAlias = array(
			'enabled'		=> 'published',
			'created_on'	=> 'created',
			'modified_on'	=> 'modified',
			'locked_on'		=> 'checked_out_time',
			'locked_by'		=> 'checked_out',
		);
	}

	/**
	 * Checks the record for validity
	 *
	 * @return  int  True if the record is valid
	 */
	public function check()
	{
		// If the title is missing, throw an error
		if (!$this->title)
		{
			$this->setError(JText::_('COM_XTCRONJOB_TASK_ERR_NEEDS_TITLE'));

			return false;
		}

		// If the catid is missing, throw an error
		if (!$this->catid)
		{
			$this->setError(JText::_('COM_XTCRONJOB_TASK_ERR_NEEDS_CATEGORY'));

			return false;
		}

		// If the catid is missing, throw an error
		if (!$this->type)
		{
			$this->setError(JText::_('COM_XTCRONJOB_TASK_ERR_NEEDS_TYPE'));

			return false;
		}

		// If the file is missing, throw an error
		if (strlen($this->file) == 0)
		{
			$this->setError(JText::_('COM_XTCRONJOB_TASK_ERR_NEEDS_FILE'));

			return false;
		}

		JLoader::import('extly.scheduler.scheduler');

		try
		{
			$cron = Scheduler::getParser($this->unix_mhdmd);
			$cron->setExpression($this->unix_mhdmd);
			$nextDate = new JDate($cron->getNextRunDate()->getTimestamp());

			$this->ran_at = $nextDate->toSql();

			return true;
		}
		catch (Exception $e)
		{
			$this->setError(JText::_('COM_XTCRONJOB_TASK_ERR_CRONEXPRESSION'));
			$this->setError($e->getMessage());

			return false;
		}
	}
}
