<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * XTCronjobTableTask
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
 */
class XtcronjobTableCategory extends FOFTable
{
	/**
	 * Instantiate the table object
	 *
	 * @param   string     $table  Param
	 * @param   string     $key    Param
	 * @param   JDatabase  &$db    The Joomla! database object	 *
	 */
	public function __construct( $table, $key, &$db )
	{
		parent::__construct('#__categories', 'id', $db);

		$this->_columnAlias = array(
			'enabled'		=> 'published',
			'slug'			=> 'alias',
			'created_on'	=> 'created_time',
			'modified_on'	=> 'modified_time',
			'locked_on'		=> 'checked_out_time',
			'locked_by'		=> 'checked_out',
		);
	}

	/**
	 * Checks
	 *
	 * @return bool True if allowed to delete
	 */
	public function check()
	{
		return false;
	}

	/**
	 * Checks if we are allowed to delete this record
	 *
	 * @param   int  $oid  The numeric ID of the vgroup to delete
	 *
	 * @return bool True if allowed to delete
	 */
	public function onBeforeDelete($oid=null)
	{
		return false;
	}
}
