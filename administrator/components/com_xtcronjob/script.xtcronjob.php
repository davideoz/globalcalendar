<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

/**
 * Script file of XTCronjob component
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
*/
class Com_XTCronjobInstallerScript
{
	/** @var string The component's name */
	protected $_extly_extension = 'com_xtcronjob';

	/** @var array The list of extra modules and plugins to install */
	private $installation_queue = array(
		// modules => { (folder) => { (module) => { (position), (published) } }* }*
		'modules' => array(
			'admin' => array(
			),
			'site' => array(
			)
		),
		// plugins => { (folder) => { (element) => (published) }* }*
		'plugins' => array(
			'system' => array(
							'xtcronjob'	=> 1,
			),
		)
	);

	/** @var array Obsolete files and folders to remove */
	private $extlyRemoveFilesCore = array(
		'files'	=> array(
				// FOF 1.x files
				'libraries/fof/controller.php',
				'libraries/fof/dispatcher.php',
				'libraries/fof/inflector.php',
				'libraries/fof/input.php',
				'libraries/fof/model.php',
				'libraries/fof/query.abstract.php',
				'libraries/fof/query.element.php',
				'libraries/fof/query.mysql.php',
				'libraries/fof/query.mysqli.php',
				'libraries/fof/query.sqlazure.php',
				'libraries/fof/query.sqlsrv.php',
				'libraries/fof/render.abstract.php',
				'libraries/fof/render.joomla.php',
				'libraries/fof/render.joomla3.php',
				'libraries/fof/render.strapper.php',
				'libraries/fof/string.utils.php',
				'libraries/fof/table.php',
				'libraries/fof/template.utils.php',
				'libraries/fof/toolbar.php',
				'libraries/fof/view.csv.php',
				'libraries/fof/view.html.php',
				'libraries/fof/view.json.php',
				'libraries/fof/view.php',

				// Extly Lib - Old CronParser
				'libraries/extly/helpers/CronParser.php',

				// Extly Lib - Old Css
				'media/lib_extly/css/extly-base.css',

				// Extly Lib - IE Shim
				'media/lib_extly/js/ie.js',
				'media/lib_extly/js/utils/ie.js',
				'media/lib_extly/js/utils/ie.min.js',

				// Utils
				'media/lib_extly/js/utils/tourist.js',

				// Extly Lib - defaultmain.js
				'media/lib_extly/js/defaultmain.js',
				'media/lib_extly/js/defaultmain.min.js',

				// Extly Lib - jquery.disabled.js
				'media/lib_extly/js/jquery/jquery.disabled.js',

				// Extly Lib - lodash.min.js new version
				'media/lib_extly/js/lodash.min.js',
				'media/lib_extly/js/backbone/lodash.min.js',
				'media/lib_extly/js/backbone/lodash.underscore.min.js',

				// Extlycorefront no more
				'media/lib_extly/js/extlycorefront.js',
				'media/lib_extly/js/extlycorefront.min.js',

				// Extly styling
				'media/lib_extly/css/extly-base-2_5_12.css',
				'media/lib_extly/css/extly-base-2_5_16.css'
		),
		'folders' => array(
				// Extly Lib - Scheduler backend
				'libraries/extly/scheduler/backend',
				'media/lib_extly/js/scheduler',

				// FontAwesome png 16x16 icons
				'media/lib_extly/images/icons'
			)
	);

	/** @var array Obsolete files and folders to remove from the Core and Pro releases */
	private $extlyRemoveFilesPro = array(
			'files'	=> array(
			),
			'folders' => array(
			)
	);

	private $extlyCliScripts = array(
	);

	/**
	 * Joomla! pre-flight event
	 *
	 * @param string $type Installation type (install, update, discover_install)
	 * @param JInstaller $parent Parent object
	 */
	public function preflight($type, $parent)
	{
		// Only allow to install on Joomla! 2.5.0 or later with PHP 5.3.0 or later
		if(defined('PHP_VERSION')) {
			$version = PHP_VERSION;
		} elseif(function_exists('phpversion')) {
			$version = phpversion();
		} else {
			$version = '5.0.0'; // all bets are off!
		}

		// PHP 5.3.1
		if(!version_compare($version, '5.3.1', 'ge')) {
			$msg = "<p>You need PHP 5.3.1 or later to install this component</p>";
			if(version_compare(JVERSION, '3.0', 'gt'))
			{
				JLog::add($msg, JLog::WARNING, 'jerror');
			}
			else
			{
				JError::raiseWarning(100, $msg);
			}
			return false;
		}

		// PHP 5.2, You shall not pass | Anonymous function variable assignment example
		try
		{
			$date = new DateTime;
			$now = $date->getTimestamp();
			$greet = function($name)
			{
	    		return sprintf("Hello %s\r\n", $name);
			};
			$test = $greet('XTCronjob');
		}
		catch (Exception $e)
		{
			$msg = "<p>You need PHP 5.3.1 or later to install this component (Anonymous function)</p>";
			if(version_compare(JVERSION, '3.0', 'gt'))
			{
				JLog::add($msg, JLog::WARNING, 'jerror');
			}
			else
			{
				JError::raiseWarning(100, $msg);
			}
			return false;
		}

		// Joomla version
		if (!version_compare(JVERSION, '2.5.6', 'ge'))
		{
			$msg = "<p>You need Joomla! 2.5.6 or later to install this component</p>";
			JError::raiseWarning(100, $msg);
			return false;
		}

		// Workarounds for JInstaller bugs
		if (in_array($type, array('install','discover_install')))
		{
			$this->_bugfixDBFunctionReturnedNoError();
		}
		else
		{
			$this->_bugfixCantBuildAdminMenus();
			$this->_fixBrokenSQLUpdates($parent);
		}

		return true;
	}

	/**
	 * Runs after install, update or discover_update
	 * @param string $type install, update or discover_update
	 * @param JInstaller $parent
	 */
	function postflight( $type, $parent )
	{
		// Install subextensions
		$status = $this->_installSubextensions($parent);

		// Install FOF
		$fofStatus = $this->_installFOF($parent);

		// Remove obsolete files and folders
		$extlyRemoveFiles = array(
				'files'		=> array_merge($this->extlyRemoveFilesPro['files'], $this->extlyRemoveFilesCore['files']),
				'folders'	=> array_merge($this->extlyRemoveFilesPro['folders'], $this->extlyRemoveFilesCore['folders']),
		);
		$this->_removeObsoleteFilesAndFolders($extlyRemoveFiles);
		$this->_copyCliFiles($parent);

		// Install Extly Straper
		$straperStatus = $this->_installStraper($parent);

		// Show the post-installation page
		$this->_renderPostInstallation($type, $status, $fofStatus, $straperStatus, $parent);

		// Kill update site
		$this->_killUpdateSite();

		if ($fofStatus)
		{
			// Load FOF
			if (!defined('FOF_INCLUDED'))
			{
				include_once JPATH_LIBRARIES . '/fof/include.php';
			}
			FOFPlatform::getInstance()->clearCache();
		}
	}

	/**
	 * Runs on uninstallation
	 *
	 * @param JInstaller $parent
	 */
	function uninstall($parent)
	{
		// Uninstall subextensions
		$status = $this->_uninstallSubextensions($parent);

		// Show the post-uninstallation page
		$this->_renderPostUninstallation($status, $parent);
	}

	/**
	 * Copies the CLI scripts into Joomla!'s cli directory
	 *
	 * @param JInstaller $parent
	 */
	private function _copyCliFiles($parent)
	{
		$src = $parent->getParent()->getPath('source');

		jimport("joomla.filesystem.file");
		jimport("joomla.filesystem.folder");

		foreach($this->extlyCliScripts as $script) {
			if(JFile::exists(JPATH_ROOT.'/cli/'.$script)) {
				JFile::delete(JPATH_ROOT.'/cli/'.$script);
			}
			if(JFile::exists($src.'/cli/'.$script)) {
				JFile::move($src.'/cli/'.$script, JPATH_ROOT.'/cli/'.$script);
			}
		}
	}

	/**
	 * Renders the post-installation message
	 */
	private function _renderPostInstallation($type, $status, $fofStatus, $straperStatus, $parent)
	{
		?>

		<?php $rows = 1;?>

		<div class="extly">

			<img src="../media/lib_extly/images/extly-icon-57.png" width="57" height="57" alt="Extly - Joomla Extensions" style="margin-right:16px;" />

			<h1>Welcome to XTCronjob!</h1>

			<p>
				<strong>Cron Jobs Management and Scheduling for Joomla!</strong>
			</p>

			<p>Please, visit the <a class="btn btn-success" href="index.php?option=com_xtcronjob">Control Panel</a> to configure.</p>

			<h2>Installation Status</h2>

			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th class="title" colspan="2">Extension</th>
						<th width="30%">Status</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="3"></td>
					</tr>
				</tfoot>
				<tbody>
					<tr class="row0 success">
						<td class="key" colspan="2">XTCronjob component</td>
						<td>Installed</td>
					</tr>

					<tr class="row0 <?php echo $straperStatus['required'] ? ($straperStatus['installed'] ? 'success' : 'error') : 'info'; ?>">
						<td class="key" colspan="2">Extly Framework <?php echo $straperStatus['version']?> [<?php echo $straperStatus['date'] ?>]
						</td>
						<td><span style=""> <?php echo $straperStatus['required'] ? ($straperStatus['installed'] ?'Installed':'Not Installed') : 'Already up-to-date'; ?>
						</span>
						</td>
					</tr>

					<?php
					if (count($status->modules))
					{
						?>
					<tr>
						<th>Module</th>
						<th>Client</th>
						<th></th>
					</tr>
					<?php
					foreach ($status->modules as $module)
					{
						?>
					<tr class="row<?php
					echo ($rows++ % 2);
					echo ($module['result']) ? " success" :  "error";
					?>">
						<td class="key"><?php echo $module['name']; ?></td>
						<td class="key"><?php echo ucfirst($module['client']); ?></td>
						<td><?php echo ($module['result'])?'Installed':'Not installed'; ?>
						</td>
					</tr>
					<?php
					}
					}
					?>
					<?php if (count($status->plugins)) : ?>
					<tr>
						<th>Plugin</th>
						<th>Group</th>
						<th></th>
					</tr>
					<?php foreach ($status->plugins as $plugin) : ?>
					<tr class="row<?php echo ($rows++ % 2); ?> <?php echo ($plugin['result'])? "success" : "error"; ?>">
						<td class="key"><?php echo ucfirst($plugin['name']); ?></td>
						<td class="key"><?php echo ucfirst($plugin['group']); ?></td>
						<td><?php echo ($plugin['result'])?'Installed':'Not installed'; ?>
						</td>
					</tr>
					<?php endforeach; ?>
					<?php endif; ?>

					<tr class="row1 <?php echo $fofStatus['required'] ? ($fofStatus['installed']?'success':'error') : 'info'; ?> ">
						<td class="key" colspan="2">Framework on Framework (FOF) <?php echo $fofStatus['version']?> [<?php echo $fofStatus['date'] ?>]
						</td>
						<td><span style=""> <?php echo $fofStatus['required'] ? ($fofStatus['installed'] ?'Installed':'Not Installed') : 'Already up-to-date'; ?>
						</span>
						</td>
					</tr>
				</tbody>
			</table>

			<hr />

			<p>
				If you have any question, please, don't hesitate to contact us.<br /> Technical Support: <a href="http://support.extly.com" target="_blank">http://support.extly.com</a>
			</p>

			<p>
				We are passionately committed to your success.<br /> Support Team<br /> <strong>Extly.com - Extensions</strong><br /> <a href="http://support.extly.com" target="_blank">http://support.extly.com</a>
				| <img src="../media/lib_extly/images/icons/twitter.png"> <a href="http://twitter.com/extly" target="_blank">@extly</a> | <a href="http://www.facebook.com/extly" target="_blank">facebook.com/extly</a>
			</p>


		</div>

		<?php
	}

	private function _renderPostUninstallation($status, $parent)
	{
		?>
<?php $rows = 0;?>
<h2>XTCronjob uninstallation status</h2>
<table class="adminlist table">
	<thead>
		<tr>
			<th class="title" colspan="2"><?php echo JText::_('Extension'); ?></th>
			<th width="30%"><?php echo JText::_('Status'); ?></th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td colspan="3"></td>
		</tr>
	</tfoot>
	<tbody>
		<tr class="row0 success">
			<td class="key" colspan="2"><?php echo 'XTCronjob '.JText::_('Component'); ?></td>
			<td><strong><?php echo JText::_('Removed'); ?> </strong></td>
		</tr>
		<?php if (count($status->modules)) : ?>
		<tr>
			<th><?php echo JText::_('Module'); ?></th>
			<th><?php echo JText::_('Client'); ?></th>
			<th></th>
		</tr>
		<?php foreach ($status->modules as $module) : ?>
		<tr class="row<?php echo (++ $rows % 2); ?> <?php echo ($module['result'])? "success" : "error"; ?>">
			<td class="key"><?php echo $module['name']; ?></td>
			<td class="key"><?php echo ucfirst($module['client']); ?></td>
			<td><strong><?php echo ($module['result'])?JText::_('Removed'):JText::_('Not removed'); ?> </strong></td>
		</tr>
		<?php endforeach;?>
		<?php endif;?>
		<?php if (count($status->plugins)) : ?>
		<tr>
			<th><?php echo JText::_('Plugin'); ?></th>
			<th><?php echo JText::_('Group'); ?></th>
			<th></th>
		</tr>
		<?php foreach ($status->plugins as $plugin) : ?>
		<tr class="row<?php echo (++ $rows % 2); ?> <?php echo ($plugin['result'])? "success" : "error"; ?>">
			<td class="key"><?php echo ucfirst($plugin['name']); ?></td>
			<td class="key"><?php echo ucfirst($plugin['group']); ?></td>
			<td><strong><?php echo ($plugin['result'])?JText::_('Removed'):JText::_('Not removed'); ?> </strong></td>
		</tr>
		<?php endforeach; ?>
		<?php endif; ?>
	</tbody>
</table>
<p></p>
<p></p>
<?php
	}



	/**
	 * Joomla! 1.6+ bugfix for "DB function returned no error"
	 */
	private function _bugfixDBFunctionReturnedNoError()
	{
		$db = JFactory::getDbo();

		// Fix broken #__assets records
		$query = $db->getQuery(true);
		$query->select('id')
		->from('#__assets')
		->where($db->qn('name').' = '.$db->q($this->_extly_extension));
		$db->setQuery($query);
		if(version_compare(JVERSION, '3.0', 'ge')) {
			$ids = $db->loadColumn();
		} else {
			$ids = $db->loadResultArray();
		}
		if(!empty($ids)) foreach($ids as $id) {
			$query = $db->getQuery(true);
			$query->delete('#__assets')
			->where($db->qn('id').' = '.$db->q($id));
			$db->setQuery($query);
			$db->execute();
		}

		// Fix broken #__extensions records
		$query = $db->getQuery(true);
		$query->select('extension_id')
		->from('#__extensions')
		->where($db->qn('element').' = '.$db->q($this->_extly_extension));
		$db->setQuery($query);
		if(version_compare(JVERSION, '3.0', 'ge')) {
			$ids = $db->loadColumn();
		} else {
			$ids = $db->loadResultArray();
		}
		if(!empty($ids)) foreach($ids as $id) {
			$query = $db->getQuery(true);
			$query->delete('#__extensions')
			->where($db->qn('extension_id').' = '.$db->q($id));
			$db->setQuery($query);
			$db->execute();
		}

		// Fix broken #__menu records
		$query = $db->getQuery(true);
		$query->select('id')
		->from('#__menu')
		->where($db->qn('type').' = '.$db->q('component'))
		->where($db->qn('menutype').' = '.$db->q('main'))
		->where($db->qn('link').' LIKE '.$db->q('index.php?option='.$this->_extly_extension));
		$db->setQuery($query);
		if(version_compare(JVERSION, '3.0', 'ge')) {
			$ids = $db->loadColumn();
		} else {
			$ids = $db->loadResultArray();
		}
		if(!empty($ids)) foreach($ids as $id) {
			$query = $db->getQuery(true);
			$query->delete('#__menu')
			->where($db->qn('id').' = '.$db->q($id));
			$db->setQuery($query);
			$db->execute();
		}
	}

	/**
	 * Joomla! 1.6+ bugfix for "Can not build admin menus"
	 */
	private function _bugfixCantBuildAdminMenus()
	{
		$db = JFactory::getDbo();

		// If there are multiple #__extensions record, keep one of them
		$query = $db->getQuery(true);
		$query->select('extension_id')
		->from('#__extensions')
		->where($db->qn('element').' = '.$db->q($this->_extly_extension));
		$db->setQuery($query);
		if(version_compare(JVERSION, '3.0', 'ge')) {
			$ids = $db->loadColumn();
		} else {
			$ids = $db->loadResultArray();
		}
		if(count($ids) > 1) {
			asort($ids);
			$extension_id = array_shift($ids); // Keep the oldest id

			foreach($ids as $id) {
				$query = $db->getQuery(true);
				$query->delete('#__extensions')
				->where($db->qn('extension_id').' = '.$db->q($id));
				$db->setQuery($query);
				$db->execute();
			}
		}

		// @todo

		// If there are multiple assets records, delete all except the oldest one
		$query = $db->getQuery(true);
		$query->select('id')
		->from('#__assets')
		->where($db->qn('name').' = '.$db->q($this->_extly_extension));
		$db->setQuery($query);
		$ids = $db->loadObjectList();
		if(count($ids) > 1) {
			asort($ids);
			$asset_id = array_shift($ids); // Keep the oldest id

			foreach($ids as $id) {
				$query = $db->getQuery(true);
				$query->delete('#__assets')
				->where($db->qn('id').' = '.$db->q($id));
				$db->setQuery($query);
				$db->execute();
			}
		}

		// Remove #__menu records for good measure!
		$query = $db->getQuery(true);
		$query->select('id')
		->from('#__menu')
		->where($db->qn('type').' = '.$db->q('component'))
		->where($db->qn('menutype').' = '.$db->q('main'))
		->where($db->qn('link').' LIKE '.$db->q('index.php?option='.$this->_extly_extension));
		$db->setQuery($query);
		if(version_compare(JVERSION, '3.0', 'ge')) {
			$ids1 = $db->loadColumn();
		} else {
			$ids1 = $db->loadResultArray();
		}
		if(empty($ids1)) $ids1 = array();
		$query = $db->getQuery(true);
		$query->select('id')
		->from('#__menu')
		->where($db->qn('type').' = '.$db->q('component'))
		->where($db->qn('menutype').' = '.$db->q('main'))
		->where($db->qn('link').' LIKE '.$db->q('index.php?option='.$this->_extly_extension.'&%'));
		$db->setQuery($query);
		if(version_compare(JVERSION, '3.0', 'ge')) {
			$ids2 = $db->loadColumn();
		} else {
			$ids2 = $db->loadResultArray();
		}
		if(empty($ids2)) $ids2 = array();
		$ids = array_merge($ids1, $ids2);
		if(!empty($ids)) foreach($ids as $id) {
			$query = $db->getQuery(true);
			$query->delete('#__menu')
			->where($db->qn('id').' = '.$db->q($id));
			$db->setQuery($query);
			$db->execute();
		}
	}

	/**
	 * Installs subextensions (modules, plugins) bundled with the main extension
	 *
	 * @param JInstaller $parent
	 * @return JObject The subextension installation status
	 */
	private function _installSubextensions($parent)
	{
		$src = $parent->getParent()->getPath('source');

		$db = JFactory::getDbo();

		$status = new JObject();
		$status->modules = array();
		$status->plugins = array();

		// Modules installation
		if(count($this->installation_queue['modules'])) {
			foreach($this->installation_queue['modules'] as $folder => $modules) {
				if(count($modules)) foreach($modules as $module => $modulePreferences) {
					// Install the module
					if(empty($folder)) $folder = 'site';
					$path = "$src/modules/$folder/$module";
					if(!is_dir($path)) {
						$path = "$src/modules/$folder/mod_$module";
					}
					if(!is_dir($path)) {
						$path = "$src/modules/$module";
					}
					if(!is_dir($path)) {
						$path = "$src/modules/mod_$module";
					}
					if(!is_dir($path)) continue;
					// Was the module already installed?
					$sql = $db->getQuery(true)
					->select('COUNT(*)')
					->from('#__modules')
					->where($db->qn('module').' = '.$db->q('mod_'.$module));
					$db->setQuery($sql);
					$count = $db->loadResult();
					$installer = new JInstaller;
					$result = $installer->install($path);
					$status->modules[] = array(
							'name'=>'mod_'.$module,
							'client'=>$folder,
							'result'=>$result
					);
					// Modify where it's published and its published state
					if(!$count) {
						// A. Position and state
						list($modulePosition, $modulePublished) = $modulePreferences;
						if($modulePosition == 'cpanel') {
							$modulePosition = 'icon';
						}
						$sql = $db->getQuery(true)
						->update($db->qn('#__modules'))
						->set($db->qn('position').' = '.$db->q($modulePosition))
						->where($db->qn('module').' = '.$db->q('mod_'.$module));
						if($modulePublished) {
							$sql->set($db->qn('published').' = '.$db->q('1'));
						}
						$db->setQuery($sql);
						$db->execute();

						// B. Change the ordering of back-end modules to 1 + max ordering
						if($folder == 'admin') {
							$query = $db->getQuery(true);
							$query->select('MAX('.$db->qn('ordering').')')
							->from($db->qn('#__modules'))
							->where($db->qn('position').'='.$db->q($modulePosition));
							$db->setQuery($query);
							$position = $db->loadResult();
							$position++;

							$query = $db->getQuery(true);
							$query->update($db->qn('#__modules'))
							->set($db->qn('ordering').' = '.$db->q($position))
							->where($db->qn('module').' = '.$db->q('mod_'.$module));
							$db->setQuery($query);
							$db->execute();
						}

						// C. Link to all pages
						$query = $db->getQuery(true);
						$query->select('id')->from($db->qn('#__modules'))
						->where($db->qn('module').' = '.$db->q('mod_'.$module));
						$db->setQuery($query);
						$moduleid = $db->loadResult();

						$query = $db->getQuery(true);
						$query->select('*')->from($db->qn('#__modules_menu'))
						->where($db->qn('moduleid').' = '.$db->q($moduleid));
						$db->setQuery($query);
						$assignments = $db->loadObjectList();
						$isAssigned = !empty($assignments);
						if(!$isAssigned) {
							$o = (object)array(
								'moduleid'	=> $moduleid,
								'menuid'	=> 0
							);
							$db->insertObject('#__modules_menu', $o);
						}
					}
				}
			}
		}

		// Plugins installation
		if(count($this->installation_queue['plugins'])) {
			foreach($this->installation_queue['plugins'] as $folder => $plugins) {
				if(count($plugins)) foreach($plugins as $plugin => $published) {
					$path = "$src/plugins/$folder/$plugin";
					if(!is_dir($path)) {
						$path = "$src/plugins/$folder/plg_$plugin";
					}
					if(!is_dir($path)) {
						$path = "$src/plugins/$plugin";
					}
					if(!is_dir($path)) {
						$path = "$src/plugins/plg_$plugin";
					}
					if(!is_dir($path)) continue;

					// Was the plugin already installed?
					$query = $db->getQuery(true)
					->select('COUNT(*)')
					->from($db->qn('#__extensions'))
					->where($db->qn('element').' = '.$db->q($plugin))
					->where($db->qn('folder').' = '.$db->q($folder));
					$db->setQuery($query);
					$count = $db->loadResult();

					$installer = new JInstaller;
					$result = $installer->install($path);

					$status->plugins[] = array('name'=>'plg_'.$plugin,'group'=>$folder, 'result'=>$result);

					if($published && !$count) {
						$query = $db->getQuery(true)
						->update($db->qn('#__extensions'))
						->set($db->qn('enabled').' = '.$db->q('1'))
						->where($db->qn('element').' = '.$db->q($plugin))
						->where($db->qn('folder').' = '.$db->q($folder));
						$db->setQuery($query);
						$db->execute();
					}
				}
			}
		}

		return $status;
	}

	/**
	 * Uninstalls subextensions (modules, plugins) bundled with the main extension
	 *
	 * @param JInstaller $parent
	 * @return JObject The subextension uninstallation status
	 */
	private function _uninstallSubextensions($parent)
	{
		jimport('joomla.installer.installer');

		$db = JFactory::getDBO();

		$status = new JObject();
		$status->modules = array();
		$status->plugins = array();

		$src = $parent->getParent()->getPath('source');

		// Modules uninstallation
		if(count($this->installation_queue['modules'])) {
			foreach($this->installation_queue['modules'] as $folder => $modules) {
				if(count($modules)) foreach($modules as $module => $modulePreferences) {
					// Find the module ID
					$sql = $db->getQuery(true)
					->select($db->qn('extension_id'))
					->from($db->qn('#__extensions'))
					->where($db->qn('element').' = '.$db->q('mod_'.$module))
					->where($db->qn('type').' = '.$db->q('module'));
					$db->setQuery($sql);
					$id = $db->loadResult();
					// Uninstall the module
					if($id) {
						$installer = new JInstaller;
						$result = $installer->uninstall('module',$id,1);
						$status->modules[] = array(
							'name'=>'mod_'.$module,
							'client'=>$folder,
							'result'=>$result
						);
					}
				}
			}
		}

		// Plugins uninstallation
		if(count($this->installation_queue['plugins'])) {
			foreach($this->installation_queue['plugins'] as $folder => $plugins) {
				if(count($plugins)) foreach($plugins as $plugin => $published) {
					$sql = $db->getQuery(true)
					->select($db->qn('extension_id'))
					->from($db->qn('#__extensions'))
					->where($db->qn('type').' = '.$db->q('plugin'))
					->where($db->qn('element').' = '.$db->q($plugin))
					->where($db->qn('folder').' = '.$db->q($folder));
					$db->setQuery($sql);

					$id = $db->loadResult();
					if($id)
					{
						$installer = new JInstaller;
						$result = $installer->uninstall('plugin',$id,1);
						$status->plugins[] = array(
							'name'=>'plg_'.$plugin,
							'group'=>$folder,
							'result'=>$result
						);
					}
				}
			}
		}

		return $status;
	}

	/**
	 * Removes obsolete files and folders
	 *
	 * @param array $extlyRemoveFiles
	 */
	private function _removeObsoleteFilesAndFolders($extlyRemoveFiles)
	{
		// Remove files
		jimport('joomla.filesystem.file');
		if(!empty($extlyRemoveFiles['files'])) foreach($extlyRemoveFiles['files'] as $file) {
			$f = JPATH_ROOT.'/'.$file;
			if (!file_exists($f))
			{
				continue;
			}
			if (!@unlink($f))
			{
				JFile::delete($f);
			}
		}

		// Remove folders
		jimport('joomla.filesystem.file');
		if(!empty($extlyRemoveFiles['folders'])) foreach($extlyRemoveFiles['folders'] as $folder) {
			$f = JPATH_ROOT.'/'.$folder;
			if (!JFolder::exists($f))
			{
				continue;
			}
			JFolder::delete($f);
		}
	}

	/**
	 * Installs the FOF framework only if the currently installed version on the
	 * user's site is out of date or if there is no version already installed.
	 *
	 * @param JInstaller $parent
	 * @return array
	 */
	private function _installFOF($parent)
	{
		$src = $parent->getParent()->getPath('source');

		// Install the FOF framework
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		jimport('joomla.utilities.date');
		$source = $src.'/fof';
		if(!defined('JPATH_LIBRARIES')) {
			$target = JPATH_ROOT.'/libraries/fof';
		} else {
			$target = JPATH_LIBRARIES.'/fof';
		}
		$haveToInstallFOF = false;
		if(!JFolder::exists($target)) {
			$haveToInstallFOF = true;
		} else {
			$fofVersion = array();
			if(JFile::exists($target.'/version.txt')) {
				$rawData = JFile::read($target.'/version.txt');
				$info = explode("\n", $rawData);
				$fofVersion['installed'] = array(
					'version'	=> trim($info[0]),
					'date'		=> new JDate(trim($info[1]))
				);
			} else {
				$fofVersion['installed'] = array(
					'version'	=> '0.0',
					'date'		=> new JDate('2011-01-01')
				);
			}
			$rawData = JFile::read($source.'/version.txt');
			$info = explode("\n", $rawData);
			$fofVersion['package'] = array(
				'version'	=> trim($info[0]),
				'date'		=> new JDate(trim($info[1]))
			);

			$haveToInstallFOF = $fofVersion['package']['date']->toUNIX() > $fofVersion['installed']['date']->toUNIX();
		}

		$installedFOF = false;
		if($haveToInstallFOF) {
			$versionSource = 'package';
			$installer = new JInstaller;
			$installedFOF = $installer->install($source);
		} else {
			$versionSource = 'installed';
		}

		if(!isset($fofVersion)) {
			$fofVersion = array();
			if(JFile::exists($target.'/version.txt')) {
				$rawData = JFile::read($target.'/version.txt');
				$info = explode("\n", $rawData);
				$fofVersion['installed'] = array(
					'version'	=> trim($info[0]),
					'date'		=> new JDate(trim($info[1]))
				);
			} else {
				$fofVersion['installed'] = array(
					'version'	=> '0.0',
					'date'		=> new JDate('2011-01-01')
				);
			}
			$rawData = JFile::read($source.'/version.txt');
			$info = explode("\n", $rawData);
			$fofVersion['package'] = array(
				'version'	=> trim($info[0]),
				'date'		=> new JDate(trim($info[1]))
			);
			$versionSource = 'installed';
		}

		if(!($fofVersion[$versionSource]['date'] instanceof JDate)) {
			$fofVersion[$versionSource]['date'] = new JDate();
		}

		return array(
			'required'	=> $haveToInstallFOF,
			'installed'	=> $installedFOF,
			'version'	=> $fofVersion[$versionSource]['version'],
			'date'		=> $fofVersion[$versionSource]['date']->format('Y-m-d'),
		);
	}

	private function _installStraper($parent)
	{
		$src = $parent->getParent()->getPath('source');

		// Install the FOF framework
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		jimport('joomla.utilities.date');

		/*
		 $source = $src.'/strapper';
		$target = JPATH_ROOT.'/media/extly_strapper';
		*/
		$source = $src.'/strapper/lib_extly';
		$target = JPATH_ROOT.'/libraries/extly';

		$haveToInstallStraper = false;
		if(!JFolder::exists($target)) {
			$haveToInstallStraper = true;
		} else {
			$straperVersion = array();
			if(JFile::exists($target.'/version.txt')) {
				$rawData = JFile::read($target.'/version.txt');
				$info = explode("\n", $rawData);
				$straperVersion['installed'] = array(
					'version'	=> trim($info[0]),
					'date'		=> new JDate(trim($info[1]))
				);
			} else {
				$straperVersion['installed'] = array(
					'version'	=> '0.0',
					'date'		=> new JDate('2011-01-01')
				);
			}
			$rawData = JFile::read($source.'/version.txt');
			$info = explode("\n", $rawData);
			$straperVersion['package'] = array(
				'version'	=> trim($info[0]),
				'date'		=> new JDate(trim($info[1]))
			);

			$haveToInstallStraper = $straperVersion['package']['date']->toUNIX() > $straperVersion['installed']['date']->toUNIX();
		}

		$installedStraper = false;
		if($haveToInstallStraper) {
			$versionSource = 'package';
			$installer = new JInstaller;
			$installedStraper = $installer->install($source);
		} else {
			$versionSource = 'installed';
		}

		if(!isset($straperVersion)) {
			$straperVersion = array();
			if(JFile::exists($target.'/version.txt')) {
				$rawData = JFile::read($target.'/version.txt');
				$info = explode("\n", $rawData);
				$straperVersion['installed'] = array(
					'version'	=> trim($info[0]),
					'date'		=> new JDate(trim($info[1]))
				);
			} else {
				$straperVersion['installed'] = array(
					'version'	=> '0.0',
					'date'		=> new JDate('2011-01-01')
				);
			}
			$rawData = JFile::read($source.'/version.txt');
			$info = explode("\n", $rawData);
			$straperVersion['package'] = array(
				'version'	=> trim($info[0]),
				'date'		=> new JDate(trim($info[1]))
			);
			$versionSource = 'installed';
		}

		if(!($straperVersion[$versionSource]['date'] instanceof JDate)) {
			$straperVersion[$versionSource]['date'] = new JDate();
		}

		return array(
			'required'	=> $haveToInstallStraper,
			'installed'	=> $installedStraper,
			'version'	=> $straperVersion[$versionSource]['version'],
			'date'		=> $straperVersion[$versionSource]['date']->format('Y-m-d'),
		);
	}

	/**
	 * Remove the update site specification from Joomla! – we no longer support
	 * that misbehaving crap, thank you very much...
	 */
	private function _killUpdateSite()
	{
		// Get some info on all the stuff we've gotta delete
		$db = JFactory::getDbo();
		$query = $db->getQuery(true)
		->select(array(
				$db->qn('s').'.'.$db->qn('update_site_id'),
				$db->qn('e').'.'.$db->qn('extension_id'),
				$db->qn('e').'.'.$db->qn('element'),
				$db->qn('s').'.'.$db->qn('location'),
			))
			->from($db->qn('#__update_sites').' AS '.$db->qn('s'))
			->join('INNER',$db->qn('#__update_sites_extensions').' AS '.$db->qn('se').' ON('.
				$db->qn('se').'.'.$db->qn('update_site_id').' = '.
				$db->qn('s').'.'.$db->qn('update_site_id')
				.')')
			->join('INNER',$db->qn('#__extensions').' AS '.$db->qn('e').' ON('.
				$db->qn('e').'.'.$db->qn('extension_id').' = '.
				$db->qn('se').'.'.$db->qn('extension_id')
				.')')
			->where($db->qn('s').'.'.$db->qn('type').' = '.$db->q('extension'))
			->where($db->qn('e').'.'.$db->qn('type').' = '.$db->q('component'))
			->where($db->qn('e').'.'.$db->qn('element').' = '.$db->q($this->_extly_extension))
		;
		$db->setQuery($query);
		$oResult = $db->loadObject();

		// If no record is found, do nothing. We've already killed the monster!
		if(is_null($oResult)) return;

		// Delete the #__update_sites record
		$query = $db->getQuery(true)
		->delete($db->qn('#__update_sites'))
		->where($db->qn('update_site_id').' = '.$db->q($oResult->update_site_id));
		$db->setQuery($query);
		try {
			$db->execute();
		} catch (Exception $exc) {
			// If the query fails, don't sweat about it
		}

		// Delete the #__update_sites_extensions record
		$query = $db->getQuery(true)
		->delete($db->qn('#__update_sites_extensions'))
		->where($db->qn('update_site_id').' = '.$db->q($oResult->update_site_id));
		$db->setQuery($query);
		try {
			$db->execute();
		} catch (Exception $exc) {
			// If the query fails, don't sweat about it
		}

		// Delete the #__updates records
		$query = $db->getQuery(true)
		->delete($db->qn('#__updates'))
		->where($db->qn('update_site_id').' = '.$db->q($oResult->update_site_id));
		$db->setQuery($query);
		try {
			$db->execute();
		} catch (Exception $exc) {
			// If the query fails, don't sweat about it
		}
	}

	/**
	 * Let's say that a user tries to install a component and it somehow fails
	 * in a non-graceful manner, e.g. a server timeout error, going over the
	 * quota etc. In this case the component's administrator directory is
	 * created and not removed (because the installer died an untimely death).
	 * When the user retries installing the component JInstaller sees that and
	 * thinks it's an update. This causes it to neither run the installation SQL
	 * file (because it's not supposed to run on extension update) nor the
	 * update files (because there is no schema version defined). As a result
	 * the files are installed, the database tables are not, the component is
	 * broken and I have to explain to non-technical users how to edit their
	 * database with phpMyAdmin.
	 *
	 * This method detects this stupid situation and attempts to execute the
	 * installation file instead.
	 */
	private function _fixBrokenSQLUpdates($parent)
	{
		// Get the extension ID
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select('extension_id')
		->from('#__extensions')
		->where($db->qn('element').' = '.$db->q($this->_extly_extension));
		$db->setQuery($query);
		$eid = $db->loadResult();

		if (!$eid)
		{
			return;
		}

		// Get the schema version
		$query = $db->getQuery(true);
		$query->select('version_id')
		->from('#__schemas')
		->where('extension_id = ' . $eid);
		$db->setQuery($query);
		$version = $db->loadResult();

		// If there is a schema version it's not a false update
		if ($version)
		{
			return;
		}

		// Execute the installation SQL file. Since I don't have access to
		// the manifest, I will improvise (again!)
		$dbDriver = strtolower($db->name);

		if ($dbDriver == 'mysqli')
		{
			$dbDriver = 'mysql';
		}
		elseif($dbDriver == 'sqlsrv')
		{
			$dbDriver = 'sqlazure';
		}

		// Get the name of the sql file to process
		$sqlfile = $parent->getParent()->getPath('source') . '/backend/sql/install/' . $dbDriver . '/install.sql';
		if (file_exists($sqlfile))
		{
			$buffer = file_get_contents($sqlfile);
			if ($buffer === false)
			{
				return;
			}

			$queries = JInstallerHelper::splitSql($buffer);

			if (count($queries) == 0)
			{
				// No queries to process
				return;
			}

			// Process each query in the $queries array (split out of sql file).
			foreach ($queries as $query)
			{
				$query = trim($query);

				if ($query != '' && $query{0} != '#')
				{
					$db->setQuery($query);

					if (!$db->execute())
					{
						JError::raiseWarning(1, JText::sprintf('JLIB_INSTALLER_ERROR_SQL_ERROR', $db->stderr(true)));

						return false;
					}
				}
			}
		}

		// Update #__schemas to the latest version. Again, since I don't have
		// access to the manifest I have to improvise...
		$path = $parent->getParent()->getPath('source') . '/backend/sql/update/' . $dbDriver;
		$files = str_replace('.sql', '', JFolder::files($path, '\.sql$'));
		if(count($files) > 0)
		{
			usort($files, 'version_compare');
			$version = array_pop($files);
		}
		else
		{
			$version = '0.0.1-2007-08-15';
		}

		$query = $db->getQuery(true);
		$query->insert($db->quoteName('#__schemas'));
		$query->columns(array($db->quoteName('extension_id'), $db->quoteName('version_id')));
		$query->values($eid . ', ' . $db->quote($version));
		$db->setQuery($query);
		$db->execute();
	}

}