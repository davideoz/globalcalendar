<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

// Check for PHP4
if (defined('PHP_VERSION'))
{
	$version = PHP_VERSION;
}
elseif (function_exists('phpversion'))
{
	$version = phpversion();
}
else
{
	// No version info. I'll lie and hope for the best.
	$version = '5.0.0';
}

// Old PHP version detected. EJECT! EJECT! EJECT!
if (!version_compare($version, '5.3.0', '>='))
{
	return JError::raise(
			E_ERROR,
			500,
			'PHP versions 4.x, 5.0, 5.1 and 5.2 are no longer supported by AutoTweetNG.',
			'The version of PHP used on your site is obsolete and contains known security vulenrabilities.
			Moreover, it is missing features required by AutoTweetNG to work properly or at all.
			Please ask your host to upgrade your server to the latest PHP 5.3/5.4 stable release. Thank you!');
}

defined('CXTCRONJOB') || define('CXTCRONJOB', 'com_xtcronjob');
defined('CXTCRONJOB_VERSION') || define('CXTCRONJOB_VERSION', '3.2.0');

// Load FOF
if (!defined('FOF_INCLUDED'))
{
	include_once JPATH_LIBRARIES . '/fof/include.php';
}

if (!defined('FOF_INCLUDED'))
{
	JError::raiseError(
	'500',
	'Your AutoTweetNG installation is broken; please re-install.
			Alternatively, extract the installation archive and copy the fof directory inside your site\'s libraries directory.');
}

JLoader::import('extly.extlyframework');

FOFDispatcher::getTmpInstance('com_xtcronjob')->dispatch();