<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * XTCronjobDispatcher
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
 */
class XtcronjobDispatcher extends FOFDispatcher
{
	public $defaultView = 'tasks';

	/**
	 * onBeforeDispatch.
	 *
	 * @return	void
	 *
	 * @since	1.5
	 */
	public function onBeforeDispatch()
	{
		$result = parent::onBeforeDispatch();

		if (!$result)
		{
			return $result;
		}

		Extly::loadStyle();
		JHtml::stylesheet('com_xtcronjob/style.css', false, true);

		return true;
	}
}
