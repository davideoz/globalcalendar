<?php

/**
 * JEvents Locations Plugin for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C)  2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');
jimport('joomla.application.component.model');
include_once(JPATH_SITE . "/components/com_jevents/jevents.defines.php");
JLoader::register('JevModal', JPATH_LIBRARIES . "/jevents/jevmodal/jevmodal.php");
JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");
JLoader::register('JEVHelper', JPATH_SITE . "/components/com_jevents/libraries/helper.php");
JModelLegacy::addIncludePath(JPATH_ADMINISTRATOR.'/components/com_jevlocations/models', 'LocationsModelLocation');

class plgJEventsJevlocations extends JPlugin
{

	var $_dbvalid = 0;

	function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$lang = JFactory::getLanguage();
		$lang->load("com_jevlocations", JPATH_SITE);
		$lang->load("com_jevlocations", JPATH_ADMINISTRATOR);
		$lang->load("plg_jevents_jevlocations", JPATH_ADMINISTRATOR);

		// replace the plugin parameters?
		$componentparams = JComponentHelper::getParams("com_jevlocations");
		if ($componentparams->get("gwidth", -1) != -1)
		{
			$this->params = $componentparams;
		}
	}

	/**
	 * When editing a JEvents menu item can add additional menu constraints dynamically
	 */
	function onEditMenuItem(&$menudata, $value, $control_name, $name, $id, $param)
	{
		JevHtmlBootstrap::framework();
		JevHtmlBootstrap::loadCss();
		JHtml::_('jquery.ui', array('core', 'sortable'));
		JLoader::register('JevModal', JPATH_LIBRARIES . "/jevents/jevmodal/jevmodal.php");

		JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");

		static $matchingextra = null;
		// find the parameter that matches jevl: (if any)
		if (!isset($matchingextra))
		{
			$params = $param->getGroup('params');
			foreach ($params as $key => $element)
			{
				$val = $element->value;
				if (strpos($key, "jform_params_extras") === 0 && strpos($val, "jevl:") === 0)
				{
					$matchingextra = $key;
					break;
				}
			}
			if (!isset($matchingextra))
			{
				$matchingextra = false;
			}
		}

		// already done this param
		if (isset($menudata[$id]))
			return;

		// either we found matching extra and this is the correct id or we didn't find matching extra and the value is blank
		if (($matchingextra == $id && strpos($value, "jevl:") === 0) || (($value == "" || $value == "0") && $matchingextra === false))
		{

			$pwidth = $this->params->get("pwidth", "750");
			$pheight = $this->params->get("pheight", "500");

			$matchingextra = $id;
			$invalue = str_replace("jevl:", "", $value);
			$invalue = str_replace(" ", "", $invalue);
			if ($invalue == "")
			{
				$invalue = array();
			}
			else
			{
				$invalue = explode(",", $invalue);
				JArrayHelper::toInteger($invalue);
			}
			JHtml::script('com_jevlocations/locations.js', false, true);
			if (version_compare(JVERSION, '3.0', ">="))
			{
				JHtml::stylesheet('com_jevlocations/jevlocations3x.css', array(), true);
			}
			$location = " -- ";
			if (count($invalue) > 0)
			{
				$db = JFactory::getDBO();
				$sql = "SELECT * FROM #__jev_locations where loc_id IN(" . implode(",", $invalue) . ")";
				$db->setQuery($sql);
				$locations = @$db->loadObjectList('loc_id');
			}
			else
			{
				$locations = false;
			}

			$link = JRoute::_("index.php?option=com_jevlocations&task=locations.select&tmpl=component");
			JText::script('COM_JEVLOCATIONS_DUPLICATED_LOCATION');
			$input = "<div style='float: left;text-align:left;'><ul id='sortableLocations'>";
			if (is_array($locations))
			{
				foreach ($invalue as $locid)
				{
					if (!array_key_exists($locid, $locations))
						continue;
					$jpm = $locations[$locid];
					$locname = $jpm->title;
					$lid = $jpm->loc_id;
					$input .= "<li id='sortableloc$lid' >$locname</li>";
				}
			}
			$input .= "</ul>";
			$input .= '<input type="hidden"  name="' . $name . '"   id="menuloc" value="' . $value . '" />';

			$input .= "</div>";
			$input .= "<img src='" . JURI::Root() . "components/com_jevlocations/assets/images/Trash.png' class='sortabletrash btn' id='trashimageloc' style='display:none;margin-right:5px;'/>";
			$input .= "<script type='text/javascript'>
				jQuery(document).on('ready', function(){
					sortableLocations.setup();
					});
			</script>";
			$input .= '<div class="button2-left">'
				. '<div class="blank">'
				. '<a class="btn" href="javascript:sortableLocations.selectLocation(\'' . $link . '\',\'' . JText::_('SELECT_LOCATION') . '\')" title="' . JText::_('SELECT_LOCATION') . '"  >'
				. '<i class="icon-location"></i>' . JText::_('SELECT_LOCATION')
				. '</a>'
				. '</div>'
				. '</div>';

			JevModal::modal(".jevmodal");

			$data = new stdClass();
			$data->name = "location";
			$data->html = $input;
			$data->label = JText::_('PLG_JEVENTS_JEVLOCATIONS_SPECIFIED_LOCATION_LBL');

			$data->description = JText::_('PLG_JEVENTS_JEVLOCATIONS_SPECIFIED_LOCATION_DSC');
			$data->options = array();
			$menudata[$id] = $data;
			return;
		}

		// Now the location category
		static $matchingextra2 = null;
		// find the parameter that matches jevlmc: (if any)
		//TODO remove backward compatibility with old single category
		if (!isset($matchingextra2))
		{
			$params = $param->getGroup('params');
			foreach ($params as $key => $element)
			{
				$val = $element->value;
				if (strpos($key, "jform_params_extras") === 0 && (strpos($val, "jevlc:") === 0 || strpos($val, "jevlmc:") === 0))
				{
					$matchingextra2 = $key;
					break;
				}
			}

			if (!isset($matchingextra2))
			{
				$matchingextra2 = false;
			}
		}

		// either we found matching extra and this is the correct id or we didn't find matching extra and the value is blank
		//TODO remove backward compatibility with old single category
		if (($matchingextra2 == $id && ( strpos($value, "jevlc:") === 0 || strpos($value, "jevlmc:") === 0 ) ) || (($value == "" || $value == "0") && $matchingextra2 === false))
		{
			$matchingextra2 = $id;
			$invalue = str_replace("jevlc:", "", $value, $replacements);

			if ($replacements > 0)
			{
				$invalue = str_replace(" ", "", $invalue);
				$invalue = explode(",", $invalue);
				JArrayHelper::toInteger($invalue);

				foreach ($invalue as $index => $oldCatId)
				{
					$invalue[$index] = JevLocationsHelper::getNewCategory($oldCatId);
					$values[] = "jevlmc:" . $invalue[$index];
				}
				$value = implode(",", $values);
			}
			else
			{
				$invalue = str_replace("jevlmc:", "", $value);
				$invalue = str_replace(" ", "", $invalue);
				$invalue = explode(",", $invalue);
				JArrayHelper::toInteger($invalue);
			}

			$loccats = JHtml::_('category.options', "com_jevlocations");

			$input = "<div style='float: left;text-align:left;'>";
			$input .= "<select multiple='multiple' id='jevloccats'  size='5' onchange='updateJevLocCats();'>";
			$selected = (count($invalue) == 0) ? "selected='selected'" : "";
			//$input .= "<option value='' $selected>---</option>";
			foreach ($loccats as $loccat)
			{
				$title = $loccat->text;
				$catid = $loccat->value;
				$selected = in_array($loccat->value, $invalue) ? "selected='selected'" : "";
				$input .= "<option value='$catid' $selected>$title</option>";
			}
			$input .= "</select>";
			$input .= '<input type="hidden"  name="' . $name . '"   id="jevloccat" value="' . $value . '" />';
			$input .= "</div>";

			$script = '
			function updateJevLocCats(){
				var select = document.getElement("select#jevloccats");
				var input = document.getElementById("jevloccat");
				input.value="";
				// jQuery array
				jQuery("select#jevloccats option:selected").each(function(index, item){
					// if select none - reset everything else
					if (item.value=="") {
						select.selectedIndex=0;
						return;
					}
					if (input.value!="") input.value+=",";
					input.value+="jevlmc:"+item.value;
				});
			}
			';
			$document = JFactory::getDocument();
			$document->addScriptDeclaration($script);


			$data = new stdClass();
			$data->name = "loccat";
			$data->html = $input;
			$data->label = JText::_('PLG_JEVENTS_JEVLOCATIONS_LOCATION_CATEGORIES_LBL');
			$data->description = JText::_('PLG_JEVENTS_JEVLOCATIONS_LOCATION_CATEGORIES_DSC');
			$data->options = array();
			$menudata[$id] = $data;
			return;
		}

		// Now the location country/state/city
		static $matchingextra3 = null;
		$locparams = JComponentHelper::getParams("com_jevlocations");

		// find the parameter that matches jevlcsc: (if any)
		if (!isset($matchingextra3))
		{
			$params = $param->getGroup('params');
			foreach ($params as $key => $element)
			{
				$val = $element->value;
				if (strpos($key, "jform_params_extras") === 0 && strpos($val, "jevlcsc:") === 0)
				{
					$matchingextra3 = $key;
					break;
				}
			}
			if (!isset($matchingextra3))
			{
				$matchingextra3 = false;
			}
		}

		// either we found matching extra and this is the correct id or we didn't find matching extra and the value is blank
		if (($matchingextra3 == $id && strpos($value, "jevlcsc:") === 0) || (($value == "" || $value == "0") && $matchingextra3 === false))
		{

			$usecats = $locparams->get("usecats", 0);
			if ($usecats)
			{

				$matchingextra3 = $id;
				$invalue = str_replace("jevlcsc:", "", $value);
				$invalue = str_replace(" ", "", $invalue);
				$invalue = explode(",", $invalue);
				JArrayHelper::toInteger($invalue);

				if (count($invalue) > 0)
				{
					$db = JFactory::getDBO();

					// Make sure there aren't too many
					$sql = 'SELECT count(c.id) FROM #__jevlocation_categories AS c WHERE c.published = 1 AND c.section = "com_jevlocations"';
					$db->setQuery($sql);
					if (intval($db->loadResult()) > 500)
					{
						$sql = 'SELECT c.id, c.title as ctitle,p.title as ptitle,' .
							' CASE WHEN CHAR_LENGTH(p.title) THEN CONCAT_WS(" => ", p.title, c.title) ELSE c.title END as title' .
							' FROM #__jevlocation_categories AS c' .
							' LEFT JOIN #__jevlocation_categories AS p ON p.id=c.parent_id' .
							' LEFT JOIN #__jevlocation_categories AS gp ON gp.id=p.parent_id ' .
							' WHERE c.published = 1 ' .
							' AND c.section = "com_jevlocations" ' .
							' ORDER BY  ptitle ASC, ctitle ASC';
					}
					else
					{

						$sql = 'SELECT c.id, c.title as ctitle,p.title as ptitle, gp.title as gptitle, ' .
							' CASE WHEN CHAR_LENGTH(p.title) THEN CONCAT_WS(" => ", p.title, c.title) ELSE c.title END as title' .
							' FROM #__jevlocation_categories AS c' .
							' LEFT JOIN #__jevlocation_categories AS p ON p.id=c.parent_id' .
							' LEFT JOIN #__jevlocation_categories AS gp ON gp.id=p.parent_id ' .
							' WHERE c.published = 1 ' .
							' AND c.section = "com_jevlocations" ' .
							' ORDER BY gptitle ASC, ptitle ASC, ctitle ASC';
					}
					$db->setQuery($sql);
					$loccats = @$db->loadObjectList('id');
				}

				$input = "<div style='float: left;text-align:left;'>";
				$input .= "<select multiple='multiple' id='jevloccats2'  size='5'  onchange='updateJevLocCats2();'>";
				$selected = (count($invalue) == 0) ? "selected='selected'" : "";
				//$input .= "<option value='' $selected>---</option>";
				foreach ($loccats as $loccat)
				{
					$title = $loccat->ctitle;
					if (!is_null($loccat->ptitle))
					{
						$title = $loccat->ptitle . "=>" . $title;
					}
					if (isset($loccat->gptitle) && !is_null($loccat->gptitle))
					{
						$title = $loccat->gptitle . "=>" . $title;
					}
					$catid = $loccat->id;
					$selected = in_array($loccat->id, $invalue) ? "selected='selected'" : "";
					$input .= "<option value='$catid' $selected>$title</option>";
				}
				$input .= "</select>";
				$input .= '<input type="hidden"  name="' . $name . '"   id="jevloccat2" value="' . $value . '" />';
				$input .= "</div>";

				$script = '
			function updateJevLocCats2(){
				var input = document.getElementById("jevloccat2");
				input.value="";
				// jQuery array
				jQuery("select#jevloccats2 option:selected").each(	function(index, item){
					// if select none - reset everything else
					if (item.value=="") {
						select.selectedIndex=0;
						return;
					}
					if (input.value!="") input.value+=",";
					input.value+="jevlcsc:"+item.value;
				});
			}
			';
				$document = JFactory::getDocument();
				$document->addScriptDeclaration($script);

				$data = new stdClass();
				$data->name = "loccsc";
				$data->html = $input;
				$data->label = JText::_('PLG_JEVENTS_JEVLOCATIONS_LOCATION_STATE_LBL');
				$data->description = JText::_('PLG_JEVENTS_JEVLOCATIONS_LOCATION_STATE_DSC');
				$data->options = array();
				$menudata[$id] = $data;
				return;
			}
			else
			{
				$matchingextra3 = $id;
				$invalue = str_replace("jevlcsc:", "", $value);
				$invalue = explode(",", $invalue);

				if (count($invalue) > 0)
				{
					$db = JFactory::getDBO();

					// Make sure there aren't too many
					$sql = 'SELECT count(CONCAT(loc.country,loc.state,loc.city)) FROM #__jev_locations as loc where loc.published=1 AND loc.global=1  AND  (loc.country !="" OR loc.state !="" OR loc.city !="")';
					$db->setQuery($sql);
					$count = $db->loadResult();
					if (intval($count) > 2000)
					{
						$sql = 'SELECT loc.country,loc.state,"" as city FROM #__jev_locations as loc ' .
							' WHERE loc.published = 1 AND loc.global=1' .
							' ORDER BY  loc.country ASC, loc.state ASC';
					}
					else
					{

						$sql = 'SELECT loc.country,loc.state,loc.city  FROM #__jev_locations as loc ' .
							' WHERE loc.published = 1 AND loc.global=1' .
							' AND  (loc.country !="" OR loc.state !="" OR loc.city !="") ' .
							' ORDER BY  loc.country ASC, loc.state ASC,loc.city ASC';
					}
					$db->setQuery($sql);
					$rows = @$db->loadObjectList();

					$loccats = array();
					foreach ($rows as $row)
					{
						if (array_key_exists($row->country, $loccats))
							continue;
						if ($row->country == "")
							continue;
						$crow = clone $row;
						$crow->state = "";
						$crow->city = "";
						$loccats[$row->country] = $crow;
					}
					foreach ($rows as $row)
					{
						if (array_key_exists($row->country . "=>" . $row->state, $loccats))
							continue;
						if ($row->state == "")
							continue;
						$crow = clone $row;
						$crow->city = "";
						$loccats[$row->country . "=>" . $row->state] = $crow;
					}
					foreach ($rows as $row)
					{
						if (array_key_exists($row->country . "=>" . $row->state . "=>" . $row->city, $loccats))
							continue;
						if ($row->city == "")
							continue;
						$loccats[$row->country . "=>" . $row->state . "=>" . $row->city] = $row;
					}

					$input = "";
					$input .= "<select id='jevloccats3'   onchange='updateJevLocCats3();' multiple='multiple' size='5'>";
					$selected = (count($invalue) == 0) ? "selected='selected'" : "";
					$input .= "<option value='' $selected>---</option>";
					foreach ($loccats as $loccat)
					{
						$title = "";
						$lookup = array();
						if (isset($loccat->city) && !is_null($loccat->city) && $loccat->city != "")
						{
							$title = $loccat->city;
							$lookup["city"] = $loccat->city;
							if (!is_null($loccat->state) && $loccat->state != "")
							{
								$title = $loccat->state . ($title != "" ? "=>" . $title : "");
								$lookup["state"] = $loccat->state;
							}
							else
							{
								$title = " -- =>" . $title;
							}
							if (!is_null($loccat->country) && $loccat->country != "")
							{
								$title = $loccat->country . ($title != "" ? "=>" . $title : "");
								$lookup["country"] = $loccat->country;
							}
							else
							{
								$title = " -- =>" . $title;
							}
						}
						else
						{
							if (!is_null($loccat->state) && $loccat->state != "")
							{
								$title = $loccat->state;
								$lookup["state"] = $loccat->state;
							}
							if (isset($loccat->country) && !is_null($loccat->country) && $loccat->country != "")
							{
								$title = $loccat->country . ($title != "" ? "=>" . $title : "");
								$lookup["country"] = $loccat->country;
							}
							else
							{
								$title = " -- =>" . $title;
							}
						}
						$fixedtitle = base64_encode(serialize($lookup));
						$selected = in_array($fixedtitle, $invalue) ? "selected='selected'" : "";
						$input .= "<option value='" . $fixedtitle . "' $selected>$title</option>";
					}
					$input .= "</select>";
					$input .= '<input type="hidden"  name="' . $name . '"  id="jevloccat3" value="' . $value . '" />';

					$script = '
					function updateJevLocCats3(){
						var input = document.getElementById("jevloccat3");
						input.value="";
						// jQuery Arrau
						jQuery("select#jevloccats3 option:selected").each(	function(index, item){
							// if select none - reset everything else
							if (item.value=="") {
								select.selectedIndex=0;
								return;
							}
							if (input.value!="") input.value+=",";
							input.value+="jevlcsc:"+item.value;
						});
					}
					';
					$document = JFactory::getDocument();
					$document->addScriptDeclaration($script);
				}

				$data = new stdClass();
				$data->name = "loccsc";
				$data->html = $input;
				$data->label = JText::_('PLG_JEVENTS_JEVLOCATIONS_LOCATION_STATE_CITY_LBL');
				$data->description = JText::_('PLG_JEVENTS_JEVLOCATIONS_LOCATION_STATE_CITY_DSC');
				$data->options = array();
				$menudata[$id] = $data;
				return;
			}
		}
	}

	/**
	 * Create custom event location setting code
	 *
	 * return true if value is set
	 *
	 * @param jEventCal $row
	 * @return unknown
	 */
	function onEditLocation(&$row)
	{
                $jinput = JFactory::getApplication()->input;

		JevHtmlBootstrap::framework();
		JevHtmlBootstrap::loadCss();

		$script = "var urlroot = '" . JURI::root() . "';\n";
		$script .= "var jsontoken = '" . JSession::getFormToken() . "';\n";

		$pwidth = $this->params->get("pwidth", "750");
		$pheight = $this->params->get("pheight", "500");

		JevModal::modal(".jevmodal", array('size' => "$pheight,$pwidth"));

		$document = JFactory::getDocument();
		$document->addScriptDeclaration($script);

		JHtml::script('com_jevlocations/locations.js', false, true);
		JHTML::script('plugins/jevents/jevlocations/jevlocation.js');

		JHtml::stylesheet('com_jevlocations/jevlocations.css', array(), true);

		$modalSelector = "jevLocationModal";

		$html = array();
		$css = '#'.$modalSelector.'{width: '.$pwidth.'px;}';
		$document->addStyleDeclaration($css);

		$location = " -- ";
		if ($row->location() == "")
		{
			$pluginsDir = JPATH_SITE . "/plugins/";
			JLoader::register('jevFilterProcessing', JEV_PATH . "/libraries/filters.php");
			$filters = jevFilterProcessing::getInstance(array("locationlookup"), $pluginsDir . "filters");
			if ($filters->filters[0]->filter_value > 0)
			{
				$row->location($filters->filters[0]->filter_value);
			}
			else if ($jinput->getInt("loc_id") > 0)
			{
				$row->location($jinput->getInt("loc_id"));
			}
		}
		if (intval($row->location()) > 0)
		{
			$db = JFactory::getDBO();
			$sql = "SELECT * FROM #__jev_locations where published=1 and loc_id=" . intval($row->location());
			$db->setQuery($sql);
			$locations = @$db->loadObjectList();
			if (count($locations) > 0)
			{
				$location = $locations[0]->title;
			}
		}

		// don't call this id location it causes problems in javascript
		$html[] = '<input type="hidden" name="location" id="locn" value="' . $row->location() . '"/>';

		$selecLocationUrl = 'javascript:jevModalPopup(\'' . $modalSelector . '\', \'' . JRoute::_("index.php?option=com_jevlocations&task=locations.select&tmpl=component") . '\', \'' . JText::_('SELECT_LOCATION', true) . '\');';
		$selectButtonLayout = '<div id="selectlocationbutton" class="jevloc-inlinebuttons" ><a class="btn" href="' . $selecLocationUrl . '" title="' . JText::_('SELECT_LOCATION') . '"  ><i class="icon-map-marker "></i> ' . JText::_('SELECT_LOCATION') . '</a></div>';

		$removeLocationText = JText::_('REMOVE_LOCATION');
		$removeButtonStyle = ( intval($row->location()) == 0 ) ? 'display:none;' : '';
		$removeButtonLayout = <<<REMOVEBUTTON
<div id="removelocationbutton"  class="jevloc-inlinebuttons" style="$removeButtonStyle">
	<a class="btn" href="javascript:removeLocation();" title="$removeLocationText"><i class="icon-remove "></i> $removeLocationText</a>
</div>
REMOVEBUTTON;

		if ($this->params->get("jsonfind", 0))
		{
			$pluginpath = 'plugins/jevents/jevlocations/';
			$client = JFactory::getApplication()->isAdmin() ? "administrator" : "site";
			if ($location == " -- ")
			{
				$location = "";
			}

			$html[] = '<div id="scrollable-dropdown-menu" style="float:left"><input name="evlocation_notused"  id="evlocation" class="jevtypeahead" value="' . $location . '"  type="text" autocomplete="off"></div>';

			JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");

			$create_locid = intval($row->location());
			$create_link = 'javascript:jevModalPopup(\'' . $modalSelector . '\', \'' . JRoute::_("index.php?option=com_jevlocations&task=locations.edit&tmpl=component&pop=1") . '\', \'' . JText::_('CREATE_LOCATION', true) . '\');';
			$create_title = JText::_('CREATE_LOCATION');
			$create_buttonname = JText::_('CREATE_LOCATION');
			$createlink2 = JRoute::_("index.php?option=com_jevlocations&task=locations.edit&tmpl=component&pop=1");

			$createButtonLayout = <<<CREATEBUTTON
<div  id="createlocationbutton" class="jevloc-inlinebuttons" style="display:none;">
	<a class="btn btn-primary" href="$create_link" title="$create_title" data-toggle="modal" ><i class="icon-edit"></i> $create_buttonname</a>
</div>
CREATEBUTTON;

			if ($this->params->get("inlineaction", 0) == 0)
			{
				if ((JevLocationsHelper::canCreateOwn() || JevLocationsHelper::canCreateGlobal()))
				{
					$html[] = str_replace('style="display:none;"', '', $createButtonLayout);
				}
				$html[] = $selectButtonLayout;
				$html[] = $removeButtonLayout;
			}
			else if ($this->params->get("inlineaction", 0) == 2 && (JevLocationsHelper::canCreateOwn() || JevLocationsHelper::canCreateGlobal()))
			{
				$html[] = $createButtonLayout;
				$html[] = $removeButtonLayout;
			}
			// if inline location creation
			else if ($this->params->get("inlineaction", 0) == 3 && (JevLocationsHelper::canCreateOwn() || JevLocationsHelper::canCreateGlobal()))
			{
				$html[] = '<div id="createlocationbutton" style="display:none;"><a class="btn btn-success" href="javascript:findGeoCodeAddress();"  title="' . JText::_('CREATE_LOCATION') . '"  >' . JText::_('CREATE_LOCATION') . '</a></div>';
				$html[] = '<div id="findmap" style="margin-top:40px;width:100%; height:300px;overflow:hidden !important;display:block;"></div>';

				$compparams = JComponentHelper::getParams("com_jevlocations");
				$zoom = $compparams->get("zoom", 0) + 2;
				$geolon = $compparams->get("long", 0);
				$geolat = $compparams->get("lat", 0);
				$maptype = $compparams->get("maptype", "ROADMAP");
				$gregion = $compparams->get("gregion", "US");
				$disableautopan = $compparams->get("autopan", 1) ? "false" : "true";

				JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");
				$googleurl = JevLocationsHelper::getApiUrl();
				JevLocationsHelper::loadApiScript();

				$script = "var urlrootloc = '" . JURI::root() . "media/com_jevlocations/images/';\n";

				$script .= <<<SCRIPT
	var findMap = false;
	var findMarker = false;
	var createLocationGeoCode = false;
	function findMapload(){
		if ( !jQuery('#findmap').length ) return;
		var myOptions = {
			center: new google.maps.LatLng($geolat,$geolon),
			zoom: $zoom,
			mapTypeId: google.maps.MapTypeId.$maptype,
			draggable:false,
			scrollwheel:false
		};
		findMap = new google.maps.Map(document.getElementById('findmap'), myOptions);

	}

	function findGeoCodeAddress(){
		if (createLocationGeoCode == false) {
			createLocationGeoCode  = new google.maps.Geocoder();
		}
		var address = jQuery('#evlocation').val();
		createLocationGeoCode.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
                                setMapIcons(results);
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});

	}

	var findMarkersArray = [];

	function setMapIcons(results)
	{
		if (results.length==0){
			if (findMarkersArray.length>0){
				findMap.setCenter(new google.maps.LatLng($geolat,$geolon));
				findMap.setZoom($zoom);
			}
			clearFindMapOverlays();
			return;
		}
		if (jQuery('#findmap').length){
			jQuery('#findmap').css("display","block");
		}

		// Create our "tiny" marker icon
		var blueIcon = new google.maps.MarkerImage(urlrootloc + 'blue-dot.png',
		// This marker is 32 pixels wide by 32 pixels tall.
		new google.maps.Size(32, 32),
		// The origin for this image is 0,0 within a sprite
		new google.maps.Point(0,0),
		// The anchor for this image is the base of the flagpole at 0,32.
		new google.maps.Point(16, 32));

		var markerOptions = {
			position:new google.maps.LatLng($geolat,$geolon),
			map:findMap,
			icon:blueIcon,
			disableAutoPan:$disableautopan,
			animation: google.maps.Animation.DROP,
			draggable:false
		};

		//  Create a new viewpoint bound
		var bounds = new google.maps.LatLngBounds ();

		for (var jp=0;jp<results.length;jp++){

			markerOptions.position = new google.maps.LatLng(results[jp].geometry.location.lat, results[jp].geometry.location.lon),
			bounds.extend (markerOptions.position);
			myMarker = new google.maps.Marker( markerOptions);
			//attachLocationToMarker(myMarker, results[jp]);
			// work around closure problem !
			//findMarkersArray.push(myMarker);
		}
		findMap.fitBounds(bounds);
	}

	function clearFindMapOverlays() {
		for (var i = 0; i < findMarkersArray.length; i++ ) {
			findMarkersArray[i].setMap(null);
		}
		findMarkersArray = [];
	}
	function attachLocationToMarker(marker, loc) {
		google.maps.event.addListener(marker, "click", function(e) {selectLocationByMarker(loc);});
	}
	function selectLocationByMarker(loc){
		alert("location = " +loc.loc_id);
		jQuery('#locnp').val( loc.loc_id);
		jQuery('#evlocation').val( loc.title);
		locnClearMatches();
		//jQuery('#findmap').css("display","none");
	}

	// delay by 1 second so that the page is properly rendered before we get the map
	jQuery( window ).on('load',function() {findMapload();});

SCRIPT;
				static $scriptloaded = false;
				if (!$scriptloaded)
				{
					JFactory::getDocument()->addScriptDeclaration($script);
					$scriptloaded = true;
				}
			}
			else
			{
				$url = 'javascript:jevModalPopup(\'' . $modalSelector . '\', \'' . JRoute::_("index.php?option=com_jevlocations&task=locations.select&tmpl=component") . '\', \'' . JText::_('SELECT_LOCATION', true) . '\');';
				$html[] = '<div>';
				$html[] = '<div id="selectlocationbutton" style="display:none;"  class="jevloc-inlinebuttons"><a class="btn" role="button" href="' . $url . '" title="' . JText::_('SELECT_LOCATION') . '"  ><i class="icon-map-marker "></i> ' . JText::_('SELECT_LOCATION') . '</a></div>';
				$html[] = $removeButtonLayout;

				if ($this->params->get("inlineaction", 0) == 1 && (JevLocationsHelper::canCreateOwn() || JevLocationsHelper::canCreateGlobal()))
				{
					$html[] = $createButtonLayout;
					// Experimental code that will lead to inline location creation
					/*
					  JevLocationsHelper::loadApiScript();
					  $compparams = JComponentHelper::getParams("com_jevlocations");
					  $gregion = $compparams->get("gregion","US");

					  JHtml::script('com_jevlocations/geolocation.js', false, true);

					  $html[] = "<input type='hidden' id='gregion' name='gregion' value='$gregion' />";
					  $html[] = '<div class="btn" id="createlocationbutton" style="display:none;"><i class="icon-edit "></i><a href="javascript:findEditAddressGeo()" title="' . JText::_('CREATE_LOCATION') . '"  >' . JText::_('CREATE_LOCATION') . '</a></div>';
					  $html[] = "<div  id='geditaddressmatches' style='display:none;padding:3px;margin:0px!important;min-width:200px;color:#111;background-color:#eee;z-index:999;'  ></div>";
					 */
				}
				$html[] = '</div>';
			}

			JLoader::register('JevTypeahead', JPATH_LIBRARIES . "/jevents/jevtypeahead/jevtypeahead.php");
			$datapath = JRoute::_("index.php?option=com_jevlocations&ttoption=com_jevlocations&typeaheadtask=gwejson&file=findlocation", false);
			$prefetchdatapath = JRoute::_("index.php?option=com_jevlocations&ttoption=com_jevlocations&typeaheadtask=gwejson&file=findlocation&prefetch=1", false);
			JevTypeahead::typeahead('#evlocation', array('remote' => $datapath,
			    //'prefetch'=>  $prefetchdatapath,
			    'data_value' => 'title',
			    'data_id' => 'loc_id',
			    'field_selector' => '#locn',
			    'minLength' => 3,
			    'limit' => 10,
			    'scrollable' => 1,
			    'emptyCallback' => 'noMatchingLocations()'));
			$typeaheadLoad = "jQuery(document).on('ready', function() {
				jQuery('#evlocation').on
										(
											'typeahead:select',
											function(ev,data)
											{
												showRemoveButton();
											}
										)});";
			JFactory::getDocument()->addScriptDeclaration($typeaheadLoad);
		}
		else
		{
			$html[] = '<input type="text" name="evlocation_notused" disabled="disabled" id="evlocation" value="' . $location . '" style="float:left"/>';
			//$html[] = '<div class="btn-group">';
			$html[] = $selectButtonLayout;
			$html[] = $removeButtonLayout;

			//$html[] = '</div>';
		}

		echo implode("\n", $html);

		JevModal::modal('#' . $modalSelector);

		return true;
	}

	function onEditCustom(&$row, &$customfields)
	{
		return true;
	}

	/**
	 * Clean out custom fields for event details not matching global event detail
	 *
	 * @param unknown_type $idlist
	 */
	function onCleanCustomDetails($idlist)
	{
		// TODO
		return true;
	}

	/**
	 * Store custom fields
	 *
	 * @param iCalEventDetail $evdetail
	 */
	function onStoreCustomEvent($event)
	{
		// if importing an ical then the location field will be non-text.  I need to convert this if possible
		$evdetail = $event->_detail;
		$compparams = JComponentHelper::getParams("com_jevlocations");
		if ($compparams->get("importlocations", 0))
		{
			JLog::addLogger(
				array(
				    'text_file' => 'com_jevlocations.import.php'
				),
				JLog::ALL,
				array('com_jevlocations')
			);

			$delimitersArray = array();

			// Meetup format
			$delimitersArray['meetup'] = " (";

			// JEvents format
			$delimitersArray['jevents'] = " -";

			// AT sign
			$delimitersArray['at'] = " @";

			// Plain comma delimited format
			$delimitersArray['comma'] = ",";

			if (isset($evdetail->location) && !is_numeric($evdetail->location) && trim($evdetail->location) != "")
			{
				// Find matching locations
				// 1. Find the creator id via the event
				$db = JFactory::getDBO();
				$db->setQuery("SELECT * FROM #__jevents_vevent WHERE ev_id = " . intval($event->ev_id));
				$event = $db->loadObject();

				$geoAccuracy = $compparams->get('importmatchinggeoaccuracy', '0.001');
				$matchingMethod = $compparams->get('importmatchingmethod', 'name');

				if (!$event || $event->created_by == 0)
					return true;

				$locationData = $this->extractLocationData($evdetail, $event->created_by, $delimitersArray);

				$locationData = $this->getMatchingLocation($evdetail, $locationData, $delimitersArray, $matchingMethod, $geoAccuracy);

				if($locationData !== false)
				{
					if($locationData->loc_id == 0)
					{
						if($compparams->get('importcreatelocations',1))
						{
							if(JevLocationsHelper::canCreateGlobal() || JevLocationsHelper::canCreateOwn())
							{

								$locationData->loc_id = $this->createLocation($locationData, $evdetail);
							}
							else
							{
								$message = JText::sprintf('COM_JEVLOCATIONS_IMPORT_LOCATION_NOT_AUTHORISED',JFactory::getUser()->username);
								JLog::add($message, JLog::ERROR, 'com_jevlocations');
							}
						}
					}

					if($locationData->loc_id)
					{
						$db = JFactory::getDBO();
						$db->setQuery("UPDATE #__jevents_vevdetail SET location = " . intval($locationData->loc_id) . ", loc_id = " . intval($locationData->loc_id) . "  WHERE evdet_id = " . intval($evdetail->evdet_id));
						$db->query();
					}
				}
			}
		}

		return true;
	}

	/**
	 * Method to get a matching location from the Database
	 * @param	object	$evdetail			Event Detail object
	 * @param	object	$location			Location Data we already have
	 * @param	string	$delimitersArray	Which character is used to split information
	 * @param	string	$matchingMethod		How to match locations in current DB
	 * @param	float	$geoAccuracy		How much accurate the geomatching should be
	 *
	 * @return	object	$location	Updated Location object
	 */
	private function getMatchingLocation($evdetail, $location, $delimitersArray, $matchingMethod = "name", $geoAccuracy = "0.001")
	{
		$location->loc_id = 0;

		// If we found the location in DB, we are good
		$locId = self::LocFound($evdetail, $location->created_by, $location->title, $location->geolon, $location->geolat, $matchingMethod, $geoAccuracy);

		if ($locId)
		{
			$location->loc_id = $locId;
			return $location;
		}

		// Since the location was not in the db, have google parse the LOCATION field for address and geolocation.
		// First, clean up the full location, since some extra punctuation confuses google.
		// We need to add the escaped comma
		$delimitersArray['escaped_comma'] = "\,";
		$clean_location = str_replace($delimitersArray, ',', $evdetail->location);

		// If no commas then no point try to do a location match since we don't have enough information
		if (strpos($clean_location, ",") === FALSE)
		{
			return $location;
		}

		// We try to get location data from Google
		do
		{
			$geo_result = plgJEventsJevlocations::GetGeoFromGoogle($clean_location, $location->geolon, $location->geolat);
			if (!$geo_result)
			{
				JLog::add(JText::sprintf('COM_JEVLOCATIONS_IMPORT_LOCATION_CANNOT_GET_DATA_FROM_GOOGLE', $clean_location),
				JLog::NOTICE, 'com_jevlocations');
				return $location;
			}

			$address = explode(',', $geo_result->formatted_address);
			$part_count = count($address);
			$firstComma = strpos($clean_location, ",");

			if ($firstComma === FALSE || $firstComma + 1 >= strlen($clean_location))
			{
				break; // No match possible.
			}

			$clean_location = substr($clean_location, $firstComma + 1);

			$possibleMatch['title'] = substr($address, 0, $firstComma);
			$possibleMatch['geolon'] = (string) $geo_result->geometry->location->lng;
			$possibleMatch['geolat'] = (string) $geo_result->geometry->location->lat;
			$possibleMatch['geoResult'] = $geo_result;


		} while ($part_count < 2); // Did the address come back as just the country? Try again after trimming the front of the location.

		if ($geo_result)
		{
			$location->geolon = (string) $geo_result->geometry->location->lng;
			$location->geolat = (string) $geo_result->geometry->location->lat;
			$location->geoResult = $geo_result;
		}

		if ($possibleMatch)
		{
			// Recheck for existing location now that we have lat/lon.
			$locId = self::LocFound($evdetail, $location->created_by,$possibleMatch['title'], $possibleMatch['geolon'], $possibleMatch['geolat'], 'geolocation');

			if ($locId > 0)
			{
				$location->loc_id = $locId;
				return $location;
			}
		}

		return $location;
	}

	function extractLocationData($evdetail, $createdBy, $delimitersArray)
	{
		$usedDelimiter = false;
		$location = new stdClass();

		if (!$createdBy)
		{
			$createdBy = JFactory::getUser()->id;
		}

		$location->created_by = $createdBy;

		$location->geolon = 0.0;
		$location->geolat = 0.0;
		$rawdata = unserialize($evdetail->_rawdata);

		if (isset($rawdata["GEO"]))
		{
			list($location->geolat, $location->geolon) = explode(";", $rawdata["GEO"]);
		}

		foreach ($delimitersArray as $delimiter)
		{
			$nameLength = strpos($evdetail->location, $delimiter);

			if ($nameLength !== false)
			{
				break;
			}
		}

		if ($nameLength === false)// There is no delimiter, so there is a name only.
		{
			$location->title = trim($evdetail->location);
		}
		else // ALL others fall through to here.
		{
			$location->title = trim(substr($evdetail->location, 0, $nameLength));
		}

		return $location;
	}

	function createLocation($location, $evdetail)
	{
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$jeventsParams = JComponentHelper::getParams('com_jevents');

		// It's a new location. Need to create a location record.
		$location->street = "";
		$location->description = "";
		$location->city = "";
		$location->state = "";
		$location->country = "";
		$location->postcode = "";

		if (isset($location->geoResult))
		{
			// Country
			$country_arr = $location->geoResult->xpath('address_component[type = "country"]/long_name');

			if ($country_arr !== FALSE)
			{
				$location->country = (string) $country_arr[0];
			}

			// State
			$state_arr = $location->geoResult->xpath('address_component[type = "administrative_area_level_1"]/long_name');
			if ($state_arr === FALSE || $state_arr[0] == "")
			{
				$state_arr = $location->geoResult->xpath('address_component[type = "administrative_area_level_2"]/long_name');
			}
			if ($state_arr !== FALSE)
			{
				$location->state = (string) $state_arr[0];
			}

			// City
			$city_arr = $location->geoResult->xpath('address_component[type = "locality"]/long_name');
			if ($city_arr === FALSE || $city_arr[0] == "")
			{
				$city_arr = $location->geoResult->xpath('address_component[type = "postal_town"]/long_name');
			}
			if ($city_arr === FALSE || $city_arr[0] == "")
			{
				$city_arr = $location->geoResult->xpath('address_component[type = "sublocality"]/long_name');
			}
			if ($city_arr !== FALSE)
			{
				$location->city = (string) $city_arr[0];
			}

			// Post code
			$postcode_arr = $location->geoResult->xpath('address_component[type = "postal_code"]/long_name');
			if ($postcode_arr !== FALSE)
			{
				$location->postcode = (string) $postcode_arr[0];
			}

			// street
			$route_arr = $location->geoResult->xpath('address_component[type = "route"]/long_name');
			$street_number_arr = $location->geoResult->xpath('address_component[type = "street_number"]/long_name');
			if ($street_number_arr !== FALSE && $street_number_arr[0] != "")
			{
				$location->street = (string) $street_number_arr[0] . " ";
			}
			if ($route_arr !== FALSE)
			{
				$location->street .= (string) $route_arr[0];
			}
		}

		if (!$compparams->get('importemptydescription',0))
		{
			$location->description = JText::sprintf('COM_JEVLOCATIONS_IMPORT_LOCATION_EXTRACTED_FROM_IMPORTED_CALENDAR',$evdetail->location);
		}

		if ($location->geolon == 0.0 || $location->geolat == 0.0) // If we still don't have a geo, set it to the default.
		{
			$location->geolon = $compparams->get("long", 0);
			$location->geolon = $compparams->get("lat", 0);
		}

		$location->geozoom = $compparams->get("zoom", 0) + 2; // Zoom two devels deeper than default view.
		$loccat = $compparams->get("importdefaultcategory", 0);

		if (!$loccat)
		{
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);
			$query->select('id')
				->from('#__categories AS c')
				->where('c.extension="com_jevlocations"')
				->order('created_time ASC')
				->limit(1);
			$db->setQuery($query);

			$loccat = $db->loadResult();

			if (!$loccat)
			{
				$loccat = 0;
			}
		}

		$location->loccat = array($loccat);


		$user = JFactory::getUser();
		$userid = $user->id;

		$location->created = JFactory::getDate()->toSql();
		$location->modified_by = $location->created_by;

		$location->global = JevLocationsHelper::canCreateGlobal() && $compparams->get('commondefault', 0);
		$location->published = 0;
		$location->access = 1;

		$locationData = Joomla\Utilities\ArrayHelper::fromObject($location);

		$model = JModelLegacy::getInstance("Location", "LocationsModel");


		$model->store($locationData);

		$location = $model->lastrow;

		//$this->notifyAdmin($location);

		$lastid = $location->loc_id;
		if ($lastid)
		{
			return $lastid;
		}
		else
		{
			$message = JText::sprintf('COM_JEVLOCATIONS_IMPORT_LOCATION_CREATE_ERROR',$loc_name);
			JLog::add($message, JLog::ERROR, 'com_jevlocations');
			return false;
		}
	}

	function onStoreCustomRepeat($event)
	{
		$event->_detail = new stdClass();
		$data = JFactory::getApplication()->input->getArray();
		foreach ($data as $key => $value)
		{
			if (strpos($key, "custom_") === 0)
			{
				$field = JString::substr($key, 7);
				$event->_detail->_customFields[$field] = $value;
			}
		}

		$event->ev_id = $event->eventid;
		$event->detail_id = $event->eventdetail_id;

		return $this->onStoreCustomEvent($event);
	}

	/**
	 * Clean out custom details for deleted event details
	 *
	 * @param comma separated list of event detail ids $idlist
	 */
	function onDeleteEventDetails($idlist)
	{
		return true;
	}

	function onListIcalEvents(& $extrafields, & $extratables, & $extrawhere, & $extrajoin, & $needsgroupdby = false)
	{
		$mainframe = JFactory::getApplication();  // RSH 10/12/10 - Make J!1.6 compatible
		$jinput = JFactory::getApplication()->input;
		if ($mainframe->isAdmin() && $jinput->getCmd("option") == "com_jevents")
		{
			return;
		}

		$pluginsDir = JPATH_SITE . "/plugins/";
		JLoader::register('jevFilterProcessing', JEV_PATH . "/libraries/filters.php");
		$filters = jevFilterProcessing::getInstance(array("locationsearch", "locationlookup", "locationcategory", "locationcity", "locationstate", "locationcountry", "geofilter", "locationcities"), $pluginsDir . "filters");
		$filters->setWhereJoin($extrawhere, $extrajoin);

		// I always do a join on the location table so can always get this extra field

		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats", 0);
		// Do we have any old style locations?
		$usehybrid = $compparams->get("hybrid", 1);
		if (!$usecats)
		{
			if ($usehybrid)
			{
				$extrafields .= ", CASE WHEN loc.title IS NULL THEN det.location ELSE loc.title END as location,loc.title as loc_title, loc.loc_id, loc.street as loc_street, loc.description as loc_desc, loc.postcode as loc_postcode, loc.city as loc_city, loc.country as loc_country, loc.state as loc_state";
			}
			else
			{
				$extrafields .= ", loc.loc_id,loc.title as loc_title, loc.title as location, loc.street as loc_street, loc.description as loc_desc, loc.postcode as loc_postcode, loc.city as loc_city, loc.country as loc_country, loc.state as loc_state";
			}
		}
		else
		{
			if ($usehybrid)
			{
				$extrafields .= ", loc.loc_id,CASE WHEN loc.title IS NULL THEN det.location ELSE loc.title END as location, loc.title as loc_title, loc.street as loc_street, loc.description as loc_desc, loc.postcode as loc_postcode";
			}
			else
			{
				$extrafields .= ", loc.loc_id,loc.title as location, loc.title as loc_title, loc.postcode as loc_postcode";
			}
			$extrajoin[] = ' #__jevlocation_categories AS locmcity ON loc.catid = locmcity.id AND locmcity.section="com_jevlocations"';
			$extrajoin[] = ' #__jevlocation_categories AS locmstate ON locmcity.parent_id = locmstate.id AND locmstate.section="com_jevlocations"';
			$extrajoin[] = ' #__jevlocation_categories AS locmcountry ON locmstate.parent_id = locmcountry.id AND locmcountry.section="com_jevlocations"';

			$extrafields .= ", locmcity.title AS loc_city, locmstate.title AS loc_state, locmcountry.title AS loc_country	";
		}

		$extrafields .= ", loc.phone as loc_phone	";
		$extrafields .= ", loc.image as loc_image	";
		$extrafields .= ", loc.url as loc_url	";
		$extrafields .= ", loc.geolon as loc_lon	";
		$extrafields .= ", loc.geolat as loc_lat	";
		$extrafields .= ", loc.geozoom as loc_zoom	";
		$extrafields .= ", loc.mapicon as loc_mapicon	";

		if ($this->params->get("alwayscatlink", 0))
		{
			$extrafields .= ", loccat.title as loc_category	";
		}

		// Have we specified specific locations for the menu item
		$compparams = JComponentHelper::getParams($jinput->getCmd("option", "com_jevents"));

		$reg = JFactory::getConfig();
		$modparams = $reg->get("jev.modparams", false);
		if ($modparams)
		{
			$compparams = $modparams;
		}
		$extraval = false;
		for ($extra = 0; $extra < 20; $extra++)
		{
			$extraval = $compparams->get("extras" . $extra, false);
			if (strpos($extraval, "jevl:") === 0)
			{
				break;
			}
		}
		if ($extraval && strpos($extraval, "jevl:") === 0)
		{
			$invalue = str_replace("jevl:", "", $extraval);
			$invalue = str_replace(" ", "", $invalue);
			if (substr($invalue, strlen($invalue) - 1) == ",")
			{
				$invalue = substr($invalue, 0, strlen($invalue) - 1);
			}
			$invalue = explode(",", $invalue);
			JArrayHelper::toInteger($invalue);

			$extrawhere[] = "det.loc_id IN (" . implode(",", $invalue) . ")";
		}

		// location categories
		//Fix for backwards compatibility with single category
		$extraval = false;
		for ($extra = 0; $extra < 20; $extra++)
		{
			$extraval = $compparams->get("extras" . $extra, false);
			if (strpos($extraval, "jevlc:") === 0)
			{
				if (JFactory::getUser()->authorise('core.admin'))
				{
					$messageQueue = JFactory::getApplication()->getMessageQueue();
					//Not good but is the easiest way of detecting location filter messages without affecting all other addons.
					if (sizeof($messageQueue) > 0)
					{
						foreach ($messageQueue as $message)
						{
							if ($message['type'] != "error" && !in_array(JText::_("COM_JEVLOCATIONS_OLD_CATEGORIES_WARNING") . "<br/>" . JText::_("JEV_ONLY_ADMIN_MESSAGE"), $message['message']))
							{
								JFactory::getApplication()->enqueueMessage(JText::_("COM_JEVLOCATIONS_OLD_CATEGORIES_WARNING") . "<br/>" . JText::_("JEV_ONLY_ADMIN_MESSAGE"), 'error');
							}
						}
					}
					else
					{
						JFactory::getApplication()->enqueueMessage(JText::_("COM_JEVLOCATIONS_OLD_CATEGORIES_WARNING") . "<br/>" . JText::_("JEV_ONLY_ADMIN_MESSAGE"), 'error');
					}
				}
				break;
			}
		}

		// multi-category locations!
		$needsgroupdby = true;

		$extrajoin[] = ' #__jevlocations_catmap AS loccatmap ON loccatmap.locid = loc.loc_id';
		// MUST join on #__categories since its used by locationcategory filter - so do not remove it from here without caution
		$extrajoin[] = ' #__categories AS loccat ON loccat.id = loccatmap.catid';
		$extrafields .= ", GROUP_CONCAT(DISTINCT loccat.title SEPARATOR ',') AS location_category_list,GROUP_CONCAT(loccatmap.catid SEPARATOR ',') AS location_catid_list";

		if ($extraval && strpos($extraval, "jevlc:") === 0)
		{
			$invalue = str_replace("jevlc:", "", $extraval);
			$invalue = str_replace(" ", "", $invalue);
			if (strlen($invalue) > 0)
			{
				$invalue = explode(",", $invalue);
				JArrayHelper::toInteger($invalue);
			}
			else
			{
				return true;
			}

			$extrajoin[] = ' #__jevlocations_oldcatmap AS oldloccatmap ON loccatmap.catid = oldloccatmap.newcatid';
			$extrawhere[] = "oldloccatmap.oldcatid IN (" . implode(",", $invalue) . ")";
		}
		$extraval = false;
		for ($extra = 0; $extra < 20; $extra++)
		{
			$extraval = $compparams->get("extras" . $extra, false);
			if (strpos($extraval, "jevlmc:") === 0)
			{
				break;
			}
		}
		if ($extraval && strpos($extraval, "jevlmc:") === 0)
		{
			$invalue = str_replace("jevlmc:", "", $extraval);
			$invalue = str_replace(" ", "", $invalue);
			if (strlen($invalue) > 0)
			{
				$invalue = explode(",", $invalue);
				JArrayHelper::toInteger($invalue);
			}
			else
			{
				return true;
			}
			$extrawhere[] = "loccatmap.catid IN (" . implode(",", $invalue) . ")";
		}

		// location country, state, city
		$extraval = false;
		for ($extra = 0; $extra < 20; $extra++)
		{
			$extraval = $compparams->get("extras" . $extra, false);
			if (strpos($extraval, "jevlcsc:") === 0)
			{
				break;
			}
		}
		if ($extraval && strpos($extraval, "jevlcsc:") === 0)
		{
			$locparams = JComponentHelper::getParams("com_jevlocations");

			$usecats = $locparams->get("usecats", 0);
			if ($usecats)
			{
				$invalue = str_replace("jevlcsc:", "", $extraval);
				$invalue = str_replace(" ", "", $invalue);
				if (strlen($invalue) > 0)
				{
					$invalue = explode(",", $invalue);
					JArrayHelper::toInteger($invalue);

					$extrawhere[] = " (loc.catid IN (" . implode(",", $invalue) . ") OR locmstate.id IN (" . implode(",", $invalue) . ") OR locmcountry.id IN (" . implode(",", $invalue) . ") ) ";
				}
				else
				{
					return true;
				}
			}
			else
			{
				$invalue = str_replace("jevlcsc:", "", $extraval);
				$invalue = str_replace(" ", "", $invalue);
				if (strlen($invalue) > 0)
				{
					$invalue = explode(",", $invalue);
					if (count($invalue) == 1)
					{
						$invalue = base64_decode($invalue[0]);
						$invalue = @unserialize($invalue);
						if (is_array($invalue))
						{
							$db = JFactory::getDBO();
							$whereparts = array();
							if (array_key_exists("country", $invalue))
							{
								$whereparts[] = "loc.country = " . $db->Quote($invalue['country']);
							}
							if (array_key_exists("state", $invalue))
							{
								$whereparts[] = "loc.state = " . $db->Quote($invalue['state']);
							}
							if (array_key_exists("city", $invalue))
							{
								$whereparts[] = "loc.city = " . $db->Quote($invalue['city']);
							}
							if (count($whereparts) > 0)
							{
								$extrawhere[] = " (" . implode(" AND ", $whereparts) . ")";
							}
						}
					}
					else
					{
						$orparts = array();
						foreach ($invalue as $val)
						{
							$val = base64_decode($val);
							$val = @unserialize($val);
							if (is_array($val))
							{
								$db = JFactory::getDBO();
								$whereparts = array();
								if (array_key_exists("country", $val))
								{
									$whereparts[] = "loc.country = " . $db->Quote($val['country']);
								}
								if (array_key_exists("state", $val))
								{
									$whereparts[] = "loc.state = " . $db->Quote($val['state']);
								}
								if (array_key_exists("city", $val))
								{
									$whereparts[] = "loc.city = " . $db->Quote($val['city']);
								}
								if (count($whereparts) > 0)
								{
									$orparts[] = " (" . implode(" AND ", $whereparts) . ")";
								}
							}
						}
						if (count($orparts) > 0)
						{
							$extrawhere[] = " (" . implode(" OR ", $orparts) . ")";
						}
					}
				}
			}
		}
	}

	function onSearchEvents(& $extrasearchfields, & $extrajoin, & $needsgroupdby = false)
	{
		$jinput = JFactory::getApplication()->input;
		static $usefilter;

		if (!isset($usefilter))
		{
			$mainframe = JFactory::getApplication();  // RSH 10/11/10 - Make J!1.6 compatible
			if ($mainframe->isAdmin() && $jinput->getCmd("option") == "com_jevents")
			{
				$usefilter = false;
				return;
			}

			$pluginsDir = JPATH_SITE . "/plugins/jevents/jevlocations/";
			$filters = jevFilterProcessing::getInstance(array("locationsearch"), $pluginsDir . "filters", false, 'jevlocations');
			$filters->setSearchKeywords($extrasearchfields, $extrajoin);
		}

		return true;
	}

	function onListEventsById(& $extrafields, & $extratables, & $extrawhere, & $extrajoin, & $needsgroupdby = false)
	{
		$jinput = JFactory::getApplication()->input;
		if (strpos($jinput->getVar("task", ""), "icals.") === 0 || $jinput->getString("option") == "com_rsvppro")
		{
			return $this->onListIcalEvents($extrafields, $extratables, $extrawhere, $extrajoin, $needsgroupdby);
		}
	}

	function onDisplayCustomFields(&$row)
	{
		$jinput = JFactory::getApplication()->input;
		JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");
		$document = JFactory::getDocument();
		if (isset($row->hidedetail) && $row->hidedetail)
		{
			return true;
		}

		$db = JFactory::getDBO();

		$menuItem = $this->params->get("target_location_itemid", 0);

		if ((!is_numeric($row->location()) || $row->location() == 0) && isset($row->_loc_id) && intval($row->_loc_id) > 0)
		{
			$row->location(intval($row->_loc_id));
		}

		// Deal with location
		if (is_numeric($row->location()) && $row->location() > 0)
		{
			$loc_id = $row->location();

			$compparams = JComponentHelper::getParams("com_jevlocations");
			$usecats = $compparams->get("usecats", 0);

			if ($usecats)
			{
				$sql = "SELECT loc.*, cc1.title AS city, cc2.title AS state, cc3.title AS country "
					. ", GROUP_CONCAT(DISTINCT loccat.title SEPARATOR ',') AS category_list,GROUP_CONCAT(map.catid SEPARATOR ',') AS catid_list"
					. ", loccat.title AS category,loccat.published AS cat_pub, loccat.access AS cat_access "
					. " FROM #__jev_locations as loc"
					. " LEFT JOIN #__jevlocations_catmap AS map ON map.locid = loc.loc_id"
					. " LEFT JOIN #__jevlocation_categories AS cc1 ON cc1.id = loc.catid  AND cc1.section='com_jevlocations'"
					. ' LEFT JOIN #__jevlocation_categories AS cc2 ON cc1.parent_id = cc2.id '
					. ' LEFT JOIN #__jevlocation_categories AS cc3 ON cc2.parent_id = cc3.id ';
			}
			else
			{
				$sql = "SELECT loc.* ";
				$sql .= ", GROUP_CONCAT(DISTINCT loccat.title SEPARATOR ',') AS category_list,GROUP_CONCAT(map.catid SEPARATOR ',') AS catid_list";
				$sql .= ", loccat.title AS category,loccat.published AS cat_pub, loccat.access AS cat_access ";
				$sql .= " FROM #__jev_locations as loc";
				$sql .= " LEFT JOIN #__jevlocations_catmap AS map ON map.locid = loc.loc_id";
				$sql .= " LEFT JOIN #__jevlocation_categories AS cat ON cat.id=loc.catid AND cat.section='com_jevlocations'";
			}
			$sql .= ' LEFT JOIN #__categories AS loccat ON loccat.id = map.catid';
			$sql .= " WHERE loc.loc_id=" . $row->location();
			$sql .= ' GROUP BY loc.loc_id';
			$db->setQuery($sql);
			$location = $db->loadObject();

			// Make sure location is accessible
			if ($location && $location->loccat > 0 && isset($location->cat_access))
			{
				$user = JFactory::getUser();
				$aids = JEVHelper::getAid($user, 'array');
				if (!(in_array($location->cat_access, $aids)))
				{
					$location = false;
					$row->_location = "";
				}
			}
			$loadmapscript = false;

			if ($location && $jinput->getString("task", "") != "icalevent.edit" && $jinput->getString("task", "") != "icalrepeat.edit")
			{
				// Popup size
				$pwidth = $this->params->get("pwidth", "750");
				$pheight = $this->params->get("pheight", "500");

				// Map size
				$gwidth = $this->params->get("gwidth", "200");
				$gheight = $this->params->get("gheight", "150");

				if (is_numeric($gwidth))
				{
					$gwidth = $gwidth . "px";
				}

				if (is_numeric($gheight))
				{
					$gheight = $gheight . "px";
				}

				$modalScript = "";

				$detailpopup = $this->params->get("detailpopup", 1);
				if ($detailpopup)
				{
					$baseUrl = "index.php?option=com_jevlocations&task=locations.detail&tmpl=component&loc_id=$loc_id&title=" . JApplication::stringURLSafe($location->title);
					if ($menuItem != 0)
					{
						$baseUrl .= "&Itemid=" . $menuItem;
					}
					$locurl = JRoute::_($baseUrl);
					$modalScript = JevLocationsHelper::setupDetailModal($this->params, $locurl);
				}
				else
				{
					$baseUrl = "index.php?option=com_jevlocations&task=locations.detail&se=1&loc_id=$loc_id&title=" . JApplication::stringURLSafe($location->title);
					if ($menuItem != 0)
					{
						$baseUrl .= "&Itemid=" . $menuItem;
					}
					$locurl = JRoute::_($baseUrl);
				}

				// zoom is reduced for the smaller map displayed here
				$zoom = $location->geozoom - $this->params->get("reducezoom", 3);

				$maptype = $this->params->get("maptype", "ROADMAP");

				$compparams = JComponentHelper::getParams("com_jevlocations");
				$gregion = $compparams->get("gregion", "US");

				$googleurl = JevLocationsHelper::getApiUrl();
				$googlekey = JevLocationsHelper::getApiKey();
				$googlescript = JevLocationsHelper::getApiScript();
				$googleStaticMapKey = $compparams->get('googlestaticmapskey');

				$long = $location->geolon;
				$lat = $location->geolat;

				$googlestatic = $googleurl . "/maps/api/staticmap?center=$lat,$long&zoom=$zoom&size=$pwidth" . "x" . "$pheight";
				if ($googleStaticMapKey != "")
				{
					$key = "&key=$googleStaticMapKey";
				}
				else if ($googlekey != "")
				{
					$key = "&key=$googlekey";
				}
				//For old domains using Google Maps Service no key is still valid
				else
				{
					//We need to override the URL to use
					$googlestatic = $googleurl . "/maps/api/staticmap?center=$lat,$long&zoom=$zoom&size=$pwidth" . "x" . "$pheight";
					$key = "";
				}

				if (!$compparams->get("staticmap", 0))
				{
					JevLocationsHelper::loadApiScript();
					$googlestatic = "";
					$loadAction = 'window.setTimeout("myMapload()",1000);';
				}
				else
				{
					JHtml::script('com_jevlocations/staticmaps.js', false, true);
					$googlestatic = 'background-image:url(' . $googlestatic . ');';
					$googlestatic = "";
					$loadAction = 'window.setTimeout("setupStaticMap()",1000);';
				}

				$redirecttodirections = $compparams->get("redirecttodirections", 0);
				$googleDirections = $compparams->get('googledirections', 'https://maps.google.com');
				$maplinknewwindow = $compparams->get('maplinktonewwindow', '1');

				$document = JFactory::getDocument();
				$script = "var urlrootloc = '" . JURI::root() . "media/com_jevlocations/images/';\n";

				if ($location->mapicon == "")
				{
					$location->mapicon = "blue-dot.png";
				}

				$directionsSlug = JevLocationsHelper::getDirectionsSlug($lat, $long, $zoom, $subtitle);

				$siteRoot = JUri::Root();
				$script .= <<<SCRIPT
var myMap = false;
var myMarker = false;

var gmapConf = {
				siteRoot:	'$siteRoot',
				googleurl:	'$googleurl',
				mapicon:	'$location->mapicon',
				latitude:	'$lat',
				longitude:	'$long',
				zoom:		'$zoom',
				region:		'$gregion',
				maptype:	'$maptype',
				key:		'$key',
				googlescript:	'$googlescript',
				};

function myMapload(){
	if (!document.getElementById("gmap")) return;

	var myOptions = {
		center: new google.maps.LatLng($lat,$long),
		zoom: $zoom,
		mapTypeId: google.maps.MapTypeId.$maptype,
		draggable:false,
		scrollwheel:false
	};
	myMap = new google.maps.Map(document.getElementById("gmap"), myOptions);

	/** Create our "tiny" marker icon */
	var blueIcon = new google.maps.MarkerImage(urlrootloc + '$location->mapicon',
	/** This marker is 32 pixels wide by 32 pixels tall. */
	new google.maps.Size(32, 32),
	/** The origin for this image is 0,0 within a sprite */
	new google.maps.Point(0,0),
	/** The anchor for this image is the base of the flagpole at 0,32. */
	new google.maps.Point(16, 32));

	var markerOptions = {
		position:new google.maps.LatLng($lat,$long),
		map:myMap,
		icon:blueIcon,
		animation: google.maps.Animation.DROP,
		draggable:false
	};

	myMarker = new google.maps.Marker( markerOptions);

	google.maps.event.addListener(myMarker, "click", function(e) {
		if ($detailpopup){
			if ($redirecttodirections){
				window.open("$locurl");
			}
			else {
				$modalScript
			}
		}
		else {
			if($maplinknewwindow){
				var googleURL = "$googleDirections" + "$directionsSlug";
				var mapsWindow = window.open(googleURL,"_blank");
				mapsWindow.opener = null;
			}
			else{
				document.location = "$locurl";
			}
		}
	});

	google.maps.event.addListener(myMap, "click", function(e) {
		if ($detailpopup){
			if ($redirecttodirections){
				var mapsWindow = window.open("$locurl");
				mapsWindow.opener = null;
			}
			else {
				$modalScript
			}
		}
		else {
			if ($redirecttodirections) {
				document.location = "$googleDirections" + "$directionsSlug";
			}
			else {
				document.location = "$locurl";
			}
		}
	});

};



/**
 * Loads in the Maps API script. This is called after some sort of user interaction.
 * The script loads asynchronously and calls loadMap once it's in.
*/
var isLoaded = false;

jQuery(document).on('ready', function(){
	$loadAction
	});
SCRIPT;
				$location->mapscript = $script;

				if ($detailpopup)
				{
					// Google no longer allows this
					if ($compparams->get("redirecttodirections"))
					{
						$location->linkstart = "<a href='$locurl' target='_blank'>";
					}
					else
					{
						$modalScript = JevLocationsHelper::setupDetailModal($compparams, $locurl);
						$location->linkstart = "<a href='javascript:$modalScript' >";
					}
				}
				else
				{
					$location->linkstart = "<a href='$locurl'>";
				}

				$loadmapscript = false;

				$addressfields = array();

				if ($location->title != "")
				{
					$addressfields[] = $location->title;
				}

				if ($location->street != "")
				{
					$addressfields[] = $location->street;
				}

				if ($location->state != "")
				{
					$addressfields[] = $location->state;
				}

				if ($location->postcode != "")
				{
					$addressfields[] = $location->postcode;
				}

				if ($location->country != "")
				{
					$addressfields[] = $location->country;
				}

				$template = $this->params->get("template", "");

				if ($template != "")
				{
					$text = $template;
					$text = str_replace("{TITLE}", $location->title == "" ? "" : $location->title, $text);
					$text = str_replace("{STREET}", $location->street == "" ? "" : $location->street, $text);
					$text = str_replace("{CITY}", $location->city == "" ? "" : $location->city, $text);
					$text = str_replace("{STATE}", $location->state == "" ? "" : $location->state, $text);
					$text = str_replace("{POSTCODE}", $location->postcode == "" ? "" : $location->postcode, $text);
					$text = str_replace("{COUNTRY}", $location->country == "" ? "" : $location->country, $text);
					$text = str_replace("{PHONE}", $location->phone == "" ? "" : $location->phone, $text);

					if ($location->image != "")
					{
						$mediaparams = JComponentHelper::getParams('com_media');
						$mediabase = JURI::root() . $mediaparams->get('image_path', 'images/stories');

						// Folder relative to media folder
						$locparams = JComponentHelper::getParams("com_jevlocations");
						$folder = "jevents/jevlocations";
						$thimg = '<img src="' . $mediabase . '/' . $folder . '/thumbnails/thumb_' . $location->image . '" />';
						$img = '<img src="' . $mediabase . '/' . $folder . '/' . $location->image . '" />';
						$text = str_replace("{IMAGE}", $img, $text);
						$text = str_replace("{THUMBNAIL}", $thimg, $text);
					}
					else
					{
						$text = str_replace("{IMAGE}", "", $text);
						$text = str_replace("{THUMBNAIL}", "", $text);
					}

					if (strlen(trim($location->url)) > 0)
					{
						$location->rawurl = $location->url;

						if (strpos($location->url, "http://") === false && strpos($location->url, "https://")  === false)
						{
							$location->url = "http://" . trim($location->url);
						}

						$searches       = array('http:///', 'https:///');
						$replacements   = array('http://', 'https://');

						$location->url = str_replace($searches, $replacements, $location->url);
						$location->url = '<a href="' . $location->url . '"  target="_blank">' . $location->url . '</a>';

						$text = str_replace("{URL}", $location->url, $text);
					}
					else
					{
						$text = str_replace("{URL}", "", $text);
					}

					$text = str_replace("{LINK}", $location->linkstart, $text);
					$text = str_replace("{/LINK}", "</a>", $text);

					$text = str_replace("{DESCRIPTION}", $location->description == "" ? "" : $location->description, $text);
					$map = '<div id="gmap" style="' . $googlestatic . 'width:' . $gwidth . '; height:' . $gheight . ';overflow:hidden !important;"></div>';
					$location->map = $map;
					JFactory::getDocument()->addStyleDeclaration(" #gmap img { max-width: none !important;}");

					if (strpos($text, "{MAP}") !== false)
					{
						$loadmapscript = true;
					}

					$text = str_replace("{MAP}", $map, $text);
				}
				else
				{
					$text = $location->title;
					if (strlen($location->street) > 0)
						$text .= '<br/>' . $location->street;
					if (strlen($location->city) > 0)
						$text .= '<br/>' . $location->city;
					if (strlen($location->state) > 0)
						$text .= "<br/>" . $location->state;
					if (strlen($location->postcode) > 0)
						$text .= "<br/>" . $location->postcode;
					if (strlen($location->country) > 0)
						$text .= "<br/>" . $location->country;
					if (strlen($location->phone) > 0)
						$text .= "<br/>" . $location->phone;

					if (strlen($location->url) > 0)
					{
						$location->rawurl = $location->url;
						if (strpos($location->url, "http://") === false && strpos($location->url, "https://")  === false)
						{
							$location->url = "http://" . trim($location->url);
						}

						$searches       = array('http:///', 'https:///');
						$replacements   = array('http://', 'https://');

						$location->url = str_replace($searches, $replacements, $location->url);
						$location->url = '<a href="' . $location->url . '"  target="_blank">' . $location->url . '</a>';
						$text .= "<br/>" . $location->url;
					}

					$map = '<div id="gmap" style="width:' . $gwidth . '; height:' . $gheight . ';overflow:hidden !important;"></div>';
					$location->map = $map;
					JFactory::getDocument()->addStyleDeclaration(" #gmap img { max-width: none !important;}");
					$loadmapscript = true;
					$text .= $map;
					if ($this->params->get("showdesc", 0) && strlen($location->description) > 0)
						$text .= "<br/>" . $location->description;
				}
				$row->location($text);

				$row->_locationaddress = implode(", ", $addressfields);
				$row->_locationsummary = $text;
				// Add reference to location info in the $event
				$row->_jevlocation = $location;
				//$row->_location = $location;
			}
			else
			{
				$row->_locationsummary = "";
			}

			if ($loadmapscript)
			{
				$document = JFactory::getDocument();
				$document->addScriptDeclaration($location->mapscript);
				$location->mapscriptloaded = true;
			}


			if ($location)
			{
				// custom fields
				JLoader::register('JEVHelper', JPATH_SITE . "/components/com_jevents/libraries/helper.php");
				JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");
				JLoader::register('JevCfForm', JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/jevcfform.php");
				$locparams = JComponentHelper::getParams("com_jevlocations");
				$template = $locparams->get("fieldtemplate", "");
				if ($template != "")
				{
					$xmlfile = JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/templates/" . $template;
					if (file_exists($xmlfile))
					{
						$db = JFactory::getDBO();
						$db->setQuery("SELECT * FROM #__jev_customfields3 WHERE target_id=" . intval($location->loc_id) . " AND targettype='com_jevlocations'");

						$cfdata = $db->loadObjectList();

						$jcfparams = JevCfForm::getInstance("com_jevlocations.customfields", $xmlfile, array('control' => 'jform', 'load_data' => true), true, "/form");

						$data = array();
						foreach ($cfdata as $dataelem)
						{
							if (strpos($dataelem->name, ".") !== false)
							{
								$dataelem->name = str_replace(".", "_", $dataelem->name);
							}
							$data[$dataelem->name] = $dataelem->value;
						}
						$jcfparams->bind($data);
						$jcfparams->setEvent($row);

						$customfields = array();
						$groups = $jcfparams->getFieldsets();
						foreach ($groups as $group => $element)
						{
							if ($jcfparams->getFieldCountByFieldSet($group))
							{
								$customfields = array_merge($customfields, $jcfparams->renderToBasicArray('params', $group));
							}
						}

						// Strange PHP bug - or is it xDebug ???
						//$row->_jevlocation->customfields = $params;
						$location = $row->_jevlocation;
						$location->customfields = $customfields;
						$row->_jevlocation = $location;
					}
				}
			}
		}
	}

	function onDisplayCustomFieldsMultiRow(&$rows)
	{
		$jinput = JFactory::getApplication()->input;
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$view = $jinput->getVar("view", "");
		if (strpos($jinput->getVar("task", ""), "icals.") !== 0 || ($this->params->get("icalexportfull", 0) == 0 && $this->params->get("icalexportgeo", 0) == 0) || !count($rows))
		{
			if ($compparams->get('loadlocationscustomfieldsinjevents', 0))
			{
				if (strpos($view, "month") !== false || strpos($view, "week") !== false || strpos($view, "range") !== false || strpos($view, "day") !== false || strpos($jinput->getVar("task", ""), "year") !== false)
				{
					JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");
					$rows = JevLocationsHelper::getLocationsCustomFieldsForEvents($rows);
				}
			}

			return "";
		}

		for ($r = 0; $r < count($rows); $r++)
		{

			if (!isset($rows[$r]->_loc_title) || $rows[$r]->_loc_title == "")
			{
				continue;
			}

			if (isset($rows[$r]->locationprocessed))
			{
				continue;
			}
			$rows[$r]->locationprocessed = 1;

			$row = &$rows[$r];

			$db = JFactory::getDBO();

			// Deal with location
			if (is_numeric($row->_loc_id) && intval($row->_loc_id) > 0)
			{
				$loc_id = intval($row->_loc_id);


				$usecats = $compparams->get("usecats", 0);

				if ($usecats)
				{
					$sql = "SELECT loc.*, cc1.title AS city, cc2.title AS state, cc3.title AS country"
						. ", GROUP_CONCAT(DISTINCT loccat.title SEPARATOR ',') AS category_list,GROUP_CONCAT(map.catid SEPARATOR ',') AS catid_list"
						. ", loccat.title AS category,loccat.published AS cat_pub, loccat.access AS cat_access "
						. " FROM #__jev_locations as loc"
						. " LEFT JOIN #__jevlocations_catmap AS map ON map.locid = loc.loc_id"
						. " LEFT JOIN #__jevlocation_categories AS cc1 ON cc1.id = loc.catid  AND cc1.section='com_jevlocations'"
						. ' LEFT JOIN #__jevlocation_categories AS cc2 ON cc1.parent_id = cc2.id '
						. ' LEFT JOIN #__jevlocation_categories AS cc3 ON cc2.parent_id = cc3.id ';
				}
				else
				{
					$sql = "SELECT loc.* ";
					$sql .= ", GROUP_CONCAT(DISTINCT loccat.title SEPARATOR ',') AS category_list,GROUP_CONCAT(map.catid SEPARATOR ',') AS catid_list";
					$sql .= ", loccat.title AS category,loccat.published AS cat_pub, loccat.access AS cat_access ";
					$sql .= " FROM #__jev_locations as loc";
					$sql .= " LEFT JOIN #__jevlocations_catmap AS map ON map.locid = loc.loc_id";
					$sql .= " LEFT JOIN #__jevlocation_categories AS cat ON cat.id=loc.catid AND cat.section='com_jevlocations'";
				}
				$sql .= 'LEFT JOIN #__categories AS loccat ON loccat.id = map.catid';
				$sql .= " WHERE loc.loc_id=" . $loc_id;
				$sql .= ' GROUP BY loc.loc_id';
				$db->setQuery($sql);
				$location = $db->loadObject();
			}
			else
			{
				continue;
			}

			if ($this->params->get("icalexportfull", 0))
			{
				$parts = array($rows[$r]->_loc_title);
				$fields = array("street", "city", "state", "postcode", "country");
				foreach ($fields as $field)
				{
					// Removed if query to include extra , at all times for import measures.
					//if (isset($location->$field) && trim($location->$field) != "")
					//{
					$parts [] = trim($location->$field);
					//}
				}
				$rows[$r]->_loc_title = implode("#£@ ", $parts);
				$rows[$r]->_loc_title = preg_replace("/#£@/", " -", $rows[$r]->_loc_title, 1);
				$rows[$r]->_loc_title = str_replace("#£@", ",", $rows[$r]->_loc_title);
				$rows[$r]->location($rows[$r]->_loc_title);
			}

			if ($this->params->get("icalexportgeo", 0))
			{
				if (isset($location->geolon) && trim($location->geolon) != "")
				{
					// append to summary/title field to make sure that wrap lines doesn't throw the pages
					$geo = "\r\nGEO:" . $location->geolon . ";" . $location->geolat;
					$row->_title .= $geo;
				}
			}
		}
	}

	/**
	 * Called by latest events module
	 *
	 * @param type $dayEvent
	 * @param type $part
	 * @param type $tempstr
	 */
	function onLatestEventsField(&$row, $part, &$tempstr)
	{
		$db = JFactory::getDBO();

		// Deal with location
		if (isset($row->_loc_id) && $row->_loc_id > 0)
		{
			$tempstr .= $this->substitutefield($row, strtoupper($part));
		}
	}

	function onCheckEventOverlaps(& $testevent, & $overlaps, $eventid, $requestObject)
	{
		$location = $requestObject->formdata->location;
		if ($location == "" || $location == 0)
			return;
		$db = JFactory::getDBO();
		if (is_numeric($location))
		{
			$db->setQuery("SELECT * FROM #__jev_locations where loc_id = " . $location);
			$loc = $db->loadObject();
			if (!$loc || !$loc->overlaps)
			{
				return true;
			}
			$locationname = $loc->title;
		}
		else
		{
			$locationname = $location;
		}
		foreach ($testevent->repetitions as $repeat)
		{

			$sql = "SELECT * FROM #__jevents_repetition as rpt ";
			$sql .= " LEFT JOIN #__jevents_vevdetail as det ON det.evdet_id=rpt.eventdetail_id ";
			$sql .= " LEFT JOIN #__jevents_vevent as evt ON evt.ev_id=rpt.eventid ";
			$sql .= " WHERE rpt.eventid<>" . intval($eventid) . " AND rpt.startrepeat<" . $db->Quote($repeat->endrepeat) . " AND rpt.endrepeat>" . $db->Quote($repeat->startrepeat);
			$sql .= " AND det.loc_id = " . $location;
			$sql .= " AND evt.state=1";
			$sql .= " GROUP BY rpt.rp_id";
			$db->setQuery($sql);
			$conflicts = $db->loadObjectList();
			if ($conflicts && count($conflicts) > 0)
			{
				foreach ($conflicts as &$conflict)
				{
					$conflict->conflictCause = JText::sprintf("JEV_LOCATION_CLASH", $locationname);
				}
				unset($conflict);
				$overlaps = array_merge($overlaps, $conflicts);
			}
		}
	}

	function onCheckRepeatOverlaps(& $repeat, & $overlaps, $eventid, $requestObject)
	{
		$location = $requestObject->formdata->location;
		if ($location == "" || $location == 0)
			return;
		$db = JFactory::getDBO();
		if (is_numeric($location))
		{
			$db->setQuery("SELECT * FROM #__jev_locations where loc_id = " . $location);
			$loc = $db->loadObject();
			if (!$loc || !$loc->overlaps)
			{
				return true;
			}
			$locationname = $loc->title;
		}
		else
		{
			$locationname = $location;
		}

		$sql = "SELECT * FROM #__jevents_repetition as rpt ";
		$sql .= " LEFT JOIN #__jevents_vevdetail as det ON det.evdet_id=rpt.eventdetail_id ";
		$sql .= " LEFT JOIN #__jevents_vevent as ev ON ev.ev_id=rpt.eventid ";
		$sql .= " WHERE rpt.rp_id<>" . intval($repeat->rp_id) . " AND rpt.startrepeat<" . $db->Quote($repeat->endrepeat) . " AND rpt.endrepeat>" . $db->Quote($repeat->startrepeat);
		$sql .= " AND det.loc_id = " . $location;
		$sql .= " AND ev.state=1";

		$db->setQuery($sql);
		$conflicts = $db->loadObjectList();
		if ($conflicts && count($conflicts) > 0)
		{
			foreach ($conflicts as &$conflict)
			{
				$conflict->conflictCause = JText::sprintf("JEV_LOCATION_CLASH", $locationname);
			}
			unset($conflict);
			$overlaps = array_merge($overlaps, $conflicts);
		}
	}

	function onSendAdminMail(&$mail, $event)
	{
		$locparams = JComponentHelper::getParams("com_jevlocations");
		if (!$locparams->get("notifycreator"))
		{
			return;
		}
		// Deal with location
		if (is_numeric($event->location()) && $event->location() > 0)
		{
			$loc_id = $event->location();

			$db = JFactory::getDbo();
			$sql = "SELECT * FROM #__jev_locations ";
			$sql .= " WHERE loc_id=" . $loc_id;
			$db->setQuery($sql);
			$location = $db->loadObject();

			if ($location && $location->created_by > 0)
			{
				$creator = JEVHelper::getUser($location->created_by);
				if ($creator && $creator->id == $location->created_by)
				{
					$mail->addCC($creator->email);
				}
			}

			// parse location title
			$mail->Subject = str_replace("{LOCATION}", isset($location->title) ? $location->title : "", $mail->Subject);
			$mail->Body = str_replace("{LOCATION}", isset($location->title) ? $location->title : "", $mail->Body);
		}
	}

	public static function fieldNameArray($layout = 'detail')
	{
		$locparams = JComponentHelper::getParams("com_jevlocations");
// only offer in detail view
//if ($layout != "detail") return array();

		$labels = array();
		$values = array();
		if ($layout == "detail")
		{
			$labels[] = JText::_("JEV_LOCATION_SUMMARY", true);
			$values[] = "JEVLOCATION_SUMMARY";
		}
		$labels[] = JText::_("JEV_LOCATION_TITLE", true);
		$values[] = "JEVLOCATION_TITLE";
		$labels[] = JText::_("JEV_LOCATION_TITLE_LINK", true);
		$values[] = "JEVLOCATION_TITLE_LINK";
		$labels[] = JText::_("JEV_LOCATION_DESCRIPTION", true);
		$values[] = "JEVLOCATION_DESCRIPTION";
		$labels[] = JText::_("JEV_LOCATION_STREET", true);
		$values[] = "JEVLOCATION_STREET";
		$labels[] = JText::_("JEV_LOCATION_CITY", true);
		$values[] = "JEVLOCATION_CITY";
		$labels[] = JText::_("JEV_LOCATION_STATE", true);
		$values[] = "JEVLOCATION_STATE";
		$labels[] = JText::_("JEV_LOCATION_COUNTRY", true);
		$values[] = "JEVLOCATION_COUNTRY";
		$labels[] = JText::_("JEV_LOCATION_POSTCODE", true);
		$values[] = "JEVLOCATION_POSTCODE";
		$labels[] = JText::_("JEV_LOCATION_PHONE", true);
		$values[] = "JEVLOCATION_PHONE";
		$labels[] = JText::_("JEV_LOCATION_CATEGORY", true);
		$values[] = "JEVLOCATION_CATEGORY";
		$labels[] = JText::_("JEV_LOCATION_MODIFIED", true);
		$values[] = "JEVLOCATION_MODIFIED";
		$labels[] = JText::_("JEV_LOCATION_CREATED_BY_USERNAME");
		$values[] = "JEVLOCATION_CREATED_BY_USERNAME";
		$labels[] = JText::_("JEV_LOCATION_CREATED_BY_NAME");
		$values[] = "JEVLOCATION_CREATED_BY_NAME";
		$labels[] = JText::_("JEV_LOCATION_CREATED_BY_ID");
		$values[] = "JEVLOCATION_CREATED_BY_ID";

		// only offer in detail view
		if ($layout != "list")
		{
			$labels[] = JText::_("JEV_LOCATION_MAP", true);
			$values[] = "JEVLOCATION_MAP";
		}
		$labels[] = JText::_("JEV_LOCATION_IMAGE", true);
		$values[] = "JEVLOCATION_IMAGE";
		$labels[] = JText::_("JEV_LOCATION_IMAGEURL", true);
		$values[] = "JEVLOCATION_IMAGEURL";
		$labels[] = JText::_("JEV_LOCATION_THUMB", true);
		$values[] = "JEVLOCATION_THUMB";
		$labels[] = JText::_("JEV_LOCATION_URL", true);
		$values[] = "JEVLOCATION_URL";
		$labels[] = JText::_("JEV_LOCATION_RAWURL", true);
		$values[] = "JEVLOCATION_RAWURL";
		$labels[] = JText::_("JEV_LOCATION_ALIAS", true);
		$values[] = "JEVLOCATION_ALIAS";
		$labels[] = JText::_("JEV_LOCATION_URL_ACTIVE", true);
		$values[] = "JEVLOCATION_URL_ACTIVE";
		$labels[] = JText::_("JEV_LOCATION_LINK_A", true);
		$values[] = "JEVLOCATION_A";
		$labels[] = JText::_("JEV_LOCATION_LINK_CLOSE_A", true);
		$values[] = "JEVLOCATION_CLOSE_A";

		$labels[] = JText::_("JEV_LOCATION_ID", true);
		$values[] = "JEVLOC_ID";
		$labels[] = JText::_("JEV_LOCATION_EVENTS", true);
		$values[] = "JEVLOC_EVENTS";
		$labels[] = JText::_("JEV_LOCATION_CITY_EVENTS", true);
		$values[] = "JEVLOC_CITYEVENTS";

		$labels[] = JText::_("JEV_LOCATION_LON", true);
		$values[] = "JEVLOCATION_LON";
		$labels[] = JText::_("JEV_LOCATION_LAT", true);
		$values[] = "JEVLOCATION_LAT";
		$labels[] = JText::_("JEV_LOCATION_TRUNCATED_DESC", true);
		$values[] = "JEVLOC_TRUNCATED_DESC:.*?";
		$labels[] = JText::_("JEV_LOCATION_PRIORITY", true);
		$values[] = "JEVLOCATION_PRIORITY";

		if ($layout == "detail" || $layout == "bloglist" || $layout == "locations" || $locparams->get('loadlocationscustomfieldsinjevents', 0))
		{
			$template = $locparams->get("fieldtemplate", "");
			if ($template != "")
			{
				$xmlfile = JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/templates/" . $template;
				if (file_exists($xmlfile) && class_exists("JevCfform"))
				{

					$jcfparams = JevCfForm::getInstance("com_jevlocations.customfields", $xmlfile, array('control' => 'jform', 'load_data' => true), true, "/form");
					$customfields = array();
					$groups = $jcfparams->getFieldsets();
					foreach ($groups as $group => $element)
					{
						if ($jcfparams->getFieldCountByFieldSet($group))
						{
							$customfields = array_merge($customfields, $jcfparams->renderToBasicArray('params', $group));
						}
					}

					if ($customfields && count($customfields) > 0)
					{
						foreach ($customfields as $customfield)
						{
							$labels[] = JText::_("JEV_LOCATION_LAYOUT_CUSTOMFIELDS_SHORTLABEL") . " - " . $customfield["label"];
							$values[] = "lcf_" . $customfield["name"];

							$label = JText::_("JEV_LOCCUSTOM_FIELD_LABEL");
							if (strpos($label, '%') === false)
							{
								$label = "%s Label";
							}
							$labels[] = JText::_("JEV_LOCATION_LAYOUT_CUSTOMFIELDS_SHORTLABEL") . " - " . JText::sprintf($label, $customfield["label"]);
							$values[] = "lcf_" . $customfield["name"] . "_lbl";


							if (isset($customfield["fieldnamearray"]) && count($customfield["fieldnamearray"]) > 0)
							{
								for ($i = 0; $i < count($customfield["fieldnamearray"]["labels"]); $i++)
								{
									$lab = $customfield["fieldnamearray"]["labels"][$i];
									$val = $customfield["fieldnamearray"]["values"][$i];

									$labels[] = "lcf_" . str_replace(":", "", $customfield["label"] . "($lab)");
									$values[] = "lcf_" . $customfield["name"] . "($val)";

									$labels[] = "lcf_" . JText::sprintf($label, str_replace(":", "", $customfield["label"] . "($lab)"));
									$values[] = "lcf_" . $customfield["name"] . "_lbl";
								}
							}
						}
					}
				}
			}
		}
		if ($layout != "list")
		{
			$labels[] = JText::_("COM_JEVLOCATIONS_UPCOMING_EVENTS", true);
			$values[] = "JEVLOC_UPCOMING";
		}

		if ($layout == "bloglist" || $layout == "locations")
		{
			$labels[] = JText::_("COM_JEVLOCATIONS_UPCOMING_EVENTS_CHECKED", true);
			$values[] = "JEVLOC_UPCOMINGCHECKED";
		}

		$return = array();
		$return['group'] = JText::_("JEV_LOCATION_ADDON", true);
		$return['values'] = $values;
		$return['labels'] = $labels;

		return $return;
	}

	public static function substitutefield($row, $code)
	{
		if (isset($row->hidedetail) && $row->hidedetail)
		{
			return "";
		}

		$jinput = JFactory::getApplication()->input;
		JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");

		if (isset($row->hidedetail) && $row->hidedetail)
		{
			return "";
		}
		switch ($code)
		{
			case "JEVLOCATION_SUMMARY":
				if (isset($row->_locationsummary))
					return $row->_locationsummary;
				break;
			case "JEVLOCATION_PRIORITY":
				if (isset($row->_locationsummary))
					return $row->_priority;
				else if (isset($row->_jevlocation->priority))
					return $row->_jevlocation->priority;
				break;
			case "JEVLOCATION_IMAGE":
			case "JEVLOCATION_IMAGEURL":
				if (isset($row->_jevlocation))
				{
					$field = "image";

					if (strlen($row->_jevlocation->$field) > 0)
					{
						$mediaparams = JComponentHelper::getParams('com_media');
						$mediabase = JURI::root() . $mediaparams->get('image_path', 'images/stories');
						// folder relative to media folder
						$locparams = JComponentHelper::getParams("com_jevlocations");
						$folder = "jevents/jevlocations";
						if ($code == "JEVLOCATION_IMAGEURL")
						{
							return $mediabase . '/' . $folder . '/' . $row->_jevlocation->$field;
						}
						else
						{
							return '<img src="' . $mediabase . '/' . $folder . '/' . $row->_jevlocation->$field . '" itemprop="image" />';
						}
					}
				}
				// list version
				else if (isset($row->_loc_image) && trim($row->_loc_image) != "")
				{
					$mediaparams = JComponentHelper::getParams('com_media');
					$mediabase = JURI::root() . $mediaparams->get('image_path', 'images/stories');
					// folder relative to media folder
					$locparams = JComponentHelper::getParams("com_jevlocations");
					$folder = "jevents/jevlocations";
					if ($code == "JEVLOCATION_IMAGEURL")
					{
						return $mediabase . '/' . $folder . '/' . $row->_loc_image;
					}
					else
					{
						return '<img src="' . $mediabase . '/' . $folder . '/' . $row->_loc_image . '" itemprop="image" />';
					}
				}
				break;
			case "JEVLOCATION_THUMB":
			case "JEVLOCATION_THUMBURL":
				if (isset($row->_jevlocation))
				{
					$field = "image";

					if (strlen($row->_jevlocation->$field) > 0)
					{
						$mediaparams = JComponentHelper::getParams('com_media');
						$mediabase = JURI::root() . $mediaparams->get('image_path', 'images/stories');
						// folder relative to media folder
						$locparams = JComponentHelper::getParams("com_jevlocations");
						$folder = "jevents/jevlocations";
						if ($code == "JEVLOCATION_THUMBURL")
						{
							return $mediabase . '/' . $folder . '/thumbnails/thumb_' . $row->_jevlocation->$field;
						}
						else
						{
							return '<img src="' . $mediabase . '/' . $folder . '/thumbnails/thumb_' . $row->_jevlocation->$field . '" />';
						}
					}
				}
				// list version
				else if (isset($row->_loc_image) && trim($row->_loc_image) != "")
				{
					$mediaparams = JComponentHelper::getParams('com_media');
					$mediabase = JURI::root() . $mediaparams->get('image_path', 'images/stories');
					// folder relative to media folder
					$locparams = JComponentHelper::getParams("com_jevlocations");
					$folder = "jevents/jevlocations";
					if ($code == "JEVLOCATION_THUMBURL")
					{
						return $mediabase . '/' . $folder . '/thumbnails/thumb_' . $row->_loc_image;
					}
					else
					{
						return '<img src="' . $mediabase . '/' . $folder . '/thumbnails/thumb_' . $row->_loc_image . '" />';
					}
				}
				break;
			case "JEVLOCATION_POPUP":
				if (isset($row->_jevlocation))
				{
					$field = "image";

					if (strlen($row->_jevlocation->$field) > 0)
					{
						$mediaparams = JComponentHelper::getParams('com_media');
						$mediabase = JURI::root() . $mediaparams->get('image_path', 'images/stories');
						// folder relative to media folder
						$locparams = JComponentHelper::getParams("com_jevlocations");
						$folder = "jevents/jevlocations";

						$img = $mediabase . '/' . $folder . '/' . $row->_jevlocation->$field;
						$thumb = $mediabase . '/' . $folder . '/thumbnails/thumb_' . $row->_jevlocation->$field;

						$modalSelector = 'jevlocmodal';

						$src = "<a class='$modalSelector' rel='{handler: \"image\",}' href='" . $img . "' ><img src='" . $thumb . "' class='jev_imagethumb'  /></a>";
						JHtmlBootstrap::modal($modalSelector);
						return $src;
					}
				}
				// list version
				else if (isset($row->_loc_image) && trim($row->_loc_image) != "")
				{
					$mediaparams = JComponentHelper::getParams('com_media');
					$mediabase = JURI::root() . $mediaparams->get('image_path', 'images/stories');
					// folder relative to media folder
					$locparams = JComponentHelper::getParams("com_jevlocations");
					$folder = "jevents/jevlocations";

					$img = $mediabase . '/' . $folder . '/' . $row->_loc_image;
					$thumb = $mediabase . '/' . $folder . '/thumbnails/thumb_' . $row->_loc_image;
					$modalSelector = 'jevlocmodal';

					$src = "<a class='$modalSelector' rel='{handler: \"image\",}' href='" . $img . "' ><img src='" . $thumb . "' class='jev_imagethumb'  /></a>";
					JHtmlBootstrap::modal($modalSelector);
					return $src;
				}
				break;
			case "JEVLOCATION_TITLE":
				if (isset($row->_jevlocation))
				{
					$field = "title";
					if (strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				else if (is_string($row->location()))
				{
					return $row->location();
				}
				// list version
				else if (isset($row->_loc_title))
					return $row->_loc_title;
				break;
			case "JEVLOCATION_TITLE_LINK":
				if (isset($row->_jevlocation))
				{
					$field = "linkstart";
					if (strlen($row->_jevlocation->$field) > 0)
					{
						JevModal::framework();
						return $row->_jevlocation->$field . $row->_jevlocation->title . '</a>';
					}
				}
				// list version
				else if (isset($row->_loc_id))
				{
					$plugin = JPluginHelper::getPlugin("jevents", "jevlocations");
					$params = new JRegistry($plugin->params);
					$compparams = JComponentHelper::getParams("com_jevlocations");
					if ($compparams->get("gwidth", -1) != -1)
					{
						$params = $compparams;
					}

					$loc_id = $row->_loc_id;
					$detailpopup = $params->get("detailpopup", 1);
					$menuItem = $params->get("target_location_itemid", 0);

					$baseUrl = "index.php?option=com_jevlocations&task=locations.detail&se=1&loc_id=$loc_id&title=" . JApplication::stringURLSafe($row->_loc_title);

					if ($menuItem != 0)
					{
						$baseUrl .= '&Itemid=' . $menuItem;
					}

					$locurl = JRoute::_($baseUrl);

					return "<a href='$locurl' >$row->_loc_title</a>";
				}
				break;
			case "JEVLOCATION_ALIAS":
				if (isset($row->_jevlocation))
				{
					$field = "alias";

					if (strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				else if (is_string($row->location()))
				{
					return $row->location();
				}
				// list version
				else if (isset($row->_loc_alias))
					return $row->_loc_alias;
				break;
			case "JEVLOCATION_DESCRIPTION":
			case "JEVLOC_TRUNCATED_DESC:.*?":
				if (isset($row->_jevlocation))
				{
					$field = "description";
					if (strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_desc))
					return $row->_loc_desc;
				break;
			case "JEVLOCATION_STREET":
				if (isset($row->_jevlocation))
				{
					$field = "street";
					if (strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_street))
					return $row->_loc_street;
				break;
			case "JEVLOCATION_CITY":
				if (isset($row->_jevlocation))
				{
					$field = "city";
					if (strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_city))
					return $row->_loc_city;
				break;
			case "JEVLOC_CITYEVENTS":
				$city = false;
				if (isset($row->_jevlocation))
				{
					$field = "city";
					if (strlen($row->_jevlocation->$field) > 0)
						$city = $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_city))
					$city = $row->_loc_city;
				if ($city)
				{
					$plugin = JPluginHelper::getPlugin("jevents", "jevlocations");
					$params = new JRegistry($plugin->params);
					$compparams = JComponentHelper::getParams("com_jevlocations");
					if ($compparams->get("gwidth", -1) != -1)
					{
						$params = $compparams;
					}
					$Itemid = $params->get("target_itemid", $jinput->getInt("Itemid", 0));
					static $task;
					if (!isset($task))
					{
						$menu = JFactory::getApplication()->getMenu('site');
						$menuItem = $menu->getItem($Itemid);
						if ($menuItem && $menuItem->component == JEV_COM_COMPONENT)
						{
							if (isset($menuItem->query["task"]))
							{
								$task = $menuItem->query["task"];
							}
							else
							{
								$task = $menuItem->query["view"] . "." . $menuItem->query["layout"];
							}
						}
						else
						{
							$task = "month.calendar";
						}
					}
					return "<a href='" . JRoute::_("index.php?option=" . JEV_COM_COMPONENT . "&Itemid=" . $Itemid . "&task=" . $task . "&loccity_fv=" . $city, false) . "'>" . $city . "</a>";
				}
				break;
			case "JEVLOCATION_STATE":
				if (isset($row->_jevlocation))
				{
					$field = "state";
					if (strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_state))
					return $row->_loc_state;
				break;
			case "JEVLOCATION_COUNTRY":
				if (isset($row->_jevlocation))
				{
					$field = "country";
					if (strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_country))
					return $row->_loc_country;
				break;
			case "JEVLOCATION_POSTCODE":
				if (isset($row->_jevlocation))
				{
					$field = "postcode";
					if (strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_postcode))
					return $row->_loc_postcode;
				break;
			case "JEVLOCATION_PHONE":
				if (isset($row->_jevlocation))
				{
					$field = "phone";
					if (strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_phone))
					return $row->_loc_phone;
				break;
			case "JEVLOCATION_MAP":
				if (isset($row->_jevlocation))
				{
					$field = "map";
					if (strlen($row->_jevlocation->$field) > 0)
					{
						if (isset($row->_jevlocation->mapscript) && !isset($row->_jevlocation->mapscriptloaded))
						{
							$row->_jevlocation->mapscriptloaded = true;
							$document = JFactory::getDocument();
							$document->addScriptDeclaration($row->_jevlocation->mapscript);
						}
						return $row->_jevlocation->$field;
					}
				}
				break;
			case "JEVLOCATION_URL":
				if (isset($row->_jevlocation))
				{
					$field = "url";
					if (strlen($row->_jevlocation->$field) > 0 && $row->_jevlocation->$field != "http://")
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_url) && $row->_loc_url != "http://")
					return $row->_loc_url;
				break;
			case "JEVLOCATION_URL_ACTIVE":
				if (isset($row->_jevlocation))
				{
					$field = "url";
					if (strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_url))
				{
					return JevLocationsHTML::getURLElement($row->_loc_url);
				}

				break;
			case "JEVLOCATION_RAWURL":
				if (isset($row->_jevlocation))
				{
					$field = "rawurl";
					if (isset($row->_jevlocation->$field) && strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_url))
					return $row->_loc_url;
				break;
			case "JEVLOC_EVENTS":
				if (isset($row->_jevlocation) || isset($row->_loc_id))
				{
					if (isset($row->_jevlocation))
					{
						$loc_id = $row->_jevlocation->loc_id;
						$title = $row->_jevlocation->title;
						;
					}
					else
					{
						$loc_id = $row->_loc_id;
						$title = $row->_loc_title;
					}
					if (intval($loc_id) <= 0)
						return "";
					$plugin = JPluginHelper::getPlugin("jevents", "jevlocations");
					$params = new JRegistry($plugin->params);
					$compparams = JComponentHelper::getParams("com_jevlocations");
					if ($compparams->get("gwidth", -1) != -1)
					{
						$params = $compparams;
					}
					$Itemid = $params->get("target_itemid", $jinput->getInt("Itemid", 0));
					static $task;
					if (!isset($task))
					{
						$menu = JFactory::getApplication()->getMenu('site');
						$menuItem = $menu->getItem($Itemid);
						if ($menuItem && $menuItem->component == JEV_COM_COMPONENT)
						{
							if (isset($menuItem->query["task"]))
							{
								$task = $menuItem->query["task"];
							}
							else
							{
								$task = $menuItem->query["view"] . "." . $menuItem->query["layout"];
							}
						}
						else
						{
							$task = "month.calendar";
						}
					}
					return "<a href='" . JRoute::_("index.php?option=" . JEV_COM_COMPONENT . "&Itemid=" . $Itemid . "&task=" . $task . "&loclkup_fv=" . $loc_id, false) . "'>" . $title . "</a>";
				}
				break;
			case "JEVLOCATION_A":
				if (isset($row->_jevlocation))
				{
					$field = "linkstart";
					if (strlen($row->_jevlocation->$field) > 0)
					{
						JevModal::framework();
						return $row->_jevlocation->$field;
					}
				}
				// list version
				else if (isset($row->_loc_id))
				{

					$plugin = JPluginHelper::getPlugin("jevents", "jevlocations");
					$params = new JRegistry($plugin->params);
					$compparams = JComponentHelper::getParams("com_jevlocations");
					if ($compparams->get("gwidth", -1) != -1)
					{
						$params = $compparams;
					}

					$loc_id = $row->_loc_id;
					$detailpopup = $params->get("detailpopup", 1);
					$menuItem = $params->get("target_location_itemid", 0);
					if ($detailpopup)
					{
						$baseUrl = "index.php?option=com_jevlocations&task=locations.detail&tmpl=component&loc_id=$loc_id&title=" . JApplication::stringURLSafe($row->_loc_title);
						if ($menuItem != 0)
						{
							$baseUrl .= "&Itemid=" . $menuItem;
						}
						$locurl = JRoute::_($baseUrl);
						$scriptModal = JevLocationsHelper::setupDetailModal($params, $locurl);

						return "<a href='javascript:$scriptModal'>";
					}
					else
					{
						$baseUrl = "index.php?option=com_jevlocations&task=locations.detail&se=1&loc_id=$loc_id&title=" . JApplication::stringURLSafe($row->_loc_title);
						if ($menuItem != 0)
						{
							$baseUrl .= "&Itemid=" . $menuItem;
						}
						$locurl = JRoute::_($baseUrl);
						return "<a href='$locurl' >";
					}
				}
				break;
			case "JEVLOCATION_CLOSE_A":
				if (isset($row->_jevlocation))
				{
					$field = "linkstart";
					if (strlen($row->_jevlocation->$field) > 0)
						return "</a>";
				}
				// list version
				else if (isset($row->_loc_id))
				{
					return "</a>";
				}

				break;
			case "JEVLOC_UPCOMING":
			case "JEVLOC_UPCOMINGCHECKED":
				if ($code == "JEVLOC_UPCOMINGCHECKED" && isset($row->_jevlocation->hasEvents) && $row->_jevlocation->hasEvents == 0)
				{
					return;
				}
				ob_start();

				$compparams = JComponentHelper::getParams("com_jevlocations");

				require_once (JPATH_SITE . "/modules/mod_jevents_latest/helper.php");

				$jevhelper = new modJeventsLatestHelper();
				$theme = JEV_CommonFunctions::getJEventsViewName();

				$loctheme = false;
				if ($compparams->get("com_calViewName", "") != "")
				{
					$loctheme = $compparams->get("com_calViewName", "");
				}
				if ($loctheme && $loctheme != "global")
				{
					$theme = $loctheme;
				}

				JPluginHelper::importPlugin("jevents");
				// set an unused extra constraint
				for ($extra = 0; $extra < 20; $extra++)
				{
					if ($compparams->get("extras" . $extra, false) == false || strpos($compparams->get("extras" . $extra, false), "jevl:") === 0)
					{
						$compparams->set("extras" . $extra, "jevl:" . $row->_jevlocation->loc_id);
						break;
					}
				}

				$viewclass = $jevhelper->getViewClass($theme, 'mod_jevents_latest', $theme . "/latest", $compparams);

				// record what is running - used by the filters
				$registry = JRegistry::getInstance("jevents");
				$registry->set("jevents.activeprocess", "mod_jevents_latest");
				$registry->set("jevents.moduleid", "loc_" . $row->_jevlocation->loc_id);
				// get new filters
				$registry->set("getnewfilters", 1);

				$menuitem = intval($compparams->get("target_itemid", 0));
				if ($row->_jevlocation->targetmenu > 0)
				{
					$menuitem = $row->_jevlocation->targetmenu;
				}
				if ($menuitem > 0)
				{
					$compparams->set("target_itemid", $menuitem);
				}
				// ensure we use these settings
				$compparams->set("modlatest_useLocalParam", 1);
				$task = $compparams->get("jevview", "month.calendar");
				$link = JRoute::_("index.php?option=com_jevents&task=$task&loclkup_fv=" . $row->_jevlocation->loc_id . "&Itemid=" . $menuitem);
				$view_all_link_html =  "<span class='cal_link'><strong>" . JText::sprintf("COM_JEVLOCATIONS_ALL_EVENTS", $link) . "</strong></span>";
				$linktocal = $compparams->get('modlatest_LinkToCal', 0);

				// WE have a confused setup where some addons use both of these :(
				$registry->set("jevents.moduleparams", $compparams);
				JFactory::getConfig()->set("jev.modparams", $compparams);

				JevHelper::setMenuFilter("loclkup_fv", $row->_jevlocation->loc_id);
				$loclkup_fv = $jinput->setVar("loclkup_fv", $row->_jevlocation->loc_id);

				$modview = new $viewclass($compparams, 0);

				if ($linktocal == 1) {
					echo $view_all_link_html;
				}

				echo $modview->displayLatestEvents();
				$jinput->setVar("loclkup_fv", $loclkup_fv);
				JevHelper::clearMenuFilter("loclkup_fv");

				echo "<br style='clear:both'/>";

				if ($linktocal == 2) {
					echo $view_all_link_html;
				}

			return ob_get_clean();
				break;
			case "JEVLOCATION_MODIFIED":
				if (isset($row->_jevlocation->modified) && $row->_jevlocation->modified != "0000-00-00 00:00:00")
				{
					include_once JPATH_SITE . "/components/com_jevents/jevents.defines.php";
					$date = new JevDate($row->_jevlocation->modified);

					return $date->format(JText::_("JEV_LOCATION_MODIFIED_FORMAT"));
				}
				break;
			case "JEVLOCATION_CREATED_BY_USERNAME":
			case "JEVLOCATION_CREATED_BY_ID":
			case "JEVLOCATION_CREATED_BY_NAME":
				if (isset($row->_jevlocation->created_by) && $row->_jevlocation->created_by != "0")
				{
					$user = new JUser;
					$userTable = $user->getTable();

					if (!$userTable->load($row->_jevlocation->created_by))
					{
						$property = strtolower(str_replace("JEVLOCATION_CREATED_BY_", "", $code));

						return $userTable->{$property};
					}

					return "";
				}
				break;
			case "JEVLOCATION_LAT":
				if (isset($row->_jevlocation))
				{
					$field = "geolat";

					if (isset($row->_jevlocation->$field) && strlen($row->_jevlocation->$field) > 0)
					{
						return $row->_jevlocation->$field;
					}
				}

				// List version
				else if (isset($row->_loc_lat))
					return $row->_loc_lat;
				break;
			case "JEVLOCATION_LON":
				if (isset($row->_jevlocation))
				{
					$field = "geolon";
					if (isset($row->_jevlocation->$field) && strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_lon))
					return $row->_loc_lon;
				break;
			case "JEVLOC_ID":
				if (isset($row->_jevlocation))
				{
					$field = "loc_id";
					if (strlen($row->_jevlocation->$field) > 0)
						return $row->_jevlocation->$field;
				}
				// list version
				else if (isset($row->_loc_id))
					return $row->_loc_id;
				break;
			case "JEVLOCATION_CATEGORY":
				static $catdetails = false;
				if (!$catdetails)
				{
					$sql = "SELECT * FROM  #__categories WHERE extension='com_jevlocations' ";
					$db = JFactory::getDbo();
					$db->setQuery($sql);
					$catdetails = $db->loadObjectList('id');
				}
				if (isset($row->_jevlocation))
				{
					// blog layout
					$field = "categories_list";
					if (isset($row->_jevlocation->$field) && strlen($row->_jevlocation->$field) > 0)
					{
						return $row->_jevlocation->$field;
					}
					// location detail
					$field = "category_list";
					if (isset($row->_jevlocation->$field) && strlen($row->_jevlocation->$field) > 0)
					{
						return $row->_jevlocation->$field;
					}
					// event detail
				}
				// event list
				else if (isset($row->_location_category_list))
				{
					return $row->_location_category_list;
				}
				break;
			default :
				$thumbnail = false;
				if (strpos($code, "(THUMBNAIL)") > 0)
				{
					$thumbnail = true;
					$code = str_replace("(THUMBNAIL)", "", $code);
				}
				$popup = false;
				if (strpos($code, "(POPUP)") > 0)
				{
					$popup = true;
					$code = str_replace("(POPUP)", "", $code);
				}


				//NEW Custom Fields Tags
				$isNewTag = 0;
				$locObj = "";
				$code = str_replace("lcf_", "", $code, $isNewTag);

				//Detect which location object property we are using. Events use _location :(
				if (is_object($row->_location))
				{
					$locObj = "_location";
				}
				else if (is_object($row->_jevlocation))
				{
					$locObj = "_jevlocation";
				}

				if ($isNewTag && $locObj)
				{
					if (strpos($code, "_lbl") > 0)
					{
						$code = substr($code, 0, strlen($code) - 4);
						if ($code != "" && isset($row->{$locObj}->customfields) && isset($row->{$locObj}->customfields[$code]))
							return $row->{$locObj}->customfields[$code]["label"];
					}
					else
					{
						if ($code != "" && isset($row->{$locObj}->customfields) && isset($row->{$locObj}->customfields[$code]))
						{
							if ($thumbnail && isset($row->{$locObj}->customfields[$code]["fieldnamearray"]["output"]["THUMBNAIL"]))
							{
								return $row->{$locObj}->customfields[$code]["fieldnamearray"]["output"]["THUMBNAIL"];
							}
							if ($popup && isset($row->{$locObj}->customfields[$code]["fieldnamearray"]["output"]["POPUP"]))
							{
								return $row->{$locObj}->customfields[$code]["fieldnamearray"]["output"]["POPUP"];
							}

							return $row->{$locObj}->customfields[$code]["value"];
						}
					}
				}

				// OLD Custom Fields Tags
				if (strpos($code, "_lbl") > 0)
				{
					$code = substr($code, 0, strlen($code) - 4);
					if ($code != "" && isset($row->_jevlocation->customfields) && isset($row->_jevlocation->customfields[$code]))
						return $row->_jevlocation->customfields[$code]["label"];
				}
				else
				{
					if ($code != "" && isset($row->_jevlocation->customfields) && isset($row->_jevlocation->customfields[$code]))
					{
						if ($thumbnail && isset($row->_jevlocation->customfields[$code]["fieldnamearray"]["output"]["THUMBNAIL"]))
						{
							return $row->_jevlocation->customfields[$code]["fieldnamearray"]["output"]["THUMBNAIL"];
						}
						if ($popup && isset($row->_jevlocation->customfields[$code]["fieldnamearray"]["output"]["POPUP"]))
						{
							return $row->_jevlocation->customfields[$code]["fieldnamearray"]["output"]["POPUP"];
						}

						return $row->_jevlocation->customfields[$code]["value"];
					}
				}
				break;
		}
		return "";
	}


public static function findLocationByGeo($longitude, $latitude, $accuracy, $createdBy)
{

	$db = JFactory::getDbo();

	$distanceQuery = "2* ASIN(SQRT( POWER( SIN( (l.geolat- $latitude )/2),2) + COS(l.geolat) * COS($latitude) * POWER( SIN( (l.geolon-($longitude))/2 ),2) ))";

	$query = $db->getQuery(true);

	$query->select("loc_id,title, geolon, geolat, $distanceQuery AS distance");
	$query->from('#__jev_locations AS l');
	$query->where($distanceQuery < $accuracy);
	$query->where("global = 1 OR created_by = " . (int) $createdBy);
	$query->order('global ASC');

	$db->setQuery($query);

	return $db->loadResult;

}

	/**
	 *  Check if location is already in the Db, and if it is, update the current record with the loc_id.
	 *
	 *  Return true if found and successful, false if not found or error.
	 */
	public static function LocFound($evdetail, $created_by, $loc_name, $loc_geolon, $loc_geolat, $matchingMethod = "name-and-geolocation", $geoAccuracy = "0.001")
	{
		$loc_idx = -1;

		$distanceQuery = "2* ASIN(SQRT( POWER( SIN( (geolat- $loc_geolat )/2),2) + COS(geolat) * COS($loc_geolat) * POWER( SIN( (geolon-($loc_geolon))/2 ),2) ))";

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('loc_id, title, geolon, geolat')
			->from($db->quoteName("#__jev_locations"))
			->where('(global=1 OR created_by = ' . (int) $created_by . ')');

		switch ($matchingMethod)
		{
			case "name":
				$query->where('title LIKE ' . $db->q($loc_name));
				break;
			case "geolocation":
				$query->where("$distanceQuery < $geoAccuracy");
				break;
			default:
				$query->where('title LIKE ' . $db->q($loc_name) );
				$query->where("$distanceQuery < $geoAccuracy");
				break;
		}

		$query->order('global ASC');

		$db->setQuery($query);

		$locations = $db->loadObjectList();

		if ($matchingMethod == "name" && count($locations) > 0)
		{
			$loc_idx = 0;
		}
		else
		{
			for ($idx = 0; $idx < count($locations); $idx++)
			{
				$curr_loc = $locations[$idx];
				$curr_geolon = floatval($curr_loc->geolon);
				$curr_geolat = floatval($curr_loc->geolat);
				$curr_postcode = $curr_loc->postcode;
				$curr_state = $curr_loc->state;
				$curr_city = $curr_loc->city;
				$curr_street = $curr_loc->street;

				// Is current location within the geo accuracy threshold?
				if (($loc_geolon != 0.0 || $loc_geolat != 0.0) && $curr_geolon - $geoAccuracy < $loc_geolon && $curr_geolon + $geoAccuracy > $loc_geolon && $curr_geolat - $geoAccuracy < $loc_geolat && $curr_geolat + $geoAccuracy > $loc_geolat)
				{
					$loc_idx = $idx;
					break; // If the name and lat/lon are a match we need look no further.
				}
				// Is there a postal code in the current location and is it in the raw event location? or...
				// Is there a city and state in the current location and are both in the raw event location?
				if (($curr_postcode != "" && stripos($evdetail->location, $curr_postcode) !== FALSE) || ($curr_city != "" && $curr_state != "" && stripos($evdetail->location, $curr_city) !== FALSE && stripos($evdetail->location, $curr_state) !== FALSE))
				{
					if ($curr_street != "")
					{
						$street_prts = preg_split("/[\s,]+/", $curr_street);
						$street_matches = 0;
						for ($j = 0; $j < count($street_prts); $j++)
						{
							if (stripos($evdetail->location, $street_prts[$j]) !== FALSE)
								$street_matches++;
						}
						if ($street_matches + 2 > count($street_prts))
						{
							$loc_idx = $idx;
							break; // If the name and most of the address match, we accept the match.
						}
					}
				}
			}
		}

		if ($loc_idx > -1)
		{
			echo 'Found existing location<br/>';

			return $locations[$loc_idx]->loc_id;
		}

		return false;
	}

	/*	 * *
	 * Send location to Google goelocation api for parsing and lat/lon values
	 *
	 * Return the xml result, including the formatted address, latitude, and longitude
	 */
	public static function GetGeoFromGoogle($location, $loc_geolon = 0.0, $loc_geolat = 0.0)
	{
		$compparams = JComponentHelper::getParams("com_jevlocations");
		// Construct the google geocode api URL
		JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");
		$googlekey = JevLocationsHelper::getApiKey("server");
		$googleurl = JevLocationsHelper::getApiUrl();
		if ($googlekey != "")
		{
			$googleurl .= "/maps/api/geocode/xml?key=" . $googlekey;
		}
		else
		{
			$googleurl .= "/maps/api/geocode/xml?sensor=false";
		}

		if($defaultRegion = $compparams->get('importdefaultregion',1))
		{
			$gregion = $compparams->get("gregion", "US");
			if ($gregion != "")
			{
				// Constrain lookup to Google maps region/country code.
				$googleurl .= "&components=country:" . $gregion; 	
			}
		}

		$googleurl .= "&address=" . urlencode(trim($location)); // Append the passed location.

		if ($loc_geolon != 0.0 || $loc_geolat != 0.0)
		{
			$geo_viewport = 0.1; // This is about 10km at middle lattitudes. Good enough for metro areas.
			$south_bounds = $loc_geolat - $geo_viewport;
			$north_bounds = $loc_geolat + $geo_viewport;
			$west_bounds = $loc_geolon - $geo_viewport;
			$east_bounds = $loc_geolon + $geo_viewport;
			$googleurl .= "&bounds=" . urlencode($south_bounds . "," . $west_bounds . "|" . $north_bounds . "," . $east_bounds);
		}
		echo "<br/>Calling google API with $googleurl<br/>";

		//$googleurl2 = str_replace("/xml", "/json", $googleurl);

		for ($i = 1; $i <= 5; $i++)
		{
			// Use JHttpFactory that allow using CURL and Sockets as alternative method when available
			// Adding a valid user agent string, otherwise googlereturning a error
			$goptions = new \joomla\Registry\Registry;

			$goptions->set('userAgent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko/20100101 Firefox/41.0');

			$gconnector = JHttpFactory::getHttp($goptions);
			try
			{
				$xml = $gconnector->get($googleurl);
				//$json = $gconnector->get($googleurl2);
				//$json = json_decode($json->body);
			}
			catch (Exception $e)
			{
				$xml = new stdClass();
				$xml->code = 500;
			}

			$status = ($xml->code < 200 || $xml->code >= 300) ? "JHttpFactory::getHttp failed " . $xml->code : "";
			// Since sometimes google api just fails randomly, try again after waiting a few seconds.
			if ($status !== "")
			{
				if ($i < 5)
				{
					echo '<br/>Google geocode API call failed with status: ' . $status . ', retrying in ' . $i . ' seconds<br/>';
					sleep($i);
				}
				else
				{
					die("Google geocode API call failed with status: " . $status . " for URL: " . $googleurl);
				}
			}
			else
			{
				break;
			}
		}
		// Failure processing
		if ($xml->code < 200 || $xml->code >= 300)
		{
			echo '<br/>Google geocode API call failed with status ' . $status . '. Skipping record.<br/>';
			return FALSE;
		}
		try
		{
			$xml = simplexml_load_string($xml->body);
		}
		catch (Exception $e)
		{
			return false;
		}
		if (!$xml || strcmp($xml->status, "OK") != 0)
		{
			echo '<br/>Google geocode API call failed with status ' . $status . '. Skipping record.<br/>';
			return FALSE;
		}
		return $xml->result;
	}

}
