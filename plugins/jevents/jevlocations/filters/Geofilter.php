<?php
/**
* JEvents Component for Joomla 1.5.x
*
* @version $Id$
* @package JEvents
* @copyright Copyright (C) 2008-2009 GWE Systems Ltd
* @license GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
* @link http://www.jevents.net
*/

defined('_JEXEC' ) or die( 'No Direct Access' );

// searches location of event
class jevGeofilterFilter extends jevFilter
{
	const filterType="geosearch";

	function __construct($tablename, $filterfield, $isstring=true){
		$this->filterLabel=JText::_( 'JEV_SEARCH_NEAR_LOCATION' );
		$this->filterNullValue="";
		$this->filterType=self::filterType;

		parent::__construct($tablename,$filterfield, true);

		$lang 		= JFactory::getLanguage();
		$lang->load("plg_jevents_jevlocations", JPATH_ADMINISTRATOR);	// RSH 11/2/10 - Make sure language file is loaded

		// Should these be ignored?
		$reg = JFactory::getConfig();
		$modparams = $reg->get("jev.modparams",false);
		if ($modparams && $modparams->get("ignorefiltermodule",false)){
			$this->filter_value = JRequest::getVar($this->filterType.'_fv', $this->filterNullValue );
		}

		if (intval(JRequest::getVar('filter_reset',0))){
			$this->filter_value = "";
		}

		$plugin = JPluginHelper::getPlugin("jevents","jevlocations");
		if ($plugin){
			$this->params = new JRegistry($plugin->params);
		}
		else  {
			$this->params = false;
		}
		$compparams = JComponentHelper::getParams("com_jevlocations");
		if ($compparams->get("gwidth",-1) !=-1){
			$this->params = $compparams;
		}

	}

	function _createFilter($prefix=""){
		if (!$this->params) return "";
		if (JRequest::getInt("skipfilter",0)) return "";
		if (!$this->filterField ) return "";
		if (trim($this->filter_value)==$this->filterNullValue || strlen(trim($this->filter_value))<3) return "";
		if (strpos($this->filter_value,",")<=0) return "";

		$googlesearchdistance= JRequest::getInt("geosearchdistance",10);
		if ($this->filter_value != "") {
			$googlesearchdistance=JFactory::getApplication()->getUserStateFromRequest( 'geosearchdistance_fv', 'geosearchdistance', 10);
		}

		// convert to kilometers
		if ($this->params->get("scale","miles")=="miles"){
			$googlesearchdistance *= 1.609344;
		}

		list($lat,$lon) = explode(",",$this->filter_value);
		$lon = deg2rad($lon);
		$lat = deg2rad($lat);

		// see http://www.movable-type.co.uk/scripts/latlong.html
		// http://www.artfulsoftware.com/infotree/queries.php?&bw=1280
		$db = JFactory::getDBO();
		//$filter = "ACOS(SIN(loc.geolat*PI()/180)*SIN($lat)+COS(loc.geolat*PI()/180)*COS($lat)*COS($lon-(loc.geolon*PI()/180)))*6371 < $googlesearchdistance";
		$filter = "ACOS(SIN(loc.geolat*PI()/180)*SIN($lat)+COS(loc.geolat*PI()/180)*COS($lat)*COS($lon-(loc.geolon*PI()/180)))*6371 < $googlesearchdistance";

		return $filter;
	}

	function _createJoinFilter($prefix=""){
		if (!$this->params) return "";
		// location looup is always included so no need for this!
		//return " #__jev_locations as loc ON loc.loc_id=det.loc_id";
		return "";
	}

	function _createfilterHTML(){
		$filterList=array();
		if (!$this->params) return $filterList;
		if (!$this->filterField) return $filterList;

		$db = JFactory::getDBO();

		$compparams = JComponentHelper::getParams("com_jevlocations");
		JLoader::register('JevLocationsHelper',JPATH_ADMINISTRATOR."/components/com_jevlocations/libraries/helper.php");
		JevLocationsHelper::loadApiScript();

		JHtml::script('com_jevlocations/geolocation.js', false, true);


		$googleaddress = JRequest::getString("googleaddress","");
		if ($this->filter_value != "") {
			$googleaddress=JFactory::getApplication()->getUserStateFromRequest( 'googleaddress_fv', 'googleaddress', "" );
		}
		if (intval(JRequest::getVar('filter_reset',0))){
			$googleaddress = "";
		}
		$gregion = $compparams->get("gregion","US");
		
		$filterList["title"]="<label class='evlocsearch_label' for='".$this->filterType."_fv'>".$this->filterLabel."</label>";
		
		//$filterList["html"] = '<input type="text" size="27" name="googleaddress" id="googleaddress" value="'.htmlspecialchars($googleaddress).'" onkeyup="findAddressGeo();" onchange="clearlonlat();"/>';
		$filterList["html"] = '<input type="text" size="27" name="googleaddress" id="googleaddress" value="'.htmlspecialchars($googleaddress).'"  onchange="clearlonlat();"/>';
		$filterList["html"] .= "<input type='hidden' name='".$this->filterType."_fv' id='".$this->filterType."_fv' class='evlocsearch' value='".$this->filter_value."' />";
		$filterList["html"] .= "<input type='hidden' id='gregion' name='gregion' value='$gregion' />";
		$filterList["html"] .= "<div  id='gaddressmatches' style='display:none;padding:3px;margin:0px!important;min-width:200px;color:#111;background-color:#eee;z-index:999;'  ></div>";

		$filterList["html"] .= "<span class='maxdistance'> ".JText::_( 'JEV_MAX_DISTANCE' )."</span>";

		$scale = " ".$this->params->get("scale","miles");
		$list = array();
		$list[] = JHTML::_( 'select.option', 5,     "5".$scale);
		$list[] = JHTML::_( 'select.option', 10,   "10".$scale);
		$list[] = JHTML::_( 'select.option', 20,   "20".$scale);
		$list[] = JHTML::_( 'select.option', 30,   "30".$scale);
		$list[] = JHTML::_( 'select.option', 50,   "50".$scale);
		$list[] = JHTML::_( 'select.option', 100, "100".$scale);

		// build the select list itself
		$googlesearchdistance= JRequest::getInt("geosearchdistance",$this->params->get("geosearchdistance",10));
		if ($this->filter_value != "") {
			$googlesearchdistance=JFactory::getApplication()->getUserStateFromRequest( 'geosearchdistance_fv', 'geosearchdistance', 10);
		}
		$filterList["html"] .= JHTML::_( 'select.genericlist', $list, "geosearchdistance", "", 'value', 'text', $googlesearchdistance);
		if (intval(JRequest::getVar('filter_reset',0))){
			$googlesearchdistance = 5;
		}
		//$filterList["html"] .= '<input type="button" name="findaddress" onclick="findAddressGeo();" value="'.JText::_( 'FIND_ADDRESS' ).'" />';

		$script = "try {JeventsFilters.filters.push({id:'".$this->filterType."_fv',value:''});} catch (e) {}";
		$script .= "try {JeventsFilters.filters.push({id:'googleaddress',value:''});} catch (e) {}";
		// geolocation
		if ($this->params->get("autogeolocate",0)){
			$autogeolocate = $this->params->get("autogeolocate",0);
			$script .= "jQuery(window).load(function(){findEventNearby($autogeolocate);});";
		}

		$document = JFactory::getDocument();
		$document->addScriptDeclaration($script);

                $filterList["html"] = '<div class="geofilter">'.$filterList["html"]."</div>";
		return $filterList;

	}
}