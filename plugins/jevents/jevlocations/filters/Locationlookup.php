<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

defined('_JEXEC' ) or die( 'No Direct Access' );

// searches location of event
class jevLocationlookupFilter extends jevFilter
{
	function __construct($tablename, $filterfield, $isstring=true){
		$this->filterType="loclkup";
		$this->filterLabel=JText::_( 'SEARCH_BY_LOCATION' );
		$this->filterNullValue=0;
		$lang 		= JFactory::getLanguage();
		$lang->load("plg_jevents_jevlocations", JPATH_ADMINISTRATOR);	// RSH 11/2/10 - Make sure language file is loaded			
		parent::__construct($tablename,$filterfield, true);

		// Should these be ignored?
		$reg = JFactory::getConfig();
		$modparams = $reg->get("jev.modparams",false);
		if ($modparams && $modparams->get("ignorefiltermodule",false)){
			$this->filter_value = $this->filterNullValue;
			return;

		}

		// Only have memory on page with the module visible for JEvents 1.5.4 onwards
		JLoader::register('JEventsVersion',JEV_ADMINPATH."/libraries/version.php");
		$version	= JEventsVersion::getInstance();
		$versionnumber = $version->RELEASE;

		if (version_compare($versionnumber,"1.5.4","<")){
			$this->filter_value =  JRequest::getVar($this->filterType.'_fv', $this->filterNullValue );
		}

	}

	function _createFilter($prefix=""){
		if (!$this->filterField ) return "";
		if (intval($this->filter_value)==$this->filterNullValue) return "";

		$db = JFactory::getDBO();
		$value = intval( $this->filter_value);

		$filter = "loc.loc_id = $value";
		return $filter;
	}

	// No need join  the location is always joined
	// function _createJoinFilter($prefix=""){}

	function _createfilterHTML(){

		if (!$this->filterField) return "";

		// Find the accessible locations
		$user = JFactory::getUser();
		$db = JFactory::getDBO();

		// Do these need to be filtered by location category filter
		static $catid;
		if (!isset($catid)){
			include_once(JPATH_SITE."/plugins/jevents/jevlocations/filters/Locationcategory.php");
			$catfilter = new jevLocationcategoryFilter("","Locationcategory");
			$catid = intval($catfilter->filter_value);
			if ($catid>0){
				$catid = " AND loc.loccat=$catid";
			}
			else {
				$catid = "";
			}
		}
		
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats",0);
		$onlyWithEvents = $compparams->get('jeventsfilter-lookup',0);
		$where = "";
		$pluginsDir = JPATH_ROOT.'/plugins/jevents/';
		
		// is the state filter active and non-null in which case filter the list of cities
		$filters = jevFilterProcessing::getInstance(array("locationcities"),$pluginsDir."filters/");
		$citywhere = $cityjoin = array();
		$filters->setWhereJoin($citywhere,$cityjoin);
		if (count($citywhere)==1){
			if (!$usecats && strpos($citywhere[0],"loc.city")!==false){
				$where = " AND ".str_replace("loc.city","city",$citywhere[0]);
			}
			else if ($usecats && strpos($citywhere[0],"loc.catid")!==false){
				$where = " AND ".str_replace("loc.catid","cat.id",$citywhere[0]);
			}
		}

		$filters = jevFilterProcessing::getInstance(array("locationcity"),$pluginsDir."filters/");
		$citywhere = $cityjoin = array();
		$filters->setWhereJoin($citywhere,$cityjoin);
		if (count($citywhere)==1){
			if (!$usecats && strpos($citywhere[0],"loc.city")!==false){
				$where = " AND ".str_replace("loc.city","city",$citywhere[0]);
			}
			else if ($usecats && strpos($citywhere[0],"loc.catid")!==false){
				$where = " AND ".str_replace("loc.catid","cat.id",$citywhere[0]);
			}
		}

		// is the state filter active and non-null in which case filter the list of cities
		$filters = jevFilterProcessing::getInstance(array("locationstate"),$pluginsDir."filters/");
		$statewhere = $statejoin = array();
		$filters->setWhereJoin($statewhere,$statejoin);
		if (count($statewhere)==1){
			if (!$usecats && strpos($statewhere[0],"loc.state")!==false){
				$where = " AND ".str_replace("loc.state","state",$statewhere[0]);
			}
			else if ($usecats && strpos($statewhere[0],"locmstate.id")!==false){
				$where = " AND ".str_replace("locmstate.id","pcat.id",$statewhere[0]);
			}
		}

		// is the state filter active and non-null in which case filter the list of cities
		$filters = jevFilterProcessing::getInstance(array( "locationcountry"),$pluginsDir."filters/");
		$countrywhere = $countryjoin = array();
		$filters->setWhereJoin($countrywhere,$countryjoin);
		if (count($countrywhere)==1){
			if (!$usecats && strpos($countrywhere[0],"loc.country")!==false){
				$where .= " AND ".str_replace("loc.country","country",$countrywhere[0]);
			}
			else if ($usecats && strpos($countrywhere[0],"locmcountry.id")!==false){
				$where .= " AND ".str_replace("locmcountry.id","gpcat.id",$countrywhere[0]);
			}
		}

		$extraJoin = $extraWhere = "";

		if ($onlyWithEvents)
		{
			$extraJoin = " INNER JOIN #__jevents_vevdetail as det ON det.loc_id = loc.loc_id
							INNER JOIN #__jevents_repetition as rpt ON det.evdet_id = rpt.eventdetail_id";

			$startdate = new JDate("-" . $compparams->get("checkeventbefore", 30) . " days");

			$enddate = new JDate("+" . $compparams->get("checkeventafter", 30) . " days");

			$extraWhere = " AND rpt.endrepeat >= '" . $startdate . "' AND rpt.startrepeat <= '" . $enddate . "'";
		}

		$query = "SELECT loc.loc_id as value, loc.title as text FROM #__jev_locations as loc
		LEFT JOIN  #__jevlocation_categories as cat ON cat.id = loc.catid
		LEFT JOIN  #__jevlocation_categories as pcat ON pcat.id=cat.parent_id 
		LEFT JOIN  #__jevlocation_categories as gpcat ON gpcat.id=pcat.parent_id";

		if ($extraJoin)
		{
			$query .= $extraJoin;
		}

		$query .= " WHERE loc.published=1 " . $catid;

		if ($extraWhere)
		{
			$query .= $extraWhere;
		}

		$query .= $where . "
		 AND loc.access  IN (0, " . JEVHelper::getAid($user) . ") GROUP BY loc.loc_id ORDER BY loc.title ASC";
		$db->setQuery($query);
		$locations = $db->loadObjectList();

		$list[] = JHTML::_('select.option', 0, JText::_('SEARCH_BY_LOCATION'));
		$list = array_merge($list, $locations);

		$filterList = array();
		$filterList["title"] = "<label class='evloclkup_label' for='" . $this->filterType . "_fv'>" . $this->filterLabel . "</label>";
		$filterList["html"] = JHTML::_('select.genericlist', $list, $this->filterType . "_fv", "id='" . $this->filterType . "_fv' class='evloclkup'", 'value', 'text', $this->filter_value);
		//$filterList["html"] = JHTML::_( 'select.genericlist', $list, $this->filterType."_fv", "id='".$this->filterType."_fv' class='evloclkup' onchange='this.form.submit()';", 'value', 'text', $this->filter_value);

		$script = "try {JeventsFilters.filters.push({id:'".$this->filterType."_fv',value:0});} catch (e) {}";
		$document = JFactory::getDocument();
		$document->addScriptDeclaration($script);

		return $filterList;

	}
}
