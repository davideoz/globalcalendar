<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

defined('_JEXEC' ) or die( 'No Direct Access' );

// searches location of event
class jevLocationcountryFilter extends jevFilter
{
	function __construct($tablename, $filterfield, $isstring=true){
		$this->filterType="loccountry";
		$this->filterLabel=JText::_("JEV_COUNTRY_SEARCH");
		$this->filterNullValue='0';
		$lang 		= JFactory::getLanguage();
		$lang->load("plg_jevents_jevlocations", JPATH_ADMINISTRATOR);	// RSH 11/2/10 - Make sure language file is loaded			
		parent::__construct($tablename,$filterfield, true);

		// Should these be ignored?
		$reg = JFactory::getConfig();
		$modparams = $reg->get("jev.modparams",false);
		if ($modparams && $modparams->get("ignorefiltermodule",false)){
			$this->filter_value = $this->filterNullValue;
			return;
		}

		// Only have memory on page with the module visible for JEvents 1.5.4 onwards
		JLoader::register('JEventsVersion',JEV_ADMINPATH."/libraries/version.php");
		$version	= JEventsVersion::getInstance();
		$versionnumber = $version->RELEASE;

		if (version_compare($versionnumber,"1.5.4","<")){
			$this->filter_value =  JRequest::getVar($this->filterType.'_fv', $this->filterNullValue );
		}

	}

	function _createFilter($prefix=""){
		if (!$this->filterField ) return "";
		if ($this->filter_value===$this->filterNullValue) return "";

		$db = JFactory::getDBO();

		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats",0);
		if ($usecats){
			$filter = "locmcountry.id=".intval( $this->filter_value);
		}
		else {
			$text = $db->Quote( $db->escape( $this->filter_value, true ), false );
			$filter = "loc.country = $text";
		}

		return $filter;
	}

	// I always get the joins via the main plugin !
	function _createJoinFilter($prefix=""){
		if (!$this->filterField ) return "";
		if ($this->filter_value===$this->filterNullValue) return "";
		/*
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats",0);
		if ($usecats){
			return ' #__jevlocation_categories AS loccity ON loc.catid = loccity.id AND loccity.section="com_jevlocations" 
				LEFT JOIN #__jevlocation_categories AS locstate ON loccity.parent_id = locstate.id AND locstate.section="com_jevlocations"
				LEFT JOIN #__jevlocation_categories AS loccountry ON locstate.parent_id = loccountry.id AND loccountry.section="com_jevlocations"
				';
		}

		 */
	}
		
	function _createfilterHTML(){

		if (!$this->filterField) return "";

		// Find the accessible locations
		$user = JFactory::getUser();
		$db = JFactory::getDBO();

		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats",0);
		if ($usecats){
			$query = "SELECT distinct gpcat.id as value, gpcat.title as text FROM #__jevlocation_categories as cat 
			INNER JOIN  #__jevlocation_categories as pcat ON pcat.id=cat.parent_id 
			INNER JOIN  #__jevlocation_categories as gpcat ON gpcat.id=pcat.parent_id 
			WHERE cat.published=1 AND cat.section='com_jevlocations' 
			ORDER BY gpcat.title ASC";
			$db->setQuery( $query );
			$locations = $db->loadObjectList();
		}
		else {
			$query = "SELECT DISTINCT country as value, country as text FROM #__jev_locations WHERE published=1 AND country<>'' ORDER BY country ASC";
			$db->setQuery( $query );
			$locations = $db->loadObjectList();			
		}
		$list[] = JHTML::_( 'select.option', 0, JText::_("JEV_COUNTRY_SEARCH_OPTION")=="JEV_COUNTRY_SEARCH_OPTION"? JText::_("JEV_COUNTRY_SEARCH") : JText::_("JEV_COUNTRY_SEARCH_OPTION"));
		$locations = (is_array($locations)) ? $locations : array();  // RSH 11/2/10 Make sure it is a valid array
		$list = array_merge($list, $locations);

		$filterList=array();
		$filterList["title"]="<label class='evloccountry_label' for='".$this->filterType."_fv'>".$this->filterLabel."</label>";
//		$filterList["html"] = JHTML::_( 'select.genericlist', $list, $this->filterType."_fv", "id='".$this->filterType."_fv' class='evloccountry_label' onchange='this.form.submit()'", 'value', 'text', $this->filter_value);
		$filterList["html"] = JHTML::_( 'select.genericlist', $list, $this->filterType."_fv", "id='".$this->filterType."_fv' class='evloccountry_label'", 'value', 'text', $this->filter_value);

		$script = "try {JeventsFilters.filters.push({id:'".$this->filterType."_fv',value:0});} catch (e) {}";
		$document = JFactory::getDocument();
		$document->addScriptDeclaration($script);

		return $filterList;

	}
}