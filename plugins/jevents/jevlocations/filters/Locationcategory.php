<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

defined('_JEXEC' ) or die( 'No Direct Access' );

// searches location of event
class jevLocationcategoryFilter extends jevFilter
{
	function __construct($tablename, $filterfield, $isstring=true){
		$this->filterType="loccat";
		$this->filterLabel=JText::_( 'SEARCH_BY_LOCATION_CATEGORY_LABEL' );
		$this->filterNullValue=0;
		$lang 		= JFactory::getLanguage();
		$lang->load("plg_jevents_jevlocations", JPATH_ADMINISTRATOR);	// RSH 11/2/10 - Make sure language file is loaded			
		parent::__construct($tablename,$filterfield, true);

		// Should these be ignored?
		$reg = JFactory::getConfig();
		$modparams = $reg->get("jev.modparams",false);
		if ($modparams && $modparams->get("ignorefiltermodule",false)){
			$this->filter_value = $this->filterNullValue;
			return;
		}

		// Only have memory on page with the module visible for JEvents 1.5.4 onwards
		JLoader::register('JEventsVersion',JEV_ADMINPATH."/libraries/version.php");
		$version	= JEventsVersion::getInstance();
		$versionnumber = $version->RELEASE;

		if (version_compare($versionnumber,"1.5.4","<")){
			$this->filter_value =  JRequest::getVar($this->filterType.'_fv', $this->filterNullValue );
		}

	}

	function _createFilter($prefix=""){
		if (!$this->filterField ) return "";
		if (intval($this->filter_value)==$this->filterNullValue) return "";

		$db = JFactory::getDBO();
		$value = intval( $this->filter_value);
		$this->needsgroupby = true;

		// Find the specified category
		$db = JFactory::getDbo();
		$db->setQuery("SELECT * from #__categories where id=$value and extension='com_jevlocations'");
		$category = $db->loadObject();
		if (!$category) return "";
		$filter = "map.catid IN (SELECT id from #__categories where lft >= $category->lft AND rgt <= $category->rgt and extension='com_jevlocations' )";

		return $filter;
	}
 
	function _createJoinFilter($prefix=""){
		$plugin = JPluginHelper::getPlugin("jevents","jevlocations");
		$params = new JRegistry($plugin->params);
		$compparams = JComponentHelper::getParams("com_jevlocations");
		if ($compparams->get("gwidth",-1) !=-1){
			$params = $compparams;
		}

		if (!$params->get("alwayscatlink",0)){
			if (!$this->filterField ) return "";
			if (intval($this->filter_value)==$this->filterNullValue) return "";
		}

		return " #__jevlocations_catmap as map ON loc.loc_id=map.locid ";
		// NO need to join loccat here since it is always done in onListIcalEvents
		// . "LEFT JOIN #__categories AS loccat ON loccat.id=map.catid AND loccat.extension='com_jevlocations'"		;

	}
	
	// No need join  the location is always joined
	// function _createJoinFilter($prefix=""){}

	function _createfilterHTML(){

		if (!$this->filterField) return "";

		$list[] = JHTML::_( 'select.option', 0, JText::_( 'SEARCH_BY_LOCATION_CATEGORY' ));
		$list = array_merge($list, JHtml::_('category.options', "com_jevlocations"));

		$filterList=array();
		$filterList["title"]="<label class='evloccat_label' for='".$this->filterType."_fv'>".$this->filterLabel."</label>";
		$filterList["html"] = JHTML::_( 'select.genericlist', $list, $this->filterType."_fv", "id='".$this->filterType."_fv' class='evloccat'", 'value', 'text', $this->filter_value);

		$script = "try {JeventsFilters.filters.push({id:'".$this->filterType."_fv',value:0});} catch (e) {}";
		$document = JFactory::getDocument();
		$document->addScriptDeclaration($script);

		return $filterList;

	}
}