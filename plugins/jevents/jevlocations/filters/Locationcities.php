<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id: Search.php 1410 2009-04-09 08:13:54Z geraint $
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

defined('_JEXEC' ) or die( 'No Direct Access' );

// searches location of event
class jevLocationcitiesFilter extends jevFilter
{
	function __construct($tablename, $filterfield, $isstring=true){
		$this->filterType="loccities";
		$this->filterLabel=JText::_( 'JEV_CITY_SEARCH' );
		$this->filterNullValue=array();
		parent::__construct($tablename,$filterfield, true);

		$lang 		= JFactory::getLanguage();
		$lang->load("plg_jevents_jevlocations", JPATH_ADMINISTRATOR);	// RSH 11/2/10 - Make sure language file is loaded			

		// Should these be ignored?
		$reg = JFactory::getConfig();
		$modparams = $reg->get("jev.modparams",false);
		if ($modparams && $modparams->get("ignorefiltermodule",false)){
			$this->filter_value = $this->filterNullValue;
			return;
		}

	}

	function _createFilter($prefix=""){
		if (!$this->filterField ) return "";
		if ($this->filter_value===$this->filterNullValue || (count($this->filter_value)==1 && $this->filter_value[0]=="0"))  return "";

		$db = JFactory::getDBO();

		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats",0);
		if ($usecats){
			$text = array();
			foreach ($this->filter_value as $val){
				$text[] = intval( $val);
			}
                        if (count($text)) {
                            $filter = "loc.catid IN (".implode(",",$text).")";
                        }                                                        
		}
		else {
			$text = array();
			foreach ($this->filter_value as $val){
				$text[] = $db->Quote( $db->escape( $val, true ), false );
			}
                        if (count($text)) {
                            $filter = "loc.city IN (".implode(",",$text).")";
                        }
		}

		return $filter;
	}

	function _createfilterHTML(){

		if (!$this->filterField) return "";

		// Find the accessible locations
		$user = JFactory::getUser();
		$db = JFactory::getDBO();
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats",0);
		
		// is the state filter active and non-null in which case filter the list of cities
		$pluginsDir = JPATH_ROOT.'/plugins/jevents/';
		$filters = jevFilterProcessing::getInstance(array("locationstate"),$pluginsDir."filters/");
		$citywhere = $cityjoin = array();
		$filters->setWhereJoin($citywhere,$cityjoin);
		$where = "";
		if (count($citywhere)==1){
			if (!$usecats && strpos($citywhere[0],"loc.state")!==false){
				$where = " AND ".str_replace("loc.state","state",$citywhere[0]);
			}
			else if ($usecats && strpos($citywhere[0],"locmstate.id")!==false){
				$where = " AND ".str_replace("locmstate.id","pcat.id",$citywhere[0]);
			}
		}
		
		// is the state filter active and non-null in which case filter the list of cities
		$pluginsDir = JPATH_ROOT.'/plugins/jevents/';
		$filters = jevFilterProcessing::getInstance(array( "locationcountry"),$pluginsDir."filters/");
		$countrywhere = $countryjoin = array();
		$filters->setWhereJoin($countrywhere,$countryjoin);
		if (count($countrywhere)==1){
			if (!$usecats && strpos($countrywhere[0],"loc.country")!==false){
				$where .= " AND ".str_replace("loc.country","country",$countrywhere[0]);
			}
			else if ($usecats && strpos($countrywhere[0],"locmcountry.id")!==false){
				$where .= " AND ".str_replace("locmcountry.id","gpcat.id",$countrywhere[0]);
			}
		}
		
		if ($usecats){
			$query = "SELECT distinct cat.id as value, cat.title as text FROM #__jevlocation_categories as cat 
			INNER JOIN  #__jevlocation_categories as pcat ON pcat.id=cat.parent_id 
			INNER JOIN  #__jevlocation_categories as gpcat ON gpcat.id=pcat.parent_id 
			WHERE cat.published=1 AND cat.section='com_jevlocations' 
			$where
			ORDER BY cat.title ASC";
			$db->setQuery( $query );
			$locations = $db->loadObjectList();
		}
		else {
			$query = "SELECT DISTINCT city as value, city as text FROM #__jev_locations WHERE published=1 $where AND city<>'' ORDER BY city ASC";
			$db->setQuery( $query );
			$locations = $db->loadObjectList();			
		}
		$list[] = JHTML::_( 'select.option', "0", JText::_( 'JEV_CITY_SEARCH' ));
		$list = array_merge($list, $locations);

		$filterList=array();
		$filterList["title"]="<label class='evloccat_label' for='".$this->filterType."_fvs'>".$this->filterLabel."</label>";
		$filterList["html"] = JHTML::_( 'select.genericlist', $list, $this->filterType."_fv[]", "id='".$this->filterType."_fv' class='evloccat_label' multiple='multiple' size='4' ", 'value', 'text', $this->filter_value);


		$script = "function reset_cities(){\$('loccities_fv').selectedIndex=0;};	try {JeventsFilters.filters.push({action:'reset_cities()',id:'loccities_fv',value:0});} catch (e) {}";

		$document = JFactory::getDocument();
		$document->addScriptDeclaration($script);

		return $filterList;

	}
}
