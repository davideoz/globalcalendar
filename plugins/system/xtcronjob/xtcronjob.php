<?php

/**
 * @package     Extly.Components
 * @subpackage  com_xtcronjob - Joomla cronjobs component
 *
 * @author      Prieco S.A. <support@extly.com>
 * @copyright   Copyright (C) 2007 - 2014 Prieco, S.A. All rights reserved.
 * @license     http://http://www.gnu.org/licenses/gpl-3.0.html GNU/GPL
 * @link        http://www.extly.com http://support.extly.com
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');
jimport('joomla.html.parameter');

/**
 * PlgSystemXTCronjob
 *
 * @package     Extly.Components
 * @subpackage  com_xtcronjob
 * @since       1.0
*/
class PlgSystemXTCronjob extends JPlugin
{
	/**
	 * PlgSystemXTCronjob
	 *
	 * @param   string  &$subject  Param
	 * @param   object  $config    Param
	 *
	 * @since	1.5
	 */
	public function PlgSystemXTCronjob(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}

	/**
	 * onAfterRender
	 *
	 * @return  void
	 */
	public function onAfterRender()
	{
		$app = JFactory::getApplication();

		if (!$app->isSite())
		{
			return;
		}

		// Pending tasks
		$dbo = JFactory::getDBO();
		$now = new JDate;
		$query = 'SELECT * from #__xtcronjob_tasks WHERE ran_at < ' . $dbo->q($now->toSql()) . ' AND published=1';

		$dbo->setQuery($query);
		$tasks = $dbo->loadObjectList();

		JLoader::import('extly.scheduler.scheduler');

		foreach ($tasks as $task)
		{
			// Update Run At
			$cron = Scheduler::getParser($task->unix_mhdmd);
			$next = $cron->getNextRunDate();
			$nextDate = new JDate($next->getTimestamp());

			$query = "UPDATE #__xtcronjob_tasks SET ran_at = " . $dbo->q($nextDate->toSql()) . ", last_run = 0, published=0 WHERE id = " . $dbo->q($task->id);
			$dbo->setQuery($query);
			$dbo->execute();

			// Execute
			$log = '';
			$command = $task->file;

			$ok = false;

			try
			{
				$ok = $this->_execute($task->type, $command, $log);
			}
			catch (Exception $e)
			{
				$ok = false;
				$log .= $e->__toString();
			}

			// ########### STORE DABASE RUN TIME AND LOGS ################################
			$query = 'UPDATE #__xtcronjob_tasks SET log_text=' . $dbo->q($log) . ' , last_run = ' . $dbo->q($ok) . ' ,published=1 WHERE id=' . $dbo->q($task->id);

			$dbo->setQuery($query);

			if (!$dbo->execute())
			{
				echo $dbo->getErrorMsg() . " on Query: " . $query . " \n";

				return;
			}

			// ########## SEND EMAIL LOGS ################################################
			if (($task->enabledlogs) && (!$ok))
			{
				JLoader::import('extly.mail.notification');
				$subject = "XTCronjob - " . $task->file;
				$body = "A cron task was failed, " . ($task->file) . " crashed when we tried to run it.\n\n" . $log;
				Notification::mailToAdmin($subject, $body);
			}
		}
	}

	/**
	 * _execute
	 *
	 * @param   int     $type     Param
	 * @param   string  $command  Param
	 * @param   string  &$log     Param
	 *
	 * @return  bool
	 */
	public function _execute($type, $command, &$log)
	{
		JLoader::import('extly.extlyframework');
		JLoader::register('TaskManagerHelper', JPATH_ADMINISTRATOR . '/components/com_xtcronjob/helpers/taskmanager.php');

		// SSH Command
		if ($type == TaskManagerHelper::TASKS_TYPE_SSH_COMMAND)
		{
			exec($command, $output);
			$log .= "\n## exec(" . $command . ") ##\n";
			$log .= "\n Output: " . print_r($output, true) . "\n";

			return true;
		}

		// Web Address fopen
		elseif ($type == TaskManagerHelper::TASKS_TYPE_WEB_URL_FOPEN)
		{
			if ($fp = fopen($command, 'r'))
			{
				while (!feof($fp))
				{
					$log .= fread($fp, 1024);
				}

				fclose($fp);
				$log .= "\n## fopen(" . $command . ") ##\n";

				return true;
			}
			else
			{
				$log = "Unable to open " . $command;
			}
		}

		// Web Address fsockopen
		elseif ($type == TaskManagerHelper::TASKS_TYPE_WEB_URL_FSOCKOPEN)
		{
			$urlpart = parse_url($command);
			$port = (isset($urlpart['port']) ? $urlpart['port'] : "80");
			$fp = fsockopen($urlpart['host'], $port, $errno, $errstr, 30);

			if (!$fp)
			{
				$log = "Error (" . $errstr . ") - (" . $errno . ")<br />\n";
			}
			else
			{
				$out = "GET " . $urlpart['path'] . (isset($urlpart['query']) ? '?' . $urlpart['query'] : '');
				$out .= " HTTP/1.1\r\n";
				$out .= "Host: " . $urlpart['host'] . "\r\n";
				$out .= "Connection: Close\r\n\r\n";
				$log = "OUT is(" . $out . ")";
				fwrite($fp, $out);

				while (!feof($fp))
				{
					$log = $log . fgets($fp, 128);
				}

				fclose($fp);

				return true;
			}
		}

		// If it's a plugin folder.event
		elseif ($type == TaskManagerHelper::TASKS_TYPE_PLUGIN_FOLDER_EVENT)
		{
			// Construct asn array from the task descriptor:
			$aryEvent = explode('.', $command);

			// If it's not an array...
			if (!is_array($aryEvent))
			{
				$log = 'Error: plugin format must be folder.event.';
			}
			// If there are two elements in the array...
			elseif (count($aryEvent) == 2)
			{
				// The elements are the plugin group and event names respectively:
				$pluginFolder = $aryEvent[0];
				$pluginEvent = $aryEvent[1];

				// Prepare the dispatcher object:
				$dispatcher = JDispatcher::getInstance();

				// Import plugins for the requested plugin group:
				JPluginHelper::importPlugin($pluginFolder);

				// Fire the requested event:
				$dispatcher->trigger($pluginEvent);

				return true;
			}
			else
			{
				$log = 'Error: plugin format must be folder.event.';
			}
		}

		elseif ($type == TaskManagerHelper::TASKS_TYPE_CURL_AKEEBA)
		{
			$siteurl = JUri::base();

			// Url: index.php?option=com_akeeba&view=backup&key='.SECRETKEY.'&profile='.PROFILE
			$siteurl = $siteurl . $command;
			$log = $this->_get_url($siteurl);

			return true;
		}

		// If it's a plugin
		elseif (TaskManagerHelper::isConfigurablePlugin($type))
		{
			// Construct asn array from the task descriptor:
			$aryEvent = explode('.', $command);

			// If it's not an array...
			if (!is_array($aryEvent))
			{
				$log = 'Error: plugin format must be folder.plugin.event.';
			}
			// If there are 3 elements in the array...
			elseif (count($aryEvent) == 3)
			{
				$pluginFolder = $aryEvent[0];
				$pluginName = $aryEvent[1];
				$pluginEvent = $aryEvent[2];

				// Prepare the dispatcher object:
				$plugin = JPluginHelper::getPlugin($pluginFolder, $pluginName);

				if ($plugin)
				{
					JPluginHelper::importPlugin($pluginFolder, $pluginName);
					$className = 'plg' . $pluginFolder . $plugin->name;

					if (class_exists($className))
					{
						$dispatcher = JDispatcher::getInstance();
						$plugin = new $className($dispatcher, (array) $plugin);

						try
						{
							if (method_exists($plugin, $pluginEvent))
							{
								$log = $plugin->$pluginEvent();

								return true;
							}
							else
							{
								$log = 'Error: plugin method not found.';
							}
						}
						catch (Exception $e)
						{
							$log = $e->__toString();
						}
					}
				}
			}
			else
			{
				$log = 'Error: plugin format must be folder.plugin.event.';
			}
		}
		else
		{
			$log = "Error: Invalid command type.";
		}

		return false;
	}

	/**
	 * _get_url
	 *
	 * @param   string  $siteurl  Param
	 *
	 * @return  bool
	 */
	private function _get_url($siteurl)
	{
		$log = '-no response-';

		$is_redirect = true;

		while ($is_redirect)
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_USERAGENT, 'joomla-xtcronjob');
			curl_setopt($ch, CURLOPT_URL, $siteurl);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_AUTOREFERER, true);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 10000);

			// To avoid 303
			curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);

			$content = curl_exec($ch);
			$response = curl_getinfo($ch);

			$http_code = $response['http_code'];
			$siteurl = $response['redirect_url'];
			$is_redirect = (($http_code == 301) || ($http_code == 302) || ($http_code == 303));

			if (!$is_redirect)
			{
				if (empty($content))
				{
					$errno = curl_errno($ch);
					$error = curl_error($ch);
					$log = "Sorry, the backup didn't work. (Error {$errno} - {$error} / {$siteurl}) " . print_r($response, true);
				}
				else
				{
					$log = $content;
				}
			}

			curl_close($ch);
		}

		return $log;
	}
}
