<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

require ('components/com_contactevents/helpers/events.php');
require ('components/com_contactevents/helpers/createKmlMap.php');
require ('components/com_contactevents/helpers/cookies.php');

/**
 * Hello World Component Controller
 *
 * @since  0.0.1
 */
class ContacteventsController extends JControllerLegacy{

}