<?php 

defined('_JEXEC') or die;

// Component Helper
jimport('joomla.application.component.helper');
jimport('joomla.application.categories');

/**
 * Build the route for the com_weblinks component
 *
 * @param array An array of URL arguments
 * @return  array The URL arguments to use to assemble the subsequent URL.
 */
  function ContacteventsBuildRoute(&$query)
    {
        $segments = array();

        /*
            I have to remove with unset all the parameters passed in the JRoute::_ call but not the Itemid
            I save in segments[] all the parameters I want to mantain in the new url
        */
        
        if (isset($query['option']))
       {
            unset($query['option']);
       }

       if (isset($query['view']))
       {
            unset($query['view']);
       }
       /*
       if (isset($query['category_id']))
       {
            unset($query['category_id']);
       }
        if (isset($query['ManagerId']))
       {
            $segments[] = $query['ManagerId'];
            unset($query['ManagerId']);
       }
       
       if (isset($query['ManagerAlias']))
       {
            $segments[] = $query['ManagerAlias'];
            unset($query['ManagerAlias']);
       }*/

       return $segments;
    }

    function ContacteventsParseRoute($segments)
	{
        
        $vars = array();

        //$vars['ManagerId'] = $segments[0];
        //$vars['ManagerAlias'] = $segments[1];
        
        return $vars;
       
	}


?>