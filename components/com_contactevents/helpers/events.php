<?php

	//include "interactive-map/map-creation/createKmlMap.php";

class events{

   /**
     * GET all active events to show in filters (needed for JSON used in dinamic filters change)
     *
     * @access     public
     * @param      none          
     * @return     array          all the results extracted
     */
	function populateEventsArray($category, $country, $city){

		// CONNECT TO DB
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

		// GET all active categories
			$query =  "SELECT DISTINCT catid, cat_name ";
			$query .= "FROM #__distilled_homepage_events AS a ";
			$query .= "WHERE 1=1 ";
			$query .= ($category) ? "AND catid ='$category' " : "";
			$query .= ($country) ? "AND countrycode ='$country' " : "";
			$query .= ($city) ? "AND city ='$city' " : "";
			$query .= " ORDER BY cat_name";
			
			$db->setQuery($query);
			$categories = $db->loadObjectList();
			//dump($categories,"categories array");

		// GET all active countries
			$query = "SELECT DISTINCT countrycode, country "; 
			$query .= "FROM #__distilled_homepage_events AS a ";
			$query .= "WHERE 1=1 ";
			$query .= ($category) ? "AND catid ='$category' " : "";
			$query .= ($country) ? "AND countrycode ='$country' " : "";
			$query .= ($city) ? "AND city ='$city' " : "";
			$query .= " ORDER BY country";
			
			$db->setQuery($query);
			$countries = $db->loadObjectList();
			//dump($countries,"countries array");

		// GET all active cities
			$query = "SELECT DISTINCT city "; 
			$query .= "FROM #__distilled_homepage_events AS a ";
			$query .= "WHERE 1=1 ";
			$query .= ($category) ? "AND catid ='$category' " : "";
			$query .= ($country) ? "AND countrycode ='$country' " : "";
			$query .= ($city) ? "AND city ='$city' " : "";
			$query .= " ORDER BY city";
			
			$db->setQuery($query);
			$cities = $db->loadObjectList();
			//dump($countries,"countries array");
			
			return array($categories, $countries, $cities);
	}

    // ---------------------------------------------------------------------------------------------------------

    /**
     * Get events to show in the result area
     * the function collect the user filter selection from GET parameters, create the query and search in DB
     *
     * @access     public
     * @param      string 		  'E' or 'C'
     *								E: return the events, limited by paginator data 
	 *		 						C: Count all articles, not limited
     * @param      number 		  number or results for every page (0 = all)          
     * @return     array          all the results extracted
     */
    function getEvents($what, $resultsPerPage) {

	// Get a db connection and create new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

	// Compose query
		switch ($what) {
			case 'E':
				$query = "SELECT * ";
			break;
			case 'C':
				$query = "SELECT count(*) as counter ";
			break;
		}

		$query .= "
			FROM #__distilled_homepage_events AS a
			JOIN 
			( SELECT ev_id, MIN(repetition_id) as rp_id
			  FROM #__distilled_homepage_events GROUP BY ev_id
			) as b
			ON a.repetition_id = b.rp_id AND a.ev_id = b.ev_id
			";

		if ((isset($_GET['continentFilter']))&&($_GET['continentFilter']!=""))
			$query .= "AND continent = '".$_GET['continentFilter']."' ";

		if ((isset($_GET['countryFilter']))&&($_GET['countryFilter']!=""))
			$query .= "AND countrycode = '".$_GET['countryFilter']."' ";
			//dump($_GET['countryFilter'],"countryFilter");
		if ((isset($_GET['cityFilter']))&&($_GET['cityFilter']!=""))
			$query .= "AND city = '".$_GET['cityFilter']."' ";
			//dump($_GET['cityFilter'],"cityFilter");
			
		if ((isset($_GET['categoryFilter']))&&($_GET['categoryFilter']!=""))
			$query .= "AND catid = '".$_GET['categoryFilter']."' ";
			//dump($_GET['categoryFilter'],"categoryFilter");
		if ((isset($_GET['location']))&&($_GET['location']!=""))
			$query .= "AND venue LIKE '%".$_GET['location']."%' ";
			//dump($_GET['location'],"location");
		if ((isset($_GET['teacher']))&&($_GET['teacher']!=""))
			$query .= "AND teachers LIKE '%".$_GET['teacher']."%' ";

		//if ((isset($_GET['startDate']))&&($_GET['endDate'])&&($_GET['startDate']<=$_GET['endDate'])) $query .= " AND a.start_date >= ".$_GET['startDate']." AND a.end_date <= ".$_GET['endDate'];

		if ((isset($_GET['startDate']))&&($_GET['startDate']!=""))
			//$query .= "AND date_start >= '".date("Y-m-d", strtotime($_GET['startDate']))." '";
			$query .= "AND date_start >= '".strtotime($_GET['startDate'])." '";
			
		if ((isset($_GET['endDate']))&&($_GET['endDate']!=""))
			//$query .= "AND date_end >= '".date("Y-m-d", strtotime($_GET['endDate']))." '";
			$query .= "AND date_end <= '".strtotime($_GET['endDate'])." '";


		switch ($what) {
			case 'E':
				$query .= " ORDER BY date_start";

				// Get pagination start 
					$start = (isset($_GET['itemStartLimit'])) ? $_GET['itemStartLimit'] : 0;

				// Execute query
					//$db->setQuery($query);
					$db->setQuery($query, $start, $resultsPerPage);
					$results = $db->loadObjectList();
					return array($results);        
			break;
			case 'C':
				$db->setQuery($query);
				$result = $db->loadObjectList();
				return $result[0]->counter;
			break;

		}
	}
    
    // ---------------------------------------------------------------------------------------------------------

    /**
     * Get the data of a single event
     *
     * @access     public
     * @param      number 		the event ID    
     * @return     object       the datas
     */
	    function getSingleEvent($eventID) {
    		// Get a db connection  and create new query object.
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

		// Compose query
			$query = "
				SELECT 
					a.id AS evdet_id, 
					a.name AS summary,
					a.description AS description,
					a.start_date AS dtstart,
					a.end_date AS dtend,
					a.facebook AS facebook,
					b.name AS location,
					b.city AS city,
					b.street AS street,
					d.name AS country,
					c.id AS catid,
					c.title AS category,
					GROUP_CONCAT(f.id SEPARATOR ', ') as teachersID,
					GROUP_CONCAT(f.name SEPARATOR ', ') as teachersName
				FROM #__contactevents_events3 AS a
				LEFT JOIN #__contactevents_locations AS b ON a.location = b.id
				LEFT JOIN #__categories AS c ON a.category = c.id
				LEFT JOIN #__contactevents_countries AS d ON b.country = d.id 
				LEFT JOIN #__contactevents_teachers AS f ON FIND_IN_SET(f.id, a.teacher)
				WHERE a.id = $eventID ";

			$query .="AND a.state = 1
				GROUP BY a.id
			";
			
			$db->setQuery($query);
			$results = $db->loadObjectList();

			//dump($results[0],"Event Datas");

			return $results[0];
    	}


    // ---------------------------------------------------------------------------------------------------------

    /**
     * Get item URL
     *
     * Return the absolute link to the item
     *
     * @access     public
     * @param      number          the id of the item
     * @return     string          the absolute link to the item
     */
	function getItemURL($itemID){

		// ITEM URL (host + uri + path + item)
		$linkUri = $_SERVER['HTTP_HOST'];
		//dump($linkUri,"linkUri");
		$linkPath = explode('?', $_SERVER['REQUEST_URI'], 2);
		//dump($linkPath,"linkPath");
		$linkNoItem = (isset($_SERVER['HTTPS'])) ? ('https://' .$linkUri.$linkPath[0]) : ('http://' .$linkUri.$linkPath[0]);
		//dump($linkNoItem,"linkNoItem");
		$linkFull = $linkNoItem."?".$linkPath[1]."&item=".$itemID;
		//dump($linkFull,"linkFull");

		return $linkFull;
	}    


// ---------------------------------------------------------------------------------------------------------

    /**
     * Render the results after event filters selection
     *
     * @access     public
     * @param      array        the list of all events extracted after user filter selection
     * @return     none          
     */
    function printResults($result, $numberOfPages, $eventsPerPage, $totalNumberEvents) {
    	//dump($result,"result ok");

    	// get user
	    	$user = JFactory::getUser();
			//echo "id:".$user->id;


	    // Print number of results found
	    	echo "<div class='row resultsFound'>
	    			<div class='col-md-9'></div>
	    			<div class='col-md-3' style='background-color:white; min-height:30px; text-align: center; line-height: 30px;'>".$totalNumberEvents." ".JText::_('COM_CONTACTEVENTS_RESULTS_FOUND')."</div>
    			 </div>";
        	//echo "<div class='totalResultsFound'>".$totalNumberEvents." results found</div>";

		// Print all events
	    	echo "<div class='list'>";
	    		foreach ($result as $key=>$event){	

	    			//dump($event,"event".$key);

	    			// To give rows different colors (white/gray)
		            	$oddEven = ($key % 2 == 0) ? 'odd' : 'even';
		            
		            // Check if the event last just one day 
            			$start_end_sameday = strcmp(date('d - m', $event->date_start),date('d - m', $event->date_end));

	            			
        			// This is for the square color of the date (year0, year1, year2)
        				$yearFromNow = date('Y', $event->date_start) - date("Y");

        				$dateStart = date('d/m/Y', $event->date_start);
        				$dateStart_day = date('d', $event->date_start);
        				$dateStart_month = date('M', $event->date_start);

        				$dateEnd = date('d/m/Y', $event->date_end);
        				$dateEnd_day = date('d', $event->date_end);
        				$dateEnd_month = date('M', $event->date_end);


        			// Get current language code
        				$lang = JFactory::getLanguage();
        				$languages = JLanguageHelper::getLanguages('lang_code');
        				$languageCode = $languages[ $lang->getTag() ]->sef;

        			// Get eventsdetail string to put in the link
        				switch ($languageCode) {
						    case 'es':
						        $eventsdetailLang = "eventodetalle";
						        break;
						    case 'it':
						        $eventsdetailLang = "dettaglievento";
						        break;
						    case 'fr':
						        $eventsdetailLang = "detailevenement";
						        break;
						    case 'de':
						        $eventsdetailLang = "eventdetail";
						        break;
						    case 'pt':
						        $eventsdetailLang = "detalhesevento";
						        break;
					       	default:
								$eventsdetailLang = "eventdetail";
						}

		            // Create LINK
		            	$alias = str_replace(' ', '-', strtolower($event->summary));
						$link = "events/".$eventsdetailLang."/".$event->rp_id."/-/".$alias;

					// Print ROW
		            	echo "<div class='row ".$oddEven."'>";

				            // Date
					            echo "<div class='col-md-1 date'>";
					            	
					            	if ($start_end_sameday){
					            		echo "<div class='row' style='margin:0;'>";

							            	echo "<div class='col-xs-6'>";
							            		
							            		// Mobile
							            			echo "<div class='date_start dateMobile year".$yearFromNow."'>";
									            		echo $dateStart;
								            		echo "</div>";
							            		// Desktop
								            		echo "<div class='date_start dateDesktop year".$yearFromNow."' tooltip='".$dateStart."' style='display:none;'>";
									            		echo $dateStart_day;
									            		echo "<br>";
									            		echo $dateStart_month;
							            			echo "</div>";
						            			
						            		echo "</div>";

							            	echo "<div class='col-xs-6'>";
							            		
								            		// Mobile
							            				echo "<div class='date_end dateMobile year".$yearFromNow."'>";
										            		echo $dateEnd;
									            		echo "</div>";
							            			// Desktop
									            		echo "<div class='date_end dateDesktop year".$yearFromNow."' tooltip='".$dateEnd."' style='display:none;'>";
										            		echo $dateEnd_day;
										            		echo "<br>";
										            		echo $dateEnd_month;
									            		echo "</div>";
							            		
						            		echo "</div>";
					            		echo "</div>";
				            		}
				            		else{
				            			echo "<div class='row' style='margin:0; padding-right:3px;'>";
					            			
							            		// Mobile
				            						echo "<div class='date_start dateMobile year".$yearFromNow."' tooltip='".$dateStart."' style='width:100%;'>";
													
									            		echo $dateStart;
								            		echo "</div>";
					            				// Desktop
						            				echo "<div class='date_start dateDesktop year".$yearFromNow."' tooltip='".$dateStart."' style='width:100%; display:none;'>";
									            		echo $dateStart_day;
									            		echo "<br>";
									            		echo $dateStart_month;
								            		echo "</div>";
						            		
					            		echo "</div>";
				            		}
				            		
					            echo "</div>";
					            echo "<div class='col-md-3 vcenter link'><a href='".$link."'>".$event->summary."</a></div>";
					            echo "<div class='col-md-2 vcenter teachers'>";
					            	echo "<i class='fa fa-users' tooltip='Teachers' style='margin-right:10px;'></i>";
					            	echo $event->teachers;
								echo "</div>";	
					            echo "<div class='col-md-2 vcenter category'>";
					            	echo "<i class='fa fa-tag' tooltip='Category' style='margin-right:10px;'></i>";
					            	echo $event->cat_name;
					            echo "</div>";


					            echo "<div class='col-md-3 vcenter location'>";
					            	echo "<i class='fa fa-map-marker' tooltip='Venue' style='margin-right:10px;'></i>";
					            	echo $event->venue." <br> ".$event->city.", ".$event->country;
								echo "</div>";				            

								if (strlen($event->facebook_link) != 0){
									echo "<div class='col-md-1 vcenter facebook'><a href='".$event->facebook_link."' target='_blank'><i class='fab fa-facebook-square'></i></i></a></div>";			
								}
					            
		        		echo "</div>";
	        		
		        } /* end foreach */


		        // PAGINATION
		        	$this->printPaginator($numberOfPages, $eventsPerPage, $totalNumberEvents);
		        	


	    	echo "</div>";
	    	

	    	// Ora il file generato contiene solo l'intestazione, devo riempire ancora con tutti gli eventi
	    	/*
	    	echo "<div class='map' id='map'>";
	    	echo "</div>";
	    	echo "<script async defer src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBy9ll-1DF7RLKYleJWnRveMXu1qJAxZw4&callback=initMap'></script>";
			*/



    }

	// ---------------------------------------------------------------------------------------------------------

    /**
     * Render the paginator - The paginator is printed just if there are more than one page
     *
     * @access     public
     * @param      number        total number of the pages
     * @param      number        if of the paginator
     * @param      number        number of the actual page
     * @return     none          
     */

    function printPaginator($numberOfPages, $eventsPerPage, $totalNumberOfResults){
		
    	//dump($numberOfPages, "number of pages");
    	//dump($eventsPerPage, "events per page");
    	//dump($totalNumberOfResults, "total number of results");

    	if ( $numberOfPages > 1){

	    	echo "<div class='text-center'>";
	    		echo "<ul class='pagination'>";
	    	
			    	$link = "";
			    	$page = (isset($_GET['page'])&& !empty($_GET['page'])) ? $_GET['page'] : 1; // your current page
			    	$itemStartLimit = (isset($_GET['itemStartLimit'])) ? $_GET['itemStartLimit'] : 0; //
			 		$pages = $numberOfPages; // Total number of pages

			 		//dump($pages, "pages");
					//dump($page, "page");

			 		// Get parameters list from page URL
						//dump($_SERVER['QUERY_STRING'],"parameters");
						//dump($_GET['itemStartLimit'],"limitStart");


					    // Set subgroup page start:
							if    ( $pages < 6        ) $start = 2;             
							elseif( $page  < 3        ) $start = 2;             
							elseif( $page  > $pages-3 ) $start = $pages - 3;    
							else                        $start = $page  - 1;    

						// Compose Paginator

							// Page 1 (always present)   
								$active = ($page == 1) ? "active" : "";
								
								//$output = "<li class='".$active."'><a href='?page=1&itemStartLimit=0'>1 </a><li>";
								$parameters = $_SERVER['QUERY_STRING'];
								$parameters = str_replace("&page=".$_GET['page'], "&page=1", $parameters);
								$parameters = str_replace("&itemStartLimit=".$_GET['itemStartLimit'], "&itemStartLimit=0", $parameters);
								$output = "<li class='".$active."'><a href='?".$parameters."'>1 </a><li>";

							// Initial separator
								if( $start > 2 ){
									$parameters = $_SERVER['QUERY_STRING'];
									$parameters = str_replace("&page=".$_GET['page'], "&page=".$page, $parameters);
									$parameters = str_replace("&itemStartLimit=".$_GET['itemStartLimit'], "&itemStartLimit=".$itemStartLimit, $parameters);

									$output .= "<li class='disabled'><a href=href='?".$parameters."'>...</a></li>";
								} 

							// Page subgroup: ends if reached +2 or pages-1
								for( $i = $start; $i < $pages; $i++ ){ 
									$active = ($page == $i) ? "active" : "";

									$parameters = $_SERVER['QUERY_STRING'];
									$parameters = str_replace("&page=".$_GET['page'], "&page=".$i, $parameters);
									$parameters = str_replace("&itemStartLimit=".$_GET['itemStartLimit'], "&itemStartLimit=".(($i-1)*$eventsPerPage), $parameters);
									
								    $output .= "<li class='".$active."'><a href='?".$parameters."'>".$i."</a></li> "; 

								    if( $i > ($start+1) ) break; 
								}
							// Final separator
								if( $start < $pages - 3 ) {
									$parameters = $_SERVER['QUERY_STRING'];
									$parameters = str_replace("&page=".$_GET['page'], "&page=".$page, $parameters);
									$parameters = str_replace("&itemStartLimit=".$_GET['itemStartLimit'], "&itemStartLimit=".$itemStartLimit, $parameters);

									$output .= "<li class='disabled'><a href='?".$parameters."'>...</a></li>";
								}
							// Last page
								$active = ($page == $pages) ? "active" : "";
								if( $pages > 1 ) {
									$parameters = $_SERVER['QUERY_STRING'];
									$parameters = str_replace("&page=".$_GET['page'], "&page=".$pages, $parameters);
									$parameters = str_replace("&itemStartLimit=".$_GET['itemStartLimit'], "&itemStartLimit=".(($pages-1)*$eventsPerPage), $parameters);

									$output .= "<li class='".$active."'><a href='?".$parameters."'>$pages </a></li>";
								}

					// Print Paginator    
						echo $output;

		    	echo "</ul>";
			echo "</div>";
		}

	}
	    
	// ---------------------------------------------------------------------------------------------------------

}


