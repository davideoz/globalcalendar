<?php
	
	joomlaInitialize();

/**
     * Initialize joomla framework in an external file
     *
     * @access     public 
     * @return     none       
     */

		function joomlaInitialize() {
			// Show errors
				ini_set('display_errors', 1);
				ini_set('display_startup_errors', 1);
				error_reporting(E_ALL);

			// Include Joomla core files
				define('_JEXEC', 1);
				define('DS', DIRECTORY_SEPARATOR);

				if (file_exists(dirname(__FILE__) . '/defines.php')) {
				    include_once dirname(__FILE__) . '/defines.php';
				}
				if (!defined('_JDEFINES')) {
				    define( 'JPATH_BASE', realpath(dirname(__FILE__).'/../../..' ));
				    require_once (JPATH_BASE . DS . 'includes' . DS . 'defines.php');
				}
				require_once (JPATH_BASE . DS . 'includes' . DS . 'framework.php');
				//require_once (JPATH_BASE . DS . 'libraries' . DS . 'joomla' . DS . 'factory.php');
			
			// Get database table PREFIX
				$app = JFactory::getApplication('site');
				$app->initialise();
				$dbPrefix = $app->getCfg('dbprefix');

			// Get database NAME
				$config = JFactory::getConfig();
				$dbName = $config->get('db');

			//echo $dbName;
			//echo $dbPrefix;

			/*
			$app->route();
			$app->render();
			echo $app;
			*/
			
			//echo "Joomla Inizialized <br>";
			return array($dbName, $dbPrefix);
		}






