<?php
	
	// DEBUG
		// call this: components/com_contactevents/assets/cookies.php
		//deleteCookie("session");
		//getUserCookie("session");

	
	/**
     * Get the cookie value that identify the user 
	 * if is not yet present it create it 
     *
     * @access    public 
     * @param     string 		the cookie name  
     * @return    number       	the cookie value   
     */

		function getUserCookie($cookieName) {
			
			if(!isset($_COOKIE[$cookieName])) {
				// Create 8 digits random value			
					$digits = 10;
					$cookieValue = rand(pow(10, $digits-1), pow(10, $digits)-1);

				// Set the cookie
				    setcookie($cookieName, $cookieValue);
				    $_COOKIE[$cookieName] = $cookieValue;
			}
			else{
				// GET the previously stored cookie
					$cookieValue = $_COOKIE[$cookieName];
			}
			//echo "cookie_attuale: ".$_COOKIE[$cookieName];

			return $cookieValue;
		}


	/** ------------------------------------------------------------------ **/


	/**
     * Delete cookie
     *
     * @access    public 
     * @param     string 		the cookie name  
     * @return    boolean 		true if the cookie is deleted
     */

		function deleteCookie($cookieName) {

			if (isset($_COOKIE[$cookieName])) {
			    unset($_COOKIE[$cookieName]);
			    setcookie($cookieName, null, -1, '/');
			    return true;
			} else {
			    return false; 
			}
		}

	/** ------------------------------------------------------------------ **/
	

	/**
     * Store upcoming events cookie
     *
     * @access    public 
     * @param     string 		kind of detail view(teacher, organizer, venue)
     * @param     number 		id of the kind stored
     * @return    boolean 		true if the cookie is deleted
     */
	/*
		function setUpcomingEventsCookie($kind, $id){
			$cookieName = "upcomingEventKind";
			$cookieValue = $kind;
			setcookie($cookieName, $cookieValue);

			$cookieName = "upcomingEventID";
			$cookieValue = $id;
			setcookie($cookieName, $cookieValue);

		}
		*/

	/** ------------------------------------------------------------------ **/
	

	/**
     * Get upcoming events cookie
     *
     * @access    public 
     * @param     string 		kind of detail view(teacher, organizer, venue)
     * @param     number 		id of the kind stored
     * @return    boolean 		true if the cookie is deleted
     */
	/*
		function getUpcomingEventsCookie(){
			return "pippone";

		}
		*/
