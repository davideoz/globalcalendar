<?php
	
	// http://localhost/test/Joomla_3.6.5-Master-bootsrap3/components/com_contactevents/assets/contacteventsAutomateFunctions.php

	// Initialize joomla access the DB
		include 'joomlaInitialize.php';
		$databaseNameAndPrefix = joomlaInitialize();
		$dbName = $databaseNameAndPrefix[0];
	    $dbPrefix = $databaseNameAndPrefix[1];

	//justSelect();
	createEmptyDistilledHomepageEvents();
	populateDistilledHomepageEvents();

	/**
     * empty distilled_homepage_events Table (if not exist)
     *
     * @access     public 
     * @return     none       
     */
	function createEmptyDistilledHomepageEvents() {
		
		// Get a db connection and create new query object
			$db = JFactory::getDBO(); 
			$query = $db->getQuery(true);

		// CREATE TABLE IF NOT EXIST
			$query = 
            		"CREATE TABLE IF NOT EXISTS awg72_distilled_homepage_events (
					id int(12) NOT NULL AUTO_INCREMENT, 
					ev_id int(12) NOT NULL, 
					summary longtext, 
					catid int(11), 
					cat_name varchar(255), 
					date_start int(11), 
					date_end int(11), 
					facebook_link varchar(255), 
					active tinyint(3), 
					venue_id int(12), 
					venue varchar(255) NOT NULL, 
					url varchar(255), 
					street varchar(255), 
					postcode varchar(255), 
					city varchar(255), 
					state varchar(255), 
					countrycode varchar(255) NOT NULL, 
					country varchar(255) NOT NULL, 
					continent varchar(255) NOT NULL,
					geo_longitude float(12,8),
					geo_latitude float(12,8),
					image varchar(255), 
					repetition_id int(12), 
					startrepeat datetime, 
					endrepeat datetime, 
					teachers text, 
					organizers text,
					PRIMARY KEY  (id)
                      )ENGINE=INNODB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";

				$db->setQuery($query);
				$db->execute();

		// EMPTY THE TABLE (if already exist)
			$query = "TRUNCATE TABLE awg72_distilled_homepage_events;";
			$db->setQuery($query);
			$db->execute();

			echo "Create or Empty table: awg72_distilled_homepage_events <br>";
	}

// ---------------------------------------------------------------------------------------------------------

   /**
     * Populate the Homepage Events Table
     *
     * @access     public 
     * @return     none       
     */
	    function populateDistilledHomepageEvents() {

    		// Get a db connection  and create new query object.
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

		// Compose query
			$query = "
				INSERT INTO awg72_distilled_homepage_events (
					ev_id, 
					summary, 
					catid, 
					cat_name,
					date_start, 
					date_end, 
					facebook_link, 
					active, 
					venue_id,
					venue, 
					url, 
					street, 
					postcode, 
					city, 
					state, 
					countrycode,
					country, 
					continent,
					geo_longitude,
					geo_latitude,
					image, 
					repetition_id, 
					startrepeat, 
					endrepeat, 
					teachers, 
					organizers)
				SELECT
					a.ev_id,
					b.summary,
					a.catid,
					c.title,
					b.dtstart as date_start,
					b.dtend as date_end,
					d.value as facebook_link,
					a.state AS active,
					b.location AS venue_id,
					e.title AS venue,
					e.url,
					e.street,
					e.postcode,
					e.city,
					e.state,
					e.country as countrycode,
					g.country,
					g.continent,
					e.geolon,
					e.geolat,
					e.image,
					min(f.rp_id),
					f.startrepeat,
					f.endrepeat,
					GROUP_CONCAT(t.title SEPARATOR ', ') as teachers,
					GROUP_CONCAT(DISTINCT o.title SEPARATOR ', ') as organizers

				FROM awg72_jevents_vevent AS a

				LEFT JOIN awg72_jevents_vevdetail
				AS b
				     ON a.detail_id = b.evdet_id


				LEFT JOIN (
				     SELECT tt.title, pe.evdet_id
				     FROM awg72_jev_peopleeventsmap AS pe
				               LEFT JOIN awg72_jev_people AS tt
				                    ON pe.pers_id = tt.pers_id
				     WHERE tt.type_id = 1
				     )
				AS t
				     ON a.ev_id = t.evdet_id

				LEFT JOIN (
				     SELECT tt.title, pe.evdet_id
				     FROM awg72_jev_peopleeventsmap AS pe
				               LEFT JOIN awg72_jev_people AS tt
				                    ON pe.pers_id = tt.pers_id
				     WHERE tt.type_id = 2
				     )
				AS o
				     ON a.ev_id = o.evdet_id


				LEFT JOIN awg72_categories
				AS c
					ON a.catid = c.id


				LEFT JOIN (
				     SELECT k.evdet_id, k.value
				     FROM awg72_jev_customfields AS k
				     WHERE k.name =  'facebook'
				)
				AS d
				     ON a.ev_id = d.evdet_id

				LEFT JOIN awg72_jev_locations
				AS e
				     ON b.location = e.loc_id

				LEFT JOIN awg72_jevents_repetition
				AS f
				     ON a.ev_id = f.eventid

				LEFT JOIN awg72_countries
				AS g
				     ON e.country = g.countrycode

				WHERE ( f.endrepeat>=NOW())
				     AND (a.state=1)

				GROUP BY
				     a.ev_id,
				     d.value,
				    f.startrepeat,
				    f.endrepeat,
				    g.country,
					g.continent 
				ORDER BY b.dtstart
				";

				// WHERE ( b.dtend>=UNIX_TIMESTAMP())
				// WHERE ( b.dtend>=UNIX_TIMESTAMP())

			$start_time = microtime(TRUE);
				$db->setQuery($query);
				$db->execute();
			$finish_time = microtime(TRUE);

			echo "Populated table: awg72_distilled_homepage_events <br>";
			
			$execution_time = ($finish_time - $start_time);
			echo "Query execution time: ".$execution_time." sec.<br>";
    	}


// ---------------------------------------------------------------------------------------------------------

  /**
     * Populate the Homepage Events Table
     *
     * @access     public 
     * @return     none       
     */
	    function justSelect() {

		// Get a db connection  and create new query object.
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

		// Compose query
			$query = "
				SELECT
					a.ev_id,
					b.summary,
					a.catid,
					b.dtstart as date_start,
					b.dtend as date_end,
					d.value as facebook_link,
					a.state AS active,
					e.title AS venue,
					e.url,
					e.street,
					e.postcode,
					e.city,
					e.state,
					e.country,
					e.image,
					min(f.rp_id),
					f.startrepeat,
					f.endrepeat,
					GROUP_CONCAT(t.title SEPARATOR ', ') as teachers,
					GROUP_CONCAT(DISTINCT o.title SEPARATOR ', ') as organizers

				FROM awg72_jevents_vevent AS a

				LEFT JOIN awg72_jevents_vevdetail
				AS b
				     ON a.detail_id = b.evdet_id


				LEFT JOIN (
				     SELECT tt.title, pe.evdet_id
				     FROM awg72_jev_peopleeventsmap AS pe
				               LEFT JOIN awg72_jev_people AS tt
				                    ON pe.pers_id = tt.pers_id
				     WHERE tt.type_id = 1
				     )
				AS t
				     ON a.ev_id = t.evdet_id

				LEFT JOIN (
				     SELECT tt.title, pe.evdet_id
				     FROM awg72_jev_peopleeventsmap AS pe
				               LEFT JOIN awg72_jev_people AS tt
				                    ON pe.pers_id = tt.pers_id
				     WHERE tt.type_id = 2
				     )
				AS o
				     ON a.ev_id = o.evdet_id

				LEFT JOIN (
				     SELECT k.evdet_id, k.value
				     FROM awg72_jev_customfields AS k
				     WHERE k.name =  'facebook'
				)
				AS d
				     ON a.ev_id = d.evdet_id

				LEFT JOIN awg72_jev_locations
				AS e
				     ON b.location = e.loc_id

				LEFT JOIN awg72_jevents_repetition
				AS f
				     ON a.ev_id = f.eventid

				WHERE ( b.dtend>=CURDATE())
				     AND (a.state=1)

				GROUP BY
				     a.ev_id,
				     d.value,
				    f.startrepeat,
				    f.endrepeat
				ORDER BY b.dtstart
				";

			$db->setQuery($query);
			$results = $db->loadObjectList();
			
			foreach ($results as $key => $result) {
				echo $result->summary;
				echo "<br>";
				//var_dump($result);
			}
    	}



