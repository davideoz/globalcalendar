<?php 


// http://globalcalendarlocale.com/components/com_contactevents/helpers/ajaxUpdateSelects.php?filter_changed=country&country_id=ES
// http://globalcalendarlocale.com/components/com_contactevents/helpers/ajaxUpdateSelects.php?filter_changed=category&category_id=24
// http://globalcalendarlocale.com/components/com_contactevents/helpers/ajaxUpdateSelects.php?filter_changed=category&category_id=24&country_id=ES

require "ajaxConnectDB.php";

// Disable warnings
	//error_reporting(E_ERROR | E_PARSE);

if (isset($_GET['filter_changed'])) {

	$filter_changed = $_GET['filter_changed'];
	$country_id = $_GET['country_id'];
	$category_id = $_GET['category_id'];
	$city_id = $_GET['city_id'];

	$eventsData = array();

	switch ($filter_changed) {
		case 'country':
			// Get all the categories active for this country at the moment
			   	$query =  "SELECT DISTINCT catid, cat_name ";
			   	$query .= "FROM awg72_distilled_homepage_events ";
			   	$query .= "WHERE countrycode = '$country_id' ";
			   	
			   	if ($category_id != '')
					$query .= "AND catid = '$category_id' ";

			   	$result = mysqli_query($db, $query);

			  	if($result->num_rows > 0){	
			       /* while($result = mysqli_fetch_all($result,MYSQLI_ASSOC)) {
			            $eventsData['categories'] = $result;
			        }*/
			        $eventsData['categories'] = mysqli_fetch_all($result,MYSQLI_ASSOC);
			    }
			    else{
			        $eventsData = "no results";
			    }

			// Get all the cities active for this country at the moment
			    $query =  "SELECT DISTINCT city ";
			    $query .= "FROM awg72_distilled_homepage_events ";
			    $query .= "WHERE countrycode = '$country_id' ";

			    if ($category_id != '')
					$query .= "AND catid = '$category_id' ";

			    $result = mysqli_query($db, $query);

			    if($result->num_rows > 0){
			        /*while($result = mysqli_fetch_all($result,MYSQLI_ASSOC)) {
			            $eventsData['cities'] = $result;
			        }*/
			        $eventsData['cities'] = mysqli_fetch_all($result,MYSQLI_ASSOC);
			    }

			break;
		
		case 'category':
			// Get the countries where this category is available

				$query =  "SELECT DISTINCT countrycode, country ";
				$query .= "FROM awg72_distilled_homepage_events ";
				$query .= "WHERE catid = '$category_id' ";

				if ($country_id != '')
					$query .= "AND countrycode = '$country_id' ";

			    $result = mysqli_query($db, $query);

			  	if($result->num_rows > 0){
			        /*while($result = mysqli_fetch_all($result,MYSQLI_ASSOC)) {
			            $eventsData['countries'] = $result;
			        }*/
			        $eventsData['countries'] = mysqli_fetch_all($result,MYSQLI_ASSOC);
			    }
			    else{
			        $eventsData = "no results";
			    }

			break;

		case 'city':
			// Get the categories where this city is available

				$query =  "SELECT DISTINCT catid, cat_name ";
				$query .= "FROM awg72_distilled_homepage_events ";
				$query .= "WHERE city = '$city_id' ";

				//if ($country_id != '')
				//	$query .= "AND countrycode = '$country_id' ";

			    $result = mysqli_query($db, $query);

			  	if($result->num_rows > 0){
			        /*while($result = mysqli_fetch_all($result,MYSQLI_ASSOC)) {
			            $eventsData['categories'] = $result;
			        }*/
			         $eventsData['categories'] = mysqli_fetch_all($result,MYSQLI_ASSOC);
			    }
			    else{
			        $eventsData = "no results";
			    }


			break;



		default:
			echo "no parameters received";
			break;
	}
}

	mysqli_free_result($result);
	mysqli_close($db);
    
    //returns data as JSON format
    header('Content-Type: application/json');
    echo json_encode($eventsData);


?>