



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>Document</title>

	<link type="text/css" href="/templates/master-bootstrap-3-master/css/bootstrap.min.css" rel="stylesheet" />
	<script type="text/javascript" src="/templates/master-bootstrap-3-master/js/jquery-2.1.3.min.js"></script>

	<script>

/*
		jQuery(document).ready(function() {

			jQuery("#report-abuse-send").on("click", function(){
	            alert("click send");

	
	            jQuery.ajax({
	                    type: 'POST',
	                    url: 'reportAbuse.php',
	                    data: jQuery("#report-abuse-form").serialize(),
	                    success: function(data) {
	                        if(data == "true") {
	                        	console.log("ciao1");
	                            jQuery("#report-abuse-form").fadeOut("fast", function(){
	                                jQuery(this).before("<p><strong>Success! Your feedback has been sent, thanks :)</strong></p>");
	                                setTimeout("$.fancybox.close()", 1000);
	                            });
	                        }
	                        else{
	                        	console.log("ciao2");
	                        }
	                    }
	                });
	

	        });
		});
*/

	</script>

</head>


<body>
	<div class="container">


<?php

// SHOW REPORT FORM - if the form is not posted yet
	if (!($_SERVER['REQUEST_METHOD'] == 'POST')){     ?>
		
		<div class="col-sm-12">

			<h3 style='color: #f08e0d;'>Report miseuse</h3>
			<p>
				You are about to report a violation of the calendar <a href="#" target="_blank">terms of use</a><br>
			</p>

			<form id="report-abuse-form" class="form-group" name="report-abuse-form" action="#" method="post" _lpchecked="1">
	
				<!-- Hidden link of the reported event -->
				<input name="eventlink" type="hidden" value="<?php echo ($_GET["link"]); ?>">

				<div class="form-group">
					<label for="reason">Reason:</label>
					<select name="reason" class="form-control" tabindex="1" required="">
						<option value="" disabled selected>Select one option</option>
						<option value="1">Not about Contact Improvisation</option>
						<option value="2">Contains wrong informations</option>
						<option value="3">It is not translated in english</option>
						<option value="4">Other (specify in the message)</option>
					</select>
				</div>

				<div class="form-group">
					<label for="message">Message: (optional)</label>
	    			<textarea class="form-control" name="message" placeholder="Include all the details you can" tabindex="2" ></textarea>
				</div>

				<p>
					<!--<button name="submit" type="submit" id="contact-submit" class="btn btn-primary">Send Email</button>-->
					<button id="report-abuse-send" class="btn btn-primary">Send Report</button>

					<!--<a id="report-abuse-send" class="btn btn-primary" href="#">Send E-mail</a>-->
				</p>
			</form>
		</div>
		
	<?php
	}

// SEND EMAIL WHEN POST IS SUBMITTED 
	else{
			// https://docs.joomla.org/Sending_email_from_extensions
			// Eg. calling: http://localhost/test/Joomla_3.6.5-Master-bootsrap3/components/com_contactevents/assets/reportAbuse.php?link=Joe

		// Initialize joomla access the DB
			//include 'joomlaInitialize.php';
			//$databaseNameAndPrefix = joomlaInitialize();
			//$dbName = $databaseNameAndPrefix[0];
		    //$dbPrefix = $databaseNameAndPrefix[1];

			include 'swiftmailer/lib/swift_required.php';

	    // GET parameters
			//$link = JRequest::getVar('link');
			//$abuserMail = JRequest::getVar('creator_mail');

	    	$eventlink = isset($_POST['eventlink']) ? $_POST['eventlink'] : ''; 
			$reasonOfAbuseCode = isset($_POST['reason']) ? $_POST['reason'] : '';
			$reasonMessage = isset($_POST['message']) ? $_POST['message'] : '';
			$abuserMail = isset($_POST['creator_mail']) ? $_POST['creator_mail'] : ''; 
			$adminMain = 'davide.casiraghi@gmail.com';



		// Assign a text to reason of abuse
			switch ($reasonOfAbuseCode) {
				case 1:
					$reasonOfAbuse = "Not about Contact Improvisation";
				break;
				case 2:
					$reasonOfAbuse = "It contains wrong informations";
				break;
				case 3:
					$reasonOfAbuse = "It is not translated in english";
				break;
				case 4:
					$reasonOfAbuse = "Other reasons";
				break;
				default:
					$reasonOfAbuse = "Nothing specified";
				break;
			}


		// Create the Transport
			//$transport = Swift_SmtpTransport::newInstance('localhost', 25);
			$transport = Swift_SmtpTransport::newInstance('smtp.googlemail.com', 465, 'ssl')
			  ->setUsername('davide.casiraghi@gmail.com')
			  ->setPassword('!1981iokiok2');

		// Create the Mailer using your created Transport
			$mailer = Swift_Mailer::newInstance($transport);

		// Create the message
			$message = Swift_Message::newInstance()

			  // Give the message a subject
			  ->setSubject('Report from CI Global Calendar')

			  // Set the From address with an associative array
			  ->setFrom(array('davide.casiraghi@gmail.com' => 'Davide Casiraghi'))

			  // Set the To addresses with an associative array
			  ->setTo(array('davide.casiraghi@gmail.com', 'davide.casiraghi@email.it'));

			  // Give it a body
			  $message->setBody(
			  	"A user report a misuse in the following event:<br>".
			  	"<a href=".$eventlink.">".$eventlink."</a><br><br>".
			  	"Why: ".$reasonOfAbuse."<br><br>".
			  	"Message: ".$reasonMessage
			  	, 'text/html');

			  // And optionally an alternative body
			  //$message->addPart('<q>Why: '.$reasonOfAbuse.'</q>', 'text/html')
			  //$message->addPart('<q>Message: '.$reasonMessage.'</q>', 'text/html');

		  // Optionally add any attachments
		  //->attach(Swift_Attachment::fromPath('my-document.pdf'))
		  //;

		// Send the message
			if (!$mailer->send($message, $failures)){
			  echo "Failures:";
			  print_r($failures);
			}
			else{
				echo "<div style='padding:20px 20px 40px 20px;'>";
					echo "<h2 style='color: #f08e0d;'>Thank you for your report</h2>\n";
					echo "The administrators will check soon the event you have specified\n";
				echo "</div>";
			}





/*
		// Create our mail object
			$mailer = JFactory::getMailer();

		// Set the mail Sender
			$config = JFactory::getConfig();
			$sender = array( 
			    $config->get( 'mailfrom' ),
			    $config->get( 'fromname' ) 
			);
			//$sender=array('davide.casiraghi@gmail.com');
			$mailer->setSender($sender);

		// Set the mail Recipients (the mail will be sent to the administrator and to the user that have inserted the event)
			//$user = JFactory::getUser();
			//$recipient = $user->email;
			$recipient = 'davide.casiraghi@gmail.com';
			//$recipient = array('davide.casiraghi@gmail.com');  // You can specify many mails here eg:  ('davide.casiraghi@gmail.com', 'person2@domain.com', 'person3@domain.com')
	 		//$recipient = array($adminMain, $abuserMail);
			$mailer->addRecipient($recipient);


		// Set content of the email
			$body = "A user report an abuse in the following event\n".$link;
			$mailer->setSubject('Report from CI Global Calendar');
			$mailer->setBody($body);
			$mailer->isHtml(false);

		// Set content of the email
			//$body   = '<h2>Our mail</h2>'
			//    . '<div>A message to our dear readers'
			//    . '<img src="cid:logo_id" alt="logo"/></div>';
			//$mailer->isHtml(true);
			//$mailer->Encoding = 'base64';
			//$mailer->setBody($body);
		
		// Optionally add embedded image
				//$mailer->AddEmbeddedImage( JPATH_COMPONENT.'/assets/logo128.jpg', 'logo_id', 'logo.jpg', 'base64', 'image/jpeg' );


		// Optional file attached
			//$mailer->addAttachment(JPATH_COMPONENT.'/assets/document.pdf');

		// Send mail
			$send = $mailer->Send();
			if ( $send !== true ) {

				echo "<div class='col-sm-12'>";
			    	echo '<h2>Error sending email</h2> ';
				echo "</div>";

			} else {
				echo "<div class='col-sm-12'>";
				    echo '<h2>Report sent</h2>';
				    echo "<p>Thank you for your help</p>";
			    echo "</div>";
			    
			    
			    //Notice message
				JError::raiseNotice(100, $messages);

				//Warning message
				JError::raiseWarning( 100, $messages );
			
			}
			
			//echo "<br>".$link;

			*/
	}

?>

	</div>

</body>
</html>