SELECT 
	a.ev_id, 
	b.summary, 
	a.catid, 
	b.dtstart, 
	b.dtend, 
	d.value, 
	a.state, 
	e.title, 
	e.url, 
	e.street, 
	e.postcode, 
	e.city, 
	e.state, 
	e.country, 
	e.image, 
	min(f.rp_id), 
	f.startrepeat, 
	f.endrepeat, 
	GROUP_CONCAT(p.title SEPARATOR ', ') as teachers
FROM awg72_jevents_vevent AS a

LEFT JOIN awg72_jevents_vevdetail 
AS b 
	ON a.detail_id = b.evdet_id










LEFT JOIN (
	SELECT tt.title
	FROM awg72_jev_peopleeventsmap AS pe
			LEFT JOIN awg72_jev_people AS tt
				ON pe.pers_id = tt.pers_id
	)
AS p
	ON a.ev_id = p.evdet_id





LEFT JOIN (
	SELECT k.evdet_id, k.value
	FROM awg72_jev_customfields AS k
	WHERE k.name =  'facebook'
) 
AS d 
	ON a.ev_id = d.evdet_id

LEFT JOIN awg72_jev_locations 
AS e
	ON b.location = e.loc_id

LEFT JOIN awg72_jevents_repetition
AS f
	ON a.ev_id = f.eventid
	
WHERE ( b.dtend>=CURDATE())
	AND (a.state=1)

GROUP BY 
	a.ev_id,
	d.value,
    f.startrepeat,
    f.endrepeat,
    p.title
ORDER BY b.dtstart