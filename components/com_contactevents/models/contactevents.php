<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
/**
 * HelloWorld Model
 *
 * @since  0.0.1
 */
class ContacteventsModelContactevents extends JModelItem
{
	/**
	 * @var string message
	 */
	protected $message;
 
	/**
	 * Get the message
         *
	 * @return  string  The message to be displayed to the user
	 */
	public function getMsg(){
		if (!isset($this->message))
		{
			$this->message = 'Hello World!';
		}
 
		return $this->message;
	}


	/**
         * @var string allEvents
         */
        protected $allEvents;

	/**
     * Get all the events
     * @return string The message to be displayed to the user
     */
    public function getAllEvents() {


    		// required for JFile::exists
			//jimport('joomla.filesystem.file');

		// Get a db connection.
			$db = JFactory::getDBO();

		// Create a new query object.
			$query = $db->getQuery(true);

			$query = "
				SELECT * 
				FROM #__distilled_homepage_events AS a
				WHERE 1=1 
				";


			$db->setQuery($query);
			$results = $db->loadObjectList();

			//dump($results,"results");



			$this->allEvents = $results;
            
            //$this->allEvents = 'All Events!';
            //$this->allEvents = $db->getPrefix();

            return $this->allEvents;        
    }




	/**
         * @var string eventCategories
         */
        protected $eventCategories;

	/**
     * Get all the events
     * @return string The message to be displayed to the user
     */
    public function getEventCategories() {

		// Get a db connection.
			$db = JFactory::getDBO();

		// Create a new query object.
			$query = $db->getQuery(true);

		// Select all extrafields to retrieve their alias name
			
			$query->select($db->quoteName(array(
				'a.id','a.title','a.extension'
				)));
			$query->from($db->quoteName('#__categories','a'));


			$conditions = array(
			    $db->quoteName('a.extension') . ' LIKE '. '\'com_jevents\''
			);
			$query->where($conditions);
			$query->order('id ASC');
			
			$db->setQuery($query);
			$results = $db->loadObjectList();


			$this->eventCategories = $results;
            
            
            //$this->eventCategories = 'All Events!';
            //$this->eventCategories = $db->getPrefix();

            return $this->eventCategories;        
    }














}