<?php
	// No direct access to this file
	defined('_JEXEC') or die('Restricted access');

    // Instantiate class events
        $events = new events; 

    // get Form URL
        //$formUrl = JRoute::_('index.php?option=com_contactevents&view=contactevents&Itemid=129');
        $formUrl = JURI::getInstance()->toString();
        $formUrl .= "#dataarea"; // Add anchor
        //echo $formUrl;

    // GET events to show in filters (needed for JSON used in dinamic filters change)
        $CountriesCategoriesArrays = $events->populateEventsArray($_GET["categoryFilter"],$_GET["countryFilter"],$_GET["cityFilter"]);
        //dump($CountriesCategoriesArrays, "CountriesCategoriesArrays");

    // Get events data and total number to show in the data area trough pagination. 
    // The function collect data from GET.
        $eventsPerPage = 50; 
        $eventsList = $events->getEvents("E", $eventsPerPage);
        $totalNumberEvents = $events->getEvents("C", $eventsPerPage);
        $numberOfPages = ceil($totalNumberEvents / $eventsPerPage);

        //dump($events,"Results Events");
        //dump($totalNumberEvents,"Total number events");
        //dump($eventsPerPage,"Events per page");
        //dump($pageNumber,"Number of pages");


?>


<?php
    // Check if the link is single item link

        // SHOW SINGLE EVENT VIEW - If an event is clicked
            if (isset($_GET["item"])) {
                $itemID = $_GET["item"];        // Item ID
                showSingleItem($itemID);
            }
        // SHOW TEACHER VIEW
            else if(isset($_GET["teacherPage"])) {
                $teacherID = $_GET["teacherPage"];
                showTeacherPage($teacherID);
            }
        // SHOW SEARCH FILTERS
            else {
?>
                <div class="eventFormContainer">
                    <div class="container">
                        <div class="eventFormTitle">
                            <h1><?php echo JText::_('COM_CONTACTEVENTS_CONTACT_IMPRO'); ?></h1>
                            <h2>- Global calendar -</h2>
                            <p class="subtitle">
                                <?php echo JText::_('COM_CONTACTEVENTS_DESC'); ?>
                            </p>
                            <p class="searchHere">
                                <?php echo JText::_('COM_CONTACTEVENTS_SEARCHHERE'); ?>
                            </p>
                        </div>

                        <form id="eventForm" class="eventForm " action="<?php echo $formUrl ?>" method="GET">

                            <input type="hidden" name="option" value="com_contactevents">
                            <input type="hidden" name="view" value="<?php echo isset($_GET['view']) ? $_GET['view'] : ''?>">
                            <input type="hidden" name="Itemid" value="<?php echo isset($_GET['Itemid']) ? $_GET['Itemid'] : ''?>">

                            <!-- Hidden input to track Pagination -->
                            <input type="hidden" name="itemStartLimit" type="text" value="<?php echo htmlspecialchars($_GET['itemStartLimit']); ?>"/>
                            <input type="hidden" name="page" type="text" value="<?php echo htmlspecialchars($_GET['page']); ?>"/>

                            <div class="row whereDetails">
                                <div class="col-md-4">
                                    <p class="what title"><?php echo JText::_('COM_CONTACTEVENTS_WHAT'); ?> <!--<i class="fa fa-info-circle" tooltip='ciao'></i></p>-->
                                    <p>
                                        <!--<select name="categoryFilter" id ="categoryFilter" class="categoryFilter" <?php echo !empty($_GET['categoryFilter']) ? "style=\"display:inline\"":""?>>
                                        </select>-->

                                        <select name="categoryFilter" id ="categoryFilter" class="categoryFilter" <?php echo !empty($_GET['categoryFilter']) ? "style=\"display:inline\"":""?>>
                                            <?php

                                                echo "<option value=''>-- select category --</option>";
                                                foreach ($CountriesCategoriesArrays[0] as $key => $category) {
                                                    $selectedValueCategory = ($_GET['categoryFilter'] == $category->catid) ? "selected='selected'" : "";
                                                    echo "<option value='".$category->catid."' ".$selectedValueCategory.">".$category->cat_name."</option>";
                                                }
                                            ?>
                                        </select>
                                    </p>
                                    
                                    <p class="who title"><?php echo JText::_('COM_CONTACTEVENTS_WHO'); ?></p>
                                    <p><input name="teacher" class="teacher" type="text" placeholder="<?php echo JText::_('COM_CONTACTEVENTS_TEACHER_NAME'); ?>" value="<?php echo isset($_GET['teacher']) ? $_GET['teacher'] : ''?>" ></p>
                                    
                                </div>
                                <div class="col-md-4">
                                    <p class="where title"><?php echo JText::_('COM_CONTACTEVENTS_WHERE'); ?> <i class="fa fa-info-circle" tooltip='Just the countries and cities with available events are shown below'></i> </p>
                                    
                                    <p>
                                        <?php
                                            $displayInline = !empty($_GET['countryFilter']) ? "style=\"display:inline\"":"";
                                            //$disabled = ($_GET['countryFilter']) ? " disabled" : "";
                                            
                                            echo "<select name='countryFilter' id ='countryFilter' class='countryFilter' ".$displayInline.">";
                                                    echo "<option value=''>-- select country --</option>";
                                                    foreach ($CountriesCategoriesArrays[1] as $key => $country) {
                                                        $selectedValueCountry = ($_GET['countryFilter'] == $country->countrycode) ? "selected='selected'" : "";                                            
                                                        echo "<option value='".$country->countrycode."' ".$selectedValueCountry.">".$country->country."</option>";
                                                    }       
                                            echo "</select>";
                                        ?>
                                    </p>

                                    <p>
                                        
                                        <?php
                                            $displayInline = !empty($_GET['cityFilter']) ? "style=\"display:inline\"":"";

                                            echo "<select name='cityFilter' id ='cityFilter' class='cityFilter' ".$displayInline.">";
                                                    echo "<option value=''>-- select city --</option>";

                                                    if ($_GET['cityFilter']||$_GET['countryFilter']){
                                                        foreach ($CountriesCategoriesArrays[2] as $key => $city) {
                                                            $selectedValueCity = ($_GET['cityFilter'] == $city->city) ? "selected='selected'" : "";                                            
                                                            echo "<option value='".$city->city."' ".$selectedValueCity.">".$city->city."</option>";
                                                        }       
                                                    }
                                            echo "</select>";
                                        ?>
                                        
                                    </p>                             
                                    <p class="justbylocation title"><?php echo JText::_('COM_CONTACTEVENTS_WHERE_LOCATION'); ?></p>
                                    <p>
                                        <input name="location" class="location" type="text" placeholder="<?php echo JText::_('COM_CONTACTEVENTS_VENUE_NAME'); ?>" value="<?php echo isset($_GET['location']) ? $_GET['location'] : ''?>" >
                                    </p>
                                </div>

                                <div class="col-md-4">
                                    <p class="when title"><?php echo JText::_('COM_CONTACTEVENTS_WHEN'); ?>  <i class="fa fa-info-circle" tooltip='You can specify just one or both date fields'></i></p>
                                    <p>
                                        <!--
                                        <div class="input-append date" id="datepicker_start_date" data-date="dateValue: Customer.DateOfBirth" data-date-format="dd-mm-yyyy">
                                            <label for="startDate" class="control-label">Start after: </label>
                                            <input id="startDate" name="startDate" size="200" type="text" placeholder="- date start -" value="<?php echo isset($_GET['startDate']) ? $_GET['startDate'] : ''?>" data-bind="value: Customer.DateOfBirth" readonly="readonly" />
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                        </div>  
                                        -->

                                        <div class="input-group input-append date" id="datepicker_start_date" data-date-format="dd-mm-yyyy">
                                            <span class="input-group-addon"><?php echo JText::_('COM_CONTACTEVENTS_DATE_START_AFTER'); ?> <i class="fa fa-calendar"></i></span>
                                            <input name="startDate" class="form-control" type="text" placeholder="<?php echo JText::_('COM_CONTACTEVENTS_SELECT_DATE'); ?>" value="<?php echo isset($_GET['startDate']) ? $_GET['startDate'] : ''?>" readonly="readonly" >
                                        </div>
                                    </p>
                                    <p>
                                        <!--
                                        <div class="input-append date" id="datepicker_end_date" data-date="dateValue: Customer.DateOfBirth" data-date-format="dd-mm-yyyy">
                                            <label for="endDate" class="control-label">End before: </label>
                                            <input id="endDate" name="endDate" size="200" type="text" placeholder="- date end -" value="<?php echo isset($_GET['endDate']) ? $_GET['endDate'] : ''?>" data-bind="value: Customer.DateOfBirth" readonly="readonly" />
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                        </div> -->

                                        <div class="input-group input-append date" id="datepicker_end_date" data-date-format="dd-mm-yyyy">
                                            <span class="input-group-addon"><?php echo JText::_('COM_CONTACTEVENTS_DATE_END_BEFORE'); ?> <i class="fa fa-calendar"></i></span>
                                            <input name="endDate" class="form-control" type="text" placeholder="<?php echo JText::_('COM_CONTACTEVENTS_SELECT_DATE'); ?>" value="<?php echo isset($_GET['endDate']) ? $_GET['endDate'] : ''?>" readonly="readonly" />
                                        </div>
                                    </p>
                                    <!--
                                    <p class="resulttype title">Show result in <i class="fa fa-info-circle" tooltip='Data format'></i></p>
                                    <p>
                                        <input name="howToShowResults" type="checkbox" <?php echo isset($_GET['howToShowResults']) ? 'checked' : ''?> data-toggle="toggle" data-onstyle="primary" data-offstyle="default" data-on="Map" data-off="List">
                                    </p>-->
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-9"></div>
                                <div class="col-md-3 form-actions">
                                    
                                        <button type="submit" class="btn btn-primary searchButton"><?php echo JText::_('COM_CONTACTEVENTS_SEARCH'); ?></button>
                                    
                                        <button type="button" class="btn removeFilterButton"><?php echo JText::_('COM_CONTACTEVENTS_RESET_FILTERS'); ?></button>
                                    
                                    
                                </div>
                            </div>



                            <!--
                                <div class="whereDetails">

                             
                           
                                    
                                    <div class="controls controls-row">
                                        
                                        <div class="input-append date" id="datepicker_start_date" data-date="dateValue: Customer.DateOfBirth" data-date-format="dd-mm-yyyy">
                                            <label for="startDate" class="control-label">Start after: </label>
                                            <input id="startDate" name="startDate" class="span2" size="200" type="text" value="<?php echo isset($_GET['startDate']) ? $_GET['startDate'] : ''?>" data-bind="value: Customer.DateOfBirth" readonly="readonly" />
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                        </div>   
                                        
                                        
                                        <div class="input-append date" id="datepicker_end_date" data-date="dateValue: Customer.DateOfBirth" data-date-format="dd-mm-yyyy">
                                            <label for="endDate" class="control-label">End before: </label>
                                            <input id="endDate" name="endDate" class="span2" size="200" type="text" value="<?php echo isset($_GET['endDate']) ? $_GET['endDate'] : ''?>" data-bind="value: Customer.DateOfBirth" readonly="readonly" />
                                            <span class="add-on"><i class="icon-calendar"></i></span>
                                        </div>  

                                    </div>
                                    
                                </div>
                                
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary searchButton">Search</button>
                                    <button type="button" class="btn removeFilterButton">Remove filters</button>
                                </div>
                            -->
                          
                        </form>
                    </div>
                </div>
                
                <div class="dataArea">
                    <a id="dataarea"></a>
                    <div class="container">
                    <?php
                        // Create the kml file for the Google Map
                            //$venuesEvents = createVenuesEventsArray($eventsList); 
                            //writeKmlFile($venuesEvents);                 

                        // If [search] button is pressed show events
                            if(isset($_GET['itemStartLimit'])) {
                                $events->printResults($eventsList[0], $numberOfPages, $eventsPerPage, $totalNumberEvents);
                            }

                    ?>
                    </div>
                </div>

                <?php } ?>