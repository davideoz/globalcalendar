<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');


// Import here all the translations strings needed in JS files (https://joomla.stackexchange.com/questions/14855/how-to-manage-language-variables-inside-javascript)
    JText::script('COM_CONTACTEVENTS_ALL_KIND_EVENTS');
    JText::script('COM_CONTACTEVENTS_SELECT_CONTINENT');
    JText::script('COM_CONTACTEVENTS_SELECT_COUNTRY');
    JText::script('COM_CONTACTEVENTS_SELECT_CITY');
 
/**
 * HTML View class for the HelloWorld Component
 */
class ContacteventsViewContactevents extends JViewLegacy
{
        // Overwriting JView display method
        function display($tpl = null) 
        {

                // Assign data to the view
                $this->msg = $this->get('Msg');
                $this->allEvents = $this->get('AllEvents');
                $this->eventCategories = $this->get('EventCategories');
 
                // Check for errors.
                if (count($errors = $this->get('Errors'))) 
                {
                        JLog::add(implode('<br />', $errors), JLog::WARNING, 'jerror');
                        return false;
                }


                $document = &JFactory::getDocument();

              // JS
                        // Include bootstap and jquery before all other contents (Joomla 3 style)
                        //JHtml::_('bootstrap.framework');



//$document->addScript(JURI::root(true) . '/templates/movementmeetslife_prod/js/jquery-ui.min.js', false, true);



                        $document->addScript(JURI::root(true) . '/components/com_contactevents/assets/js/bootstrap-datepicker.min.js', false, true);
                        $document->addScript(JURI::root(true) . '/components/com_contactevents/assets/js/contactevents.js', false, true);
                        $document->addScript(JURI::root(true) . '/components/com_contactevents/assets/js/select2.js', false, true);
                        $document->addScript(JURI::root(true) . '/components/com_contactevents/assets/js/jquery.bgswitcher.js', false, true);
                        $document->addScript(JURI::root(true) . '/components/com_contactevents/assets/js/interactive_map.js', false, true); 
            
                        // I need this to call language constants in javasripts with Joomla.JText. functions
                            $document->addScript(JURI::root(true) . '/media/system/js/core.js', false, true);

                // CSS
                        $document->addStyleSheet(JURI::root(true) . '/components/com_contactevents/assets/css/select2.min.css');
                        $document->addStyleSheet(JURI::root(true) . '/components/com_contactevents/assets/css/datepicker.css');
                        $document->addStyleSheet(JURI::root(true) . '/components/com_contactevents/assets/css/contactevents.min.css');

                // Display the view
                parent::display($tpl);
        }
}