


// Get parameters from url
	function GetURLParameter(sParam){
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');

		for (var i = 0; i < sURLVariables.length; i++){
				var sParameterName = sURLVariables[i].split('=');
				if (sParameterName[0] == sParam){
					var parameter = sParameterName[1];

					// replace + with spaces and restore special characters
						parameter = parameter.replace("+", " ");
						parameter = unescape(parameter);
					
					return parameter;
				}
			}
	}


// Remove special chars and submit form
	function submitForm(resetPaginator) {

		// Remove placeholder value from input boxes (ie7-ie8 fix)	
			jQuery(':text.hasPlaceholder').each(function( index ) {
				var value = jQuery(this).val();
				if ((value=="Search for people units or legal entities involved")||(value=="nn")){
					jQuery(this).val('');
				}
			});
			
		// SUBMIT
			jQuery( ".eventForm" ).submit();
		
	}

// Detect mobile devices - https://stackoverflow.com/questions/11381673/detecting-a-mobile-browser#11381730
	window.mobilecheck = function() {
		var check = false;
		(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
		return check;
	};

	window.mobileAndTabletcheck = function() {
		var check = false;
		(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
		return check;
	};



// JQUERY ACTIONS
jQuery(function () {


	// click - RESET filters
		jQuery( ".removeFilterButton" ).on( "click", function() {
			document.location.href="/";
		});

	// click - SUBMIT button (no input to avoid black border in ie7)
		/*jQuery( ".searchButton" ).on( "click", function() {
			submitForm(1);
		});*/

	// enter - location input box - SUBMIT form
		jQuery('input.location').keypress(function (e) {
			if (e.which == 13) 
				submitForm(1);
		});

	// enter - teacher input box - SUBMIT form
		jQuery('input.teacher').keypress(function (e) {
			if (e.which == 13) 
				submitForm(1);
		});

	// Update filters on change CATEGORY
		jQuery('#categoryFilter').change(function () {
			var filter_changed = "category";
			var cat_id = jQuery(this).val();

			jQuery.ajax({
	            type:'GET',
	            url:'/components/com_contactevents/helpers/ajaxUpdateSelects.php',
	            dataType: "json",
	            data:{
	            	filter_changed:filter_changed,
	            	category_id:cat_id
	            },
	            success:function(data){
	            	//console.log(data);

	            	// Update country filter

	            		// Save the value of the country filter (if already set)
		            		var countryCodeVal = jQuery("#countryFilter").val();

		            	// Empty the country filter
		            		jQuery('#countryFilter').empty();

		            
			            // Fill the country filter
			            	jQuery('#countryFilter').append('<option value="">' + "-- select country --" + '</option>');
			            	for (i in data['countries']){
			            		//console.log (data[0][i].countrycode);
			            		//console.log (data[0][i].country);
			            		jQuery('#countryFilter').append('<option value="' + data['countries'][i].countrycode + '">' + data['countries'][i].country + '</option>');
			            	}

			            // Set again the previous value
			            	if (countryCodeVal != ""){
			            		jQuery('#countryFilter').val(countryCodeVal);
			            	}
	            }
        	});

		});	

	// Update filters on change COUNTRY 
		jQuery('#countryFilter').change(function () {
			var filter_changed = "country";
			var country_code = jQuery(this).val();

			// Save the value of the other filters  (if already set)
	            var category_id = jQuery('#categoryFilter').val();

			jQuery.ajax({
	            type:'GET',
	            url:'/components/com_contactevents/helpers/ajaxUpdateSelects.php',
	            dataType: "json",
	            data:{
	            	filter_changed:filter_changed,
	            	country_id:country_code,
	            	category_id:category_id
	            },
	            success:function(data){
	            	//console.log(data);

	            	// Update category filter 

		            	// Empty the category filter 
			            	jQuery('#categoryFilter').empty();
			         
		            	// Fill the categories filter
			            	jQuery('#categoryFilter').append('<option value="">' + "-- select category --" + '</option>');
			            	for (i in data['categories']){
			            		//console.log (data[0][i].countrycode);
			            		//console.log (data[0][i].country);
			            		jQuery('#categoryFilter').append('<option value="' + data['categories'][i].catid + '">' + data['categories'][i].cat_name + '</option>');
			            	}

			            // Set again the previous value
			            	if (category_id != ""){
			            		jQuery('#categoryFilter').val(category_id);
			            	}

		            // Update the city filter
		            	jQuery('#cityFilter').empty();
		            	jQuery('#cityFilter').append('<option value="">' + "-- select city --" + '</option>');
		            	
		            	for (i in data['cities']){
		            		//console.log (data[0][i].countrycode);
		            		//console.log (data[0][i].country);
		            		jQuery('#cityFilter').append('<option value="' + data['cities'][i].city + '">' + data['cities'][i].city + '</option>');
		            	}
	            }
        	});
		});	


	// Update filters on change CITY 
		jQuery('#cityFilter').change(function () {
			var filter_changed = "city";
			var city = jQuery(this).val();
		
			// Save the value of the other filters  (if already set)
	            var category_id = jQuery('#categoryFilter').val();
	            var country_code = jQuery("#countryFilter").val();

			jQuery.ajax({
	            type:'GET',
	            url:'/components/com_contactevents/helpers/ajaxUpdateSelects.php',
	            dataType: "json",
	            data:{
	            	filter_changed:filter_changed,
	            	city_id:city,
	            	category_id: category_id,
	            	country_id: country_code
	            },
	            success:function(data){
	            	console.log(data);

	            	// Category filter 
		            	
	            		// Save the value of the category filter (if already set)
	            			var categoryVal = jQuery('#categoryFilter').val();

		            	// Update the category filter 
			            	jQuery('#categoryFilter').empty();
			            	jQuery('#categoryFilter').append('<option value="">' + "-- select category --" + '</option>');
			            	for (i in data['categories']){
			            		//console.log (data[0][i].countrycode);
			            		//console.log (data[0][i].country);
			            		jQuery('#categoryFilter').append('<option value="' + data['categories'][i].catid + '">' + data['categories'][i].cat_name + '</option>');
			            	}

			            // Set again the previous value
			            	if (categoryVal != ""){
			            		jQuery('#categoryFilter').val(categoryVal);
			            	}

            		// Country filter

            			// Save the value of the country filter
	            			var countryVal = jQuery('#countryFilter').val();
	            			var countryText = jQuery('#countryFilter option:selected').text();

	            		// Update the country filter (just with the country of the selected city)
			            	jQuery('#countryFilter').empty();
			            	jQuery('#countryFilter').append('<option value="">' + "-- select country --" + '</option>');
			            	jQuery('#countryFilter').append('<option value="' + countryVal + '">' + countryText + '</option>');
			            	
			            // Set again the previous value
			            	jQuery('#countryFilter').val(countryVal);  
	            }
        	});
		});		
});




jQuery(document).ready(function(){
	
	jQuery(".eventFormContainer").parent().parent().parent().parent(".container").wrap( "<div class='contactEvents jumbotron'></div>" );
	jQuery(".contactEvents").append('<div class="bg-overlay"></div>')	


	//alert(Joomla.JText._('COM_CONTACTEVENTS_ALL_KIND_EVENTS'));
	

	// GET checked parameters from url
		var continentFilter = GetURLParameter('continentFilter');		
		var countryFilter = GetURLParameter('countryFilter');
		var cityFilter = GetURLParameter('cityFilter');
		var categoryFilter = GetURLParameter('categoryFilter');
		var location = GetURLParameter('location');
		var teacher = GetURLParameter('teacher');

	// Prettify SELECTS
		jQuery('.whereDetails select.continentFilter').select2({
		  //placeholder: "Select a continent",
		  placeholder: Joomla.JText._('COM_CONTACTEVENTS_SELECT_CONTINENT'),

		  allowClear: true
		});
		jQuery('.whereDetails select.countryFilter').select2({
		  //placeholder: "Select a country",
		  placeholder: Joomla.JText._('COM_CONTACTEVENTS_SELECT_COUNTRY'),

		  allowClear: true,
		  language: {
		    noResults: function (params) {
		      return "Select continent before";
		    }
		  }
		});
		jQuery('.whereDetails select.cityFilter').select2({
		  //placeholder: "Select a city",
		  placeholder: Joomla.JText._('COM_CONTACTEVENTS_SELECT_CITY'),

		  allowClear: true,
		  language: {
		    noResults: function (params) {
		      return "Select country before";
		    }
		  }
		});

		
		jQuery('.whereDetails select.categoryFilter').select2({
		  //placeholder: "All kind of events",
		  placeholder: Joomla.JText._('COM_CONTACTEVENTS_ALL_KIND_EVENTS'),  // !!! la stringa da tradurre va importata anche in view.html-php

		  allowClear: true,
		  language: {
		    noResults: function (params) {
		      return "---";
		    }
		  }
		});

		// FIX needed when resize screen (select2 don't adapt to the new size)
			window.onresize = function() {
				jQuery(".select2").css("width", "100%");
			}


	// DATEPICKER
		jQuery('#datepicker_start_date input').datepicker();
		jQuery('#datepicker_end_date input').datepicker();

	// Change background
	jQuery(function() {

		var element = jQuery('.contactEvents');
		
		// Create background array
			var backgrounds = new Array();
			var base_url = window.location.origin;

			// Image path is different for desktop and mobile devices (for mobile vertical and smaller images are provided)
				if ((mobilecheck())&&( window.orientation == 0)){
					// Mobile and vertical - 305x550
						var images = ["image_1.jpg", "image_2.jpg", "image_3.jpg"];
						var image_path = "/components/com_contactevents/assets/images/vertical/";
				}
				else{
					// Desktop - 1100x733 65% quality
						var images = ["image_1.jpg", "image_2.jpg", "image_3.jpg", "image_4.jpg", "image_5.jpg", "image_6.jpg", "image_7.jpg", "image_8.jpg"];
						var image_path = "/components/com_contactevents/assets/images/";
				}


			for(var i=0; i<images.length; i++){
		    	backgrounds[i] = 'url('+base_url + image_path + images[i]+')';
		    }

		// Function to change background
			var current = 0;
			function nextBackground() {
				element.css(
					'background-image',
					backgrounds[current = ++current % backgrounds.length]
				);

				setTimeout(nextBackground, 10000);
			}

			setTimeout(nextBackground, 10000);

			element.css('background-image', backgrounds[0]);

	});

		
});