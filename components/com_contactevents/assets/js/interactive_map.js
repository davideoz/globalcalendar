function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          //center: {lat: 43.45866013, lng: 11.89655972}
        });

        var ctaLayer = new google.maps.KmlLayer({
          //url: 'https://contactimprov.it/uploads/mappaInterattiva/mappaInterattiva.kml',
			url: 'https://contactimprov.it/uploads/mappaInterattiva/interactiveMap_2469689444.kml',
          map: map
        });
      }

      