

<?php

echo "eeet";

	// Initialize joomla access the DB
		//include '../../joomlaInitialize.php';
		//$databaseNameAndPrefix = joomlaInitialize();
		//$dbName = $databaseNameAndPrefix[0];
	    //$dbPrefix = $databaseNameAndPrefix[1];
	
	// Create cookie (if not exist yet) - I will call the kms filename for this user with this cookie value
	    //include '../../cookies.php';
		//$cookie = createRandomCookie("globalCalendarWebsite");

	   
	//$events = retrieveEvents($dbName,$dbPrefix);
	
	//$venuesEventsArray = createVenuesEventsArray($events);
	//writeKmlFile($venuesEventsArray, $cookie);



  /**
     * Retrieve all the events from the DB
     *
     * @access     public 
     * @return     none       
     */

	function retrieveEvents($dbName,$dbPrefix){
		// Get a db connection and create new query object
			$db = JFactory::getDBO(); 
			$query = $db->getQuery(true);
	
		// QUERY ESTRAZIONE da tabella 'markers'
		
		$query = "
			SELECT 
				a.summary, 
				a.date_start, 
				a.date_end, 
				a.venue, 
				a.street, 
				a.city, 
				a.country,
				a.geo_latitude, 
				a.geo_longitude, 
				a.url, 
				a.cat_name, 
				b.rp_id
			FROM ".$dbName.".".$dbPrefix."distilled_homepage_events AS a
		
			LEFT JOIN ".$dbName.".".$dbPrefix."jevents_repetition
			AS b
				ON a.ev_id = b.eventid	
			
			ORDER BY a.date_start
		";


		$db->setQuery($query);
		$results = $db->loadObjectList();
		

		/*foreach ($results as $key => $result) {
			echo $result->summary;
			echo "<br>";
			//var_dump($result);
		}*/

		return $results;
	}

/*

	function createVenuesEventsArray($events){
		$venues = array();
		$i = 0;
		while ( $row = mysql_fetch_array ( $events, MYSQL_ASSOC ) ) {
			$venues [$i]['name'] = $row ['name'];			//nome venue
			$venues [$i]['address'] = $row ['address'];
			$venues [$i]['lat'] = $row ['lat'];
			$venues [$i]['lng'] = $row ['lng'];
			$venues [$i]['locimage'] = $row ['locimage'];
			$venues [$i]['url'] = $row ['url'];
			$venues [$i]['id'] = $row ['loc_id'];
			$venues [$i]['title'] = $row ['summary'];			//nome evento
			$venues [$i]['catsid'] = $row ['catid'];
			$venues [$i]['dates'] = date('d-n-Y',($row ['dtstart']));
			$venues [$i]['enddates'] = date('d-n-Y',($row ['dtend']));
			$venues [$i]['published'] = $row ['state'];
			$venues [$i]['rp_id'] = $row ['rp_id'];
			$i++;
		}	
	}
*/

/*
	function writeKmlFile($venuesEventsArray, $cookie){
		setlocale(LC_TIME, 'ita', 'it_IT');

		//Open KML file for this user
			$kmlFile=fopen("../interactiveMap".$cookie.".kml","w");

		//Add HEADING to KML file
			$kmlContent="<?xml version='1.0' encoding='UTF-8'?>
				<kml xmlns='http://www.opengis.net/kml/2.2'>
					<Document>
			";
			fwrite($kmlFile,$kmlContent);

		//Add ICONS to KML file
			$kmlContent = "
				<Style id='iconJAM'>
			      <IconStyle>
			         <scale>0.4</scale>
			         <Icon>
			            <href>http://maps.google.com/mapfiles/ms/micons/yellow-dot.png</href>
			         </Icon>
			      </IconStyle>
				  <LabelStyle>
			         <scale>0.4</scale>
			      </LabelStyle>
			   </Style>   

			   <Style id='iconCORSO'>
			      <IconStyle>
			         <scale>0.4</scale>
			         <Icon>
			            <href>http://maps.google.com/mapfiles/ms/micons/orange-dot.png</href>
			         </Icon>
			      </IconStyle>
				  <LabelStyle>
			         <scale>0.4</scale>
			      </LabelStyle>
			   </Style>
			   
			   <Style id='iconWORKSHOP-FEST'>
			      <IconStyle>
			         <scale>0.4</scale>
			         <Icon>
			            <href>http://maps.google.com/mapfiles/ms/micons/green-dot.png</href>
			         </Icon>
			      </IconStyle>
				  <LabelStyle>
			         <scale>0.4</scale>
			      </LabelStyle>
			   </Style>
			   
			   <Style id='iconPER'>
			      <IconStyle>
			         <scale>0.4</scale>
			         <Icon>
			            <href>http://maps.google.com/mapfiles/ms/micons/blue-dot.png</href>
			         </Icon>
			      </IconStyle>
				  <LabelStyle>
			         <scale>0.4</scale>
			      </LabelStyle>
			   </Style>	   
			   
			   <Style id='iconMULTIPLE'>
			      <IconStyle>
			         <scale>0.4</scale>
			         <Icon>
			            <href>http://maps.google.com/mapfiles/ms/micons/purple-dot.png</href>
			         </Icon>
			      </IconStyle>
				  <LabelStyle>
			         <scale>0.4</scale>
			      </LabelStyle>
			   </Style>
			   
			   ";
			fwrite($kmlFile,$kmlContent);

		//Add EVENTS to KML file
			foreach ($venuesEventsArray as $key => $event) {
				$kmlContent="<Placemark>
				  <name>".$venues [$j]['name']."</name>
				  <address>".$venues [$j]['address']."</address>
				  <styleUrl>".$stilePlacemark."</styleUrl>
				  <Point>
					 <coordinates>".$venues [$j]['lng'].",".$venues [$j]['lat']."</coordinates>
				  </Point>
				  <description>
			        	<![CDATA[
			        	<div style='position:relative;text-align: left; width:350px; height:500px;'>
			        		<img style='postion:absolute; top:0px; left:0px; height:100px; width;100px;' src='http://www.contactimprov.it/images/jevents/jevlocations/thumbnails/thumb_".$venues [$j]['locimage']."'/>
			        		<br>
			        		
			        		<div style='position:absolute; top:0px; left:120px; width:200px;padding-top:15px;'>
								<b>Attivita in corso:</b><br>
			        			".$multiple_title."
								<br/>
								<b>Sito Web:</b> <a href='".$venues [$j]['url']."'>".$venues [$j]['url']."</a>
							</div>
			        		<br><br>
			        		
			        	</div>
			        	]]>
			      </description>
			   </Placemark>
			   ";
				fwrite($kmlFile,$kmlContent);
			}



		//Add FOOTER to KML file
			$stringa=" 			
				</Document>
			</kml>";
			fwrite($kmlFile,$kmlContent);	
			
		//Close KML file
			fclose($kmlFile);
	}



	*/
/*
	
	
	// CONNESSIONE DB
	
	//APERTURA FILE KML
	$scrivi_file=fopen("../mappaInterattiva.kml","w");
	
	//INTESTAZIONE - KML
	$stringa="<?xml version='1.0' encoding='UTF-8'?>
			<kml xmlns='http://www.opengis.net/kml/2.2'>
				<Document>
	";
	fwrite($scrivi_file,$stringa);
	
	
	//TEMPLATE
	$stringa="
		<Style id='iconJAM'>
	      <IconStyle>
	         <scale>0.4</scale>
	         <Icon>
	            <href>http://maps.google.com/mapfiles/ms/micons/yellow-dot.png</href>
	         </Icon>
	      </IconStyle>
		  <LabelStyle>
	         <scale>0.4</scale>
	      </LabelStyle>
	   </Style>   

	   <Style id='iconCORSO'>
	      <IconStyle>
	         <scale>0.4</scale>
	         <Icon>
	            <href>http://maps.google.com/mapfiles/ms/micons/orange-dot.png</href>
	         </Icon>
	      </IconStyle>
		  <LabelStyle>
	         <scale>0.4</scale>
	      </LabelStyle>
	   </Style>
	   
	   <Style id='iconWORKSHOP-FEST'>
	      <IconStyle>
	         <scale>0.4</scale>
	         <Icon>
	            <href>http://maps.google.com/mapfiles/ms/micons/green-dot.png</href>
	         </Icon>
	      </IconStyle>
		  <LabelStyle>
	         <scale>0.4</scale>
	      </LabelStyle>
	   </Style>
	   
	   <Style id='iconPER'>
	      <IconStyle>
	         <scale>0.4</scale>
	         <Icon>
	            <href>http://maps.google.com/mapfiles/ms/micons/blue-dot.png</href>
	         </Icon>
	      </IconStyle>
		  <LabelStyle>
	         <scale>0.4</scale>
	      </LabelStyle>
	   </Style>	   
	   
	   <Style id='iconMULTIPLE'>
	      <IconStyle>
	         <scale>0.4</scale>
	         <Icon>
	            <href>http://maps.google.com/mapfiles/ms/micons/purple-dot.png</href>
	         </Icon>
	      </IconStyle>
		  <LabelStyle>
	         <scale>0.4</scale>
	      </LabelStyle>
	   </Style>
	   
	   ";
	fwrite($scrivi_file,$stringa);
	
	// TIMESTAMP ORA ATTUALE
	$ora=time();
	
	//ELENCO PLACEMARK

		// QUERY ESTRAZIONE da tabella 'markers'
		
		$query = "
		SELECT a.summary, a.date_start, a.date_end, b.loc_id, b.name, b.address, b.lat, b.lng, b.type, b.locimage, b.url, c.ev_id, c.state, c.catid, f.rp_id
		FROM ue0swcm3_contactimprov.vf2mq_jevents_vevdetail
		AS a
		LEFT JOIN ue0swcm3_contactimprov.markers 
		AS b
			ON  a.location = b.loc_id
		LEFT JOIN ue0swcm3_contactimprov.vf2mq_jevents_vevent
		AS c
			ON a.evdet_id = c.ev_id
		LEFT JOIN ue0swcm3_contactimprov.vf2mq_jevents_repetition
		AS f
			ON c.ev_id = f.eventid	
			
		WHERE ($ora<a.date_end)
		AND (c.state=1)
		
		ORDER BY a.date_start
		";		
		

		$risultato = mysql_query ( $query ) or die ( 'Errore, query fallita' );
	
		//RIEMPIMENTO ARRAY (tutti i posti con eventi disponibili - una riga x evento)
		$venues = array();
		$i = 0;
		while ( $row = mysql_fetch_array ( $risultato, MYSQL_ASSOC ) ) {
			$venues [$i]['name'] = $row ['name'];			//nome venue
			$venues [$i]['address'] = $row ['address'];
			$venues [$i]['lat'] = $row ['lat'];
			$venues [$i]['lng'] = $row ['lng'];
			$venues [$i]['locimage'] = $row ['locimage'];
			$venues [$i]['url'] = $row ['url'];
			$venues [$i]['id'] = $row ['loc_id'];
			$venues [$i]['title'] = $row ['summary'];			//nome evento
			//$venues [$i]['alias'] = $row ['alias'];
			$venues [$i]['catsid'] = $row ['catid'];
			//$venues [$i]['dates'] = $row ['date_start'];
			//$venues [$i]['enddates'] = $row ['date_end'];
			$venues [$i]['dates'] = date('d-n-Y',($row ['date_start']));
			$venues [$i]['enddates'] = date('d-n-Y',($row ['date_end']));
			$venues [$i]['published'] = $row ['state'];
			$venues [$i]['rp_id'] = $row ['rp_id'];
			//echo date('d-n-Y',($row ['date_start']));
			$i++;
		}	
		
		// SCRITTURA PLACEMARK NEL FILE (con la descrizione multievento)
		for ($j=0;$j<$i;$j++){	
			
			//GESTIONE EVENTI MULTIPLI IN UN CENTRO
			$multiple_title ="";	
			$conta_multi=0;
			
			do{	
				a:			//goto compatibile dalla versione php 5.3
				
				//ICONA SINGOLA
				switch ($venues [$j]['catsid']) {
				    case 15:
				        $imm="cor.gif";
				        $stilePlacemark="#iconCORSO";
				        break;
				    case 16:
				        $imm="jam.gif";
				        $stilePlacemark="#iconJAM";
				        break;
				    case 14:
				        $imm="wor.gif";
				        $stilePlacemark="#iconWORKSHOP-FEST";
				        break;
					case 17:
				        $imm="per.gif";
				        $stilePlacemark="#iconPER";
				        break;
				}
				
				//ICONA MULTIPLA
				if ($conta_multi>0){
					$imm="wor.gif";
				    $stilePlacemark="#iconMULTIPLE";
				}
				$conta_multi++;
				
				
				//data
				if (strcmp($venues [$j]['dates'],$venues [$j+1]['enddates'])==0){
					$datadata = $venues [$j]['dates'];
				}
				else{
					$datadata = $venues [$j]['dates']." - ".$datadata = $venues [$j]['enddates'];
				}
				//immagine
				$immagine="<img class='event_ico' style='margin-left:10px;' src='images/stories/icons/".$imm."'/>";
				
				
				switch ($venues [$j]['catsid']) {
					case 14:
						$categoria= 191;			//workshop
						break;
					case 15:
						$categoria= 188;			//corsi
						break;
					case 16:
						$categoria= 192;			//jam
						break;
					case 17:
						$categoria= 195;			//performance
						break;	
				}
				
				
				//link
				//$link="./index.php?view=details&id=".$venues [$j]['id']."%3A".$venues [$j]['id'].alias."&option=com_eventlist&Itemid=74";
				$link="./index.php?option=com_jevents&task=icalrepeat.detail&evid=".$venues [$j]['rp_id']."&Itemid=".$categoria;
				
				
				//scrittura riga evento
				if (($venues [$j]['published'])==1){
					$multiple_title = $multiple_title.$immagine."<a href='".$link."'>".$venues [$j]['title']."</a> ".$datadata."<br>";
				}
				//SE MULTIEVENTO (comparazione=0)
				if (strcmp($venues [$j]['name'],$venues [$j+1]['name'])==0){ 
					$j++;
					goto a;		
				}
				$a=$venues [$j]['name'];
				$b=$venues [$j+1]['name'];
				$test=strcmp($venues [$j]['name'],$venues [$j+1]['name']);
			} while (strcmp($venues [$j]['name'],$venues [$j+1]['name'])==0);
			
			//SCRITTURA SU STRINGA DEL PLACEMARK
			$stringa="<Placemark>
				  <name>".$venues [$j]['name']."</name>
				  <address>".$venues [$j]['address']."</address>
				  <styleUrl>".$stilePlacemark."</styleUrl>
				  <Point>
					 <coordinates>".$venues [$j]['lng'].",".$venues [$j]['lat']."</coordinates>
				  </Point>
				  <description>
			        	<![CDATA[
			        	<div style='position:relative;text-align: left; width:350px; height:500px;'>
			        		<img style='postion:absolute; top:0px; left:0px; height:100px; width;100px;' src='http://www.contactimprov.it/images/jevents/jevlocations/thumbnails/thumb_".$venues [$j]['locimage']."'/>
			        		<br>
			        		
			        		<div style='position:absolute; top:0px; left:120px; width:200px;padding-top:15px;'>
								<b>Attivita in corso:</b><br>
			        			".$multiple_title."
								<br/>
								<b>Sito Web:</b> <a href='".$venues [$j]['url']."'>".$venues [$j]['url']."</a>
							</div>
			        		<br><br>
			        		
			        	</div>
			        	]]>
			      </description>
			   </Placemark>
			   ";
			fwrite($scrivi_file,$stringa);
		}
	
	//CALCE - KML
	$stringa=" 			
				</Document>
		</kml>
	";
	fwrite($scrivi_file,$stringa);	
	
	
	//CHIUSURA FILE KML
	fclose($scrivi_file);

	//CHIUSURA DB
	mysql_free_result ( $risultato );
	mysql_close ( $con );
*/
?>


