<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

defined('_JEXEC') or die('Restricted access');

if(!defined('DS')) { define('DS',DIRECTORY_SEPARATOR); }

// Require the base controller
require_once JPATH_COMPONENT.DS.'controller.php';
require_once( JPATH_COMPONENT.DS.'helpers'.DS.'route.php' );

// Get component params
jimport( 'joomla.application.component.helper' );
$params = JComponentHelper::getParams('com_faqbookpro');

$document = JFactory::getDocument();

// Fix relative links
if ($params->get('fix_relative', 0)) 
{
	$document->base = juri::root();
}

// Add stylesheets
$document->addStyleSheet(JURI::base().'components/com_faqbookpro/assets/css/faqbook.css?v=3.7.0');
if ($params->get('load_fontawesome', 1)) 
{
	$document->addStyleSheet('https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css');
}

// Load jQuery
if ($params->get('load_jquery', 1))
{
	JHtml::_('jquery.framework');
}

// Load Bootstrap
if ($params->get('load_bootstrap', 0))
{
	JHtml::_('bootstrap.framework');
	$document->addStyleSheet(JURI::base().'media/jui/css/bootstrap.min.css');
}

// Add scripts
$app = JFactory::getApplication();	
$view = $app->input->get('view');
if ($view == 'section' || $view == 'topic' || $view == 'question')
{
	$document->addScript(JURI::base().'components/com_faqbookpro/assets/js/faqbook.js?v=3.6.9');
}

// Add controller
$controller	= JControllerLegacy::getInstance('FaqBookPro');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
