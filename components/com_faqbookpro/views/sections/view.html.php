<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

defined('_JEXEC') or die('Restricted access');

use Joomla\Registry\Registry;

jimport('joomla.application.component.view');
 
class FaqBookProViewSections extends JViewLegacy
{
  	function display($tpl = null) 
  	{
		$document = JFactory::getDocument();
	  	$app = JFactory::getApplication();	
		$this->model = $this->getModel();
		$sectionModel = JModelLegacy::getInstance('Section', 'FaqBookProModel'); 
		$activeMenu = $app->getMenu()->getActive();	
		$this->home_title = $activeMenu->title;
		$this->home_itemid = $activeMenu->id;
	
		// Get Params & Attribs
		$this->utilities = $this->model->utilities;
		$this->params = $this->utilities->getParams('com_faqbookpro');
		
		// Get Sections				
		$specific_sections = $this->params->get('fbp_sections', '');
		$this->sections = $this->model->getSections($specific_sections);
		
		// Sections params
		$this->sections_topnav = $this->params->get('sections_topnav', true);
		$this->sections_page_title = $this->params->get('sections_page_title', false);	
		$this->sections_page_description = $this->params->get('sections_page_description', false);	
		$this->sections_cols = $this->params->get('sections_cols', 3);	
		$this->sections_title = $this->params->get('sections_title', 1);	
		$this->sections_description = $this->params->get('sections_description', 1);	
		$this->sections_topics = $this->params->get('sections_topics', false);	
		
		// Get Top Navigation
		if ($this->sections_topnav)
		{
			$this->topnavigation = $this->model->navigation->getTopNav(false);
		}
		
		// Extra Section data
		foreach ($this->sections as $key => $section)
		{
			$section->topics = $sectionModel->getSectionTopics($section->id);
			
			foreach ($section->topics as $topic)
			{
				$topicParams = json_decode($topic->params, false);
				$topic->icon_class = false;
				if (isset($topicParams->topic_icon_class) && $topicParams->topic_icon_class)
				{
					$topic->icon_class = $topicParams->topic_icon_class;
				}
			}
		}
		
		// Set metadata
		$document->setTitle($this->params->get('page_title'));
		
		if ($this->params->get('menu-meta_description'))
		{
			$document->setDescription($this->params->get('menu-meta_description'));
		}
		
		if ($this->params->get('menu-meta_keywords'))
		{
			$document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}
		
		if ($this->params->get('robots'))
		{
			$document->setMetadata('robots', $this->params->get('robots'));
		}
		
		if (!is_object($this->params->get('metadata')))
		{
			$metadata = new Registry($this->params->get('metadata'));
		}
		
		$mdata = $metadata->toArray();

		foreach ($mdata as $k => $v)
		{
			if ($v)
			{
				$document->setMetadata($k, $v);
			}
		}
		
		// Menu page display options
		if ($this->params->get('page_heading'))
		{
		  	$this->params->set('page_title', $this->params->get('page_heading'));
		}
		$this->params->set('show_page_title', $this->params->get('show_page_heading'));
																									
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
			  
		// Display the view
		parent::display($tpl);					
  	}
}
