<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

defined('_JEXEC') or die('Restricted access');
?>

<div id="fbpExtended" class="fbpSectionExtended">
  
  	<?php if (isset($this->topnavigation) && $this->topnavigation) {
		echo $this->topnavigation; 
	} ?>
	
	<div id="fbpcontent" class="fbpContent_core noleftnav">
		<div class="fbpContent_root">
			<?php echo $this->loadTemplate('content'); ?>
		</div>
	</div>
	
</div>

<div class="clearfix"> </div>
