<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

defined('_JEXEC') or die ;

use Joomla\String\StringHelper;

jimport('joomla.filesystem.folder');

class FaqBookProLibUtilities
{
	public static function getParams($option)
	{
		$application = JFactory::getApplication();
		if ($application->isSite())
		{
		  $params = $application->getParams($option);
		}
		else
		{
		  $params = JComponentHelper::getParams($option);
		}
		
		return $params;
	}
			
	public static function getWordLimit($text, $limit, $end_char = '&#8230;')
	{
	   	if(StringHelper::trim($text) == '')
			return $text;

		// always strip tags for text
		$text = strip_tags($text);

		$find = array(
			"/\r|\n/u",
			"/\t/u",
			"/\s\s+/u"
		);
		$replace = array(
			" ",
			" ",
			" "
		);
		$text = preg_replace($find, $replace, $text);

		preg_match('/\s*(?:\S*\s*){'.(int)$limit.'}/u', $text, $matches);
		if (StringHelper::strlen($matches[0]) == StringHelper::strlen($text))
			$end_char = '';
			
		return StringHelper::rtrim($matches[0]).$end_char;		
	}
						
	public static function getActions($component = '', $section = '', $id = 0)
	{
		$user    = JFactory::getUser();
		$result    = new JObject;

		$path = JPATH_ADMINISTRATOR . '/components/' . $component . '/access.xml';
		
		if ($section && $id)
		{
			$assetName = $component . '.' . $section . '.' . (int) $id;
		}
		else
		{
			$assetName = $component;
		}

		$actions = JAccess::getActionsFromFile($path, "/access/section[@name='component']/");
		
		foreach ($actions as $action)
		{
			$result->set($action->name, $user->authorise($action->name, $assetName));
		}
		
		return $result;
	}
	
	public static function getAuthorisedTopics($action)
	{
		// Brute force method: get all published topic rows for the component and check each one
		// TODO: Modify the way permissions are stored in the db to allow for faster implementation and better scaling
		$db = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select('t.id AS id, a.name AS asset_name')
			->from('#__minitek_faqbook_topics AS t')
			->join('INNER', '#__assets AS a ON t.asset_id = a.id')
			->where('t.published = 1');
		$db->setQuery($query);
		$allTopics = $db->loadObjectList('id');
		$allowedTopics = array();

		foreach ($allTopics as $topic)
		{
			if (JFactory::getUser()->authorise($action, $topic->asset_name))
			{
				$allowedTopics[] = (int) $topic->id;
			}
		}

		return $allowedTopics;
	}
	
	public static function checkMenuItem($sectionid)
	{
		$app = JFactory::getApplication();
		
		if (!self::getSectionMenuItem($sectionid))
		{
			$app->enqueueMessage(\JText::_('COM_FAQBOOKPRO_ERROR_SECTION_MENU_ITEM'), 'error');
			$app->setHeader('status', 403, true);	
		}
	}
	
	public static function getSectionMenuItem($sectionid)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select('COUNT(*)')
			->from('#__menu')
			->where('published = 1')
			->where('link='.$db->quote('index.php?option=com_faqbookpro&view=section&id='.$sectionid));
		$db->setQuery($query);
		$count = $db->loadResult();

		return $count;
	}
}
