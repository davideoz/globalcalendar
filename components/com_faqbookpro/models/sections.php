<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

defined('_JEXEC') or die('Restricted access');
   
class FaqBookProModelSections extends JModelLegacy
{ 
	var $utilities = null;
	var $navigation = null;
	
	function __construct() 
	{
		// Set referer for return page
		$return = base64_encode(JUri::getInstance()->toString());
		JFactory::getApplication()->setUserState( 'com_faqbookpro.return_page', $return );	

		$this->utilities = $this->getUtilitiesLib();
		$this->navigation = $this->getNavigationLib();
		
	  	parent::__construct();
	}
	
	public function getUtilitiesLib()
	{
		$utilities = new FAQBookProLibUtilities;
		
		return $utilities;
	}
	
	public function getNavigationLib()
	{
		$navigation = new FAQBookProLibUtilitiesNavigation;
		
		return $navigation;
	}
			
	public static function getSections($sections = false)
	{
		$db = JFactory::getDbo();
		$user = JFactory::getUser();

		$query = $db->getQuery(true);
		
		$query->select('*')
			->from('#__minitek_faqbook_sections');
		if ($sections)
		{
			$query->where('id IN (' . implode(',', $sections) . ')');	
		}
		$query->where('state = 1');
		$query->where('access IN (' . implode(',', $user->getAuthorisedViewLevels()) . ')');
		$query->order('ordering');
		
		$db->setQuery($query);
		
		$rows = $db->loadObjectList();
		if ($rows)
		{
			return $rows;	
		}
		else
		{
			return false;
		}
	}
}
