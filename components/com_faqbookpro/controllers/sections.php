<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

defined('_JEXEC') or die('Restricted access');
 
jimport('joomla.application.component.controller');
 
class FaqBookProControllerSections extends JControllerLegacy
{
  	function __construct() 
	{		
		parent::__construct();	
	}
}
