<?php
defined('_JEXEC') or die ('Restricted access');

$cfg = JEVConfig::getInstance();

$this->data = $data = $this->datamodel->getDayData($this->year, $this->month, $this->day);
$this->Redirectdetail();

$cfg    = JEVConfig::getInstance();
$Itemid = JEVHelper::getItemid();

// previous and following month names and links
$followingDay = $this->datamodel->getFollowingDay($this->year, $this->month, $this->day);
$precedingDay = $this->datamodel->getPrecedingDay($this->year, $this->month, $this->day);

?>
<div class="day_top_header">
	<?php
	echo JevDate::strftime("%A, %d.%m.%Y", strtotime($this->year . '-' . $this->month . '-'. $this->day));
	?>
</div>
<?php
// Re-Organise the events into resource grouping
$resources = array();
foreach ($data as $row)
{

	foreach ($row as $dkey => $day)
	{
		if (count($day['events']) !== 0)
		{
			foreach ($day['events'] as $ekey => $event)
			{
				if (count($event->jevp_custompeople_jpm) > 0)
				{
					foreach ($event->jevp_custompeople_jpm as $rkey => $resource)
					{
						// Only find resources that are named 'Raum'
						if ($resource->typename === 'Raum')
						{
							$resources[$resource->title]['color'] = $event->jevp_custompeople['color']['value'] ;
							$resources[$resource->title]['txt_color'] = $event->jevp_custompeople['txt_color']['value'];
						}
					}
				} // Umm do nothing for now.
			}
		}
	}
}

// Current Resource count
$resource_count = count($resources);

?>
<style>
    #custom_day_view {
        background-color: #FFF;
        margin-left: -1px;
        margin-right: -1px;
        margin-top: -11px;
        margin-bottom: -26px;
    }
</style>
<div id="custom_day_view">
    <div class="uk-overflow-container">
        <table class="uk-table uk-table-striped uk-table-condensed uk-text-nowrap">
            <thead>
			<?php
			// Add the hour column in
			?>
            <th class="hour">Hour</th>
			<?php
			// Generate the heading
			foreach ($resources as $reskey => $rs)
			{
				echo '<th style="background-color:' . $rs['color'] . '; 
                color: ' . $rs['txt_color'] . ';">' . $reskey . '</th>';
			}
			?>
            </thead>
            <tbody>
			<?php
			// Generate the columns
			foreach ($data['hours'] as $hkey => $hour)
			{
				if ($hkey < 6) continue;
				?>
                <tr>
                    <td><?php echo $hkey; ?></td>
					<?php
					foreach ($resources as $rekey => $resource)
					{
						?>
                        <td>
                            <ul>
								<?php
								foreach ($hour['events'] as $ekey => $event)
								{
									$rowlink = $event->viewDetailLink($event->yup(), $event->mup(), $event->dup(), false);
									foreach ($event->jevp_custompeople_jpm as $ev_resource)
									{
										if ($ev_resource->title !== $rekey)
										{
											continue;
										}
										echo '<li><a href="' . $rowlink . '" alt="' . $event->title . '">' . $event->title . '</a></li>';
									}

								} ?>
                            </ul>
                        </td>
					<?php } ?>
                </tr>
				<?php

			} ?>
            </tbody>
            <tfoot>

            </tfoot>
        </table>
    </div>

    <div id='jev_maincal' class='jev_listview' style="display:none;">
        <div class="jev_listrow">

			<?php
			$hasevents = false;

			// // Timeless Events First
			if (count($data ['hours'] ['timeless'] ['events']) > 0)
			{
				$hasevents  = true;
				$start_time = JText::_('TIMELESS');

				echo '<ul class="ev_ul">' . "\n";
				foreach ($data ['hours'] ['timeless'] ['events'] as $row)
				{

					$listyle = 'style="border-color:' . $row->bgcolor() . ';"';
					echo "<li class='ev_td_li' $listyle>\n";

					if (!$this->loadedFromTemplate('icalevent.list_row', $row, 0))
					{
						$this->viewEventRowNew($row);
						echo '&nbsp;::&nbsp;';
						$this->viewEventCatRowNew($row);
					}
					echo "</li>\n";
				}
				echo "</ul>\n";
			}

			for ($h = 0; $h < 24; $h++)
			{
				if (count($data ['hours'] [$h] ['events']) > 0)
				{
					$hasevents  = true;
					$start_time = JEVHelper::getTime($data ['hours'] [$h] ['hour_start']);

					echo '<ul class="ev_ul">' . "\n";
					foreach ($data ['hours'] [$h] ['events'] as $row)
					{
						$listyle = 'style="border-color:' . $row->bgcolor() . ';"';
						echo "<li class='ev_td_li' $listyle>\n";

						if (!$this->loadedFromTemplate('icalevent.list_row', $row, 0))
						{
							$this->viewEventRowNew($row);
							echo '&nbsp;::&nbsp;';
							$this->viewEventCatRowNew($row);
						}
						echo "</li>\n";
					}
					echo "</ul>\n";
				}
			}

			if (!$hasevents)
			{
				echo '<div class="list_no_e">' . "\n";
				echo JText::_('JEV_NO_EVENTS_FOUND');
				echo "</div>\n";
			}

			?>
        </div>
        <div class="jev_clear"></div>
    </div>
</div>