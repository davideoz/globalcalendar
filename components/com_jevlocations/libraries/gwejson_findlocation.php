<?php
/**
* @copyright	Copyright (C) 2008-2015 GWE Systems Ltd. All rights reserved.
 * @license		By negoriation with author via http://www.gwesystems.com
*/
defined('JPATH_PLATFORM') or die;

function ProcessJsonRequest(&$requestObject, $returnData){

	if (!isset($requestObject->typeahead)){
		return array();
	}
	
	$returnData->titles	= array();
	$returnData->exactmatch=false;

	ini_set("display_errors",0);

	$params = JComponentHelper::getParams("com_jevlocations");

	include_once(JPATH_SITE."/components/com_jevlocations/models/jevlocations.php");
	$model = JModelLegacy::getInstance("JevlocationsModelJevlocations");
	$model->typeahead =$requestObject->typeahead;
	$items =  $model->getItems();
	return $items;

	$user = JFactory::getUser();
	if ($user->id==0){
		throwerror("There was an error");
	}
}

