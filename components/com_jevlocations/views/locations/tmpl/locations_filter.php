<?php 
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */

defined('_JEXEC' ) or die('Restricted access'); 

?>

<table>
<tr>
	<td align="left" width="100%">
		<?php echo JText::_( 'COM_JEVLOCATIONS_FILTER' ); ?>:
		<input type="text" name="search" id="jevsearch" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
		<?php echo $this->lists['loccat']; ?>
		<button onclick="this.form.submit();"><?php echo JText::_( 'COM_JEVLOCATIONS_GO' ); ?></button>
		<button onclick="document.getElementById('jevsearch').value='';this.form.getElementById('filter_catid').value='0';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_( 'COM_JEVLOCATIONS_RESET' ); ?></button>
	</td>
	<td nowrap="nowrap">
		<?php
		if ($this->usecats){
			echo $this->lists['catid'];
		}
		?>
	</td>
</tr>
</table>
