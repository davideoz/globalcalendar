<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */
defined('_JEXEC' ) or die('Restricted access');

//We add breadcrumbs
if($this->locparams->get('show_breadcrumbs',1))
{
	JFactory::getApplication()->getPathway()->addItem($this->location->title, '');
}

if (!$this->loadedFromTemplate('com_jevlocations.locations.detail', $this->location))
{
	// pass location through content plugins
	JPluginHelper::importPlugin('content');
	// pass location through jevents plugins
	JPluginHelper::importPlugin('jevents');
	$tmprow = new stdClass();
	$tmprow->text = $this->location->description;
	$params =new JRegistry(null);
	$dispatcher	= JEventDispatcher::getInstance();
	$dispatcher->trigger( 'onContentPrepare', array('com_jevents', &$tmprow, &$params, 0 ));
	$this->location->description = $tmprow->text ;
	$dispatcher->trigger('onLocationDisplay', array(&$this->location));

	$compparams = $this->locparams;
	$task = $compparams->get("jevview", "month.calendar");
	$menuitem = (int) $compparams->get("targetmenu", 0);
	$view_all_link = JRoute::_("index.php?option=com_jevents&task=$task&loclkup_fv=" . $this->location->loc_id . "&Itemid=" . $menuitem);
	$view_all_link_html =  "<span class='cal_link'><strong>" . JText::sprintf("COM_JEVLOCATIONS_ALL_EVENTS", $view_all_link) . "</strong></span>";
	$linktocal = $compparams->get('modlatest_LinkToCal', 0);


	?>
	<div class="jevbootstrap" style="margin:3px;">
		<?php echo "<h3>" . $this->location->title . "</h3>" ?>

		<fieldset class="adminform jevloc_details">
			<legend><?php echo JText::_('DESCRIPTION'); ?></legend>
			<?php
			$usecats = $compparams->get("usecats", 0);
			if ($usecats)
			{
				$structuredLocation = "";
				echo $this->location->street . "<br/>";
				if( (isset($this->location->c3title)) &&  ($this->location->c3title !== "") )
				{
					$structuredLocation .= $this->location->c3title;
				}

				if( (isset($this->location->c2title)) &&  ($this->location->c2title !== "") )
				{
					$structuredLocation .= " => " . $this->location->c2title;
				}

				if( (isset($this->location->c1title)) &&  ($this->location->c1title !== "") )
				{
					$structuredLocation .= " => " . $this->location->c1title;
				}


				echo $structuredLocation . "<br/><br/>";
			}
			else
			{
				if (strlen($this->location->street) > 0)
					echo $this->location->street . "<br/>";
				if (strlen($this->location->city) > 0)
					echo $this->location->city . "<br/>";
				if (strlen($this->location->state) > 0)
					echo $this->location->state . "<br/>";
				if (strlen($this->location->postcode) > 0)
					echo $this->location->postcode . "<br/>";
				if (strlen($this->location->country) > 0)
					echo $this->location->country . "<br/>";
			}

			if (strlen($this->location->phone) > 0)
			{
				echo $this->location->phone . "<br/>";
			}

			if (strlen($this->location->url) > 0)
			{
				echo JevLocationsHTML::getURLElement($this->location->url) . "<br/>";
			}

			if ($this->location->image != "")
			{
				// Get the media component configuration settings
				$params = JComponentHelper::getParams('com_media');
				// Set the path definitions
				$mediapath = JURI::root(true) . '/' . $params->get('image_path', 'images/stories');

				// folder relative to media folder
				$locparams = JComponentHelper::getParams("com_jevlocations");
				$folder = "jevents/jevlocations";
				$thimg = '<img src="' . $mediapath . '/' . $folder . '/thumbnails/thumb_' . $this->location->image . '" />';
				$img = '<img src="' . $mediapath . '/' . $folder . '/' . $this->location->image . '" />';
				echo $thimg . "<br/>";
			}


			echo $this->location->description;

			$template = $compparams->get("fieldtemplate", "");
			if ( ($template != "")  && (JPluginHelper::isEnabled("jevents", 'jevcustomfields')) )
			{
				$html = "";
				// New custom fields
				if (isset($this->location->customfields))
				{
					$customfields = $this->location->customfields;
				}
				else {
					$customfields = array();
				}

				$plugin = JPluginHelper::getPlugin('jevents', 'jevcustomfields' );
				$pluginparams = new JRegistry($plugin->params);

				$templatetop = $pluginparams->get("templatetop", "<table border='0'>");
				$templaterow = $pluginparams->get("templatebody", "<tr><td class='label'>{LABEL}</td><td>{VALUE}</td>");
				$templatebottom = $pluginparams->get("templatebottom", "</table>");

				$html = $templatetop;
				$user = JFactory::getUser();

				foreach ($customfields as $customfield)
				{

					if (version_compare(JVERSION, '1.6.0', '>='))
					{
						if (!in_array(intval($customfield["access"]), JEVHelper::getAid($user, 'array')))
							continue;
					}
					else
					{
						if ($user->aid < intval($customfield["access"]))
							continue;
					}
					if (!is_null($customfield["hiddenvalue"]) && trim($customfield["value"]) == $customfield["hiddenvalue"])
						continue;
					$outrow = str_replace("{LABEL}", $customfield["label"], $templaterow);
					$outrow = str_replace("{VALUE}", nl2br($customfield["value"]), $outrow);
					$html .= $outrow;
				}
				$html .= $templatebottom;

				echo $html;
			}
			?>
		</fieldset>

		<fieldset class="adminform jevloc_gmap">
			<legend><?php echo JText::_('Google_Map'); ?></legend>
			<?php echo JText::_('COM_JEVLOCATIONS_CLICK_MAP'); ?><br/><br/>
			<div id="gmap" style="width: 450px; height: 300px"></div>
		</fieldset>

		<?php
		if (JRequest::getInt("se", 0))
		{
			?>
			<fieldset class="adminform">
				<legend><?php echo JText::_('COM_JEVLOCATIONS_UPCOMING_EVENTS'); ?></legend>
				<?php
				require_once (JPATH_SITE . "/modules/mod_jevents_latest/helper.php");

				if ($linktocal == 1) {
					echo $view_all_link_html;
				}

				$jevhelper = new modJeventsLatestHelper();
				$theme = JEV_CommonFunctions::getJEventsViewName();

				JPluginHelper::importPlugin("jevents");
				$viewclass = $jevhelper->getViewClass($theme, 'mod_jevents_latest', $theme ."/". "latest", $compparams);

				// record what is running - used by the filters
				$registry = JRegistry::getInstance("jevents");
				$registry->set("jevents.activeprocess", "mod_jevents_latest");
				$registry->set("jevents.moduleid", "cb");

				if ($this->location->targetmenu > 0)
				{
					$menuitem = $this->location->targetmenu;
				}
				if ($menuitem > 0)
				{
					$compparams->set("target_itemid", $menuitem);
				}
				// ensure we use these settings
				$compparams->set("modlatest_useLocalParam", 1);
				// disable link to main component
				$compparams->set("modlatest_LinkToCal", 0);
				$compparams->set("layout", $compparams->get("loclayout",""));

                                // WE have a confused setup where some addons use both of these :(
				$registry->set("jevents.moduleparams", $compparams);
                                JFactory::getConfig()->set("jev.modparams",$compparams);

				//we set parameters to show only this location
				$loclkup_fv = JRequest::setVar("loclkup_fv", $this->location->loc_id);
                                JevHelper::setMenuFilter("loclkup_fv", $this->location->loc_id);

				$compparams->set("extras18","jevl:".$this->location->loc_id);

				//Display latest events
				$modview = new $viewclass($compparams, 0);
				echo $modview->displayLatestEvents();

				//We set loclkup parameter back to original state.
				$compparams->set("extras19","");
				JRequest::setVar("loclkup_fv", $loclkup_fv);
                                JevHelper::clearMenuFilter("loclkup_fv");

				echo "<br style='clear:both'/>";

				if ($linktocal == 2) {
                    echo $view_all_link_html;
				}

				?>

			</fieldset>
			<?php
		}
		if(property_exists($this->location, "_jcomments"))
		{
			echo $this->location->_jcomments;
		}
		if(property_exists($this->location, "_fblike"))
		{
			echo $this->location->_fblike;
		}
		if(property_exists($this->location, "_fbshare"))
		{
			echo $this->location->_fbshare;
		}
		if(property_exists($this->location, "_fbcomments"))
		{
			echo $this->location->_fbcomments;
		}
        ?>
	</div>
	<?php
}
