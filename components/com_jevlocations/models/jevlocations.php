<?php

/*
 *  @author    Carlos Cámara - JEvents.net
 *  @copyright Copyright (C) GWE Systems
 *  @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
 *  @link http://www.jevents.net
 */

defined('JPATH_PLATFORM') or die;
jimport('joomla.application.component.modellist');
JLoader::register('JEVHelper',JPATH_SITE."/components/com_jevents/libraries/helper.php");
/**
 * Frontend model for Managed Locations
 *
 * @author carcam
 */
class JevlocationsModelJevlocations extends JModelList
{
	public $typeahead="";
	
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'title', 'a.title',
				'alias', 'a.alias',
				'postcode', 'a.postcode',
				'city', 'a.city',
				'catid', 'a.catid', 'category_title',
				'state', 'a.state',
			);
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		// Create a new query object.
		$user = JFactory::getUser();
		$db    = $this->getDbo();
		$query = $db->getQuery(true);
		$params = JComponentHelper::getParams("com_jevlocations");
		$typeahead = $this->typeahead;

		$isAdmin = JEVHelper::isAdminUser($user);
		$userAccess = implode(',',JAccess::getAuthorisedViewLevels($user->id));

		$select = array();
		$where = array();
		$leftjoin = array();

		$select[] = 'loc.loc_id';
		$select[] = 'loc.title';
		/*$select[] = 'loc.geolon';
		$select[] = 'loc.geolat';
		$select[] = 'loc.mapicon';
*/
		if(!$params->get('usecats',0))
		{
			$select[] = 'loc.city';
			$select[] = 'loc.state';
			$select[] = 'loc.country';
		}
		else
		{
			$select[] = 'stloc.title AS city';
			$leftjoin[] = '#__jevlocation_categories AS stloc ON stloc.id = loc.catid';
			
			$query->join('LEFT',$leftjoin);
		}

		$query->select($select);
		$query->from('`#__jev_locations` AS loc');

		if( !$params->get('selectfromall',0) && !$isAdmin )
		{
			$where[] = '(loc.global = 1 OR loc.created_by = '. (int) $user->id .')';
		}

		if(!$isAdmin)
		{
			$where[] = 'loc.published = 1';
			$where[] = 'loc.access IN (' . $userAccess .')';
		}

		if($typeahead)
		{
			$where[] = 'loc.title LIKE "%' . $db->escape($typeahead) . '%"';
			// if searching custom fields too then use this
			// $where[] = '( loc.title LIKE "%' . $db->escape($typeahead) . '%" ' .
			// 'OR loc.loc_id IN (SELECT target_id FROM #__jev_customfields3 WHERE value LIKE "%' . $db->escape($typeahead) . '%") )';

			// Add city to location match
			if(!$params->get('usecats',0))
			{
				$city = 'loc.city';
			}
			else {
				$city = "stloc.title";
				if(!$isAdmin)
				{
					$where[] = 'stloc.published = 1';
					$where[] = 'stloc.access IN (' . $userAccess .')';
				}
			}
			$select[1] = "CASE WHEN CHAR_LENGTH($city) THEN CONCAT_WS(', ',loc.title,$city)  ELSE loc.title END as title";
			$query->select($select);
		}

		if($where)
		{
			$query->where($where, "AND");
		}


		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering', 'loc.title');
		$orderDirn = $this->state->get('list.direction', 'ASC');
		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}
		
		return $query;
	}
}
