<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

// ensure this file is being included by a parent file
defined('_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );



/**
 * Filters events to restrict events to those at particular locations
 */

class jevLocationlistFilter extends jevFilter
{

	function jevLocationlistFilter($tablename, $filterfield, $isstring=true,$yesLabel="jev_Yes", $noLabel="jev_No"){
		$this->filterNullValue="";
		parent::__construct($tablename, "locationlist", $isstring);

	}

	function _createFilter($prefix=""){
		$registry = JFactory::getConfig();
		$loclist = $registry->get("jevlocations.locations",-1);
		return "det.location IN ($loclist)";
	}


}
