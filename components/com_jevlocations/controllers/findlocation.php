<?php

/**
 * @copyright	Copyright (C) 2008-2011 GWE Systems Ltd. All rights reserved.
 * @license		By negoriation with author via http://www.gwesystems.com
 */
ini_set("display_errors", 0);


list($usec, $sec) = explode(" ", microtime());
define('_SC_START', ((float) $usec + (float) $sec));

// Set flag that this is a parent file
define('_JEXEC' , 1);
$x = realpath(dirname(__FILE__) . "/" . ".." . "/" . ".." . "/" . ".." . "/");
if (!file_exists($x . "/" . "plugins"))
{
	$x = realpath(dirname(__FILE__) . "/" . ".." . "/" . ".." . "/" . ".." . "/" . ".." . "/");
}
if (!file_exists($x .  "/plugins") && isset($_SERVER["SCRIPT_FILENAME"]))
{
	$x = dirname(dirname(dirname(dirname($_SERVER["SCRIPT_FILENAME"]))));
}
define('JPATH_BASE', $x);

// create the mainframe object
$_REQUEST['tmpl'] = 'component';

// Create JSON data structure
$data = new stdClass();
$data->error = 0;
$data->result = "ERROR";
$data->user = "";

// Get JSON data
if (!array_key_exists("json", $_REQUEST))
{
	throwerror("There was an error - no request data");
}
else
{
	$requestData = $_REQUEST["json"];

	if (isset($requestData))
	{
		try {
			if (ini_get("magic_quotes_gpc"))
			{
				$requestData = stripslashes($requestData);
			}

			$requestObject = json_decode($requestData, 0);
			if (!$requestObject)
			{
				$requestObject = json_decode(utf8_encode($requestData), 0);
			}
		}
		catch (Exception $e) {
			throwerror("There was an exception");
		}

		if (!$requestObject)
		{
			file_put_contents(dirname(__FILE__) . "/cache/error.txt", var_export($requestData, true));
			throwerror("There was an error - no request object ");
		}
		else if ($requestObject->error)
		{
			throwerror("There was an error - Request object error " . $requestObject->error);
		}
		else
		{

			try {
				$data = ProcessRequest($requestObject, $data);
			}
			catch (Exception $e) {
				throwerror("There was an exception " . $e->getMessage());
			}
		}
	}
	else
	{
		throwerror("Invalid Input");
	}
}

header("Content-Type: application/x-javascript; charset=utf-8");

list ($usec, $sec) = explode(" ", microtime());
$time_end = (float) $usec + (float) $sec;
$data->timing = round($time_end - _SC_START, 4);

// Must suppress any error messages
@ob_end_clean();
echo json_encode($data);

function ProcessRequest(&$requestObject, $returnData)
{

	define("REQUESTOBJECT", serialize($requestObject));
	define("RETURNDATA", serialize($returnData));

	require_once JPATH_BASE . "/" . 'includes' . "/" . 'defines.php';
	require_once JPATH_BASE . "/" . 'includes' . "/" . 'framework.php';

	$requestObject = unserialize(REQUESTOBJECT);
	$returnData = unserialize(RETURNDATA);

	$returnData->titles = array();
	$returnData->exactmatch = false;

	ini_set("display_errors", 0);

	$client = "site";
	if (isset($requestObject->client) && in_array($requestObject->client, array("site", "administrator")))
	{
		$client = $requestObject->client;
	}
	$mainframe =  JFactory::getApplication($client);
	$mainframe->initialise();

	$GLOBALS["mainframe"] = $mainframe;

	if (!version_compare(JVERSION, "1.6.0", 'ge')){
		JPluginHelper::importPlugin('system');
		$mainframe->triggerEvent('onAfterInitialise');
	}

	jimport("joomla.html.parameter");

	include_once(JPATH_SITE."/components/com_jevents/jevents.defines.php");
	
	// Enforce referrer
	if (true)
	{
		if (!array_key_exists("HTTP_REFERER", $_SERVER))
		{
			throwerror("There was an error");
		}

		$live_site = $_SERVER['HTTP_HOST'];
		$ref_parts = parse_url($_SERVER["HTTP_REFERER"]);

		if (!isset($ref_parts["host"]) || $ref_parts["host"] != $live_site)
		{
			throwerror("There was an error - missing host in referrer");
		}
	}

	$token = JSession::getFormToken();
	if (!isset($requestObject->token) || $requestObject->token != $token)
	{
		throwerror("There was an error - bad token.  Please refresh the page and try again.");
	}

	$user = JFactory::getUser();
	if ($user->id == 0)
	{
		throwerror("There was an error");
	}

	if ($requestObject->error)
	{
		return "Error";
	}
	if ($requestObject->task == "findLoc" && isset($requestObject->title) && trim($requestObject->title) !== "")
	{
		$returnData->result = "title is " . $requestObject->title;
	}
	else 
	{
		throwerror("There was an error - no valid argument");
	}

	$db = JFactory::getDBO();

	$title = JFilterInput::getInstance()->clean($requestObject->title, "string");
	$text = $db->Quote('%' . $db->escape($text, true) . '%', false);

	// Remove any dodgy characters from fiel"/"
	// Only allow a to z , 0 to 9, ', " space (\\040), hyphen (\\-), underscore (\\_)
	/*
	  $regex     = '/[^a-zA-Z0-9_\'\"\'\\40\\-\\_]/';
	  $title    = preg_replace($regex, "", $title);
	  $title = substr($title."    ",0,4);
	 */

	if ($requestObject->task == "findLoc" && trim($title) == "" && trim($title) == "")
	{
		throwerror("There was an error - no valid argument");
	}

	if ($requestObject->task == "findLoc" && strlen($title) < 2 && $title != "*")
	{
		$returnData->result = 0;
		return $returnData;
	}

	if($requestObject->task == "findLoc")
	{
		if ($title != "*")
		{
			$sql = "SELECT loc_id, title, street, postcode, city, country, geolon, geolat, mapicon FROM #__jev_locations WHERE (global=1 or created_by = ".$user->id.") AND  (title LIKE " . $db->Quote($title . "%") . " OR city LIKE " . $db->Quote($title . "%") . ") order by title, city, postcode, country asc";
		}
		else
		{
			$sql = "SELECT loc_id, title, street, postcode, city, country , geolon, geolat, mapicon FROM #__jev_locations  WHERE (global=1 or created_by = ".$user->id.") order by title, city, postcode, country asc";
		}
	}

	$db->setQuery($sql);
	$matches = $db->loadObjectList();
	if (count($matches) == 0)
	{
		$returnData->result = 0;
	}
	else
	{
		$returnData->result = count($matches);
		foreach ($matches as $match)
		{
			if (trim(strtolower($match->name)) == trim(strtolower($title)) || trim(strtolower($match->username)) == trim(strtolower($title)))
				$returnData->exactmatch = true;
			$parts = array($match->title, $match->street, $match->city, $match->postcode, $match->country);
			$returnData->titles[] = array("title" => $match->title, "long" => implode(", ", $parts), "loc_id" => $match->loc_id, "geolon" => $match->geolon, "geolat" => $match->geolat, "mapicon" => $match->mapicon);
		}
	}
	
	return $returnData;
}

function throwerror($msg)
{
	$data = new stdClass();
	$data->error = "alert('" . $msg . "')";
	$data->result = "ERROR";
	$data->user = "";

	header("Content-Type: application/x-javascript");

	// Must suppress any error messages
	@ob_end_clean();
	echo json_encode($data);
	exit();

}