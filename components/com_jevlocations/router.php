<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd, 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

defined('_JEXEC' ) or die( 'No Direct Access' );

JLoader::register('JEVConfig',JPATH_ADMINISTRATOR."/components/com_jevents/libraries/config.php");
JLoader::register('JEVHelper',JPATH_SITE."/components/com_jevents/libraries/helper.php");

function JevLocationsBuildRoute(&$query)
{
	$cfg = JEVConfig::getInstance();
	$segments = array();
	// We don't need the view - its only used to manipulate parameters
	if (isset($query['view'])){
		unset($query['view']);
	}

	$task = false;
	if (!isset($query['task'])){
		if (isset($query["Itemid"])){
			$menu =  JFactory::getApplication()->getMenu();
			$menuitem = $menu->getItem($query["Itemid"]);
			if (!is_null($menuitem) && isset($menuitem->query["task"])){
				$task = $menuitem->query["task"];
			}
			if (!is_null($menuitem) && isset($menuitem->query["view"]) && isset($menuitem->query["layout"]) ){
				$task = $menuitem->query["view"] .".".$menuitem->query["layout"];
			}

		}
		if (!$task){
			$task = 'locations.locations';
		}
	}
	else {
		$task=$query['task'];
		unset($query['task']);
	}

	$task = str_replace("locations.","",$task);
	switch ($task) {
		case "locations":
		case "overview":
		case "list":
			$segments[]=$task;
			break;
		case "detail":
			$segments[]=$task;
			if(isset($query['loc_id'])) {
				$segments[] = $query['loc_id'];
				unset($query['loc_id']);
			}
			else {
				if(isset($menuitem->query['loc_id']))
				{
					$segments[] = $menuitem->query['loc_id'];
					$segments = array();
					break;
				}
				else
				{
					$segments[] = 0;
				}
			}
			if(isset($query['se'])) {
				$segments[] = $query['se'];
				unset($query['se']);
			}
			else {
				$segments[] = 0;
			}
			if(isset($query['title'])) {
				$segments[] = $query['title'];
				unset($query['title']);
			}
			else {
				$segments[] = '-';
			}
			break;

		default:
			$segments[]=$task;
			break;
	}


	return $segments;
}

function JevLocationsParseRoute($segments)
{
	$vars = array();

	//Get the active menu item
	$menu = JFactory::getApplication()->getMenu();
	$item = $menu->getActive();

	// Count route segments
	$count = count($segments);

	if ($count>0){
		// task
		$task = $segments[0];
		// backwards compatability
		if (strpos($task,"locations.")===false) $task = "locations.".$task;
		$vars["task"]=$task;

		switch 	($task){
			case "locations.locations":
					$vars['layout'] = "locations";
					break;
			case "locations.locations_blog":
					$vars['layout'] = "locations_blog";
					break;
			case "locations.overview":
			case "locations.list":
				break;
			case "locations.detail":
				if($count>1) {
					$vars['loc_id'] = $segments[1];
				}
				if($count>2) {
					$vars['se'] = $segments[2];
				}
				break;
			default:
				break;
		}


	}
	return $vars;

}
