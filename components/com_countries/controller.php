<?php

/**
 * @version    CVS: 1.0.7
 * @package    Com_Countries
 * @author     Davide Casiraghi <davide.casiraghi@gmail.com>
 * @copyright  2017 Davide Casiraghi
 * @license    GNU General Public License versione 2 o successiva; vedi LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Class CountriesController
 *
 * @since  1.6
 */
class CountriesController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'countries');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}
}
