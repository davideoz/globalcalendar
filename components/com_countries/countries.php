<?php
/**
 * @version    CVS: 1.0.7
 * @package    Com_Countries
 * @author     Davide Casiraghi <davide.casiraghi@gmail.com>
 * @copyright  2017 Davide Casiraghi
 * @license    GNU General Public License versione 2 o successiva; vedi LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Countries', JPATH_COMPONENT);
JLoader::register('CountriesController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Countries');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
