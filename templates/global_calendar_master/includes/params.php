<?php
	/*------------------------------------------------------------------------
# author    Gonzalo Suez
# copyright Copyright © 2013 gsuez.cl. All rights reserved.
# @license  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website   http://www.gsuez.cl
-------------------------------------------------------------------------*/
	defined('_JEXEC') or die;

	// Getting params from template
		$params = JFactory::getApplication()->getTemplate(true)->params;
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();


	// Unload default joomla css and js
		$dontInclude = array(
			'/media/jui/js/jquery.js',
			'/media/jui/js/jquery.min.js',
			'/media/jui/js/jquery-noconflict.js',
			'/media/jui/js/jquery-migrate.js',
			'/media/jui/js/jquery-migrate.min.js',
			'/media/jui/js/bootstrap.js',
			'/media/jui/js/bootstrap.min.js',
			'/media/system/js/core-uncompressed.js',
			'/media/system/js/tabs-state.js',
			'/media/system/js/core.js',
			'/media/system/js/mootools-core.js',
			'/media/system/js/mootools-core-uncompressed.js',
		);

		foreach($doc->_scripts as $key => $script){
		    //if(in_array($key, $dontInclude)){
		        unset($doc->_scripts[$key]);
		        unset($doc->_scripts[JURI::root(true).$key]);
		    //}
		}

		//unset($doc->_scripts[JURI::root(true).'/media/jui/js/bootstrap.min.js']);

	// Add javascript files
		$doc->addScript('templates/' . $this->template . '/js/jquery-2.1.3.min.js');
		$doc->addScript('templates/' . $this->template . '/js/bootstrap.min.js');
	
	// Add Stylesheets
		$doc->addStyleSheet('templates/' . $this->template . '/css/bootstrap.min.css');
		$doc->addStyleSheet('templates/' . $this->template . '/css/template.min.css');

	// Variables
		$headdata = $doc->getHeadData();
		$menu = $app->getMenu();
		$active = $app->getMenu()->getActive();
		$pageclass = $params->get('pageclass_sfx');
		$tpath = $this->baseurl . '/templates/' . $this->template;
	

	// Generator tag
		$this->setGenerator(null);
	// Force latest IE & chrome frame
		$doc->setMetadata('x-ua-compatible', 'IE=edge,chrome=1');

	// Add javascripts
		$doc->addScript($tpath . '/js/modernizr-2.8.3.js');
		$doc->addScript($tpath . '/js/jquery.qtip.js');

	// Add stylesheets
		$doc->addStyleSheet($tpath . '/css/font-awesome.min.css');
		$doc->addStyleSheet($tpath . '/css/jquery.qtip.min.css');

	
	
	
