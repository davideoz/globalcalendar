<?php
	/*------------------------------------------------------------------------
# author    Gonzalo Suez
# copyright Copyright © 2013 gsuez.cl. All rights reserved.
# @license  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website   http://www.gsuez.cl
-------------------------------------------------------------------------*/
	defined('_JEXEC') or die;
	
	// Getting params from template
		$params = JFactory::getApplication()->getTemplate(true)->params;
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();

	// Unload default joomla css and js
		// CSS
			unset($doc->_styleSheets[$this->baseurl .'/media/jui/css/bootstrap.min.css']);
		
		// JS
			unset($doc->_scripts[JURI::root(true).'/templates/master-bootstrap-3-master/js/jui/bootstrap.min.js']);
			unset($doc->_scripts[JURI::root(true).'/templates/master-bootstrap-3-master/js/jui/jquery.min.js']);
			unset($doc->_scripts[JURI::root(true).'/templates/master-bootstrap-3-master/js/jui/jquery-noconflict.js']);
			unset($doc->_scripts[JURI::root(true).'/templates/master-bootstrap-3-master/js/jui/jquery-migrate.min.js']);
			
			unset($doc->_scripts[JURI::root(true).'/media/system/js/mootools-more.js']);
			unset($doc->_scripts[JURI::root(true).'/media/system/js/mootools-core.js']);



	// Column widths
		$leftcolgrid = ($this->countModules('left') == 0) ? 0 :
		$this->params->get('leftColumnWidth', 3);
		$rightcolgrid = ($this->countModules('right') == 0) ? 0 :
		$this->params->get('rightColumnWidth', 3);
	
	
		
	
	// Variables
		$headdata = $doc->getHeadData();
		$menu = $app->getMenu();
		$active = $app->getMenu()->getActive();
		$pageclass = $params->get('pageclass_sfx');
		$tpath = $this->baseurl . '/templates/' . $this->template;

	// Parameter
		$frontpageshow = $this->params->get('frontpageshow', 0);
		$modernizr = $this->params->get('modernizr');
		$fontawesome = $this->params->get('fontawesome');
		$pie = $this->params->get('pie');
		$materialdesign = $this->params->get('materialdesign');
	
	//Layout Options
	$layout = $this->params->get('layout');
	
	//Pattern options
	$pattern = $this->params->get('pattern');
	
	// Generator tag
	$this->setGenerator(null);
	
	// Force latest IE & chrome frame
	$doc->setMetadata('x-ua-compatible', 'IE=edge,chrome=1');


	// Add javascript files
		$doc->addScript($tpath . '/js/custom.js');
		//$doc->addScript('templates/' . $this->template . '/js/jquery-2.1.3.min.js');
		//$doc->addScript('templates/' . $this->template . '/js/bootstrap.min.js');
		$doc->addScript('templates/' . $this->template . '/js/jquery.qtip.js');
		$doc->addScript($tpath . '/js/bootstrap-toggle.min.js');
		$doc->addScript($tpath . '/js/modernizr-2.8.3.js');
		$doc->addScript($tpath . '/js/jquery.fancybox.min.js');



	// Add Stylesheets
		$doc->addStyleSheet('templates/' . $this->template . '/css/bootstrap-button-override.css');
		$doc->addStyleSheet('templates/' . $this->template . '/css/bootstrap.min.css');
		$doc->addStyleSheet('templates/' . $this->template . '/css/icons.css');
		$doc->addStyleSheet('templates/' . $this->template . '/css/template.min.css');
	
		$doc->addStyleSheet($tpath . '/css/font-awesome.min.css');
		$doc->addStyleSheet($tpath . '/css/jquery.qtip.min.css');
		$doc->addStyleSheet($tpath . '/css/bootstrap-toggle.min.css');
		$doc->addStyleSheet($tpath . '/css/jquery.fancybox.min.css');
		
		
		
		
		

		
