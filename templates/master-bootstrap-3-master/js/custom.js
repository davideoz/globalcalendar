


jQuery(document).ready(function(){


    // TOOLTIP: Assign a tooltip to all elements that have an attribute called tooltip
        jQuery('[tooltip]').each(function(){ // Select all elements with the "tooltip" attribute
           jQuery(this).qtip({ content: jQuery(this).attr('tooltip') }); // Retrieve the tooltip attribute value from the current element
        });

    // Move screen to anchor after event search
        //jQuery(function() {
            if ( jQuery( ".resultsFound" ).length ) {
                jQuery('html, body').animate({scrollTop:jQuery(".resultsFound").position().top}, 'slow');
            }
        //});

	// Fix for jeventpeople frontend admin interface (add class to toolbar and change title to the page)
		jQuery( ".jevpeople" ).prev("#toolbar-box").addClass('jevpeople-toolbar');
        jQuery('body').find('.jevpeople-toolbar h1.page-title').html('Organizers/Teachers Manager');
        
    // Jevents specific JS
        if ( jQuery( "#jevents" ).length ) {

            // Event details
            // Substitute icons
                jQuery( "#jevents_header .actions .email-icon span.icon-envelope" ).remove();
                jQuery( "#jevents_header .actions .email-icon a" ).append("<i class='fa fa-envelope-o'></i>");

                jQuery( "#jevents_header .actions .print-icon span.icon-print" ).remove();
                jQuery( "#jevents_header .actions .print-icon a" ).append("<i class='fa fa-print'></i>");

            // Remove admin interface from event description page when logged in 
                if ( jQuery( "#jevents_body .jev_evdt" ).length ) {
                    jQuery( "#jevents_body .ev_adminpanel").hide();   
                } 
        }

    // Fix - add chosen to select of Jevents Persons
        //jQuery("select#type_id").chosen();
        

    // Report abuse button (get mail of event creator)
        if ( jQuery( ".jev_evdt_creator span a" ).length ) {
            var creatorMail = jQuery(".jev_evdt_creator span a").attr('href');
            creatorMail = creatorMail.split(":").pop();
            //alert(mailCreator);         
        }


        var currentURL = window.location.href;
     	var link = "/components/com_contactevents/assets/reportAbuse.php?creator_mail="+encodeURIComponent(creatorMail)+"&link="+encodeURIComponent(currentURL);
     	
     	//jQuery("ul.jf_sidebuttons a").attr("href", link);

        jQuery( "ul.jf_sidebuttons a" ).click(function() {
            //jQuery.fancybox.open('<div class="message"><h2>Hello!</h2><p>You are awesome!</p></div>');

            jQuery.fancybox.open({
                //src  : 'http://codepen.io/fancyapps/full/jyEGGG/',
                src: link,
                type : 'iframe',
                opts : {
                    /*beforeClose : function( instance, current, e ) {
                        return false;
                    },*/
                    onComplete : function() {
                        console.info('done!');
                    }

                }
            });
        });

        //jQuery("#report-abuse-form").submit(function() { alert("pressed"); return false; });

        jQuery("#report-abuse-send").on("click", function(){
            alert("click");

/*
            $.ajax({
                    type: 'POST',
                    url: 'sendmessage.php',
                    data: $("#contact").serialize(),
                    success: function(data) {
                        if(data == "true") {
                            $("#contact").fadeOut("fast", function(){
                                $(this).before("<p><strong>Success! Your feedback has been sent, thanks :)</strong></p>");
                                setTimeout("$.fancybox.close()", 1000);
                            });
                        }
                    }
                });
*/

        });




});

