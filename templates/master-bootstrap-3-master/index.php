<?php
/*------------------------------------------------------------------------
# author Gonzalo Suez
# copyright Copyright © 2013 gsuez.cl. All rights reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website http://www.gsuez.cl
-------------------------------------------------------------------------*/ // no direct access
defined('_JEXEC') or die;
include 'includes/params.php';

if ($params->get('compile_sass', '0') === '1'){
    require_once "includes/sass.php";
}

$app = JFactory::getApplication();
$menu = $app->getMenu();

$weAreInHomepage = ($menu->getActive() == $menu->getDefault()) ? '1' : '0';
$activeComponent = $app->input->get('option');

/*
dump($menu->getActive(),"active menu");
dump($weAreInHomepage,"default menu item");

dump($weAreInHomepage,"we are in hp");
dump($activeComponent,"active component");
*/
?>

<!DOCTYPE html>
<html lang="en">
    <?php
    include 'includes/head.php'; ?>

    <body>

        <?php  if($this->countModules('sidebutton')) : ?>    
            <jdoc:include type="modules" name="sidebutton" style="block" />
        <?php  endif; ?>

        <?php if ($weAreInHomepage || ($activeComponent == 'com_contactevents')) { ?>
            <div class="jumbotron">  <!-- Ony in HP we include the NAV in Jumbotron -->
        <?php } ?>
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <!--<a class="navbar-brand" href="#">Project name</a>-->
                        </div>
                        <div class="collapse navbar-collapse" id="navbar">
                            <jdoc:include type="modules" name="navigation" style="none" />
                        </div><!--/.nav-collapse -->
                    </div>
                </nav>
        <?php if ($weAreInHomepage || ($activeComponent == 'com_contactevents')) { ?>
                <div class="container">
                    <div id="main-box">
                        <jdoc:include type="message" />
                        <jdoc:include type="component" />
                    </div>
                </div>
                
            </div>  <!-- close Jumbotron -->
            

                <!-- ATTENTION: the jumbotron that has to be closed here it is closed in the com_event view -->
            
        <?php } ?>

    
        
        <?php if (!$weAreInHomepage && !($activeComponent == 'com_contactevents')) { ?>       
            <!-- CONTENT -->
                <div class="container">
                    <div id="main" class="row show-grid">
                        <!-- Left -->
                            <?php  if($this->countModules('left')) : ?>
                                <div id="sidebar" class="col-sm-<?php  echo $leftcolgrid; ?>">
                                    <jdoc:include type="modules" name="left" style="block" />
                                </div>
                            <?php  endif; ?>
                    
                    <!-- Component -->
                        <div id="container" class="col-sm-<?php  echo (12-$leftcolgrid-$rightcolgrid); ?>">
                            <!-- TOP content -->
                                <?php  if($this->countModules('content-top')) : ?>
                                    <div id="content-top">
                                        <div class="row">
                                            <jdoc:include type="modules" name="content-top" style="block" />
                                        </div>
                                    </div>
                                <?php  endif; ?>

                            <!-- MAIN content -->
                                <div id="main-box">
                                    <jdoc:include type="message" />
                                    <jdoc:include type="component" />
                                </div>  

                            <!-- BOTTOM content -->
                                <?php  if($this->countModules('content-bottom')) : ?>
                                    <div id="content-bottom">
                                        <div class="row">
                                            <jdoc:include type="modules" name="content-bottom" style="block" />
                                        </div>
                                    </div>
                                <?php  endif; ?>
                        </div>
                    <!-- Right -->
                        <?php  if($this->countModules('right')) : ?>
                            <div id="sidebar-2" class="col-sm-<?php  echo $rightcolgrid; ?>">
                                <jdoc:include type="modules" name="right" style="block" />
                            </div>
                        <?php  endif; ?>
                    </div>
                    
                    <div class="push"></div>
                </div>
            <!-- end CONTENT -->
        <?php } ?>

        
       
        <footer class="footer">
            <div class="container">
                <!--<p class="text-muted">Place sticky footer content here.</p>-->
                <div class="row">
                    <jdoc:include type="modules" name="copy" style="block" />
                </div>
            </div>
        </footer>
        
        <!-- JS -->
        <script type="text/javascript" src="<?php echo $tpath; ?>/js/template.min.js"></script>
        <!-- JS end -->
    </body>
</html>