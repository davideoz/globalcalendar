<?php
/*------------------------------------------------------------------------
# author Gonzalo Suez
# copyright Copyright © 2013 gsuez.cl. All rights reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website http://www.gsuez.cl
-------------------------------------------------------------------------*/ // no direct access
defined('_JEXEC') or die;
include 'includes/params.php';

// Get if HP - https://docs.joomla.org/How_to_determine_if_the_user_is_viewing_the_front_page
    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    if ($menu->getActive() == $menu->getDefault( 'en-GB' )) {
        $hpClass = "homepage";
    }
    elseif ($menu->getActive() == $menu->getDefault( 'fr-FR' )) {
        $hpClass = "homepage";
    }


?>

<!DOCTYPE html>
<html lang="en">
    <?php
    
    // Get parameters
        include 'includes/head.php'; 

    ?>

    <body class=" <?php echo $hpClass; ?>">

        <?php  if($this->countModules('sidebutton')) : ?>
            <!-- position: sidebutton -->
            <jdoc:include type="modules" name="sidebutton" style="block" />
        <?php  endif; ?>

                
        <!--  NAVBAR-->
            <?php  if($this->countModules('new-navigation')) : ?>
                <jdoc:include type="modules" name="new-navigation" style="block" />
            <?php  endif; ?>
        <!-- end NAVBAR-->
        

        <!-- Jumbo Module Before -->
            <div class="jumboModule before">  
                <?php  if($this->countModules('jumbomodule-bc')) : ?>
                    <div id="sidebar" class="col-sm-<?php  echo $leftcolgrid; ?>">
                        <!-- position: jumbomodule-bc -->
                        <jdoc:include type="modules" name="jumbomodule-bc" style="block" />
                    </div>
                <?php  endif; ?>
            </div>
        
        <!-- Invisible Div to get the H2 color parameter to use for JS -->
            <div class="h2Color" style="color:<?php echo $h2_color?>;"></div>

        <!-- Component -->
            <div class="container">
                <div id="main" class="row show-grid">
                    <!-- Left -->
                        <?php  if($this->countModules('left')) : ?>
                            <div id="sidebar" class="col-sm-<?php  echo $leftcolgrid; ?>">
                                <!-- position: left -->
                                <jdoc:include type="modules" name="left" style="block" />
                            </div>
                        <?php  endif; ?>
                
                <!-- Component -->
                    <div id="container" class="col-sm-<?php  echo (12-$leftcolgrid-$rightcolgrid); ?>">
                        <!-- TOP content -->
                            <?php  if($this->countModules('content-top')) : ?>
                                <div id="content-top">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- position: content-top -->
                                            <jdoc:include type="modules" name="content-top" style="block" />
                                        </div>
                                    </div>
                                </div>
                            <?php  endif; ?>

                        <!-- MAIN content -->
                            <div id="main-box">
                                <jdoc:include type="message" />
                                <jdoc:include type="component" />
                            </div>  

                        <!-- BOTTOM content -->
                            <?php  if($this->countModules('content-bottom')) : ?>
                                <div id="content-bottom">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!-- position: content-bottom -->
                                            <jdoc:include type="modules" name="content-bottom" style="block" />
                                        </div>
                                    </div>
                                </div>
                            <?php  endif; ?>
                    </div>
                <!-- Right -->
                    <?php  if($this->countModules('right')) : ?>
                        <div id="sidebar-2" class="col-sm-<?php  echo $rightcolgrid; ?>">
                            <!-- position: right -->
                            <jdoc:include type="modules" name="right" style="block" />
                        </div>
                    <?php  endif; ?>
                </div>
                
                <div class="push"></div>
            </div>
        <!-- end component -->
            

        <!-- Jumbo Module After -->
            <div class="jumboModule after">  
                <?php  if($this->countModules('jumbomodule-ac')) : ?>
                    <div id="sidebar" class="col-sm-<?php  echo $leftcolgrid; ?>">
                        <!-- position: jumbomodule-ac -->
                        <jdoc:include type="modules" name="jumbomodule-ac" style="block" />
                    </div>
                <?php  endif; ?>
            </div>

            <!-- Footer Small Menu -->
            

            <!-- Footer -->
                <?php  if($this->countModules('footer')) : ?>
                    <jdoc:include type="modules" name="footer" style="block" />
                <?php  endif; ?>

        

        <!-- CSS (deferred - by Google - Optimize CSS Delivery) -->
            <!--<noscript id="deferred-styles">
                <link rel="stylesheet" type="text/css" href="<?php echo $tpath; ?>/css/templateDeferred.min.css"/>
            </noscript>
           
            <script>
                var loadDeferredStyles = function() {
                    var addStylesNode = document.getElementById("deferred-styles");
                    var replacement = document.createElement("div");
                    replacement.innerHTML = addStylesNode.textContent;
                    document.body.appendChild(replacement)
                    addStylesNode.parentElement.removeChild(addStylesNode);
                };

                var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
                if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
                else window.addEventListener('load', loadDeferredStyles);
            </script>-->


        <!-- JS -->
            <script async src="<?php echo $tpath; ?>/js/templateDeferred.min.js" type="text/javascript"></script>

            <!-- Mailchimp signup form load - called by jquery click (custom)-->
                <!--<script type="text/javascript" src="//downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script>-->
    
        <!-- JS end -->
    </body>
</html>