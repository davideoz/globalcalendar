<?php
/*------------------------------------------------------------------------
# author    Davide Casiraghi
# copyright Copyright © 2018 movementmeetslife.com. All rights reserved.
# @license  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website   http://www.movementmeetslife.com
-------------------------------------------------------------------------*/
	defined('_JEXEC') or die;
	
	// Getting params from template
		$params = JFactory::getApplication()->getTemplate(true)->params;
		$app = JFactory::getApplication();
		$doc = JFactory::getDocument();

	// Variables
		$headdata = $doc->getHeadData();
		$menu = $app->getMenu();
		$active = $app->getMenu()->getActive();
		$pageclass = $params->get('pageclass_sfx');
		$tpath = JURI::base() . 'templates/' . $this->template;

	// Unload default joomla css and js
		// CSS
			unset($doc->_styleSheets[JURI::root(true).'/media/jui/css/bootstrap.min.css']);
		
		// JS
			unset($doc->_scripts[JURI::root(true).'/media/jui/js/bootstrap.min.js']);
			unset($doc->_scripts[JURI::root(true).'/media/jui/js/jquery.min.js']);
			unset($doc->_scripts[JURI::root(true).'/media/jui/js/jquery-noconflict.js']);
			unset($doc->_scripts[JURI::root(true).'/media/jui/js/jquery-migrate.min.js']);
			unset($doc->_scripts[JURI::root(true).'/media/jui/js/mootools-more.js']);
			unset($doc->_scripts[JURI::root(true).'/media/jui/js/mootools-core.js']);
			unset($doc->_scripts[JURI::root(true).'/media/system/js/caption.js']);

			// unset any js -- this is too much, it unset also the js of any plugin
			/*$this->_scripts = array();
			unset($this->_script['text/javascript']);*/

			// Remove Jcaption
			if (isset($this->_script['text/javascript'])){
				$this->_script['text/javascript'] = preg_replace('%jQuery\(window\).on\(\'load\',\s*function\(\)\s*\{\s*new\s*JCaption\(\'img.caption\'\)\;\s*\}\)\;\s*%', '', $this->_script['text/javascript']);
			}



	// Parameters
		$h2_color = $this->params->get('h2_color');
	
		// Check if are still needed
			$layout = $this->params->get('layout');
			$pattern = $this->params->get('pattern');
	
	// Generator tag
		$this->setGenerator(null);
	
	// Force latest IE & chrome frame
		$doc->setMetadata('x-ua-compatible', 'IE=edge,chrome=1');

	// Add javascript files
		//$doc->addScript($tpath . '/js/template.min.js'); -->> added in the end of index.php
	
		

		
