<?php
 /*------------------------------------------------------------------------
# author    Gonzalo Suez
# Copyright © 2013 gsuez.cl. All rights reserved.
# @license  http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website   http://www.gsuez.cl
-------------------------------------------------------------------------*/

// no direct access
	defined('_JEXEC') or die;


	
$tpath = JURI::base() . 'templates/' . $this->template;

?>
<head>

	<link rel="stylesheet" type="text/css" href="<?php echo $tpath; ?>/css/template.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $tpath; ?>/css/templateDeferred.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $tpath; ?>/css/templateSiteSpecific.css"/>
	
	<script src="<?php echo $tpath  ?>/js/template.min.js" type="text/javascript"></script>

	<!--<script src="<?php echo $tpath  ?>/js/jquery-2.1.3.min.js" type="text/javascript"></script>
     <script async src="<?php echo $tpath  ?>/js/bootstrap.min.js" type="text/javascript"></script>-->
	<!--<script src="<?php echo $tpath  ?>/js/jquery.fancybox.min.js" type="text/javascript"></script>-->

	<jdoc:include type="head" />

	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<!--[if lte IE 8]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<?php  if ($pie == 1) : ?>
			<style>
				{behavior:url(<?php  echo $tpath; ?>/js/PIE.htc);}
			</style>
		<?php  endif; ?>
	<![endif]-->
<?php
 if($layout=='boxed'){ ?>
<?php  $path= JURI::base().'templates/'.$this->template."/images/elements/pattern".$pattern.".png"; ?>
<style type="text/css">
 body {
    background: url("<?php  echo $path ; ?>") repeat fixed center top rgba(0, 0, 0, 0);
 }
</style>
  <?php  } ?>


	<!-- Cookie Policy -->
	<!-- https://cookieconsent.insites.com/download/# -->
	<!--<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>-->
	<script>
		window.addEventListener("load", function(){
		window.cookieconsent.initialise({
		  "palette": {
		    "popup": {
		      "background": "#000"
		    },
		    "button": {
		      "background": "#f1d600"
		    }
		  },
		  "position": "bottom-right"
		})});
	</script>





</head>