

// Scroll jumbotron image banner (parallax)
    jQuery(window).scroll(function(event){            
        slow = -(jQuery(window).scrollTop() );
        new_position = slow + 'px';
        jQuery('.jumboModule.before .custom').css('top', new_position);                
     });

// Load Jquery UI accordion, the one with (+)
    jQuery(function () {
        var icons = {
            header: "iconClosed",    // custom icon class
            activeHeader: "iconOpen" // custom icon class
        };
        jQuery("#accordion").accordion({
            icons: icons,
            collapsible: true,
            active: false
        });
    });


// DEFER IMAGE LOAD - https://varvy.com/pagespeed/defer-images.html
    function init() {
        var imgDefer = document.getElementsByTagName('img');
        for (var i=0; i<imgDefer.length; i++) {
            if(imgDefer[i].getAttribute('data-src')) {
            imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
        } 
        } 
    }
    window.onload = init;




jQuery(document).ready(function(){
    // Set the H2 color from Joomla template's parameters
        var h2Color = jQuery('body .h2Color').css("color");
        jQuery('h2, h3, h4, .item-page a').css('color', h2Color);
        jQuery('.lifeHeaderMenu button.navbar-toggle').css('background-color', h2Color);
        //jQuery('.item-page h2, .item-page h3, .item-page h4').css('color', h2Color);

    // Deal with CLICK of "Contact improvisation" menu voice. Click avoided from smartmenu, because has childs
        jQuery('.item-104 a').click(function(){
            var href = jQuery(this).attr('href');
            window.location = href;
        });

    // TOOLTIP: Assign a tooltip to all elements that have an attribute called tooltip
        jQuery('[tooltip]').each(function(){ // Select all elements with the "tooltip" attribute
           jQuery(this).qtip({ content: jQuery(this).attr('tooltip') }); // Retrieve the tooltip attribute value from the current element
        });

	// Fix for jeventpeople frontend admin interface (add class to toolbar and change title to the page)
		jQuery( ".jevpeople" ).prev("#toolbar-box").addClass('jevpeople-toolbar');
        jQuery('body').find('.jevpeople-toolbar h1.page-title').html('Organizers/Teachers Manager');
        
    // Jevents specific JS
        if ( jQuery( "#jevents" ).length ) {

            // Event details
            // Substitute icons
                jQuery( "#jevents_header .actions .email-icon span.icon-envelope" ).remove();
                jQuery( "#jevents_header .actions .email-icon a" ).append("<i class='fa fa-envelope-o'></i>");

                jQuery( "#jevents_header .actions .print-icon span.icon-print" ).remove();
                jQuery( "#jevents_header .actions .print-icon a" ).append("<i class='fa fa-print'></i>");

            // Remove admin interface from event description page when logged in 
                if ( jQuery( "#jevents_body .jev_evdt" ).length ) {
                    jQuery( "#jevents_body .ev_adminpanel").hide();   
                } 
        }


        var currentURL = window.location.href;
     	


        jQuery(".fullwidth_video_bg_wrapper").parent().parent().parent(".fullwidth_video_bg_wrapper").parent().parent().css("height","100%");

        

    // Mailchimp - Open mailing list subscribe fancybox on click
        if ( jQuery( "#open-newsletter-popup" ).length ) {
            function showMailingPopUp() {
                require(["mojo/signup-forms/Loader"], function(L) { 
                    L.start({"baseUrl":"mc.us13.list-manage.com","uuid":"8cf63bda3ef626061c7a7bf3a","lid":"3dc262a1c4"});
                    document.cookie = 'MCPopupClosed=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/';
                })
            };

            document.getElementById("open-newsletter-popup").onclick = function() {showMailingPopUp()};        
        }

    // BLOG - Move the leading image to the Jumbotron
        // BLOG Main page
            if ( jQuery( ".blog" ).length ) {
                var imageLink = jQuery('.item.leading-0 img').attr('src');
                var jumboBackground = "height: 300px; background-image: url("+imageLink+")";
                
                jQuery("<div class='custom' style='"+jumboBackground+"'></div>").appendTo(".jumboModule.before");
                jQuery('.item.leading-0 .item-image').hide();

                // Add class blog to Body
                jQuery('body').addClass('blogBody');

            }

        // BLOG Article page
            if ( jQuery( ".articleBlogIntroImage" ).length ) {
                var imageLink = jQuery('.articleBlogIntroImage img').attr('src');
                var jumboBackground = "height: 300px; background-image: url("+imageLink+")";
            
                jQuery("<div class='custom' style='"+jumboBackground+"'></div>").appendTo(".jumboModule.before");
                jQuery('.articleBlogIntroImage').hide();

                // Add class blog to Body
                jQuery('body').addClass('blogBody');
            }

        // Page with big banner in Jumbotron
            if ( jQuery( ".headingBanner" ).length ) {
                jQuery('body').addClass('bannerBody');
            }
            
        

            jQuery('.open').off('click');
            jQuery('.deeper').off('click');




});

